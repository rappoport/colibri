INSTALL
=======

Third-party Dependencies
------------------------
* MongoDB server
* pymongo/bson/gridfs -- python client
* Memcached server
* pymemcache -- python client
* igraph -- python bindings
* openbabel/pybel
* RDKit
* ORCA
* MOPAC


Installation Procedures
-----------------------

* Install MongoDB 3.0.3 binaries
    cd $HOME
    wget http://fastdl.mongodb.org/linux/mongodb-linux-x86_64-rhel62-3.0.3.tgz
    tar -xzf mongodb-linux-x86_64-rhel62-3.0.3.tgz
    ln -s mongodb-linux-x86_64-rhel62-3.0.3 mongodb

* Start MongoDB daemon

    mongod  --bind_ip ADDRESS --port PORT --dbpath PATH --logpath PATH --logappend

* Install Memcached 1.4.22 from source
    cd $HOME
    wget http://memcached.org/files/memcached-1.4.24.tar.gz
    tar -xzf memcached-1.4.24.tar.gz
    mv memcached-1.4.24 memcached-install
    cd memcached-install
    ./configure --prefix=$HOME/memcached-1.4.24 && make && make test && make install
    ln -s memcached-1.4.24 memcached

* Start Memcached daemon

    memcached -p PORT -m MEMORY -d

* Create Python site packages directory at
    mkdir -p $HOME/lib/python2.7/site-packages

* Install pymongo 3.0.2 using easy_install
    easy_install --prefix $HOME pymongo

* Install pymemcache 1.2.9 using easy_install
    easy_install --prefix $HOME pymemcache

* Install igraph  using easy_install

    easy_install --prefix $HOME python-igraph

* Install RDKit 2015_03_1 from source
    - Get dependencies
    + OS X with MacPorts: port install cmake flex bison sqlite3 python27 py27-numpy boost; port select python python27
    cd $HOME
    wget https://github.com/rdkit/rdkit/archive/Release_2015_03_1.tar.gz
    tar -xzf Release_2015_03_1.tar.gz
    mv Release_2015_03_1 RDKit-install
    cd RDKit-install
    export RDBASE=${HOME}/RDKit
    export PYTHONPATH=${PYTHONPATH}:${RDBASE}
    export DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:${RDBASE}/lib
    cd ${RDBASE}/External/INCHI-API
    sh download-inchi.sh
    mkdir build
    cd build
    cmake -D PYTHON_INCLUDE_DIR=/n/sw/python-2.7.1/include/python2.7 -D PYTHON_LIBRARY=/n/sw/python-2.7.1/lib/libpython2.7.dylib -D PYTHON_EXECUTABLE=/n/sw/python-2.7.1/bin/python -D RDK_BUILD_INCHI_SUPPORT=ON -D INCHI_LIBRARY=${RDBASE}/External/INCHI-API/src -D INCHI_INCLUDE_DIR=${RDBASE}/External/INCHI-API/src ..
    make && make test && make install

* Load OpenBabel 2.3.1 module

    module load hpc/openbabel-2.3.1

* Load ORCA 3.0.2 Module

    module load centos6/orca-3.0.2_openmpi-1.6.5_gcc-4.8.0

* Loac MOPAC 2012 Module

    module load chem/MOPAC2012

* In case of problems with the installed OpenBabel version:
  Install OpenBabel 2.3.2 from source

    - Get dependencies
    + Odyssey: Unload all previously loaded modules. Boost shadows system regex.h and causes compilation of gamessukformat to crash.
    + Odyssey: Load compiler and dependencies
    module load hpc/gcc-4.8.0 centos6/python-2.7.3 centos6/cmake-2.8.11.2 hpc/eigen-3.2.0 centos6/swig-2.0.11
    + OS X with MacPorts: port install gcc48 swig swig-python cmake python27; port select gcc gcc48; port select python python27
    cd $HOME
    wget https://github.com/openbabel/openbabel/archive/openbabel-2-3-2.tar.gz
    tar -xzf openbabel-2-3-2.tar.gz
    mv openbabel-openbabel-2-3-2 openbabel-install
    cd openbabel-install
    mkdir build
    cd build
    + Odyssey: Run CMake and make
    cmake -DPYTHON_BINDINGS=ON -D PYTHON_LIBRARY=/n/sw/python-2.7.1/lib/libpython2.7.so -D CMAKE_INSTALL_PREFIX=$HOME/openbabel-2.3.2 -D CMAKE_C_COMPILER=/n/sw/fasrcsw/apps/Core/gcc/4.8.2-fasrc01/bin/gcc -D CMAKE_CXX_COMPILER=/n/sw/fasrcsw/apps/Core/gcc/4.8.2-fasrc01/bin/g++ -D RUN_SWIG=ON -D EIGEN3_INCLUDE_DIR=/n/sw/eigen-3.2.0/include/eigen3/ -D EIGEN3_VERSION_OK=1 ..
    make && make test && make install
