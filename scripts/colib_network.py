#!/usr/bin/env python

"""
colib network script

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/09/2015'

#
# Library modules
#

import sys
from collections import OrderedDict

#
# colibri submodules
#

from colibri.api import RunCommand
from colibri.console import console_program, ConsoleCommand
from colibri.enums import Result
from colibri.exceptions import CommandError, ScriptError
from colibri.options import Options

#
# Module variables
#

cmd_names = OrderedDict([
    ('network', 'NetworkConsoleCommand')])


#
# Public functions
#


def main(argv):

    # Get script subcommands
    subcommands = OrderedDict(
        [(cmd, globals()[cmd_names[cmd]]) for cmd in cmd_names])

    # Execute console program
    return console_program(subcommands, argv)

#
# Classes
#


class NetworkConsoleCommand(ConsoleCommand):

    @classmethod
    def add_subparser(cls, subparsers):

        # Network subparser
        cls._subparser = subparsers.add_parser(
            'network', help='run task suite for creating networks')

        # Network execution argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'network execution arguments')

        # Network execution argument names
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-b', '--openbabel-build', dest='openbabel_build', type=int,
            nargs='?', default=0, const=1, metavar='N',
            help='OpenBabel build tasks')
        cls._exec_parser_args.append('openbabel_build')

        cls._exec_parser.add_argument(
            '-r', '--rdkit-build', dest='rdkit_build', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='RDKit build tasks')
        cls._exec_parser_args.append('rdkit_build')

        cls._exec_parser.add_argument(
            '-m', '--mopac-geometry', dest='mopac_geometry', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='MOPAC geometry tasks')
        cls._exec_parser_args.append('mopac_geometry')

        cls._exec_parser.add_argument(
            '-z', '--mopac-nudge-geometry', dest='mopac_nudge_geometry',
            type=int, nargs='?', default=1, const=1, metavar='N',
            help='MOPAC nudge geometry tasks')
        cls._exec_parser_args.append('mopac_nudge_geometry')

        cls._exec_parser.add_argument(
            '-w', '--orca-geometry', dest='orca_geometry', type=int,
            nargs='?', default=0, const=1, metavar='N',
            help='ORCA geometry tasks')
        cls._exec_parser_args.append('orca_geometry')

        cls._exec_parser.add_argument(
            '-P', '--flask-mapper', dest='flask_mapper', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='flask mapper tasks')
        cls._exec_parser_args.append('flask_mapper')

        cls._exec_parser.add_argument(
            '-D', '--flask-reducer', dest='flask_reducer', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='flask reducer tasks')
        cls._exec_parser_args.append('flask_reducer')

        cls._exec_parser.add_argument(
            '-C', '--rdkit-flask-react', dest='rdkit_flask_react', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='RDKit flask reaction tasks')
        cls._exec_parser_args.append('rdkit_flask_react')

        cls._exec_parser.add_argument(
            '-x', '--mol-cleanup', dest='mol_cleanup', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='molecule cleanup task')
        cls._exec_parser_args.append('mol_cleanup')

        cls._exec_parser.add_argument(
            '-X', '--flask-cleanup', dest='flask_cleanup', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='flask cleanup task')

        cls._exec_parser.add_argument(
            '-y', '--react-rule', dest='react_rule', nargs='+',
            action='append', metavar='RULE',
            help='reaction rule as SMARTS')
        cls._exec_parser_args.append('react_rule')

        cls._exec_parser.add_argument(
            '-Y', '--react-rule-file', dest='react_rule_file',
            action='append', metavar='FILE',
            help='reaction rule input from file, one rule per line')
        cls._exec_parser_args.append('react_rule_file')

        cls._exec_parser.add_argument(
            '-W', '--suite-size', dest='suite_size', type=int,
            nargs='?', default=0, const=8, metavar='N',
            help='number of calculators in task suite')
        cls._exec_parser_args.append('suite_size')

        # Network configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'network configuration arguments')

        # Network configuration argument names
        cls._conf_parser_args = []

        # Individual configuration arguments
        cls._conf_parser.add_argument(
            '-G', '--max-generation', dest='max_generation', type=int,
            metavar='GEN', help='maximum generation number')
        cls._conf_parser_args.append('max_generation')

        cls._conf_parser.add_argument(
            '-9', '--max-energy-change', dest='max_energy_change', type=float,
            metavar='EN', help='maximum energy change in eV')
        cls._conf_parser_args.append('max_energy_change')

        cls._conf_parser.add_argument(
            '-0', '--max-rel-energy', dest='max_rel_energy', type=float,
            metavar='EN', help='maximum energy above reference in eV')
        cls._conf_parser_args.append('max_rel_energy')

        cls._conf_parser.add_argument(
            '-1', '--ref-energy', dest='ref_energy', type=float, metavar='EN',
            help='reference energy in eV')
        cls._conf_parser_args.append('ref_energy')

        cls._conf_parser.add_argument(
            '-@', '--tags', nargs='+', help='molecule/flask tags')
        cls._conf_parser_args.append('tags')

        cls._conf_parser.add_argument(
            '-M', '--batch-size', dest='batch_size', type=int, metavar='N',
            help='batch size')
        cls._conf_parser_args.append('batch_size')

        cls._conf_parser.add_argument(
            '--optimistic-lock', dest='optimistic_lock', action='store_true',
            default=None, help='use optimistic locking in tasks')
        cls._conf_parser.add_argument(
            '--no-optimistic-lock', dest='optimistic_lock',
            action='store_false', default=None,
            help='do not use optimistic locking in tasks')
        cls._conf_parser_args.append('optimistic_lock')

        cls._conf_parser.add_argument(
            '--pessimistic-lock', dest='pessimistic_lock',
            action='store_true', default=None,
            help='use pessimistic locking in tasks')
        cls._conf_parser.add_argument(
            '--no-pessimistic-lock', dest='pessimistic_lock',
            action='store_false', default=None,
            help='do not use pessimistic locking in tasks')
        cls._conf_parser_args.append('pessimistic_lock')

        cls._conf_parser.add_argument(
            '-V', '--cleanup-interval', dest='cleanup_interval', type=int,
            metavar='T',
            help='interval between execution of cleanup tasks in s')
        cls._conf_parser_args.append('cleanup_interval')

        cls._conf_parser.add_argument(
            '--configuration-program', dest='configuration_program',
            metavar='PROG', help='program name in build task')
        cls._conf_parser_args.append('configuration_program')

        cls._conf_parser.add_argument(
            '--configuration-version', dest='configuration_version',
            metavar='VER', help='program version in build task')
        cls._conf_parser_args.append('configuration_version')

        cls._conf_parser.add_argument(
            '--configuration-steps', dest='configuration_steps',
            type=int, default=None, metavar='N',
            help='number of force field optimization steps in build task')
        cls._conf_parser_args.append('configuration_steps')

        cls._conf_parser.add_argument(
            '--configuration-validation', dest='configuration_validation',
            action='store_true', default=None, help='validate configuration')
        cls._conf_parser.add_argument(
            '--no-configuration-validation', dest='configuration_validation',
            action='store_false', default=None,
            help='do not validate configuration')
        cls._conf_parser_args.append('configuration_validation')

        cls._conf_parser.add_argument(
            '--configuration-attempts', dest='configuration_attempts',
            type=int, default=None, metavar='N',
            help='number of attempts in build tasks')
        cls._conf_parser_args.append('configuration_attempts')

        cls._conf_parser.add_argument(
            '--geometry-program', dest='geometry_program',
            metavar='PROG', help='program name in geometry task')
        cls._conf_parser_args.append('geometry_program')

        cls._conf_parser.add_argument(
            '--geometry-version', dest='geometry_version',
            metavar='VER', help='program version in geometry task')
        cls._conf_parser_args.append('geometry_version')

        cls._conf_parser.add_argument(
            '--geometry-method', dest='geometry_method',
            metavar='METH', help='computation method in geometry task')
        cls._conf_parser_args.append('geometry_method')

        cls._conf_parser.add_argument(
            '--geometry-exe', dest='geometry_exe',
            metavar='PATH', help='executable program in geometry task')
        cls._conf_parser_args.append('geometry_exe')

        cls._conf_parser.add_argument(
            '--geometry-options', dest='geometry_options',
            metavar='OPT', help='program options in geometry task')
        cls._conf_parser_args.append('geometry_options')

        cls._conf_parser.add_argument(
            '--geometry-steps', dest='geometry_steps',
            type=int, default=None, metavar='N',
            help='number of structure optimization steps in geometry task')
        cls._conf_parser_args.append('configuration_steps')

        cls._conf_parser.add_argument(
            '--geometry-validation', dest='geometry_validation',
            action='store_true', default=None, help='validate geometry')
        cls._conf_parser.add_argument(
            '--no-geometry-validation', dest='geometry_validation',
            action='store_false', default=None,
            help='do not validate geometry')
        cls._conf_parser_args.append('geometry_validation')

        cls._conf_parser.add_argument(
            '--geometry-timeout', dest='geometry_timeout',
            type=int, default=None, metavar='T',
            help='execution timeout in s for geometry tasks')
        cls._conf_parser_args.append('geometry_timeout')

        cls._conf_parser.add_argument(
            '--geometry-attempts', dest='geometry_attempts',
            type=int, default=None, metavar='N',
            help='number of attempts in geometry tasks')
        cls._conf_parser_args.append('geometry_attempts')

        cls._conf_parser.add_argument(
            '-U', '--geometry-nudge', dest='geometry_nudge',
            action='store_true', default=None, help='use geometry nudge')
        cls._conf_parser.add_argument(
            '--no-geometry-nudge', dest='geometry_nudge',
            action='store_false', default=None,
            help='do not use geometry nudge')
        cls._conf_parser_args.append('geometry_nudge')

        cls._conf_parser.add_argument(
            '--geometry-nudge-strength', dest='geometry_nudge_strength',
            type=float, default=5., metavar='F',
            help='restraint strength for nudge geometry tasks')
        cls._conf_parser_args.append('geometry_nudge_strength')

        cls._conf_parser.add_argument(
            '--geometry-nudge-factor', dest='geometry_nudge_factor',
            type=float, default=2., metavar='N',
            help='restraint factor for nudge geometry tasks')
        cls._conf_parser_args.append('geometry_nudge_factor')

        cls._conf_parser.add_argument(
            '--geometry-nudge-program', dest='geometry_nudge_program',
            metavar='PROG', help='program name in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_program')

        cls._conf_parser.add_argument(
            '--geometry-nudge-version', dest='geometry_nudge_version',
            metavar='VER', help='program version in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_version')

        cls._conf_parser.add_argument(
            '--geometry-nudge-method', dest='geometry_nudge_method',
            metavar='METH', help='computation method in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_method')

        cls._conf_parser.add_argument(
            '--geometry-nudge-exe', dest='geometry_nudge_exe',
            metavar='PATH', help='executable program in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_exe')

        cls._conf_parser.add_argument(
            '--geometry-nudge-options', dest='geometry_nudge_options',
            metavar='OPT', help='program options in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_options')

        cls._conf_parser.add_argument(
            '--geometry-nudge-timeout', dest='geometry_nudge_timeout',
            type=int, default=None, metavar='T',
            help='execution timeout in s for geometry nudge tasks')
        cls._conf_parser_args.append('geometry_nudge_timeout')

        cls._conf_parser.add_argument(
            '--geometry-nudge-attempts', dest='geometry_nudge_attempts',
            type=int, default=None, metavar='N',
            help='number of attempts in geometry nudge tasks')
        cls._conf_parser_args.append('geometry_nudge_attempts')

        cls._conf_parser.add_argument(
            '--reduce-attempts', dest='reduce_attempts',
            type=int, default=None, metavar='N',
            help='number of attempts in FlaskReducerTask')
        cls._conf_parser_args.append('reduce_attempts')

    def __init__(self, args, opts):
        """colib network console subcommand.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Reaction rules as reaction SMARTS strings
        react_smarts = []

        # Reaction rules as JSON strings
        react_json = []

        # --react-rule option
        if args.get('react_rule'):
            for rule_list in args['react_rule']:
                for rule in rule_list:
                    react_smarts.append(rule)

        # DEBUG
        # Eventually change to JSON format
        # DEBUG

        # --react-rule-file option
        if args.get('react_rule_file'):
            for rule_file in args['react_rule_file']:
                for rule in open(rule_file, 'r'):
                    react_smarts.append(rule.strip())

        # Process configuration command-line arguments for network parser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

            # Add to dictionary if value is available
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        self._opts = opts.update(conf_opts)

        # Number of calculators for RunCommand #2 from command-line arguments
        openbabel_build = args.get('openbabel_build', 0)
        rdkit_build = args.get('rdkit_build', 0)
        mopac_geometry = args.get('mopac_geometry', 0)
        mopac_nudge_geometry = args.get('mopac_nudge_geometry', 0)
        orca_geometry = args.get('orca_geometry', 0)
        orca_property = args.get('orca_property', 0)
        flask_mapper = args.get('flask_mapper', 0)
        flask_reducer = args.get('flask_reducer', 0)
        rdkit_flask_react = args.get('rdkit_flask_react', 0)
        mol_cleanup = args.get('mol_cleanup', 0)
        flask_cleanup = args.get('flask_cleanup', 0)

        # Total number of calculators for RunCommand #2
        total_calcs = (openbabel_build + rdkit_build + mopac_geometry +
                       mopac_nudge_geometry + orca_geometry + orca_property +
                       flask_mapper + flask_reducer + rdkit_flask_react +
                       mol_cleanup + flask_cleanup)

        # Use suite size specification
        suite_size = args.get('suite_size', 0)
        if suite_size and suite_size > total_calcs:
            free_calcs = suite_size - total_calcs

            # Provide additional calculators in the order
            # flask_mapper, flask_reducer, rdkit_flask_react,
            # geometry (mopac_geometry or orca_geometry) tasks
            flask_mapper += (free_calcs + 3) / 4
            flask_reducer += (free_calcs + 2) / 4
            rdkit_flask_react += (free_calcs + 1) / 4
            if orca_geometry:
                orca_geometry += free_calcs / 4
            elif mopac_geometry:
                mopac_geometry += free_calcs / 4

        try:

            # Check if minimum number of calculators is provided
            if openbabel_build == 0 and rdkit_build == 0:
                raise CommandError('Need OpenBabelTask or RDKitTask')
            if mopac_geometry == 0 and orca_geometry == 0:
                raise CommandError(
                    'Need MOPACGeometryTask or OrcaGeometryTask')
            if flask_mapper == 0:
                raise CommandError('Need FlaskMapperTask')
            if flask_reducer == 0:
                raise CommandError('Need FlaskReducerTask')
            if rdkit_flask_react == 0:
                raise CommandError('Need RDKitFlaskReactionTask')

            # Check if reaction rules are provided
            if not react_smarts and not react_json:
                raise CommandError('Need reaction rules')

            # Construct and save main RunCommand object #2
            self._run_cmd = RunCommand(
                openbabel_build=openbabel_build, rdkit_build=rdkit_build,
                mopac_geometry=mopac_geometry,
                mopac_nudge_geometry=mopac_nudge_geometry,
                orca_geometry=orca_geometry, orca_property=orca_property,
                flask_mapper=flask_mapper, flask_reducer=flask_reducer,
                rdkit_flask_react=rdkit_flask_react, mol_cleanup=mol_cleanup,
                flask_cleanup=flask_cleanup, react_smarts=react_smarts,
                react_json=react_json, opts=self._opts)

        except CommandError as e:
            self.print_help()
            raise SystemExit(e)

    def execute(self):

        try:

            # Execute RunCommand
            self._run_cmd.execute()

            return Result.OK

        except ScriptError:
            return Result.SCRIPT_ERROR


if __name__ == '__main__':
    sys.exit(main(sys.argv))
