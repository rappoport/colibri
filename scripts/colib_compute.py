#!/usr/bin/env python

"""
colib compute script

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/20/2016'

#
# Library modules
#

import sys
import json
from collections import OrderedDict

#
# colibri submodules
#

from colibri.api import AddCommand, RunCommand, FetchCommand
from colibri.data import Flask
from colibri.console import console_program, ConsoleCommand
from colibri.enums import Result
from colibri.exceptions import CommandError, ScriptError
from colibri.options import Options
from colibri.utils import list_accumulator, object_hook


#
# Module variables
#

cmd_names = OrderedDict([
    ('compute', 'ComputeConsoleCommand')])

#
# Public functions
#


def main(argv):

    # Get script subcommands
    subcommands = OrderedDict(
        [(cmd, globals()[cmd_names[cmd]]) for cmd in cmd_names])

    # Execute console program
    return console_program(subcommands, argv)

#
# Classes
#


class ComputeConsoleCommand(ConsoleCommand):
    """colib compute console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):

        # Compute subparser
        cls._subparser = subparsers.add_parser(
            'compute', help='add flasks and compute their energies')

        # Compute execution argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'compute execution arguments')

        # Compute execution argument names
        cls._exec_parser_args = []

        cls._exec_parser.add_argument(
            '-E', '--flasks', nargs='+', metavar='FLASK',
            help='input flasks as SMILES/JSON string')
        cls._exec_parser_args.append('flasks')

        cls._exec_parser.add_argument(
            '-b', '--openbabel-build', dest='openbabel_build', type=int,
            nargs='?', default=0, const=1, metavar='N',
            help='OpenBabel build tasks')
        cls._exec_parser_args.append('openbabel_build')

        cls._exec_parser.add_argument(
            '-r', '--rdkit-build', dest='rdkit_build', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='RDKit build tasks')
        cls._exec_parser_args.append('rdkit_build')

        cls._exec_parser.add_argument(
            '-m', '--mopac-geometry', dest='mopac_geometry', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='MOPAC geometry tasks')
        cls._exec_parser_args.append('mopac_geometry')

        cls._exec_parser.add_argument(
            '-z', '--mopac-nudge-geometry', dest='mopac_nudge_geometry',
            type=int, nargs='?', default=0, const=1, metavar='N',
            help='MOPAC nudge geometry tasks')
        cls._exec_parser_args.append('mopac_nudge_geometry')

        cls._exec_parser.add_argument(
            '-w', '--orca-geometry', dest='orca_geometry', type=int,
            nargs='?', default=0, const=1, metavar='N',
            help='ORCA geometry tasks')
        cls._exec_parser_args.append('orca_geometry')

        cls._exec_parser.add_argument(
            '-P', '--flask-mapper', dest='flask_mapper', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='flask mapper tasks')
        cls._exec_parser_args.append('flask_mapper')

        cls._exec_parser.add_argument(
            '-D', '--flask-reducer', dest='flask_reducer', type=int,
            nargs='?', default=1, const=1, metavar='N',
            help='flask reducer tasks')
        cls._exec_parser_args.append('flask_reducer')

        # Compute configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'compute configuration arguments')

        # Compute configuration argument names
        cls._conf_parser_args = []

        cls._conf_parser.add_argument(
            '-@', '--tags', nargs='+', help='Molecule/flask tags')
        cls._conf_parser_args.append('tags')

        cls._conf_parser.add_argument(
            '-M', '--batch-size', dest='batch_size', type=int, metavar='N',
            help='batch size')
        cls._conf_parser_args.append('batch_size')

        cls._conf_parser.add_argument(
            '--optimistic-lock', dest='optimistic_lock', action='store_true',
            default=None, help='Use optimistic locking in tasks')
        cls._conf_parser.add_argument(
            '--no-optimistic-lock', dest='optimistic_lock',
            action='store_false', default=None,
            help='Do not use optimistic locking in tasks')
        cls._conf_parser_args.append('optimistic_lock')

        cls._conf_parser.add_argument(
            '--pessimistic-lock', dest='pessimistic_lock',
            action='store_true', default=None,
            help='Use pessimistic locking in tasks')
        cls._conf_parser.add_argument(
            '--no-pessimistic-lock', dest='pessimistic_lock',
            action='store_false', default=None,
            help='Do not use pessimistic locking in tasks')
        cls._conf_parser_args.append('pessimistic_lock')

        cls._conf_parser.add_argument(
            '--configuration-program', dest='configuration_program',
            metavar='PROG', help='program name in build task')
        cls._conf_parser_args.append('configuration_program')

        cls._conf_parser.add_argument(
            '--configuration-version', dest='configuration_version',
            metavar='VER', help='program version in build task')
        cls._conf_parser_args.append('configuration_version')

        cls._conf_parser.add_argument(
            '--configuration-steps', dest='configuration_steps',
            type=int, default=None, metavar='N',
            help='number of force field optimization steps in build task')
        cls._conf_parser_args.append('configuration_steps')

        cls._conf_parser.add_argument(
            '--configuration-validation', dest='configuration_validation',
            action='store_true', default=None, help='validate configuration')
        cls._conf_parser.add_argument(
            '--no-configuration-validation', dest='configuration_validation',
            action='store_false', default=None,
            help='do not validate configuration')
        cls._conf_parser_args.append('configuration_validation')

        cls._conf_parser.add_argument(
            '--configuration-attempts', dest='configuration_attempts',
            type=int, default=None, metavar='N',
            help='number of attempts in build tasks')
        cls._conf_parser_args.append('configuration_attempts')

        cls._conf_parser.add_argument(
            '--geometry-program', dest='geometry_program',
            metavar='PROG', help='program name in geometry task')
        cls._conf_parser_args.append('geometry_program')

        cls._conf_parser.add_argument(
            '--geometry-version', dest='geometry_version',
            metavar='VER', help='program version in geometry task')
        cls._conf_parser_args.append('geometry_version')

        cls._conf_parser.add_argument(
            '--geometry-method', dest='geometry_method',
            metavar='METH', help='computation method in geometry task')
        cls._conf_parser_args.append('geometry_method')

        cls._conf_parser.add_argument(
            '--geometry-exe', dest='geometry_exe',
            metavar='PATH', help='executable program in geometry task')
        cls._conf_parser_args.append('geometry_exe')

        cls._conf_parser.add_argument(
            '--geometry-options', dest='geometry_options',
            metavar='OPT', help='program options in geometry task')
        cls._conf_parser_args.append('geometry_options')

        cls._conf_parser.add_argument(
            '--geometry-steps', dest='geometry_steps',
            type=int, default=None, metavar='N',
            help='number of structure optimization steps in geometry task')
        cls._conf_parser_args.append('configuration_steps')

        cls._conf_parser.add_argument(
            '--geometry-validation', dest='geometry_validation',
            action='store_true', default=None, help='validate geometry')
        cls._conf_parser.add_argument(
            '--no-geometry-validation', dest='geometry_validation',
            action='store_false', default=None,
            help='do not validate geometry')
        cls._conf_parser_args.append('geometry_validation')

        cls._conf_parser.add_argument(
            '--geometry-timeout', dest='geometry_timeout',
            type=int, default=None, metavar='T',
            help='execution timeout in s for geometry tasks')
        cls._conf_parser_args.append('geometry_timeout')

        cls._conf_parser.add_argument(
            '--geometry-attempts', dest='geometry_attempts',
            type=int, default=None, metavar='N',
            help='number of attempts in geometry tasks')
        cls._conf_parser_args.append('geometry_attempts')

        cls._conf_parser.add_argument(
            '-U', '--geometry-nudge', dest='geometry_nudge',
            action='store_true', default=None, help='use geometry nudge')
        cls._conf_parser.add_argument(
            '--no-geometry-nudge', dest='geometry_nudge',
            action='store_false', default=None,
            help='do not use geometry nudge')
        cls._conf_parser_args.append('geometry_nudge')

        cls._conf_parser.add_argument(
            '--geometry-nudge-strength', dest='geometry_nudge_strength',
            type=float, default=5., metavar='F',
            help='restraint strength for nudge geometry tasks')
        cls._conf_parser_args.append('geometry_nudge_strength')

        cls._conf_parser.add_argument(
            '--geometry-nudge-factor', dest='geometry_nudge_factor',
            type=float, default=2., metavar='N',
            help='restraint factor for nudge geometry tasks')
        cls._conf_parser_args.append('geometry_nudge_factor')

        cls._conf_parser.add_argument(
            '--geometry-nudge-program', dest='geometry_nudge_program',
            metavar='PROG', help='program name in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_program')

        cls._conf_parser.add_argument(
            '--geometry-nudge-version', dest='geometry_nudge_version',
            metavar='VER', help='program version in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_version')

        cls._conf_parser.add_argument(
            '--geometry-nudge-method', dest='geometry_nudge_method',
            metavar='METH', help='computation method in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_method')

        cls._conf_parser.add_argument(
            '--geometry-nudge-exe', dest='geometry_nudge_exe',
            metavar='PATH', help='executable program in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_exe')

        cls._conf_parser.add_argument(
            '--geometry-nudge-options', dest='geometry_nudge_options',
            metavar='OPT', help='program options in geometry nudge task')
        cls._conf_parser_args.append('geometry_nudge_options')

        cls._conf_parser.add_argument(
            '--geometry-nudge-timeout', dest='geometry_nudge_timeout',
            type=int, default=None, metavar='T',
            help='execution timeout in s for geometry nudge tasks')
        cls._conf_parser_args.append('geometry_nudge_timeout')

        cls._conf_parser.add_argument(
            '--geometry-nudge-attempts', dest='geometry_nudge_attempts',
            type=int, default=None, metavar='N',
            help='number of attempts in geometry nudge tasks')
        cls._conf_parser_args.append('geometry_nudge_attempts')

    def __init__(self, args, opts):
        """colib compute console subcommand.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Need flask input
        if not args.get('flasks'):
            raise CommandError('Need flask input')

        # Process configuration command-line arguments for compute parser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

            # Add to dictionary if value is available
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        self._opts = opts.update(conf_opts)

        try:

            # Options for AddCommand object
            add_opts1 = self._opts

            # Construct AddCommand object
            self._add_cmd1 = AddCommand(
                flasks=args['flasks'], opts=add_opts1)

            # Options for RunCommand object #1
            num_tasks1 = len(args['flasks'])
            run_opts1 = self._opts.update(Options(
                precedence='command', tasks_per_calc=num_tasks1,
                tasks_per_cycle=num_tasks1, batch_size=num_tasks1,
                idle_interval=1, count_idle_tasks=True, persistent_pool=False,
                polling_interval=1, action_delay=1))

            # Construct RunCommand object #1
            self._run_cmd1 = RunCommand(
                flask_mapper=args.get('flask_mapper', 0),
                opts=run_opts1)

            # Options for RunCommand object #2
            num_tasks2 = len(set.union(
                *[set(flask.split('.')) for flask in args['flasks']]))
            run_opts2 = self._opts.update(Options(
                precedence='command', tasks_per_calc=num_tasks2,
                tasks_per_cycle=num_tasks2, batch_size=num_tasks2,
                idle_interval=1, count_idle_tasks=True, persistent_pool=False,
                polling_interval=1, action_delay=1))

            # Construct RunCommand object #2
            self._run_cmd2 = RunCommand(
                openbabel_build=args.get('openbabel_build', 0),
                rdkit_build=args.get('rdkit_build', 0),
                mopac_geometry=args.get('mopac_geometry', 0),
                mopac_nudge_geometry=args.get('mopac_nudge_geometry', 0),
                orca_geometry=args.get('orca_geometry', 0),
                opts=run_opts2)

            # Options for RunCommand object #3
            num_tasks3 = len(args['flasks'])
            run_opts3 = self._opts.update(Options(
                precedence='command', tasks_per_calc=num_tasks3,
                tasks_per_cycle=num_tasks3, batch_size=num_tasks3,
                idle_interval=1, count_idle_tasks=True, persistent_pool=False,
                polling_interval=1, action_delay=1))

            # Construct RunCommand object #3
            self._run_cmd3 = RunCommand(
                flask_reducer=args.get('flask_reducer', 0),
                opts=run_opts3)

            # Create flask accumulator
            self._flasks, flask_reader = list_accumulator()

            # Options for FetchCommand object
            fetch_opts = self._opts

            # Construct FetchCommand object
            self._fetch_cmd = FetchCommand(
                flasks=flask_reader, flask_ids=args['flasks'],
                opts=fetch_opts)

        except CommandError as e:
            self.print_help()
            raise SystemExit(e)

    def execute(self):

        try:

            # AddCommand
            self._add_cmd1.execute()

            # RunCommands
            self._run_cmd1.execute()
            self._run_cmd2.execute()
            self._run_cmd3.execute()

            # FetchCommand
            self._fetch_cmd.execute()

            for data in self._flasks:
                flask = Flask.fromdict(
                    json.loads(data, object_hook=object_hook))
                print flask.smiles, flask.energy

            return Result.OK

        except ScriptError:
            return Result.SCRIPT_ERROR


if __name__ == '__main__':
    sys.exit(main(sys.argv))
