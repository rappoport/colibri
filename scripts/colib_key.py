#!/usr/bin/env python

"""
colib key script

Copyright 2016 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/09/2015'

#
# Library modules
#

import sys
from collections import OrderedDict

#
# colibri submodules
#

from colibri.console import console_program, ConsoleCommand
from colibri.data import Molecule, Flask
from colibri.exceptions import CommandError

#
# Module variables
#

cmd_names = OrderedDict([
    ('key', 'KeyConsoleCommand')])


#
# Public functions
#


def main(argv):

    # Get script subcommands
    subcommands = OrderedDict(
        [(cmd, globals()[cmd_names[cmd]]) for cmd in cmd_names])

    # Execute console program
    return console_program(subcommands, argv)

#
# Classes
#


class KeyConsoleCommand(ConsoleCommand):

    @classmethod
    def add_subparser(cls, subparsers):

        # Key subparser
        cls._subparser = subparsers.add_parser(
            'key', help='return flask or molecule keys')

        # Key execution argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'key execution arguments')

        # Key execution argument names
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-e', '--molecules', nargs='*', metavar='MOL',
            help='return molecule keys')
        cls._exec_parser_args.append('molecules')

        cls._exec_parser.add_argument(
            '-E', '--flasks', nargs='*', metavar='FLASK',
            help='return flask keys')
        cls._exec_parser_args.append('flasks')

    def __init__(self, args, opts):
        """colib key console subcommand.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Molecule keys
        if args.get('molecules') is not None:
            self._molecules = args['molecules']
        else:
            self._molecules = []

        # Flask keys
        if args.get('flasks') is not None:
            self._flasks = args['flasks']
        else:
            self._flasks = []

        try:

            # Nothing to do
            if args.get('molecules') is None and args.get('flasks') is None:
                raise CommandError('Nothing to do')

        except CommandError as e:
            self.print_help()
            raise SystemExit(e)

    def execute(self):

        # Return molecule keys
        for mol in self._molecules:
            try:
                print Molecule(smiles=mol)
            except:
                print mol, 'IS INVALID'

        # Return flasks keys
        for flask in self._flasks:
            try:
                print Flask(smiles=flask)
            except:
                print flask, 'IS INVALID'


if __name__ == '__main__':
    sys.exit(main(sys.argv))
