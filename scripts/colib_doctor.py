#!/usr/bin/env python

"""
colib doctor script

Copyright 2016 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '03/16/2016'

#
# Library modules
#

import sys
import json
from collections import OrderedDict

#
# colibri submodules
#

from colibri.api import FetchCommand
from colibri.console import console_program, ConsoleCommand
from colibri.data import Molecule
from colibri.enums import Level, Result
from colibri.exceptions import CommandError, ScriptError
from colibri.options import Options
from colibri.utils import object_hook, list_accumulator

#
# Module variables
#

cmd_names = OrderedDict([
    ('doctor', 'DoctorConsoleCommand')])


#
# Public functions
#


def main(argv):

    # Get script subcommands
    subcommands = OrderedDict(
        [(cmd, globals()[cmd_names[cmd]]) for cmd in cmd_names])

    # Execute console program
    return console_program(subcommands, argv)

#
# Classes
#


class DoctorConsoleCommand(ConsoleCommand):

    @classmethod
    def add_subparser(cls, subparsers):

        # Doctor subparser
        cls._subparser = subparsers.add_parser(
            'doctor', help='check database for potential issues')

        # Doctor execution argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'doctor execution arguments')

        # Doctor execution argument names
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '--check-proton', dest='check_proton', action='store_true',
            default=None, help='check for proton energy')
        cls._exec_parser.add_argument(
            '--no-check-proton', dest='check_proton', action='store_false',
            default=None, help='do not check for proton energy')
        cls._exec_parser_args.append('check_proton')

        cls._exec_parser.add_argument(
            '--check-unfinished', dest='check_unfinished',
            action='store_true', default=None,
            help='check for unfinished molecules and flasks')
        cls._exec_parser.add_argument(
            '--no-check-unfinished', dest='check_unfinished',
            action='store_false', default=None,
            help='check for unfinished molecules and flask')
        cls._exec_parser_args.append('check_unfinished')

        # Doctor configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'doctor configuration arguments')

        # Doctor configuration argument names
        cls._conf_parser_args = []

    def __init__(self, args, opts):
        """colib doctor console subcommand.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Check for proton energy
        self._check_proton = args.get('check_proton', True)

        # Check for unfinished molecules and flasks
        self._check_unfinished = args.get('check_unfinished', True)

        # Process configuration command-line arguments for proton parser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

            # Add to dictionary if value is available
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        self._opts = opts.update(conf_opts)

        try:

            # Check for proton energy
            if self._check_proton:

                # Create molecule accumulator for proton
                self._proton, mol_reader1 = list_accumulator()

                # Options for FetchCommand object
                fetch_opts1 = self._opts.update(Options(
                    precedence='command', logging_level='WARNING'))

                # Construct FetchCommand object
                self._fetch_cmd1 = FetchCommand(
                    molecules=mol_reader1, molecule_ids=['[H+]'],
                    opts=fetch_opts1)

            # Check for unfinished molecules and flasks
            if self._check_unfinished:

                # Create molecule accumulator
                self._molecules, mol_reader2 = list_accumulator()

                # Create flask accumulator
                self._flasks, flask_reader = list_accumulator()

                # Fetch options
                fetch_opts2 = self._opts.update(Options(
                    precedence='command', logging_level='WARNING',
                    level=['FORMULA', 'FORMULA_LOCKED', 'CONFIGURATION',
                           'CONFIGURATION_LOCKED', 'CONFIGURATION_NUDGE',
                           'GEOMETRY_LOCKED', 'PROPERTY_LOCKED'],
                    status=['INITIAL', 'INITIAL_LOCKED', 'RUNNING',
                            'RUNNING_LOCKED', 'REACTIVE', 'REACTIVE_LOCKED',
                            'UNAVAILABLE']))

                # Construct FetchCommand object
                self._fetch_cmd2 = FetchCommand(
                    molecules=mol_reader2, flasks=flask_reader,
                    opts=fetch_opts2)

            # Nothing to do
            if not self._check_proton and not self._check_unfinished:
                raise CommandError('Nothing to do')

        except CommandError as e:
            self.print_help()
            raise SystemExit(e)

    def execute(self):

        try:

            # Check for proton energy
            if self._check_proton:

                # FetchCommand #1
                self._fetch_cmd1.execute()

                # No proton fetched
                if not self._proton:
                    print 'WARNING: PROTON MISSING'

                else:

                    # Convert to Molecule object
                    proton = Molecule.fromdict(json.loads(self._proton[0],
                                               object_hook=object_hook))

                    # Check level and energy
                    if proton.level != Level.GEOMETRY or proton.energy is None:
                        print 'WARNING: PROTON ENERGY MISSING'

            # Check for unfinished molecules and flasks
            if self._check_unfinished:

                # FetchCommand #2
                self._fetch_cmd2.execute()

                # Check if unfinished molecules are present
                if self._molecules:
                    print 'WARNING: UNFINISHED MOLECULES'

                # Check if unfinished flasks are present
                if self._flasks:
                    print 'WARNING: UNFINISHED FLASKS'

            return Result.OK

        except ScriptError:
            return Result.SCRIPT_ERROR


if __name__ == '__main__':
    sys.exit(main(sys.argv))
