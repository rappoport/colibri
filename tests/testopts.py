"""Configuration options for colibri tests.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_test_options: Read configuration options for colibri tests
"""

__all__ = []

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/20/2015'


#
# Library modules
#

import os.path
import json

#
# colibri modules
#

from colibri.options import Options

#
# Public functions
#


def get_test_options():

    # Default options dict
    conf_dir = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'config')
    site_conf_file = os.path.join(conf_dir, 'colibri.json')

    # Read site configuration defaults
    if os.path.exists(site_conf_file):
        with open(site_conf_file) as f:
            return Options.fromdict(precedence='site', **json.load(f))
    else:
        return Options()
