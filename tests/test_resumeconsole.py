"""Test classes for colib resume console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib resume console subcommand

Test Classes:
    TestResumeConsoleCommand: Test classes for ResumeConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, ResumeConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_resumeconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib resume console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestResumeConsoleCommand)
    return suite

#
# Test classes
#


class TestResumeConsoleCommand(unittest.TestCase):

    def _init_resumeconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        ResumeConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return ResumeConsoleCommand(args=args, opts=opts)

    def test_resume_mol(self):

        # Initialize ResumeConsoleCommand
        console = self._init_resumeconsole(['colib', 'resume', '-e'])

        # Check generated ResumeCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ResumeCommand\(molecules=True, flasks=False, " +
            r"opts=Options\(.*\)\)")

    def test_resume_mol_tags(self):

        # Initialize ResumeConsoleCommand
        console = self._init_resumeconsole(
            ['colib', 'resume', '-e', '-@', 'alkane'])

        # Check generated ResumeCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ResumeCommand\(molecules=True, flasks=False, " +
            r"opts=Options\(.*tags=OptionItem\(.*" +
            r"value=\['alkane'\].*\).*\).*\)")

    def test_resume_mol_interval(self):

        # Initialize ResumeConsoleCommand
        console = self._init_resumeconsole(
            ['colib', 'resume', '-e', '-V', '10'])

        # Check generated ResumeCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ResumeCommand\(molecules=True, flasks=False, " +
            r"opts=Options\(.*resume_interval=OptionItem\(.*" +
            r"value=10.*\).*\).*\)")

    def test_resume_flask(self):

        # Initialize ResumeConsoleCommand
        console = self._init_resumeconsole(['colib', 'resume', '-E'])

        # Check generated ResumeCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ResumeCommand\(molecules=False, flasks=True, " +
            r"opts=Options\(.*\)\)")

    def test_resume_nothing(self):

        # Initialize ResumeConsoleCommand
        with self.assertRaisesRegexp(SystemExit, 'Nothing to do'):
            self._init_resumeconsole(['colib', 'resume'])


if __name__ == '__main__':
    unittest.main()
