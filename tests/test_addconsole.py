"""Test classes for colib add console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib add console subcommand

Test Classes:
    TestAddConsoleCommand: Test classes for AddConsoleCommand
"""

#
# Standard library
#

import os
import unittest
import logging
import tempfile

#
# colibri modules
#

from colibri.console import ConsoleProgram, AddConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_addconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib add console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAddConsoleCommand)
    return suite

#
# Test classes
#


class TestAddConsoleCommand(unittest.TestCase):

    def _create_mol_file(self):

        # Construct temporary file
        _, mol_file = tempfile.mkstemp()

        # Insert molecule SMILES strings
        with open(mol_file, 'w') as f:
            f.write('N\nF\n')
        return mol_file

    def _create_flask_file(self):

        # Construct temporary file
        _, flask_file = tempfile.mkstemp()

        # Insert flask SMILES strings
        with open(flask_file, 'w') as f:
            f.write('CO.CO\nCO.C=O\n')
        return flask_file

    def _init_addconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        AddConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return AddConsoleCommand(args=args, opts=opts)

    def test_addconsole_mol_args(self):

        # Create temporary molecule file
        mol_file = self._create_mol_file()

        # Initialize AddConsoleCommand
        console = self._init_addconsole(
            ['colib', 'add', '-e', 'C', 'O', '-i', mol_file])

        # Check generated AddCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^AddCommand\(molecules=\['C', 'O', 'N', 'F'\]," +
            r" flasks=None, opts=Options\(.*\)\)")

        # Remove temporary file
        os.remove(mol_file)

    def test_addconsole_flask_args(self):

        # Create temporary flask file
        flask_file = self._create_flask_file()

        # Initialize AddConsoleCommand
        console = self._init_addconsole(
            ['colib', 'add', '-E', 'C=O.C=O', '-I', flask_file])

        # Check generated AddCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^AddCommand\(molecules=None, " +
            r"flasks=\['C=O\.C=O', 'CO.CO', 'CO.C=O'\], opts=Options\(.*\)\)")

        # Remove temporary file
        os.remove(flask_file)

    def test_addconsole_noinput(self):

        with self.assertRaisesRegexp(SystemExit, 'No input data'):
            self._init_addconsole(['colib', 'add'])


if __name__ == '__main__':
    unittest.main()
