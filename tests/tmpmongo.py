"""Temporary MongoDB instance for colibri testing.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    TmpMongoClient: Singleton to manage a temporary MongoDB instance

"""

__all__ = ['TmpMongoClient']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/10/2015'


#
# Library modules
#

import os
import tempfile
import subprocess
import shutil
import atexit


#
# Third-party modules
#

try:
    import port_for
except ImportError:
    port_for = None

#
# colibri modules
#

from colibri.mongostorage import MongoClient
from colibri.exceptions import ConnectionError

#
# Local variables
#

TEST_PORT_MIN = 42159
TEST_PORT_MAX = 42189


#
# Classes
#


class TmpMongoClient(object):
    """
    Singleton to manage a temporary MongoDB instance at localhost
    for testing. The database instance is destroyed at exit.
    Wraps the mongostorage.MongoClient API.

    """

    _instance = None

    @classmethod
    def get_instance(cls):
        """Ensure a singleton instance."""
        if cls._instance is None:
            cls._instance = cls()

        # Register with atexit
        atexit.register(cls._instance.destroy)

        return cls._instance

    def __init__(self):

        # Iterate over test ports
        for port in xrange(TEST_PORT_MIN, TEST_PORT_MAX):

            # if port_for is not None:
            if port_for is not None:

                # Check port for availability using port_for
                if not port_for.is_available(port):
                    continue

            # Try to connect directly using mongo
            # Slower but has no additional requirements
            else:

                # Check if a mongod instance is already listening
                mongoclient = MongoClient(host='localhost', port=port)

                # Check connection by requesting server info
                try:
                    mongoclient.server_info()
                except ConnectionError:
                    pass
                else:
                    continue

            # Create temporary directory for db files
            tmpdir = tempfile.mkdtemp(prefix='tmpmongo')

            # Start MongoDB daemon process
            try:

                process = subprocess.Popen(
                    ['mongod', '--bind_ip', 'localhost', '--port', str(port),
                     '--dbpath', tmpdir, '--nohttpinterface', '--noauth',
                     '--smallfiles', '--maxConns', '128', '--nssize', '1'],
                    stdout=open(os.devnull, 'wb'), stderr=subprocess.STDOUT)

            except (OSError, IOError):
                raise RuntimeError('Cannot start subprocess')

            # Open MongoClient instance, no connection is yet made
            mongoclient = MongoClient(host='localhost', port=port)

            # Check connection by requesting server info
            try:
                mongoclient.server_info()
            except ConnectionError:
                process.terminate()
                process.wait()
            else:
                # Save local variables
                self._mongoclient = mongoclient
                self._process = process
                self._host = 'localhost'
                self._port = port
                self._tmpdir = tmpdir
                break
        else:
            raise RuntimeError('Cannot find suitable port for testing')

    def __str__(self):
        return 'TmpMongoClient %s:%d' % (self._host, self._port)

    def __getattr__(self, attr):
        """Delegate to the MongoClient API."""
        return getattr(self.mongoclient, attr)

    @property
    def mongoclient(self):
        return self._mongoclient

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    def destroy(self):
        """Shut down mongod daemon instance and remove all database files."""

        # Terminate the MongoDB daemon process
        if getattr(self, '_process', None):
            self._process.terminate()
            self._process.wait()
            self._process = None

            # Remove the database data directory
            shutil.rmtree(self._tmpdir, ignore_errors=True)

        # Remove the temporary MongoDB instance
        cls = type(self)
        if getattr(cls, '_instance', None) is not None:
            cls._instance = None
