"""Test classes for colib delete console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib stat console subcommand

Test Classes:
    TestStatConsoleCommand: Test classes for StatConsoleCommand
"""

#
# Standard library
#

import os
import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, StatConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_statconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib stat console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestStatConsoleCommand)
    return suite

#
# Test classes
#


class TestStatConsoleCommand(unittest.TestCase):

    def _init_statconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        StatConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return StatConsoleCommand(args=args, opts=opts)

    def test_statconsole(self):

        # Initialize StatConsoleCommand
        console = self._init_statconsole(['colib', 'stat'])

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^StatCommand\(output_file=None, opts=Options\(.*\)\)")

    def test_statconsole_file(self):

        # Initialize StatConsoleCommand
        console = self._init_statconsole(['colib', 'stat', '-o', 'test.txt'])

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^StatCommand\(output_file='test.txt', opts=Options\(.*\)\)")

        # Remove temporary file
        os.remove('test.txt')

    def test_statconsole_tags(self):

        # Initialize StatConsoleCommand
        console = self._init_statconsole(['colib', 'stat', '-@', 'LL'])

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^StatCommand\(output_file=None, opts=Options\(.*" +
            r"tags=OptionItem\(.*value=\['LL'\].*\).*\).*\)")

if __name__ == '__main__':
    unittest.main()
