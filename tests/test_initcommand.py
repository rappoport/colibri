"""Test classes for colibri init command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri init command

Test Classes:
    TestInitCommand: Test classes for InitCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/30/2015'

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.api import init
from colibri.enums import Result

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_initcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri init command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestInitCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestInitCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def test_init(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri init command
        result = init(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Check indexes on mol collection
        indexes1 = self._client.test.mol.index_information()
        self.assertDictEqual(indexes1, {
            u'l_1_d_1': {u'ns': u'test.mol', u'key': [(u'l', 1), (u'd', 1)],
                         u'v': 1},
            u'_id_': {u'ns': u'test.mol', u'key': [(u'_id', 1)], u'v': 1}})

        # Check indexes on flask collection
        indexes2 = self._client.test.flask.index_information()
        self.assertDictEqual(indexes2, {
            u'j_1_d_1': {u'ns': u'test.flask', u'key': [(u'j', 1), (u'd', 1)],
                         u'v': 1},
            u'_id_': {u'ns': u'test.flask', u'key': [(u'_id', 1)], u'v': 1}})

    def test_init_tags(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri init command
        result = init(tags=True, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Check indexes on mol collection
        indexes1 = self._client.test.mol.index_information()
        self.assertDictEqual(indexes1, {
            u'l_1_d_1': {u'ns': u'test.mol', u'key': [(u'l', 1), (u'd', 1)],
                         u'v': 1},
            u'_id_': {u'ns': u'test.mol', u'key': [(u'_id', 1)], u'v': 1},
            u't_1': {u'ns': u'test.mol', u'key': [(u't', 1)], u'v': 1}})

        # Check indexes on flask collection
        indexes2 = self._client.test.flask.index_information()
        self.assertDictEqual(indexes2, {
            u'j_1_d_1': {u'ns': u'test.flask', u'key': [(u'j', 1), (u'd', 1)],
                         u'v': 1},
            u'_id_': {u'ns': u'test.flask', u'key': [(u'_id', 1)], u'v': 1},
            u't_1': {u'ns': u'test.flask', u'key': [(u't', 1)], u'v': 1}})

if __name__ == '__main__':
    unittest.main()
