"""Test classes for colib run console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib run console subcommand

Test Classes:
    TestRunConsoleCommand: Test classes for RunConsoleCommand
"""

#
# Standard library
#

import unittest
import logging
import tempfile

#
# colibri modules
#

from colibri.console import ConsoleProgram, RunConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_runconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib debug console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestRunConsoleCommand)
    return suite

#
# Test classes
#


class TestRunConsoleCommand(unittest.TestCase):

    def _create_react_rule_file(self):

        # Construct temporary file
        _, react_rule_file = tempfile.mkstemp()

        # Insert reaction rule SMART string
        with open(react_rule_file, 'w') as f:
            f.write('[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]\n')
        return react_rule_file

    def _create_react_rule_file_hal(self):

        # Construct temporary file
        _, react_rule_file = tempfile.mkstemp()

        # Insert reaction rule SMART string
        with open(react_rule_file, 'w') as f:
            f.write('[C+0:1]-[F,Cl,Br,I+0:2]>>[C+1:1].[*-1:2]\n')
        return react_rule_file

    def _init_runconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        RunConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return RunConsoleCommand(args=args, opts=opts)

    def test_runconsole_openbabel_build(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-b'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=1, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_openbabel_build_2(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-b', '2'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=2, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_openbabel_build_tags(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-b', '-@', 'LL'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=1, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.* tags=OptionItem\(.*, value=\['LL'\], .*\).*\)")

    def test_runconsole_rdkit_build(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-r'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=1, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_mopac_geometry(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-m'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=1, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_mopac_nudge_geometry(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-z'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=1, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_orca_geometry(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-w'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=1, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_orca_property(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-u'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=1, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_flask_mapper(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-P'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=1, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_flask_reducer(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-D'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=1, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_flask_reducer_max_generation(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-D', '-G', '6'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=1, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*" +
            r"max_generation=OptionItem\(.*, value=6, .*\).*\)")

    def test_runconsole_flask_reducer_max_rel_energy(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(
            ['colib', 'run', '-D', '-0' '6.0', '-1', ' -950.0'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=1, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*" +
            r"max_rel_energy=OptionItem\(.*, value=6.0, .*\), .*" +
            r"ref_energy=OptionItem\(.*, value=-950.0, .*\).*\)")

    def test_runconsole_flask_reducer_max_energy_change(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-D', '-9', '2.0'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=1, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*" +
            r"max_energy_change=OptionItem\(.*, value=2.0, .*\).*\)")

    def test_runconsole_rdkit_react(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(
            ['colib', 'run', '-c', '-y', '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=1, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\['\[O\+0:1\]-\[#1\+0:2\]" +
            r">>\[O-1:1\].\[#1\+1:2\]'\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_rdkit_react_rule_file(self):

        # Write reaction rule file
        react_rule_file = self._create_react_rule_file()

        # Initialize RunConsoleCommand
        console = self._init_runconsole(
            ['colib', 'run', '-c', '-Y', react_rule_file])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=1, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\['\[O\+0:1\]-\[#1\+0:2\]" +
            r">>\[O-1:1\].\[#1\+1:2\]'\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_rdkit_react_norules(self):

        # Initialize RunConsoleCommand
        with self.assertRaisesRegexp(CommandError, 'Need reaction rules'):
            self._init_runconsole(['colib', 'run', '-c'])

    def test_runconsole_rdkit_flask_react(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(
            ['colib', 'run', '-C', '-y', '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=1, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\['\[O\+0:1\]-\[#1\+0:2\]" +
            r">>\[O-1:1\].\[#1\+1:2\]'\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_rdkit_flask_react4(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(
            ['colib', 'run', '-C', '-y', '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
             '[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]', '-y',
             '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]',
             '[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]'])

        # Check generated RunCommand
        # Order of rules is not fixed, need to check rules individually
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=1, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[.*" +
            r"'\[O\+0:1\]-\[#1\+0:2\]>>\[O-1:1\].\[#1\+1:2\]'.*" +
            r"\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(.*, react_smarts=\[.*" +
            r"'\[O\+1:1\]-\[#1\+0:2\]>>\[O\+0:1\].\[#1\+1:2\]'.*" +
            r"\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(.*, react_smarts=\[.*" +
            r"'\[O-1:1\].\[#1\+1:2\]>>\[O\+0:1\]-\[#1\+0:2\]'.*" +
            r"\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(.* react_smarts=\[.*" +
            r"'\[O\+0:1\].\[#1\+1:2\]>>\[O\+1:1\]-\[#1\+0:2\]'.*" +
            r"\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_rdkit_flask_react_rule_file(self):

        # Write reaction rule file
        react_rule_file = self._create_react_rule_file()

        # Initialize RunConsoleCommand
        console = self._init_runconsole(
            ['colib', 'run', '-C', '-Y', react_rule_file])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=1, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\['\[O\+0:1\]-\[#1\+0:2\]" +
            r">>\[O-1:1\].\[#1\+1:2\]'\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_rdkit_flask_react_rule_file2(self):

        # Write reaction rule files
        react_rule_file_1 = self._create_react_rule_file()
        react_rule_file_2 = self._create_react_rule_file_hal()

        # Initialize RunConsoleCommand
        console = self._init_runconsole(
            ['colib', 'run', '-C', '-Y', react_rule_file_1,
             '-Y', react_rule_file_2])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=1, mol_cleanup=0, " +
            r"flask_cleanup=0, react_smarts=\[.*" +
            r"'\[O\+0:1\]-\[#1\+0:2\]" +
            r">>\[O-1:1\].\[#1\+1:2\]'.*\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(.*, react_smarts=\[.*" +
            r"'\[C\+0:1\]-\[F,Cl,Br,I\+0:2\]>>\[C\+1:1\]\.\[\*-1:2\]'.*" +
            r"\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_rdkit_flask_react_norules(self):

        # Initialize RunConsoleCommand
        with self.assertRaisesRegexp(CommandError, 'Need reaction rules'):
            self._init_runconsole(['colib', 'run', '-C'])

    def test_runconsole_mol_cleanup(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-x'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=1, " +
            r"flask_cleanup=0, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_flask_cleanup(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole(['colib', 'run', '-X'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=0, " +
            r"mopac_geometry=0, mopac_nudge_geometry=0, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=0, flask_reducer=0, " +
            r"rdkit_react=0, rdkit_flask_react=0, mol_cleanup=0, " +
            r"flask_cleanup=1, react_smarts=\[\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_flask_multiple(self):

        # Initialize RunConsoleCommand
        console = self._init_runconsole([
            'colib', 'run', '-b', '0', '-r', '1', '-m', '2', '-z', '1',
            '-w', '0', '-u', '0', '-D', '4', '-P', '4', '-c', '0', '-C', '2',
            '-x', '-X', '1', '-y', '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^RunCommand\(openbabel_build=0, rdkit_build=1, " +
            r"mopac_geometry=2, mopac_nudge_geometry=1, orca_geometry=0, " +
            r"orca_property=0, flask_mapper=4, flask_reducer=4, " +
            r"rdkit_react=0, rdkit_flask_react=2, mol_cleanup=1, " +
            r"flask_cleanup=1, react_smarts=\['\[O\+0:1\]-\[#1\+0:2\]" +
            r">>\[O-1:1\].\[#1\+1:2\]'\], react_json=\[\], " +
            r"opts=Options\(.*\)\)")

    def test_runconsole_notasks(self):

        # Initialize RunConsoleCommand
        with self.assertRaisesRegexp(CommandError, 'Nothing to do'):
            self._init_runconsole(['colib', 'run'])


if __name__ == '__main__':
    unittest.main()
