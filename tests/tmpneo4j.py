"""Temporary Neo4J instance for colibri testing.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    TmpNeo4JClient: Singleton to manage a temporary Neo4J instance

"""

__all__ = ['TmpNeo4JClient']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '01/06/2016'


#
# Library modules
#

import os
import re
import subprocess
import shutil
import tempfile
import time
import atexit


#
# Third-party modules
#

try:
    import port_for
except ImportError:
    port_for = None
try:
    import neokit
except ImportError:
    neokit = None

#
# colibri modules
#

from colibri.neo4jstorage import Neo4JClient
from colibri.exceptions import ConnectionError, ExecutionError

#
# Local variables
#

TEST_PORT_MIN = 42199
TEST_PORT_MAX = 42229


#
# Classes
#


class TmpNeo4JClient(object):
    """
    Singleton to manage a temporary Neo4J instance at localhost
    for testing. The database instance is destroyed at exit.

    """

    _instance = None

    @classmethod
    def get_instance(cls):
        """Ensure a singleton instance."""
        if cls._instance is None:
            cls._instance = cls()

        # Register with atexit
        atexit.register(cls._instance.destroy)

        return cls._instance

    def __init__(self):

        # Raise exception if Neo4J server library is not available
        if neokit is None:
            raise ExecutionError('No Neo4J server found')

        # Find two consecutive free ports
        for port in xrange(TEST_PORT_MIN, TEST_PORT_MAX, 3):

            # if port_for is not None:
            if port_for is not None:

                # Check port for availability using port_for
                if (not port_for.is_available(port) or
                        not port_for.is_available(port + 1) or
                        not port_for.is_available(port + 2)):
                    continue

            else:

                # Check if a Neo4J HTTP server is already listening
                neo4jclient = Neo4JClient(host='localhost', port=port,
                                          proto='http')

                # Check connection by requesting version info
                try:
                    a = neo4jclient.server_info()
                    print a
                except ConnectionError:
                    pass
                else:
                    continue

                # # Check if a Neo4J HTTPS client is already listening
                # neo4jclient = Neo4JClient(host='localhost', port=port + 1,
                #                           proto='https')

                # # Check if a Neo4J BOLT client is already listening
                # neo4jclient = Neo4JClient(host='localhost', port=port + 2,
                #                           proto='bolt')

            # Determine location of the Neo4J installation
            neo4j_home = os.environ['NEO4J_HOME']

            # Create and populate temporary directory for conf / db files
            tmpdir = tempfile.mkdtemp(prefix='tmpneo4j')

            # Reset the location variable
            os.environ['NEO4J_HOME'] = tmpdir

            # Copy conf and bin subdirectories from Neo4J installation
            # Neo4J start script seems to infer db location from script path
            shutil.copytree(
                os.path.join(neo4j_home, 'conf'), os.path.join(tmpdir, 'conf'))
            shutil.copytree(
                os.path.join(neo4j_home, 'bin'), os.path.join(tmpdir, 'bin'))

            # Make data, run, logs subdirectories
            os.mkdir(os.path.join(tmpdir, 'data'))
            os.mkdir(os.path.join(tmpdir, 'run'))
            os.mkdir(os.path.join(tmpdir, 'logs'))

            # Change into the temporary directory
            startdir = os.getcwd()
            os.chdir(tmpdir)

            # Create symlink for the  lib subdirectory
            # from Neo4J installation
            for path in ['lib']:
                os.symlink(os.path.join(neo4j_home, path), path)

            # Change back
            os.chdir(startdir)

            # Edit Neo4J configuration files
            re_http_port = re.compile(
                r'^\s*#?\s*(dbms\.connector\.http\.address)=.*$',
                re.MULTILINE)
            re_https_port = re.compile(
                r'^\s*#?\s*(dbms\.connector\.https\.address)=.*$',
                re.MULTILINE)
            re_bolt_port = re.compile(
                r'^\s*#?\s*(dbms\.connector\.bolt\.address)=.*$',
                re.MULTILINE)

            # Insert temporary port numbers into conf/neo4j.conf
            filename = os.path.join(tmpdir, 'conf', 'neo4j.conf')
            tmp_fd, tmp_filename = tempfile.mkstemp()
            tmp_f = os.fdopen(tmp_fd, 'w+')
            with open(filename, 'r') as f:
                for line in f:
                    line = re_http_port.sub(r'\1=0.0.0.0:' + str(port), line)
                    line = re_https_port.sub(
                        r'\1=0.0.0.0:' + str(port + 1), line)
                    line = re_bolt_port.sub(
                        r'\1=0.0.0.0:' + str(port + 2), line)
                    tmp_f.write(line)
            tmp_f.close()
            shutil.move(tmp_filename, filename)

            # Start server in the temporary directory
            try:
                neo4jserver = neokit.GraphServer(tmpdir)
                neo4jserver.start()

            except (OSError, IOError, subprocess.CalledProcessError):
                raise RuntimeError('Cannot start subprocess')

            # Open Neo4J client connection
            neo4jclient = Neo4JClient(host='localhost', port=port,
                                      proto='http')

            # Check connection
            try:
                time.sleep(1)
                neo4jclient.server_info()
            except ConnectionError:
                neo4jserver.stop()
            else:
                # Save local variables
                self._client = neo4jclient
                self._server = neo4jserver
                self._host = 'localhost'
                self._port = port
                self._tmpdir = tmpdir
                break

        else:
            raise RuntimeError('Cannot find suitable port for testing')

    def __str__(self):
        return 'TmpNeo4JClient %s:%d (%s)' % (self._host, self._port,
                                              self._proto)

    def __getattr__(self, attr):
        """Delegate to the Neo4J client API."""
        return getattr(self.neo4jclient, attr)

    @property
    def neo4jclient(self):
        return self._client

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    def destroy(self):
        """Shut down Neo4J server and remove all temporary files."""

        # Stop Neo4J server
        if getattr(self, '_server', None):
            self._server.stop()
            self._server = None

            # Remove the database data directory
            time.sleep(1)
            shutil.rmtree(self._tmpdir, ignore_errors=True)

        # Remove the temporary Neo4J instance
        cls = type(self)
        if getattr(cls, '_instance', None) is not None:
            cls._instance = None
