"""Test classes for the moltask classes.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for moltask classes

Test Classes:
    TestCleanupTask: Test classes for CleanupTask
    TestSuspendTask: Test classes for SuspendTask
    TestResumeTask: Test classes for ResumeTask
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/13/2015'

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.data import Molecule
from colibri.calc import Calculator
from colibri.enums import Level
from colibri.utils import randomid

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options


#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_moltask')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for moltask classes."""
    moltask_suite = []

    # CleanupTask tests
    mol_cleanup_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestCleanupTask)
    moltask_suite.append(mol_cleanup_suite)

    # SuspendTask tests
    mol_suspend_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestSuspendTask)
    moltask_suite.append(mol_suspend_suite)

    # CleanupTask tests
    mol_resume_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestResumeTask)
    moltask_suite.append(mol_resume_suite)

    return unittest.TestSuite(moltask_suite)

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestCleanupTask(unittest.TestCase):
    """Test classes for CleanupTask."""

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_locked(self):

        # Insert FORMULA_LOCKED record
        self._client.test.mol.insert({
            "z": [], "d": 44, "f": {"i": None, "s": "CCC", "d": 44},
            "l": 1001, "3": 0, "2": "", "5": "rdkit_build_i0b8hg",
            "t": ["organic"],
            "4": "", "8": 0, "_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"})

        # Insert FORMULA_INVALID record
        self._client.test.mol.insert({
            "z": [], "d": 52, "f": {"i": None, "s": "C12C3C1C23", "d": 52},
            "l": 1091, "3": 0, "2": "", "4": "", "8": 1,
            "_id": "FJGIHZCEZAZPSP-UHFFFAOYSA-N-VIEIRRFESP"})

        # Insert CONFIGURATION_LOCKED record
        self._client.test.mol.insert({
            "c": {"d": 52, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

F         -1.12799       -0.23990       -0.00000
C         -0.00007        0.55775        0.00001
F          1.12804       -0.23986       -0.00000
H         -0.00006        1.19829        0.90579
H         -0.00004        1.19829       -0.90585"""},
            "z": [], "d": 52, "f": {"i": None, "s": "FCF", "d": 52},
            "l": 1101, "3": 0, "2": "", "5": "mopac_geometry_umIYYj",
            "t": ["organic", "halogenated"],
            "4": "", "8": 0, "_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"})

        # Insert GEOMETRY_LOCKED record
        self._client.test.mol.insert({
            "c": {"d": 30, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

C         -0.61340       -0.00000       -0.00000
O          0.60608        0.00000        0.00000
H         -1.15558       -0.93914        0.00000
H         -1.15564        0.93913        0.00000"""},
            "z": [], "d": 30, "g": {
                "e": -444.38128, "d": 30, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC", "7": True,
                "v": "2012", "x": """\
4

C    -0.602789     0.000065    -0.000000
O     0.603275    -0.000020     0.000000
H    -1.168300    -0.933944     0.000000
H    -1.168372     0.933787     0.000000"""},
            "f": {"i": None, "s": "C=O", "d": 30}, "l": 1201,
            "3": 0, "2": "", "5": "orca_geometry_kaRpLT",
            "t": ["organic", "ketone"],
            "4": "", "8": 0, "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"})

        # Insert PROPERTY_LOCKED record
        self._client.test.mol.insert({
            "c": {"d": 28, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
2

N          0.56000        0.00000        0.00000
N         -0.56000        0.00000        0.00000"""}, "z": [], "d": 28,
            "g": {"e": -371.72947, "d": 28, "i": None, "h": "PM7", "m": 1,
                  "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                  "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""},
            "f": {"i": None, "s": "N#N", "d": 28}, "l": 1301, "3": 0,
            "2": "", "5": "orca_property_8uG5PU", "4": "", "y": {
                "e": -11.74325043, "d": 28, "i": None,
                "h": "ZINDO/s/COSMO(methanol)", "m": 1,
                "o": """\
rhf zindo/s tightscf diis norico nomoprint cosmo(water)
%cis NRoots 10
Maxdim 100
end""",
                "n": {
                    "transition_dipoles_z": [
                        -4e-05, -1e-05, -0.00101, 0.74538,
                        -0.11784, 0.00026, -0.0009, -0.0, 0.0, 0.0],
                    "transition_dipoles_y": [
                        -2e-05, -3e-05, -2e-05, 0.11784,
                        0.74539, -0.0009, -0.00025, -1e-05,
                        -1e-05, 0.0],
                    "transition_dipoles_x": [
                        1e-05, 0.00024, 0.0, -2e-05, -0.00015,
                        -0.00035, -1e-05, -1.81418, 1.26879,
                        -6e-05],
                    "excitation_energies": [
                        90255.0, 90266.4, 127154.2, 132373.2,
                        132384.6, 133406.2, 133406.2, 194452.8,
                        262473.3, 290974.4]},
                "q": 0, "u": {
                    "transition_dipoles_z": "au",
                    "transition_dipoles_y": "au",
                    "transition_dipoles_x": "au",
                    "excitation_energies": "cm^-1"},
                "w": "ORCA", "v": "3.0.1", "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""}, "8": 0,
            "_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"})

    def _insert_data_unreactive(self):

        # Insert FORMULA_UNREACTIVE record
        self._client.test.mol.insert(
            Molecule(smiles='C', level=Level.FORMULA_UNREACTIVE,
                     tags=['organic']).todict())

    def _run_cleanuptask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'mol_cleanup_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'CleanupTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1

        # Cleanup interval
        input_kv['cleanup_interval'] = 0

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_cleanup(self):

        # Insert molecule records
        self._insert_data_locked()
        self._insert_data_unreactive()

        # Run CleanupTask
        self._run_cleanuptask()

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'CCC')
        self.assertEqual(mol1.etag, '')
        self.assertListEqual(mol1.tags, ['organic'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "FJGIHZCEZAZPSP-UHFFFAOYSA-N-VIEIRRFESP"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA_INVALID)
        self.assertEqual(mol2.formula.smiles, 'C12C3C1C23')
        self.assertEqual(mol2.etag, '')

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"}))

        # Check result
        self.assertEqual(mol3.level, Level.CONFIGURATION)
        self.assertEqual(mol3.formula.smiles, 'FCF')
        self.assertEqual(mol3.etag, '')
        self.assertListEqual(mol3.tags, ['organic', 'halogenated'])

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY)
        self.assertEqual(mol4.formula.smiles, 'C=O')
        self.assertEqual(mol4.etag, '')
        self.assertListEqual(mol4.tags, ['organic', 'ketone'])

        # Get result from database
        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result
        self.assertEqual(mol5.level, Level.PROPERTY)
        self.assertEqual(mol5.formula.smiles, 'N#N')
        self.assertEqual(mol5.etag, '')
        self.assertListEqual(mol5.tags, [])

        # Get result from the database
        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}))

        # Check result
        self.assertEqual(mol6.level, Level.FORMULA)
        self.assertEqual(mol6.formula.smiles, 'C')
        self.assertListEqual(mol6.tags, ['organic'])

    def test_cleanup_empty(self):

        # Run CleanupTask
        self._run_cleanuptask()

        # Molecule database is empty
        self.assertEqual(self._client.test.mol.count(), 0)

    def test_cleanup_tags(self):

        # Insert molecule records
        self._insert_data_locked()

        # Add tags
        input_kv = {'tags': ['organic']}

        # Run CleanupTask
        self._run_cleanuptask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"}))

        # Check result, should be unlocked
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'CCC')
        self.assertEqual(mol1.etag, '')
        self.assertListEqual(mol1.tags, ['organic'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result, shouldbe still locked
        self.assertEqual(mol2.level, Level.PROPERTY_LOCKED)
        self.assertEqual(mol2.formula.smiles, 'N#N')
        self.assertEqual(mol2.etag, 'orca_property_8uG5PU')
        self.assertListEqual(mol2.tags, [])


class TestSuspendTask(unittest.TestCase):
    """Test classes for SuspendTask."""

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data(self):

        # Insert FORMULA record
        self._client.test.mol.insert(
            {"z": [], "d": 31, "f": {"i": None, "s": "O=O", "d": 32},
             "l": 1000, "3": 0, "2": "", "4": "", "8": 0,
             "t": [],
             "_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"})

        # Insert CONFIGURATION record
        self._client.test.mol.insert({
            "c": {"d": 52, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

F         -1.12799       -0.23990       -0.00000
C         -0.00007        0.55775        0.00001
F          1.12804       -0.23986       -0.00000
H         -0.00006        1.19829        0.90579
H         -0.00004        1.19829       -0.90585"""},
            "z": [], "d": 52, "f": {"i": None, "s": "FCF", "d": 52},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "t": ["organic", "halogenated"],
            "_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"})

        # Insert GEOMETRY record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17,
            "g": {"e": -226.7529, "d": 17, "i": None, "h": "PM7",
                  "m": 1, "o": "xyz", "q": 0, "w": "MOPAC", "7": True,
                  "v": "2012", "x": """\
4

N    -0.000022    -0.000019     0.038027
H     0.569814    -0.749684    -0.283921
H    -0.934098    -0.118619    -0.283910
H     0.364387     0.868362    -0.283954"""},
            "f": {"i": None, "s": "N", "d": 17},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0,
            "t": [],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

        # Insert PROPERTY record
        self._client.test.mol.insert({
            "c": {"d": 28, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
2

N          0.56000        0.00000        0.00000
N         -0.56000        0.00000        0.00000"""}, "z": [], "d": 28,
            "g": {"e": -371.72947, "d": 28, "i": None, "h": "PM7", "m": 1,
                  "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                  "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""},
            "f": {"i": None, "s": "N#N", "d": 28}, "l": 1300, "3": 0,
            "2": "", "4": "", "y": {
                "e": -11.74325043, "d": 28, "i": None,
                "h": "ZINDO/s/COSMO(methanol)", "m": 1,
                "o": """\
rhf zindo/s tightscf diis norico nomoprint cosmo(water)
%cis NRoots 10
Maxdim 100
end""",
                "n": {
                    "transition_dipoles_z": [
                        -4e-05, -1e-05, -0.00101, 0.74538,
                        -0.11784, 0.00026, -0.0009, -0.0, 0.0, 0.0],
                    "transition_dipoles_y": [
                        -2e-05, -3e-05, -2e-05, 0.11784,
                        0.74539, -0.0009, -0.00025, -1e-05,
                        -1e-05, 0.0],
                    "transition_dipoles_x": [
                        1e-05, 0.00024, 0.0, -2e-05, -0.00015,
                        -0.00035, -1e-05, -1.81418, 1.26879,
                        -6e-05],
                    "excitation_energies": [
                        90255.0, 90266.4, 127154.2, 132373.2,
                        132384.6, 133406.2, 133406.2, 194452.8,
                        262473.3, 290974.4]},
                "q": 0, "u": {
                    "transition_dipoles_z": "au",
                    "transition_dipoles_y": "au",
                    "transition_dipoles_x": "au",
                    "excitation_energies": "cm^-1"},
                "w": "ORCA", "v": "3.0.1", "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""}, "8": 0,
            "_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"})

    def _run_suspendtask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'mol_suspend_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'SuspendTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1

        # Suspend interval
        input_kv['suspend_interval'] = 0

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_suspend(self):

        # Insert new molecule record
        self._insert_data()

        # Run SuspendTask
        self._run_suspendtask()

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_SUSPENDED)
        self.assertEqual(mol1.formula.smiles, 'O=O')

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"}))

        # Check result
        self.assertEqual(mol2.level, Level.CONFIGURATION_SUSPENDED)
        self.assertEqual(mol2.formula.smiles, 'FCF')
        self.assertEqual(mol2.configuration.priority, 52)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY_SUSPENDED)
        self.assertEqual(mol3.formula.smiles, 'N')
        self.assertEqual(mol3.geometry.energy, -226.7529)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result
        self.assertEqual(mol4.level, Level.PROPERTY_SUSPENDED)
        self.assertEqual(mol4.formula.smiles, 'N#N')
        self.assertEqual(mol4.geometry.energy, -371.72947)
        self.assertEqual(mol4.property.energy, -11.74325043)

    def test_suspend_empty(self):

        # Run SuspendTask
        self._run_suspendtask()

        # Molecule database is empty
        self.assertEqual(self._client.test.mol.count(), 0)

    def test_suspend_tags(self):

        # Insert new molecule record
        self._insert_data()

        # Input options
        input_kv = {'tags': ['halogenated']}

        # Run SuspendTask
        self._run_suspendtask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'O=O')

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"}))

        # Check result
        self.assertEqual(mol2.level, Level.CONFIGURATION_SUSPENDED)
        self.assertEqual(mol2.formula.smiles, 'FCF')
        self.assertEqual(mol2.configuration.priority, 52)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.smiles, 'N')
        self.assertEqual(mol3.geometry.energy, -226.7529)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result
        self.assertEqual(mol4.level, Level.PROPERTY)
        self.assertEqual(mol4.formula.smiles, 'N#N')
        self.assertEqual(mol4.geometry.energy, -371.72947)
        self.assertEqual(mol4.property.energy, -11.74325043)


class TestResumeTask(unittest.TestCase):
    """Test classes for ResumeTask."""

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data(self):

        # Insert FORMULA record
        self._client.test.mol.insert(
            {"z": [], "d": 31, "f": {"i": None, "s": "O=O", "d": 32},
             "l": 1002, "3": 0, "2": "", "4": "", "8": 0,
             "t": [],
             "_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"})

        # Insert CONFIGURATION record
        self._client.test.mol.insert({
            "c": {"d": 52, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

F         -1.12799       -0.23990       -0.00000
C         -0.00007        0.55775        0.00001
F          1.12804       -0.23986       -0.00000
H         -0.00006        1.19829        0.90579
H         -0.00004        1.19829       -0.90585"""},
            "z": [], "d": 52, "f": {"i": None, "s": "FCF", "d": 52},
            "l": 1102, "3": 0, "2": "", "4": "", "8": 0,
            "t": ["organic", "halogenated"],
            "_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"})

        # Insert GEOMETRY record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17,
            "g": {"e": -226.7529, "d": 17, "i": None, "h": "PM7",
                  "m": 1, "o": "xyz", "q": 0, "w": "MOPAC", "7": True,
                  "v": "2012", "x": """\
4

N    -0.000022    -0.000019     0.038027
H     0.569814    -0.749684    -0.283921
H    -0.934098    -0.118619    -0.283910
H     0.364387     0.868362    -0.283954"""},
            "f": {"i": None, "s": "N", "d": 17},
            "l": 1202, "3": 0, "2": "", "4": "", "8": 0,
            "t": [],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

        # Insert PROPERTY record
        self._client.test.mol.insert({
            "c": {"d": 28, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
2

N          0.56000        0.00000        0.00000
N         -0.56000        0.00000        0.00000"""}, "z": [], "d": 28,
            "g": {"e": -371.72947, "d": 28, "i": None, "h": "PM7", "m": 1,
                  "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                  "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""},
            "f": {"i": None, "s": "N#N", "d": 28}, "l": 1302, "3": 0,
            "2": "", "4": "", "y": {
                "e": -11.74325043, "d": 28, "i": None,
                "h": "ZINDO/s/COSMO(methanol)", "m": 1,
                "o": """\
rhf zindo/s tightscf diis norico nomoprint cosmo(water)
%cis NRoots 10
Maxdim 100
end""",
                "n": {
                    "transition_dipoles_z": [
                        -4e-05, -1e-05, -0.00101, 0.74538,
                        -0.11784, 0.00026, -0.0009, -0.0, 0.0, 0.0],
                    "transition_dipoles_y": [
                        -2e-05, -3e-05, -2e-05, 0.11784,
                        0.74539, -0.0009, -0.00025, -1e-05,
                        -1e-05, 0.0],
                    "transition_dipoles_x": [
                        1e-05, 0.00024, 0.0, -2e-05, -0.00015,
                        -0.00035, -1e-05, -1.81418, 1.26879,
                        -6e-05],
                    "excitation_energies": [
                        90255.0, 90266.4, 127154.2, 132373.2,
                        132384.6, 133406.2, 133406.2, 194452.8,
                        262473.3, 290974.4]},
                "q": 0, "u": {
                    "transition_dipoles_z": "au",
                    "transition_dipoles_y": "au",
                    "transition_dipoles_x": "au",
                    "excitation_energies": "cm^-1"},
                "w": "ORCA", "v": "3.0.1", "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""}, "8": 0,
            "_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"})

    def _run_resumetask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'mol_resume_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'ResumeTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1

        # Resume interval
        input_kv['resume_interval'] = 0

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_resume(self):

        # Insert new molecule record
        self._insert_data()

        # Run ResumeTask
        self._run_resumetask()

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'O=O')

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"}))

        # Check result
        self.assertEqual(mol2.level, Level.CONFIGURATION)
        self.assertEqual(mol2.formula.smiles, 'FCF')
        self.assertEqual(mol2.configuration.priority, 52)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.smiles, 'N')
        self.assertEqual(mol3.geometry.energy, -226.7529)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result
        self.assertEqual(mol4.level, Level.PROPERTY)
        self.assertEqual(mol4.formula.smiles, 'N#N')
        self.assertEqual(mol4.geometry.energy, -371.72947)
        self.assertEqual(mol4.property.energy, -11.74325043)

    def test_resume_empty(self):

        # Run ResumeTask
        self._run_resumetask()

        # Molecule database is empty
        self.assertEqual(self._client.test.mol.count(), 0)

    def test_resume_tags(self):

        # Insert new molecule record
        self._insert_data()

        # Input options
        input_kv = {'tags': ['halogenated']}

        # Run ResumeTask
        self._run_resumetask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_SUSPENDED)
        self.assertEqual(mol1.formula.smiles, 'O=O')

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"}))

        # Check result
        self.assertEqual(mol2.level, Level.CONFIGURATION)
        self.assertEqual(mol2.formula.smiles, 'FCF')
        self.assertEqual(mol2.configuration.priority, 52)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY_SUSPENDED)
        self.assertEqual(mol3.formula.smiles, 'N')
        self.assertEqual(mol3.geometry.energy, -226.7529)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result
        self.assertEqual(mol4.level, Level.PROPERTY_SUSPENDED)
        self.assertEqual(mol4.formula.smiles, 'N#N')
        self.assertEqual(mol4.geometry.energy, -371.72947)
        self.assertEqual(mol4.property.energy, -11.74325043)


if __name__ == '__main__':
    unittest.main()
