"""Test classes for colibri graph command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri graph command

Test Classes:
    TestGraphCommand: Test classes for GraphCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/01/2015'

#
# Standard library
#

import unittest
import logging

#
# Third-party modules
#

try:
    import py2neo
    assert py2neo
except ImportError:
    raise unittest.SkipTest('Neo4J is not available')

#
# colibri modules
#

from colibri.api import graph
from colibri.enums import Status, Result
from colibri.exceptions import ExecutionError

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from tmpneo4j import TmpNeo4JClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Temporary Neo4J instance
_tmpneo4j = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_graphcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri graph command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGraphCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _tmpneo4j, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)

    # Temporary Neo4J instance
    try:
        _tmpneo4j = TmpNeo4JClient.get_instance()
        _logger.info('Starting %s' % _tmpneo4j)
    except ExecutionError:
        _logger.warning('Cannot start Neo4J')
        _tmpneo4j = None


def tearDownModule():
    global _tmpmongo, _tmpneo4j, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

    # Temporary Neo4J instance
    if _tmpneo4j is not None:
        _logger.info('Destroying %s' % _tmpneo4j)
        _tmpneo4j.destroy()

#
# Test classes
#


class TestGraphCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _tmpneo4j, _logger

        self._client = _tmpmongo.mongoclient
        self._neo4jclient = _tmpneo4j.neo4jclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.flask.remove({})
        self._neo4jclient.graph_storage().run('MATCH (n) DETACH DELETE n;')

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _tmpneo4j, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Add hostname and port for Neo4J
        input_kv['graph_hostname'] = 'localhost'
        input_kv['graph_port'] = _tmpneo4j.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_flask(self):

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                },
                {
                    "1": "[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                }
            ],
            "9": -10.625260000000026,
            "b": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                },
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                }
            ],
            "e": -932.51315,
            "d": 1638,
            "3*": [
                1,
                3
            ],
            "k": [
                {
                    "i": None,
                    "s": "[CH3+]",
                    "_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"
                },
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[CH3+].CO.[OH-]",
            "q": 0,
            "0": -921.88789,
            "3": 1,
            "2": "",
            "4": "",
            "0*": [
                -944.10842,
                -921.88789,
                -935.1186399999999
            ],
            "8": 0,
            "t": [],
            "_id": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                }
            ],
            "9": 11.501790000000028,
            "b": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                }
            ],
            "e": -922.93802,
            "d": 1876,
            "3*": [
                2,
                4
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "[CH3+]",
                    "_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"
                },
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[H+].[CH3+].C[O-].[OH-]",
            "q": 0,
            "0": -934.4398100000001,
            "3": 2,
            "2": "",
            "4": "",
            "0*": [
                -934.5332900000001,
                -934.4398100000001
            ],
            "8": 0,
            "t": [],
            "_id": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                }
            ],
            "9": -9.575129999999945,
            "b": [
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                }
            ],
            "e": -944.10842,
            "d": 2048,
            "0": -934.5332900000001,
            "k": [
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                },
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "CO.CO",
            "q": 0,
            "3*": [
                0,
                2
            ],
            "3": 0,
            "2": "",
            "4": "",
            "0*": [
                -934.5332900000001
            ],
            "8": 0,
            "t": ["neutral"],
            "_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                }
            ],
            "9": -9.575130000000172,
            "b": [
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                }
            ],
            "e": -934.5332900000001,
            "d": 2086,
            "3*": [
                1,
                3
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[H+].C[O-].CO",
            "q": 0,
            "0": -924.9581599999999,
            "3": 1,
            "2": "",
            "4": "",
            "0*": [
                -944.10842,
                -924.9581599999999,
                -934.9957899999999
            ],
            "8": 0,
            "t": [],
            "_id": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
                },
                {
                    "1": "[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
                },
                {
                    "1": "[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
                }
            ],
            "9": -9.878060000000005,
            "b": [
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                }
            ],
            "e": -932.81608,
            "d": 2410,
            "3*": [
                3,
                5
            ],
            "k": [
                {
                    "i": None,
                    "s": "[CH3+]",
                    "_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"
                },
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "O",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[CH3+].C[O-].O",
            "q": 0,
            "0": -922.93802,
            "3": 3,
            "2": "",
            "4": "",
            "0*": [
                -922.93802,
                -934.9957899999999,
                -944.3178700000001
            ],
            "8": 0,
            "t": [],
            "_id": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                }
            ],
            "9": -18.056859999999915,
            "b": [
                {
                    "1": "[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                }
            ],
            "e": -934.9957899999999,
            "d": 2450,
            "3*": [
                2,
                4
            ],
            "k": [
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "C[OH2+]",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-O-GNQMJBYIZY"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "C[O-].C[OH2+]",
            "q": 0,
            "0": -916.93893,
            "3": 2,
            "2": "",
            "4": "",
            "0*": [
                -934.5332900000001,
                -916.93893
            ],
            "8": 0,
            "t": [],
            "_id": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                }
            ],
            "9": -3.2946699999999964,
            "b": [
                {
                    "1": "[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                }
            ],
            "e": -935.1186399999999,
            "d": 2898,
            "3*": [
                2,
                4
            ],
            "k": [
                {
                    "i": None,
                    "s": "C[OH+]C",
                    "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-O-OWRHUZFXEA"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "C[OH+]C.[OH-]",
            "q": 0,
            "0": -931.8239699999999,
            "3": 2,
            "2": "",
            "4": "",
            "0*": [
                -932.51315,
                -931.8239699999999
            ],
            "8": 0,
            "t": [],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                },
                {
                    "1": "[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                }
            ],
            "9": -11.501790000000028,
            "b": [
                {
                    "1": "[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                },
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                }
            ],
            "e": -934.4398100000001,
            "d": 3306,
            "3*": [
                3,
                5
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "COC",
                    "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[H+].COC.[OH-]",
            "q": 0,
            "0": -922.93802,
            "3": 3,
            "2": "",
            "4": "",
            "0*": [
                -922.93802,
                -935.1186399999999,
                -944.3178700000001,
                -924.08536
            ],
            "8": 0,
            "t": [],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                },
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                }
            ],
            "9": -10.354450000000043,
            "b": [
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                }
            ],
            "e": -944.3178700000001,
            "d": 4040,
            "3*": [
                4,
                6
            ],
            "k": [
                {
                    "i": None,
                    "s": "COC",
                    "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"
                },
                {
                    "i": None,
                    "s": "O",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "COC.O",
            "q": 0,
            "0": -933.96342,
            "3": 4,
            "2": "",
            "4": "",
            "0*": [
                -934.4398100000001,
                -933.96342
            ],
            "8": 0,
            "t": ["neutral"],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "MAEMOWMWKQVMOJ-UHFFFAOYSA-O-CCZZODOWAB",
                    "r": "QOGDLAHJUJGQFA-UHFFFAOYSA-N-GVMIBFZGDC"
                },
                {
                    "1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "QOGDLAHJUJGQFA-UHFFFAOYSA-N-GVMIBFZGDC"
                }
            ],
            "9": -3.0871400000000904,
            "b": [],
            "e": -940.0857400000001,
            "d": 3306,
            "3*": [
                3,
                5
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "[CH2-][OH+]C",
                    "_id": "LDPVXGJORQTADQ-UHFFFAOYSA-N-BFXHELKEJY"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[H+].[CH2-][OH+]C.[OH-]",
            "q": 0,
            "0": -936.9986,
            "3": 3,
            "2": "",
            "4": "",
            "0*": [
                -936.9986,
                -942.1985999999999,
                -939.2193900000001
            ],
            "8": 0,
            "_id": "QOGDLAHJUJGQFA-UHFFFAOYSA-N-GVMIBFZGDC",
            "+": 0,
            "t": []
        })

        self._client.test.flask.insert({
            "a": [],
            "9": None,
            "b": [
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "MAEMOWMWKQVMOJ-UHFFFAOYSA-O-CCZZODOWAB",
                    "r": "QOGDLAHJUJGQFA-UHFFFAOYSA-N-GVMIBFZGDC"
                }
            ],
            "e": None,
            "d": 3916,
            "3*": [
                4
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "[CH2-][OH+][CH2-]",
                    "_id": "QSCPRLUUMDZSBC-UHFFFAOYSA-N-TUKPVEXHYN"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2213,
            "m": 1,
            "s": "[H+].[H+].[CH2-][OH+][CH2-].[OH-]",
            "q": 0,
            "0": -940.0857400000001,
            "3": 4,
            "2": "BadChemistry",
            "4": "",
            "0*": [
                -940.0857400000001
            ],
            "8": 1,
            "_id": "MAEMOWMWKQVMOJ-UHFFFAOYSA-O-CCZZODOWAB",
            "+": 0,
            "t": []
        })

    def test_graph(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        node1 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node1.has_label('Flask'))
        self.assertEqual(node1['status'], Status.UNREACTIVE)
        self.assertEqual(node1['smiles'], '[CH3+].CO.[OH-]')
        self.assertEqual(node1['generation'], 1)
        self.assertAlmostEqual(node1['energy'], -932.51315, places=4)

        # Get result from database
        node2 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node2.has_label('Flask'))
        self.assertEqual(node2['status'], Status.UNREACTIVE)
        self.assertEqual(node2['smiles'], '[H+].[CH3+].C[O-].[OH-]')
        self.assertEqual(node2['generation'], 2)
        self.assertAlmostEqual(node2['energy'], -922.93802, places=4)

        # Get result from database
        node3 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node3.has_label('Flask'))
        self.assertEqual(node3['status'], Status.UNREACTIVE)
        self.assertEqual(node3['smiles'], 'CO.CO')
        self.assertEqual(node3['generation'], 0)
        self.assertAlmostEqual(node3['energy'], -944.10842, places=4)

        # Get result from database
        node4 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node4.has_label('Flask'))
        self.assertEqual(node4['status'], Status.UNREACTIVE)
        self.assertEqual(node4['smiles'], '[H+].C[O-].CO')
        self.assertEqual(node4['generation'], 1)
        self.assertAlmostEqual(node4['energy'], -934.53329, places=4)

        # Get result from database
        node5 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node5.has_label('Flask'))
        self.assertEqual(node5['status'], Status.UNREACTIVE)
        self.assertEqual(node5['smiles'], 'C[O-].C[OH2+]')
        self.assertEqual(node5['generation'], 2)
        self.assertAlmostEqual(node5['energy'], -934.99579, places=4)

        # Get result from database
        node6 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node6.has_label('Flask'))
        self.assertEqual(node6['status'], Status.UNREACTIVE)
        self.assertEqual(node6['smiles'], 'C[OH+]C.[OH-]')
        self.assertEqual(node6['generation'], 2)
        self.assertAlmostEqual(node6['energy'], -935.11864, places=4)

        # Get result from database
        node7 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node7.has_label('Flask'))
        self.assertEqual(node7['status'], Status.UNREACTIVE)
        self.assertEqual(node7['smiles'], '[H+].COC.[OH-]')
        self.assertEqual(node7['generation'], 3)
        self.assertAlmostEqual(node7['energy'], -934.43981, places=4)

        # Get result from database
        node8 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node8.has_label('Flask'))
        self.assertEqual(node8['status'], Status.UNREACTIVE)
        self.assertEqual(node8['smiles'], 'C[OH+]C.[OH-]')
        self.assertEqual(node8['generation'], 2)
        self.assertAlmostEqual(node8['energy'], -935.11864, places=4)

        # Get result from database
        node9 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"QOGDLAHJUJGQFA-UHFFFAOYSA-N-GVMIBFZGDC"}) RETURN f;').evaluate()

        # Check result
        self.assertTrue(node9.has_label('Flask'))
        self.assertEqual(node9['status'], Status.UNREACTIVE)
        self.assertEqual(node9['smiles'], '[H+].[CH2-][OH+]C.[OH-]')
        self.assertEqual(node9['generation'], 3)
        self.assertAlmostEqual(node9['energy'], -940.08574, places=4)

        # Get result from database
        node10 = self._neo4jclient.graph.run(
            'MATCH (f:Flask {key: ' +
            '"MAEMOWMWKQVMOJ-UHFFFAOYSA-O-CCZZODOWAB"}) RETURN f;').evaluate()

        # Node was removed because of status
        self.assertIsNone(node10)

    def test_graph_tags(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts(tags=['neutral'])

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 2)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 0)

    def test_graph_min_max_generation(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts(min_generation=2, max_generation=3)

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 6)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 18)

    def test_graph_max_energy_change(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts(max_energy_change=-5.)

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 7)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 22)

    def test_graph_max_rel_energy(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts(max_rel_energy=9.6, ref_energy=-944.0)

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 7)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 18)

    def test_graph_max_rel_energy_ref(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts(
            max_rel_energy=9.6,
            ref_flask='COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF')

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 6)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 10)

    def test_graph_max_rel_energy_noref(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts(max_rel_energy=6.0)

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 3)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 0)

    def test_graph_labels(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri graph command
        result = graph(labels='LL', opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask:LL) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 10)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 46)

    def test_graph_keys(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri graph command
        result = graph(keys='[A-F]', opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get number of flasks and transformations
        flasks = self._neo4jclient.graph.run(
            'MATCH (f:Flask) RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(flasks, 4)
        transformations = self._neo4jclient.graph.run(
            'MATCH ()-[t:Transformation]-() ' +
            'RETURN COUNT (*) AS count;').evaluate()
        self.assertEqual(transformations, 8)

    def test_graph_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri graph command
        result = graph(opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

if __name__ == '__main__':
    unittest.main()
