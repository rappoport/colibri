"""Test classes for colibri add command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri add command

Test Classes:
    TestAddCommand: Test classes for AddCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/25/2015'

#
# Standard library
#

import unittest
import logging
import json

#
# colibri modules
#

from colibri.api import add
from colibri.data import Molecule, Flask
from colibri.enums import Level, Status, Result
from colibri.exceptions import CommandError
from colibri.utils import default

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_addcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri add command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAddCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestAddCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Batch size
        input_kv['batch_size'] = 3

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_mol_h2o_geometry(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {
                "d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""}, "z": [], "d": 18,
            "g": {
                "e": -322.6792, "d": 18, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC",
                "7": True, "v": "2012", "x": """\
3

O    -0.000001     0.058320     0.000000
H    -0.759932    -0.519445     0.000000
H     0.760055    -0.519304     0.000000"""},
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0,
            "t": ["liquid", "hydrophilic"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_mol_o2_formula(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "z": [], "d": 30, "f": {"i": None, "s": "O=O", "d": 30},
            "l": 1000, "3": 0, "2": "", "5": "", "4": "", "8": 0,
            "t": ["gas"],
            "_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"})

    def _insert_data_flask_LL0_reactive(self):

        # Insert new flask record
        self._client.test.flask.insert({
            "a": [], "9": None, "b": [], "e": -944.31787,
            "d": 2440, "0": None, "k": [
                {"i": None, "s": "COC",
                 "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"},
                {"i": None, "s": "O",
                 "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2200, "m": 1, "s": "COC.O", "q": 0, "3*": [0],
            "3": 0, "2": "", "4": "", "0*": [], "8": 0,
            "t": ["LL"],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"})

    def _insert_data_flask_LL0_reactive_priority(self):

        # Insert new flask record
        self._client.test.flask.insert({
            "a": [], "9": None, "b": [], "e": -944.31787,
            "d": 1001, "0": None, "k": [
                {"i": None, "s": "COC",
                 "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"},
                {"i": None, "s": "O",
                 "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2200, "m": 1, "s": "COC.O", "q": 0, "3*": [0],
            "3": 0, "2": "", "4": "", "0*": [], "8": 0,
            "t": ["LL"],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"})

    def test_add_mol_smiles(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(molecules=['O', '[NH]', '[CH+]=C', 'O=N(=O)O'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "PDCKRJPYJMCOFO-UHFFFAOYSA-N-JCYDNYPDRE"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 3)
        self.assertEqual(mol2.formula.smiles, '[NH]')
        self.assertEqual(mol2.priority, 15)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QLLSNXSJBAXPLZ-UHFFFAOYSA-N-VHVUJEYRQC"}))

        # Check result
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.charge, 1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[CH+]=C')
        self.assertEqual(mol3.priority, 27)

        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GRYLNZFGIOXLOG-UHFFFAOYSA-N-HDUVWHLPEU"}))

        # Check result
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, 'O=[N+]([O-])O')
        self.assertEqual(mol4.priority, 63)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_add_mol_smiles_no_overwrite(self):

        # Insert molecule record
        self._insert_data_mol_h2o_geometry()

        # Get configuration options
        opts = self._update_conf_opts(tags=['water'])

        # Run colibri add command
        result = add(molecules=['O'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result, should get old result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertAlmostEqual(mol.geometry.energy, -322.6792, places=4)
        self.assertListEqual(mol.tags, ['liquid', 'hydrophilic', 'water'])

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

    def test_add_mol_smiles_update(self):

        # Insert molecule record
        self._insert_data_mol_o2_formula()

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"}))

        # Check result, should have wrong multiplicity (singlet)
        # and priority (30)
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O=O')
        self.assertEqual(mol1.priority, 30)
        self.assertListEqual(mol1.tags, ['gas'])

        # Get configuration options
        opts = self._update_conf_opts(tags=['oxygen'], update_on_insert=True)

        # Run colibri add command
        result = add(molecules=[json.dumps(Molecule(
                smiles='O=O', charge=0, mult=3).todict(),
                default=default)], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"}))

        # Check result, should have been updated with input data
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 3)
        self.assertEqual(mol2.formula.smiles, 'O=O')
        self.assertEqual(mol2.priority, 32)
        self.assertListEqual(mol2.tags, ['gas', 'oxygen'])

    def test_add_mol_smiles_overwrite(self):

        # Insert molecule record
        self._insert_data_mol_h2o_geometry()

        # Get configuration options
        opts = self._update_conf_opts(
            tags=['water'], overwrite_on_insert=True)

        # Run colibri add command
        result = add(molecules=['O'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result, should have been replaced with input data
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.formula.charge, 0)
        self.assertEqual(mol.formula.mult, 1)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)
        self.assertListEqual(mol.tags, ['water'])
        with self.assertRaises(AttributeError):
            mol.configuration
        with self.assertRaises(AttributeError):
            mol.geometry

    def test_add_mol_smiles_error(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(molecules=['C(C)(C)(C)(C)C'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Nothing got inserted
        self.assertEqual(self._client.test.mol.find().count(), 0)

    def test_add_mol_json(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(molecules=[
            '{"z": [], "d": 32, "f": {"i": null, "s": "CO",' +
            '"d": 32}, "l": 1000, "3": 0, "2": "", "4": "", ' +
            '"8": 0, ' +
            '"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}',
            '{"c": {"d": 28, "i": null, "m": 1, "q": 0, ' +
            '"w": "RDKit", "v": "2015.3.1", ' +
            '"x": "2\\n\\nN 0.56000 0.00000 0.00000\\n' +
            'N -0.56000 0.00000 0.00000\\n"}, ' +
            '"z": [], "d": 28, "g": {"e": -371.72947, "d": 28, ' +
            '"i": null, "h": "PM7", "m": 1, "o": "xyz", "q": 0, ' +
            '"w": "MOPAC", "7": true, "v": "2012", ' +
            '"x": "2\\n\\nN 0.559717 -0.000000 -0.000000\\n' +
            'N -0.559717 -0.000000 -0.000000\\n"}, ' +
            '"f": {"i": null, "s": "N#N", "d": 28}, ' +
            '"l": 1200, "3": 0, "2": "", "4": "", ' +
            '"8": 0, ' +
            '"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.priority, 32)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, 'N#N')
        self.assertEqual(mol2.priority, 28)
        self.assertAlmostEqual(mol2.energy, -371.72947, places=4)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 2)

    def test_add_mol_json_error(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(molecules=['{"f": {"s": "CO"}, "_id": "OKKJ"'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_add_mol_json_mol_error(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(molecules=['{"f": {"s": "N1=N=N1"}}'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_add_flask_smiles(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(flasks=[
            'CO.CO', 'COC.O', '[CH3+].[OH-].[CH3+].[OH-]'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'CO.CO')
        self.assertEqual(flask1.priority, 2048)

        # Get result from database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"}))

        # Check result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'COC.O')
        self.assertEqual(flask2.priority, 2440)

        # Get result from database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "XHHWBYCTYJSFOD-UHFFFAOYSA-L-RPFBHIANUN"}))

        # Check result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[CH3+].[CH3+].[OH-].[OH-]')

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 3)

    def test_add_flask_smiles_no_overwrite(self):

        # Insert flask record
        self._insert_data_flask_LL0_reactive()

        # Get configuration options
        opts = self._update_conf_opts(tags=['ether'])

        # Run colibri add command
        result = add(flasks=['COC.O'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"}))

        # Check result
        # Currently, the tags are combined per union even if the rest
        # is not updated. Can improve without compromising atomicity?
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, 'COC.O')
        self.assertEqual(flask.priority, 2440)
        self.assertAlmostEqual(flask.energy, -944.31787, places=4)
        self.assertListEqual(flask.tags, ['LL', 'ether'])

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_add_flask_smiles_update(self):

        # Insert flask record
        self._insert_data_flask_LL0_reactive_priority()

        # Get result from database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"}))

        # Check result, should have wrong priority (1001)
        self.assertEqual(flask1.status, Status.REACTIVE)
        self.assertEqual(flask1.smiles, 'COC.O')
        self.assertEqual(flask1.priority, 1001)
        self.assertAlmostEqual(flask1.energy, -944.31787, places=4)
        self.assertListEqual(flask1.tags, ['LL'])

        # Get configuration options
        opts = self._update_conf_opts(tags=['ether'], update_on_insert=True)

        # Run colibri add command
        result = add(flasks=['COC.O'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"}))

        # Check result, should have right priority, union of tags
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'COC.O')
        self.assertEqual(flask2.priority, 2440)
        self.assertAlmostEqual(flask2.energy, -944.31787, places=4)
        self.assertListEqual(flask2.tags, ['LL', 'ether'])

    def test_add_flask_smiles_overwrite(self):

        # Insert flask record
        self._insert_data_flask_LL0_reactive_priority()

        # Get result from database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"}))

        # Check result, should have wrong priority (1001)
        self.assertEqual(flask1.status, Status.REACTIVE)
        self.assertEqual(flask1.smiles, 'COC.O')
        self.assertEqual(flask1.priority, 1001)
        self.assertAlmostEqual(flask1.energy, -944.31787, places=4)
        self.assertListEqual(flask1.tags, ['LL'])

        # Get configuration options
        opts = self._update_conf_opts(tags=['ether'], overwrite_on_insert=True)

        # Run colibri add command
        result = add(flasks=['COC.O'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"}))

        # Check result, should not have energy set, tags are replaced
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'COC.O')
        self.assertEqual(flask2.priority, 2440)
        self.assertIsNone(flask2.energy)
        self.assertListEqual(flask2.tags, ['ether'])

    def test_add_flask_smiles_error(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(flasks=['C(C)(C)(C)(C)C.C'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Nothing got inserted
        self.assertEqual(self._client.test.flask.find().count(), 0)

    def test_add_flask_json(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(flasks=[json.dumps(
            {"a": [], "b": [], "d": 613, "k": [
                {"i": None, "s": "N",
                 "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"},
                {"i": None, "s": "O",
                 "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
                "j": 2000, "m": 1, "s": "N.O", "q": 0, "3*": [0],
                "3": 0, "2": "", "5": "", "4": "", "0*": [],
                "8": 0, "_id": "VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ"}),
            json.dumps(
                {"a": [], "9": None, "b": [], "e": -656.5736,
                 "d": 656, "0": None, "k": [
                    {"i": None, "s": "F",
                     "_id": "KRHYYFGTRYWZRS-UHFFFAOYSA-N-MMIJCGAIQJ"},
                    {"i": None, "s": "C",
                     "_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}],
                 "j": 2300, "m": 1, "s": "F.C", "q": 0, "3*": [0],
                 "3": 0, "2": "", "4": "", "0*": [], "8": 0,
                 "_id": "UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS"})
            ], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ"}))

        # Check result,
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'N.O')
        self.assertEqual(flask1.priority, 613)

        # Get result from database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS"}))

        # Check result,
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, 'F.C')
        self.assertEqual(flask2.priority, 656)
        self.assertAlmostEqual(flask2.energy, -656.5736, places=4)

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_add_flask_json_error(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(flasks=['{"_id": "A" "j: "2200" }'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Nothing got inserted
        self.assertEqual(self._client.test.flask.find().count(), 0)

    def test_add_flask_json_flask_error(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(flasks=['{"f": ["[C]=O", "C"]}'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Nothing got inserted
        self.assertEqual(self._client.test.flask.find().count(), 0)

    def test_add_mol_flask(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = add(
            molecules=['O', '{"z": [], "d": 32, "f": {"i": null, "s": "CO",' +
                       '"d": 32}, "l": 1000, "3": 0, "2": "", "4": "", ' +
                       '"8": 0, ' +
                       '"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}'],
            flasks=['CO.CO', json.dumps(
                {"a": [], "b": [], "d": 613, "k": [
                    {"i": None, "s": "N",
                     "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"},
                    {"i": None, "s": "O",
                     "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
                    "j": 2000, "m": 1, "s": "N.O", "q": 0, "3*": [0],
                    "3": 0, "2": "", "5": "", "4": "", "0*": [],
                    "8": 0, "_id": "VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ"})],
            opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, 'CO')
        self.assertEqual(mol2.priority, 32)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 2)

        # Get result from database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'CO.CO')
        self.assertEqual(flask1.priority, 2048)

        # Get result from database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ"}))

        # Check result,
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'N.O')
        self.assertEqual(flask2.priority, 613)

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_add_noinput(self):

        # Run colibri add command
        with self.assertRaisesRegexp(CommandError, 'No input data'):
            add()

        # No molecules or flasks inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_add_nodata(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        with self.assertRaisesRegexp(CommandError, 'No input data'):
            add(opts=opts)

        # No molecules or flasks inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_add_nooptions(self):

        # Run colibri add command
        with self.assertRaisesRegexp(CommandError, 'No options'):
            add(molecules=['O', 'N'])

        # No molecules or flasks inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)
        self.assertEqual(self._client.test.flask.find({}).count(), 0)


if __name__ == '__main__':
    unittest.main()
