"""Test classes for colib network script.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib network script

Test Classes:
    TestNetworkScript: Test classes for colib network script
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/11/2015'

#
# Standard library
#

import os
import sys
import unittest
import logging
import subprocess


#
# colibri submodules
#

from colibri.data import Molecule, Flask
from colibri.enums import Level, Status
from colibri.utils import ensure_logger

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None


# Logging level
_logging_level = 'ERROR'

# Get logger
_logger = ensure_logger('networkscript', _logging_level)
_logger.addHandler(logging.NullHandler())

#
# Public functions
#


def get_suite():
    """Construct test suite for colib network script."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestNetworkScript)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestNetworkScript(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_script_args(self, args=[]):
        global _tmpmongo, _logging_level

        # List of script arguments
        script_args = []

        # Add hostname and port options
        script_args.extend(['--db-hostname', 'localhost'])
        script_args.extend(['--db-port', str(_tmpmongo.port)])

        # Logging level
        script_args.extend(['--logging-level', _logging_level])

        # Ignore all configuration files
        script_args.extend(['--no-config'])

        # Shutdown arguments
        script_args.extend(['--with-shutdown', '--idle-tasks-per-calc', '5'])

        # Additional script arguments
        script_args.extend(args)

        return script_args

    def _insert_data_flask_NN0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O').todict())

    def _insert_data_flask_LL0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='CO.CO').todict())

    def _run_networkscript(self, script_args=[], cmd_args=[]):
        global _logging_level

        if _logging_level == 'DEBUG':
            stdout = sys.stdout
            stderr = sys.stderr
        else:
            stdout = open(os.devnull, 'w')
            stderr = open(os.devnull, 'w')

        return subprocess.call(
            ['colib'] + script_args + ['network'] + cmd_args,
            stdout=stdout, stderr=stderr)

    def test_network(self):

        # Insert initial flask
        self._insert_data_flask_NN0()

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args, cmd_args=[
            '-y', '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]', '-V', '2', '-W', '50'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'C=O')
        self.assertEqual(mol1.priority, 30)
        self.assertAlmostEqual(mol1.geometry.energy, -444.38128, places=4)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[CH2+][O-]')
        self.assertEqual(mol2.priority, 30)
        self.assertAlmostEqual(mol2.geometry.energy, -444.38128, places=4)
        self.assertEqual(mol2.generation, 1)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DARIEFVNEA"}))

        # Check result
        self.assertEqual(mol3.level, Level.FORMULA_INVALID)
        self.assertEqual(mol3.formula.smiles, '[CH2][O]')
        self.assertEqual(mol3.generation, 2)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, '[CH2+]OC[O-]')
        self.assertEqual(mol4.priority, 60)
        self.assertAlmostEqual(mol4.geometry.energy, -888.93619, places=2)
        self.assertEqual(mol4.generation, 3)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertAlmostEqual(flask1.energy, -888.76256, places=4)
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, '[CH2+][O-].C=O')
        self.assertAlmostEqual(flask2.energy, -888.76256, places=4)
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-WZTVMDNTKY"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.REACTIVE_BADCHEM)
        self.assertEqual(flask3.smiles, '[CH2][O].C=O')
        self.assertIsNone(flask3.energy)
        self.assertEqual(flask3.generation, 2)
        self.assertListEqual(flask3.generation_journal, [2])
        self.assertListEqual(flask3.rules_applied, [])

        # Get flask result from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.UNREACTIVE)
        self.assertEqual(flask4.smiles, '[CH2+][O-].[CH2+][O-]')
        self.assertAlmostEqual(flask4.energy, -888.76256, places=4)
        self.assertEqual(flask4.generation, 2)
        self.assertListEqual(flask4.generation_journal, [2])
        self.assertListEqual(flask4.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask5 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-OAXWZWGXUO"}))

        # Check flask result
        self.assertEqual(flask5.status, Status.REACTIVE_BADCHEM)
        self.assertEqual(flask5.smiles, '[CH2][O].[CH2+][O-]')
        self.assertIsNone(flask5.energy)
        self.assertEqual(flask5.generation, 3)
        self.assertListEqual(flask5.generation_journal, [3])
        self.assertListEqual(flask5.rules_applied, [])

        # Get flask result from the database
        flask6 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}))

        # Check flask result
        self.assertEqual(flask6.status, Status.INVALID)
        self.assertEqual(flask6.smiles, '[CH2+]OC[O-]')
        self.assertIsNone(flask6.energy)
        self.assertEqual(flask6.generation, 3)
        self.assertListEqual(flask6.generation_journal, [3])
        self.assertListEqual(flask6.rules_applied, [])

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 6)

    def test_network_max_generation(self):

        # Insert initial flask
        self._insert_data_flask_LL0()

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args, cmd_args=[
            '-y', '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]', '-G', '2',
            '-V', '2', '-W', '50'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.priority, 32)
        self.assertAlmostEqual(mol1.geometry.energy, -472.05421, places=4)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[CH3+]')
        self.assertEqual(mol2.priority, 15)
        self.assertAlmostEqual(mol2.geometry.energy, -152.14755, places=4)
        self.assertEqual(mol2.generation, 1)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, -1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[OH-]')
        self.assertEqual(mol3.priority, 17)
        self.assertAlmostEqual(mol3.geometry.energy, -308.31139, places=4)
        self.assertEqual(mol3.generation, 1)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'CO.CO')
        self.assertAlmostEqual(flask1.energy, -944.10842, places=4)
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0, 2])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[O+0:2]>>([C+1:1].[O-1:2])',
            '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, '[CH3+].CO.[OH-]')
        self.assertAlmostEqual(flask2.energy, -932.51315, places=4)
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[O+0:2]>>([C+1:1].[O-1:2])',
            '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "XHHWBYCTYJSFOD-UHFFFAOYSA-L-RPFBHIANUN"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.REACTIVE_MAXGEN)
        self.assertEqual(flask3.smiles, '[CH3+].[CH3+].[OH-].[OH-]')
        self.assertIsNone(flask3.energy)
        self.assertEqual(flask3.generation, 2)
        self.assertListEqual(flask3.generation_journal, [2])
        self.assertListEqual(flask3.rules_applied, [])

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 3)

    def test_network_max_rel_energy(self):

        # Insert initial flask
        self._insert_data_flask_LL0()

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args, cmd_args=[
            '-y', '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]', '-0', '10.0',
            '-1', ' -950.0', '-V', '2', '-W', '50'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.priority, 32)
        self.assertAlmostEqual(mol1.geometry.energy, -472.05421, places=4)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[CH3+]')
        self.assertEqual(mol2.priority, 15)
        self.assertAlmostEqual(mol2.geometry.energy, -152.14755, places=4)
        self.assertEqual(mol2.generation, 1)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, -1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[OH-]')
        self.assertEqual(mol3.priority, 17)
        self.assertAlmostEqual(mol3.geometry.energy, -308.31139, places=4)
        self.assertEqual(mol3.generation, 1)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'CO.CO')
        self.assertAlmostEqual(flask1.energy, -944.10842, places=4)
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[O+0:2]>>([C+1:1].[O-1:2])',
            '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.REACTIVE_HIENERGY)
        self.assertEqual(flask2.smiles, '[CH3+].CO.[OH-]')
        self.assertAlmostEqual(flask2.energy, -932.51315, places=4)
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_network_max_energy_change(self):

        # Insert initial flask
        self._insert_data_flask_LL0()

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args, cmd_args=[
            '-y', '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]', '-9', '0.1',
            '-V', '2', '-W', '50'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.priority, 32)
        self.assertAlmostEqual(mol1.geometry.energy, -472.05421, places=4)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[CH3+]')
        self.assertEqual(mol2.priority, 15)
        self.assertAlmostEqual(mol2.geometry.energy, -152.14755, places=4)
        self.assertEqual(mol2.generation, 1)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, -1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[OH-]')
        self.assertEqual(mol3.priority, 17)
        self.assertAlmostEqual(mol3.geometry.energy, -308.31139, places=4)
        self.assertEqual(mol3.generation, 1)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'CO.CO')
        self.assertAlmostEqual(flask1.energy, -944.10842, places=4)
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[O+0:2]>>([C+1:1].[O-1:2])',
            '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.REACTIVE_HIBARRIER)
        self.assertEqual(flask2.smiles, '[CH3+].CO.[OH-]')
        self.assertAlmostEqual(flask2.energy, -932.51315, places=4)
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_network_novalidation(self):

        # Insert initial flask
        self._insert_data_flask_NN0()

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args, cmd_args=[
            '-y', '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]', '-V', '2', '-W', '50',
            '--no-geometry-validation'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'C=O')
        self.assertEqual(mol1.priority, 30)
        self.assertAlmostEqual(mol1.geometry.energy, -444.38128, places=4)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[CH2+][O-]')
        self.assertEqual(mol2.priority, 30)
        self.assertAlmostEqual(mol2.geometry.energy, -444.38128, places=4)
        self.assertEqual(mol2.generation, 1)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DARIEFVNEA"}))

        # Check result
        self.assertEqual(mol3.level, Level.FORMULA_INVALID)
        self.assertEqual(mol3.formula.smiles, '[CH2][O]')
        self.assertEqual(mol3.generation, 2)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY)
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, '[CH2+]OC[O-]')
        self.assertEqual(mol4.priority, 60)
        self.assertAlmostEqual(mol4.geometry.energy, -888.93619, places=2)
        self.assertEqual(mol4.generation, 3)

        # Get result from database
        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check result
        self.assertEqual(mol5.formula.charge, 0)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.formula.smiles, 'C1OCO1')
        self.assertEqual(mol5.priority, 60)
        self.assertEqual(mol5.generation, 4)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 5)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertAlmostEqual(flask1.energy, -888.76256, places=4)
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, '[CH2+][O-].C=O')
        self.assertAlmostEqual(flask2.energy, -888.76256, places=4)
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-WZTVMDNTKY"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.REACTIVE_BADCHEM)
        self.assertEqual(flask3.smiles, '[CH2][O].C=O')
        self.assertIsNone(flask3.energy)
        self.assertEqual(flask3.generation, 2)
        self.assertListEqual(flask3.generation_journal, [2])
        self.assertListEqual(flask3.rules_applied, [])

        # Get flask result from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.UNREACTIVE)
        self.assertEqual(flask4.smiles, '[CH2+][O-].[CH2+][O-]')
        self.assertAlmostEqual(flask4.energy, -888.76256, places=4)
        self.assertEqual(flask4.generation, 2)
        self.assertListEqual(flask4.generation_journal, [2])
        self.assertListEqual(flask4.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask5 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-OAXWZWGXUO"}))

        # Check flask result
        self.assertEqual(flask5.status, Status.REACTIVE_BADCHEM)
        self.assertEqual(flask5.smiles, '[CH2][O].[CH2+][O-]')
        self.assertIsNone(flask5.energy)
        self.assertEqual(flask5.generation, 3)
        self.assertListEqual(flask5.generation_journal, [3])
        self.assertListEqual(flask5.rules_applied, [])

        # Get flask result from the database
        flask6 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}))

        # Check flask result
        self.assertEqual(flask6.status, Status.UNREACTIVE)
        self.assertEqual(flask6.smiles, '[CH2+]OC[O-]')
        self.assertAlmostEqual(flask6.energy, -888.93619, places=2)
        self.assertEqual(flask6.generation, 3)
        self.assertListEqual(flask6.generation_journal, [3])
        self.assertListEqual(flask6.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Get flask result from the database
        flask7 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask7.status, Status.UNREACTIVE)
        self.assertEqual(flask7.smiles, 'C1OCO1')
        self.assertAlmostEqual(flask7.energy, -888.74776, places=4)
        self.assertEqual(flask7.generation, 4)
        self.assertListEqual(flask7.generation_journal, [4])
        self.assertListEqual(flask7.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 7)

    def test_network_nobuild(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args, cmd_args=[
            '-b', '0', '-r', '0'])

        # Check script return value
        self.assertEqual(ret_value, 1)

    def test_network_nogeometry(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args, cmd_args=[
            '-m', '0', '-w', '0'])

        # Check script return value
        self.assertEqual(ret_value, 1)

    def test_network_norules(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib network script
        ret_value = self._run_networkscript(script_args)

        # Check script return value
        self.assertEqual(ret_value, 1)


if __name__ == '__main__':
    unittest.main()
