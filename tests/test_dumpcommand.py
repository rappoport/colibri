"""Test classes for colibri dump command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri dump command

Test Classes:
    TestDumpCommand: Test classes for DumpCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/01/2015'

#
# Standard library
#

import os
import unittest
import logging
import tempfile

#
# Third-party modules
#

try:
    import igraph
except ImportError:
    igraph = None


#
# colibri modules
#

from colibri.api import dump
from colibri.enums import Result

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_dumpcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri dump command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDumpCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestDumpCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger

        # Need igraph
        if igraph is None:
            self.skipTest('igraph is not available')

        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_flask(self):

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "XHHWBYCTYJSFOD-UHFFFAOYSA-L-RPFBHIANUN",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                },
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "GZLQLFNBRWEPAS-UHFFFAOYSA-N-OLBSKCRUBD",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                },
                {
                    "1": "[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                },
                {
                    "1": "[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]",
                    "p": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                }
            ],
            "9": -10.625260000000026,
            "b": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                },
                {
                    "1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "GZLQLFNBRWEPAS-UHFFFAOYSA-N-OLBSKCRUBD"
                },
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                }
            ],
            "e": -932.51315,
            "d": 1638,
            "3*": [
                1,
                3
            ],
            "k": [
                {
                    "i": None,
                    "s": "[CH3+]",
                    "_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"
                },
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[CH3+].CO.[OH-]",
            "q": 0,
            "0": -921.88789,
            "3": 1,
            "2": "",
            "4": "",
            "0*": [
                -944.10842,
                -921.88789,
                -935.1186399999999
            ],
            "8": 0,
            "t": [],
            "_id": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "FVCOABIROKUAIA-UHFFFAOYSA-O-EDPLISEFTP",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                }
            ],
            "9": 11.501790000000028,
            "b": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                }
            ],
            "e": -922.93802,
            "d": 1876,
            "3*": [
                2,
                4
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "[CH3+]",
                    "_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"
                },
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[H+].[CH3+].C[O-].[OH-]",
            "q": 0,
            "0": -934.4398100000001,
            "3": 2,
            "2": "",
            "4": "",
            "0*": [
                -934.5332900000001,
                -934.4398100000001
            ],
            "8": 0,
            "t": [],
            "_id": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                },
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "DBCQSZHWQGOSTM-UHFFFAOYSA-O-FXYSLWNJLA",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                }
            ],
            "9": -9.575129999999945,
            "b": [
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                }
            ],
            "e": -944.10842,
            "d": 2048,
            "0": -934.5332900000001,
            "k": [
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                },
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "CO.CO",
            "q": 0,
            "3*": [
                0,
                2
            ],
            "3": 0,
            "2": "",
            "4": "",
            "0*": [
                -934.5332900000001
            ],
            "8": 0,
            "t": ["neutral"],
            "_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "ZMHMSVKEOFUCOW-UHFFFAOYSA-P-SJDUPDMULF",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "ACZIGRJFYWFQAB-UHFFFAOYSA-P-YYUGBEBCIH",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "APIYVCFWDYYOER-UHFFFAOYSA-P-OWYQHEGOFS",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                }
            ],
            "9": -9.575130000000172,
            "b": [
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "ACZIGRJFYWFQAB-UHFFFAOYSA-P-YYUGBEBCIH"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                }
            ],
            "e": -934.5332900000001,
            "d": 2086,
            "3*": [
                1,
                3
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "CO",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[H+].C[O-].CO",
            "q": 0,
            "0": -924.9581599999999,
            "3": 1,
            "2": "",
            "4": "",
            "0*": [
                -944.10842,
                -924.9581599999999,
                -934.9957899999999
            ],
            "8": 0,
            "t": [],
            "_id": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "FVCOABIROKUAIA-UHFFFAOYSA-O-DAEMTHZCYS",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
                },
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
                },
                {
                    "1": "[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
                },
                {
                    "1": "[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
                }
            ],
            "9": -9.878060000000005,
            "b": [
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                },
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                }
            ],
            "e": -932.81608,
            "d": 2410,
            "3*": [
                3,
                5
            ],
            "k": [
                {
                    "i": None,
                    "s": "[CH3+]",
                    "_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"
                },
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "O",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[CH3+].C[O-].O",
            "q": 0,
            "0": -922.93802,
            "3": 3,
            "2": "",
            "4": "",
            "0*": [
                -922.93802,
                -934.9957899999999,
                -944.3178700000001
            ],
            "8": 0,
            "t": [],
            "_id": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "ZMHMSVKEOFUCOW-UHFFFAOYSA-P-RSMDERKVSS",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                },
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                },
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "WRVHGDCWLAKRRY-UHFFFAOYSA-O-TPLLPGRZXO",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
                }
            ],
            "9": -18.056859999999915,
            "b": [
                {
                    "1": "[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL",
                    "r": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"
                },
                {
                    "1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                    "p": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL",
                    "r": "ZMHMSVKEOFUCOW-UHFFFAOYSA-P-RSMDERKVSS"
                }
            ],
            "e": -934.9957899999999,
            "d": 2450,
            "3*": [
                2,
                4
            ],
            "k": [
                {
                    "i": None,
                    "s": "C[O-]",
                    "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"
                },
                {
                    "i": None,
                    "s": "C[OH2+]",
                    "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-O-GNQMJBYIZY"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "C[O-].C[OH2+]",
            "q": 0,
            "0": -916.93893,
            "3": 2,
            "2": "",
            "4": "",
            "0*": [
                -934.5332900000001,
                -916.93893
            ],
            "8": 0,
            "t": [],
            "_id": "NPRPHDSQZBFINE-UHFFFAOYSA-O-CBVLXCEAHL"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]",
                    "p": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                },
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "QOGDLAHJUJGQFA-UHFFFAOYSA-N-GVMIBFZGDC",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                }
            ],
            "9": -3.2946699999999964,
            "b": [
                {
                    "1": "[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"
                },
                {
                    "1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "QOGDLAHJUJGQFA-UHFFFAOYSA-N-GVMIBFZGDC"
                }
            ],
            "e": -935.1186399999999,
            "d": 2898,
            "3*": [
                2,
                4
            ],
            "k": [
                {
                    "i": None,
                    "s": "C[OH+]C",
                    "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-O-OWRHUZFXEA"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "C[OH+]C.[OH-]",
            "q": 0,
            "0": -931.8239699999999,
            "3": 2,
            "2": "",
            "4": "",
            "0*": [
                -932.51315,
                -931.8239699999999
            ],
            "8": 0,
            "t": [],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                },
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "FAPPJOHJPIYMMF-UHFFFAOYSA-O-PEAPJBTOKE",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                },
                {
                    "1": "[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                },
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                }
            ],
            "9": -11.501790000000028,
            "b": [
                {
                    "1": "[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-BLUEBVXPUB"
                },
                {
                    "1": "[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"
                },
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                },
                {
                    "1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "FAPPJOHJPIYMMF-UHFFFAOYSA-O-PEAPJBTOKE"
                }
            ],
            "e": -934.4398100000001,
            "d": 3306,
            "3*": [
                3,
                5
            ],
            "k": [
                {
                    "i": None,
                    "s": "[H+]",
                    "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"
                },
                {
                    "i": None,
                    "s": "COC",
                    "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"
                },
                {
                    "i": None,
                    "s": "[OH-]",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "[H+].COC.[OH-]",
            "q": 0,
            "0": -922.93802,
            "3": 3,
            "2": "",
            "4": "",
            "0*": [
                -922.93802,
                -935.1186399999999,
                -944.3178700000001,
                -924.08536
            ],
            "8": 0,
            "t": [],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
        })

        self._client.test.flask.insert({
            "a": [
                {
                    "1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                    "p": "NHPCRZRPQZSTTA-UHFFFAOYSA-N-AUUCZIPBTE",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                },
                {
                    "1": "[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]",
                    "p": "FAPPJOHJPIYMMF-UHFFFAOYSA-O-NCNIOZDSQN",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                },
                {
                    "1": "[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
                }
            ],
            "9": -10.354450000000043,
            "b": [
                {
                    "1": "[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
                    "r": "DRECBBFIAREDAS-UHFFFAOYSA-N-CBRPIYSZLS"
                },
                {
                    "1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                    "p": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
                    "r": "FAPPJOHJPIYMMF-UHFFFAOYSA-O-NCNIOZDSQN"
                }
            ],
            "e": -944.3178700000001,
            "d": 4040,
            "3*": [
                4,
                6
            ],
            "k": [
                {
                    "i": None,
                    "s": "COC",
                    "_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"
                },
                {
                    "i": None,
                    "s": "O",
                    "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"
                }
            ],
            "j": 2300,
            "m": 1,
            "s": "COC.O",
            "q": 0,
            "0": -933.96342,
            "3": 4,
            "2": "",
            "4": "",
            "0*": [
                -934.4398100000001,
                -933.96342
            ],
            "8": 0,
            "t": ["neutral"],
            "_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND"
        })

    def test_dump(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 9)
        self.assertEqual(g.ecount(), 23)
        self.assertTrue(g.is_directed())

        # Initial vertex
        v = g.vs(smiles='CO.CO')[0]

        # Check vertex attributes
        self.assertEqual(v['key'], 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF')
        self.assertAlmostEqual(v['energy_change'], -9.57513, places=4)
        self.assertAlmostEqual(v['energy'], -944.1080, places=4)
        self.assertEqual(v['status'], 2300)
        self.assertEqual(v['smiles'], 'CO.CO')
        self.assertEqual(v['generation'], 0)

        # Pick edge
        e = g.es(_from=g.vs(smiles='CO.CO')[0].index,
                 _to=g.vs(smiles='[H+].C[O-].CO')[0].index)[0]

        # Check edge attributes
        self.assertEqual(g.vs[e.source]['smiles'], 'CO.CO')
        self.assertEqual(g.vs[e.target]['smiles'], '[H+].C[O-].CO')
        self.assertAlmostEqual(e['energy_change'], 9.57513, places=4)
        self.assertEqual(e['rule'], '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]')

        # Remove temporary file
        os.remove(output_file)

    def test_dump_tags(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts(tags=['neutral'])

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 2)
        self.assertEqual(g.ecount(), 0)
        self.assertTrue(g.is_directed())

        # Remove temporary file
        os.remove(output_file)

    def test_dump_export_dep_var(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri dump command
        result = dump(export_dep=True, export_var=True, export_default=True,
                      exclude=['precursor'], output_file=output_file,
                      opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 9)
        self.assertEqual(g.ecount(), 23)
        self.assertTrue(g.is_directed())

        # Get vertex
        v = g.vs(smiles='[CH3+].CO.[OH-]')[0]

        # Check vertex attributes
        # Required attributes
        self.assertEqual(v['key'], 'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertAlmostEqual(v['energy'], -932.513, places=4)
        self.assertEqual(v['generation'], 1)

        # Dependent attribute
        self.assertEqual(v['sumformula'], 'C2H8O2')

        # Variable attribute, non-default value
        self.assertEqual(v['status'], 2300)

        # Variable attribute, default value
        self.assertEqual(v['attempts'], 0)

        # Excluded attribute
        self.assertNotIn('1', g.vs[0].attributes())

        # Remove temporary file
        os.remove(output_file)

    def test_dump_min_max_generation(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts(min_generation=2, max_generation=3)

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 5)
        self.assertEqual(g.ecount(), 8)
        self.assertTrue(g.is_directed())

        # Remove temporary file
        os.remove(output_file)

    def test_dump_max_energy_change(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts(max_energy_change=-5.)

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 7)
        self.assertEqual(g.ecount(), 12)
        self.assertTrue(g.is_directed())

        # Remove temporary file
        os.remove(output_file)

    def test_dump_max_rel_energy(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts(
            max_rel_energy=10, ref_energy=-944.5)

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 5)
        self.assertEqual(g.ecount(), 4)
        self.assertTrue(g.is_directed())

        # Remove temporary file
        os.remove(output_file)

    def test_dump_max_rel_energy_ref(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts(
            max_rel_energy=10,
            ref_flask='COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF')

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 6)
        self.assertEqual(g.ecount(), 8)
        self.assertTrue(g.is_directed())

        # Remove temporary file
        os.remove(output_file)

    def test_graph_max_rel_energy_noref(self):

        # Insert flask records
        self._insert_data_flask()

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts(max_rel_energy=12)

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get graph result
        g = igraph.Graph.Read_GraphML(output_file)

        # Check graph result
        self.assertEqual(g.vcount(), 8)
        self.assertEqual(g.ecount(), 16)
        self.assertTrue(g.is_directed())

        # Remove temporary file
        os.remove(output_file)

    def test_dump_noinput(self):

        # Output file
        _, output_file = tempfile.mkstemp(suffix='.graphml')

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri dump command
        result = dump(output_file=output_file, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Remove temporary file
        os.remove(output_file)


if __name__ == '__main__':
    unittest.main()
