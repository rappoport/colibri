"""Test classes for colibri resume command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri resume command

Test Classes:
    TestResumeCommand: Test classes for ResumeCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/01/2015'

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.api import resume
from colibri.data import Molecule, Flask
from colibri.enums import Level, Status, Result

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_resumecommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri resume command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestResumeCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestResumeCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_mol(self):

        # Insert FORMULA record
        self._client.test.mol.insert(
            {"z": [], "d": 31, "f": {"i": None, "s": "O=O", "d": 32},
             "l": 1002, "3": 0, "2": "", "4": "", "8": 0,
             "t": [],
             "_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"})

        # Insert CONFIGURATION record
        self._client.test.mol.insert({
            "c": {"d": 52, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

F         -1.12799       -0.23990       -0.00000
C         -0.00007        0.55775        0.00001
F          1.12804       -0.23986       -0.00000
H         -0.00006        1.19829        0.90579
H         -0.00004        1.19829       -0.90585"""},
            "z": [], "d": 52, "f": {"i": None, "s": "FCF", "d": 52},
            "l": 1102, "3": 0, "2": "", "4": "", "8": 0,
            "t": ["organic", "halogenated"],
            "_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"})

        # Insert GEOMETRY record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17,
            "g": {"e": -226.7529, "d": 17, "i": None, "h": "PM7",
                  "m": 1, "o": "xyz", "q": 0, "w": "MOPAC", "7": True,
                  "v": "2012", "x": """\
4

N    -0.000022    -0.000019     0.038027
H     0.569814    -0.749684    -0.283921
H    -0.934098    -0.118619    -0.283910
H     0.364387     0.868362    -0.283954"""},
            "f": {"i": None, "s": "N", "d": 17},
            "l": 1202, "3": 0, "2": "", "4": "", "8": 0,
            "t": [],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

        # Insert PROPERTY record
        self._client.test.mol.insert({
            "c": {"d": 28, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
2

N          0.56000        0.00000        0.00000
N         -0.56000        0.00000        0.00000"""}, "z": [], "d": 28,
            "g": {"e": -371.72947, "d": 28, "i": None, "h": "PM7", "m": 1,
                  "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                  "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""},
            "f": {"i": None, "s": "N#N", "d": 28}, "l": 1302, "3": 0,
            "2": "", "4": "", "y": {
                "e": -11.74325043, "d": 28, "i": None,
                "h": "ZINDO/s/COSMO(methanol)", "m": 1,
                "o": """\
rhf zindo/s tightscf diis norico nomoprint cosmo(water)
%cis NRoots 10
Maxdim 100
end""",
                "n": {
                    "transition_dipoles_z": [
                        -4e-05, -1e-05, -0.00101, 0.74538,
                        -0.11784, 0.00026, -0.0009, -0.0, 0.0, 0.0],
                    "transition_dipoles_y": [
                        -2e-05, -3e-05, -2e-05, 0.11784,
                        0.74539, -0.0009, -0.00025, -1e-05,
                        -1e-05, 0.0],
                    "transition_dipoles_x": [
                        1e-05, 0.00024, 0.0, -2e-05, -0.00015,
                        -0.00035, -1e-05, -1.81418, 1.26879,
                        -6e-05],
                    "excitation_energies": [
                        90255.0, 90266.4, 127154.2, 132373.2,
                        132384.6, 133406.2, 133406.2, 194452.8,
                        262473.3, 290974.4]},
                "q": 0, "u": {
                    "transition_dipoles_z": "au",
                    "transition_dipoles_y": "au",
                    "transition_dipoles_x": "au",
                    "excitation_energies": "cm^-1"},
                "w": "ORCA", "v": "3.0.1", "x": """\
2

N     0.559717    -0.000000    -0.000000
N    -0.559717    -0.000000    -0.000000"""}, "8": 0,
            "_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"})

    def _insert_data_flask(self):

        # Insert INITIAL records
        self._client.test.flask.insert({
            "d": 1800, "k": [
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"},
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}],
            "j": 2002, "m": 1, "s": "C=O.C=O", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "t": ["NN"],
            "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"})

        self._client.test.flask.insert({
            "d": 2048, "k": [
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"},
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}],
            "j": 2002, "m": 1, "s": "CO.CO", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "t": ["LL"],
            "_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"})

        # Insert RUNNING record
        self._client.test.flask.insert({
            "b": [{"1": "[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]",
                   "p": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK",
                   "r": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-RDGEAJXOVL"}],
            "d": 8488, "3*": [8], "k": [
                {"s": "C=C=O",
                 "_id": "CCGKOQOJPYTBIH-UHFFFAOYSA-N-ACYGJWNMWD"},
                {"s": "O", "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2102, "m": 1, "s": "C=C=O.O", "q": 0, "0": -889.86602,
            "3": 8, "2": "", "4": "", "5": "flask_reducer_f4Ssb5",
            "0*": [-889.86602], "8": 0,
            "_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"})

        # Insert REACTIVE record
        self._client.test.flask.insert({
            "9": -9.89066, "b": [
                {"1": "([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"},
                {"1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR"}],
            "e": -888.74776, "d": 5200, "3*": [4, 6],
            "k": [
                {"s": "C1OCO1",
                 "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}],
            "j": 2202, "m": 1, "s": "C1OCO1", "q": 0, "0": -878.8571,
            "3": 4, "2": "", "4": "", "0*": [-888.93686, -878.8571],
            "8": 0, "t": ["NN"],
            "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"})

    def test_resume_mol(self):

        # Insert molecule records
        self._insert_data_mol()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri resume command
        result = resume(molecules=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MYMOFIZGZYHOMD-UHFFFAOYSA-N-CQTZHNDUAY"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'O=O')

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"}))

        # Check result
        self.assertEqual(mol2.level, Level.CONFIGURATION)
        self.assertEqual(mol2.formula.smiles, 'FCF')
        self.assertEqual(mol2.configuration.priority, 52)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.smiles, 'N')
        self.assertEqual(mol3.geometry.energy, -226.7529)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJGRMHOSHXDMSA-UHFFFAOYSA-N-MDBVBASJIB"}))

        # Check result
        self.assertEqual(mol4.level, Level.PROPERTY)
        self.assertEqual(mol4.formula.smiles, 'N#N')
        self.assertEqual(mol4.geometry.energy, -371.72947)
        self.assertEqual(mol4.property.energy, -11.74325043)

    def test_resume_flask(self):

        # Insert flask records
        self._insert_data_flask()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri resume command
        result = resume(flasks=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING)
        self.assertEqual(flask3.smiles, 'C=C=O.O')
        self.assertEqual(flask3.generation, 8)
        self.assertListEqual(flask3.generation_journal, [8])
        self.assertEqual(flask3.attempts, 0)
        self.assertListEqual(flask3.tags, [])

        # Get flask results from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.REACTIVE)
        self.assertEqual(flask4.smiles, 'C1OCO1')
        self.assertEqual(flask4.generation, 4)
        self.assertListEqual(flask4.generation_journal, [4, 6])
        self.assertEqual(flask4.attempts, 0)
        self.assertListEqual(flask4.tags, ['NN'])

if __name__ == '__main__':
    unittest.main()
