"""Test classes for colib delete console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib delete console subcommand

Test Classes:
    TestDeleteConsoleCommand: Test classes for DeleteConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, DeleteConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_deleteconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib delete console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestDeleteConsoleCommand)
    return suite

#
# Test classes
#


class TestDeleteConsoleCommand(unittest.TestCase):

    def _init_deleteconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        DeleteConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return DeleteConsoleCommand(args=args, opts=opts)

    def test_deleteconsole_mol(self):

        # Initialize DeleteConsoleCommand
        console = self._init_deleteconsole(['colib', 'delete', '-e'])

        # Check generated DeleteCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DeleteCommand\(molecules=True, molecule_ids=\[\], " +
            r"flasks=False, flask_ids=None, opts=Options\(.*\)\)")

    def test_deleteconsole_mol_ids(self):

        # Initialize DeleteConsoleCommand
        console = self._init_deleteconsole(['colib', 'delete', '-e', 'C'])

        # Check generated DeleteCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DeleteCommand\(molecules=True, molecule_ids=\['C'\], " +
            r"flasks=False, flask_ids=None, opts=Options\(.*\)\)")

    def test_deleteconsole_flask(self):

        # Initialize DeleteConsoleCommand
        console = self._init_deleteconsole(['colib', 'delete', '-E'])

        # Check generated DeleteCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DeleteCommand\(molecules=False, molecule_ids=None, " +
            r"flasks=True, flask_ids=\[\], opts=Options\(.*\)\)")

    def test_deleteconsole_flask_ids(self):

        # Initialize DeleteConsoleCommand
        console = self._init_deleteconsole(['colib', 'delete', '-E', 'CO.CO'])

        # Check generated DeleteCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DeleteCommand\(molecules=False, molecule_ids=None, " +
            r"flasks=True, flask_ids=\['CO\.CO'\], opts=Options\(.*\)\)")

    def test_deleteconsole_nothing(self):

        # Initialize DeleteConsoleCommand
        with self.assertRaisesRegexp(SystemExit, 'Nothing to do'):
            self._init_deleteconsole(['colib', 'delete'])


if __name__ == '__main__':
    unittest.main()
