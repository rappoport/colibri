"""Test classes for the flasktask classes.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for flasktask classes

Test Classes:
    TestFlaskMapperTask: Test classes for FlaskMapperTask
    TestFlaskReducerTask: Test classes for FlaskReducerTask
    TestFlaskCleanupTask: Test classes for FlaskCleanupTask
    TestFlaskSuspendTask: Test classes for FlaskSuspendTask
    TestFlaskResumeTask: Test classes for FlaskResumeTask
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/20/2015'

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.data import Molecule, Flask
from colibri.calc import Calculator, Context
from colibri.enums import Level, Status
from colibri.exceptions import ExecutionError
from colibri.utils import randomid
import colibri.task

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from tmpmemcached import TmpMemcachedClient
from testopts import get_test_options


#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Temporary Memcached instance
_tmpmemcached = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_flasktask')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for flasktask classes."""
    flasktask_suite = []

    # FlaskMapperTask tests
    flask_mapper_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestFlaskMapperTask)
    flasktask_suite.append(flask_mapper_suite)

    # FlaskReducerTask tests
    flask_reducer_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestFlaskReducerTask)
    flasktask_suite.append(flask_reducer_suite)

    # CleanupTask tests
    flask_cleanup_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestFlaskCleanupTask)
    flasktask_suite.append(flask_cleanup_suite)

    # FlaskSuspendTask tests
    flask_suspend_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestFlaskSuspendTask)
    flasktask_suite.append(flask_suspend_suite)

    # CleanupTask tests
    flask_resume_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestFlaskResumeTask)
    flasktask_suite.append(flask_resume_suite)

    return unittest.TestSuite(flasktask_suite)

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _tmpmemcached, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)

    # Temporary Memcached instance
    try:
        _tmpmemcached = TmpMemcachedClient.get_instance()
        _logger.info('Starting %s' % _tmpmemcached)
    except ExecutionError:
        _logger.warning('Cannot start Memcached')


def tearDownModule():
    global _tmpmongo, _tmpmemcached, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

    # Temporary Memcached instance
    if _tmpmemcached is not None:
        _logger.info('Destroying %s' % _tmpmemcached)
        _tmpmemcached.destroy()

#
# Test classes
#


class TestFlaskMapperTask(unittest.TestCase):
    """Test classes for FlaskMapperTask."""

    def setUp(self):
        global _tmpmongo, _tmpmemcached, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)
        if _tmpmemcached is not None:
            self._cache = _tmpmemcached.cache(key_prefix='MAP:')
            self._logger.info('Connecting to %s' % _tmpmemcached)

    def tearDown(self):
        global _tmpmemcached
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})
        if _tmpmemcached is not None:
            self._logger.info('Flushing %s' % _tmpmemcached)
            self._cache.clear()

    def _insert_data_flask_NN0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O', tags=['NN']).todict())

    def _insert_data_flask_NN0_energy(self):

        # Insert new flask record (generations 0, 2)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'rule': '[C+1:1]-[O-1:2]>>[C+0:1]=[O+0:2]'}],
                  outgoing=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'},
                      {'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                       'product': 'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA',
                       'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'}],
                  rules_applied=['[C+1:1]-[O-1:2]>>[C+0:1]=[O+0:2]',
                                 '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
                                 '[C+0:1]-[#1+0:2]>>([C-1:1].[#1+1:2])'],
                  precursor=None, precursor_journal=[-888.76256],
                  energy=-888.76256, priority=1800,
                  generation=0, generation_journal=[0, 2],
                  status=2000, tags=['NN']).todict())

    def _insert_data_flask_NN0_energy_norules_applied(self):

        # Insert new flask record (generations 0, 2)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'rule': '[C+1:1]-[O-1:2]>>[C+0:1]=[O+0:2]'}],
                  outgoing=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'},
                      {'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                       'product': 'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA',
                       'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'}],
                  rules_applied=[],
                  precursor=None, precursor_journal=[-888.76256],
                  energy=-888.76256, priority=1800,
                  generation=0, generation_journal=[0, 2],
                  status=2000, tags=['NN']).todict())

    def _insert_data_flask_LL0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='CO.CO', tags=['LL']).todict())

    def _insert_data_flask_NN1(self):

        # Insert new flask record (generation 1)
        self._client.test.flask.insert(
            Flask(smiles='[CH2+][O-].C=O',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'}],
                  generation=1, generation_journal=[1],
                  precursor=-888.76256, precursor_journal=[-888.76256],
                  status=2000, tags=['NN']).todict())

    def _insert_data_flask_NN2(self):

        # Insert new flask record (generation 2)
        self._client.test.flask.insert(
            Flask(smiles='[CH2+][O-].[CH2+][O-]',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE',
                      'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'}],
                  generation=2, generation_journal=[2],
                  precursor=-888.76256, precursor_journal=[-888.76256],
                  status=2000, tags=['NN']).todict())

    def _insert_data_flask_NN4(self):

        # Insert new flask record (generation 6)
        # (Generation and energy are updated from respective journals)
        # No energy available (flask creation precedes energy computation)
        self._client.test.flask.insert(
            Flask(smiles='C1OCO1',
                  incoming=[{
                      'reactant': 'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX',
                      'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOX',
                      'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'},
                      {'reactant': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                       'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                       'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'}],
                  outgoing=[{
                      'reactant': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                      'product': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                      'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'}],
                  generation=6, generation_journal=[4, 6],
                  precursor=-888.93517,
                  precursor_journal=[-888.93517, -878.8571],
                  status=2000, tags=['NN']).todict())

    def _insert_data_flask_NN4_energy(self):

        # Insert new flask record (generation 6)
        # (Generation and energy are updated from respective journals)
        # Energy is available (energy computation follows flask creation)
        self._client.test.flask.insert(
            Flask(smiles='C1OCO1',
                  incoming=[{
                      'reactant': 'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX',
                      'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOX',
                      'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'},
                      {'reactant': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                       'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                       'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'}],
                  outgoing=[{
                      'reactant': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                      'product': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                      'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'}],
                  rules_applied=['[C+0:1]-[#1+0:2]>>([C-1:1].[#1+1:2])'
                                 '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'],
                  generation=6, generation_journal=[4, 6],
                  precursor=-888.93517,
                  precursor_journal=[-888.93517, -878.8571],
                  energy_change=0.18741, energy=-888.74776,
                  status=2000, tags=['NN']).todict())

    def _insert_data_flask_NN13_energy(self):

        # Insert new flask record (generation 13)
        # (Energy is updated from journal)
        self._client.test.flask.insert(
            Flask(smiles='[C-]#CO.[H+].O',
                  incoming=[{
                      'reactant': 'MCIDLSNMEGIFCA-UHFFFAOYSA-N-UMUTKHSCDS',
                      'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                      'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-POAJXMCVAO',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'rule': '[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]'}],
                  outgoing=[{
                      'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                      'product': 'PXNRYTDTQNVFKV-UHFFFAOYSA-N-XYZTPCCCCS',
                      'rule': '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EQJNDBMSPY',
                       'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-PDNVFABJBM',
                       'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-POAJXMCVAO',
                       'rule': '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'MCIDLSNMEGIFCA-UHFFFAOYSA-N-UMUTKHSCDS',
                       'rule': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-JJWWSAZBCQ',
                       'rule': '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'}],
                  rules_applied=['([C-1:1].[#1+1:2])>>[C+0:1]-[#1+0:2]'
                                 '([O+0:1].[#1+1:2])>>[O+1:1]-[#1+0:2]',
                                 '[C+0:1]-[O+0:2]>>([C+1:1].[O-1:2])',
                                 '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
                                 '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'
                                 '[O+0:1]-[#1+0:2]>>([O-1:1].[#1+1:2])',
                                 '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
                                 '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'],
                  generation=13, generation_journal=[13, 15],
                  precursor=-888.53291,
                  precursor_journal=[-888.53291, -883.78013],
                  energy_change=10.12929, energy=-878.40362,
                  status=2000, tags=['NN']).todict())

    def _insert_data_flask_ES3_energy(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='[CH2-][C+]=O.CO.O', generation=3,
                  generation_journal=[3], energy=-1362.58152,
                  precursor=-1361.2147, precursor_journal=[-1361.2147],
                  energy_change=-1.36682,
                  incoming=[{
                      'reactant': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-MUTKTNEQJD',
                      'product': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL',
                      'rule': '[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]'}],
                  outgoing=[{
                      'reactant': 'RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU',
                      'product': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL',
                      'rule': '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]'}],
                  rules_applied=['[C+0:1]-[O+1:2]>>([C+1:1].[O+0:2])',
                                 '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]'],
                  status=2000, tags=['ES']).todict())

    def _insert_data_flask_badchem(self):

        # Insert new flask record (BadChemistry)
        self._client.test.flask.insert(
            Flask(smiles='[CH2][O].C=O',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-WZTVMDNTKY',
                      'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'}],
                  outgoing=[], status=2000, priority=2200,
                  precursor=-888.76256, precursor_journal=[-888.76256],
                  generation=2, generation_journal=[2], error='BadChemistry',
                  tags=['NN'], energy=None, energy_change=None,
                  attempts=1, mult=3, charge=0).todict())

    def _insert_data_flask_error(self):

        # Insert new flask record (ExecutionError)
        self._client.test.flask.insert(
            Flask(smiles='[H+]', incoming=[], outgoing=[], status=2000,
                  generation=0, generation_journal=[0],
                  precursor=None, precursor_journal=[],
                  tags=['proton'], energy=None, energy_change=None,
                  attempts=1, mult=1, charge=1,
                  error='ExecutionError').todict())

    def _insert_data_flask_invalid(self):

        # Insert new flask record (ValidationError)
        self._client.test.flask.insert(
            Flask(smiles='[OH3+]', incoming=[], outgoing=[], status=2000,
                  generation=0, generation_journal=[0],
                  precursor=None, precursor_journal=[],
                  tags=['proton'], energy=None, energy_change=None,
                  attempts=1, mult=1, charge=1,
                  error='ValidationError').todict())

    def _insert_data_flask_hienergy(self):

        # Insert new flask record (HiEnergy)
        self._client.test.flask.insert(
            Flask(smiles='[CH2-][C+]=O.CO.O', generation=3,
                  generation_journal=[3], energy=-1362.58152,
                  precursor=-1361.2147, precursor_journal=[-1361.2147],
                  energy_change=-1.36682,
                  incoming=[{
                      'reactant': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-MUTKTNEQJD',
                      'product': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL',
                      'rule': '[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]'}],
                  outgoing=[{
                      'reactant': 'RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU',
                      'product': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL',
                      'rule': '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]'}],
                  rules_applied=['[C+0:1]-[O+1:2]>>([C+1:1].[O+0:2])',
                                 '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]'],
                  status=2000, tags=['ES'], error='HiEnergy').todict())

    def _insert_data_mol_NN0(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='C=O', generation=0, tags=['test']).todict())

    def _insert_cache_data_mol(self):

        # Insert molecules into cache (generations 0--2)
        self._cache.set_blocking({
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': '',
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': ''})

    def _run_flaskmappertask(self, **kwargs):
        global _opts, _logging_level, _tmpmongo, _tmpmemcached

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'flask_mapper_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'FlaskMapperTask'}

        # Add storage hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Add cache hostname and port options
        if _tmpmemcached is not None:
            input_kv['cache_hostname'] = 'localhost'
            input_kv['cache_port'] = _tmpmemcached.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

        return calc

    def test_mapper_nocache(self):

        # Insert flask record
        self._insert_data_flask_NN0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertListEqual(flask.tags, ['NN'])

        # Get molecule result from the database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.formula.smiles, 'C=O')
        self.assertEqual(mol.priority, 30)
        self.assertEqual(mol.generation, 0)
        self.assertListEqual(mol.tags, ['NN'])

    def test_mapper_nocache_tags(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_LL0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tags
        input_kv['tags'] = ['LL']

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.precursor, None)
        self.assertListEqual(flask1.precursor_journal, [])
        self.assertListEqual(flask1.tags, ['NN'])

        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.RUNNING)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertListEqual(flask2.tags, ['LL'])
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.precursor, None)
        self.assertListEqual(flask2.precursor_journal, [])

        # Molecules with NN were not inserted
        self.assertEqual(self._client.test.mol.find({'t': 'NN'}).count(), 0)

        # Molecules with LL tag were inserted
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.formula.smiles, 'CO')
        self.assertEqual(mol.priority, 32)
        self.assertEqual(mol.generation, 0)
        self.assertListEqual(mol.tags, ['LL'])

    def test_mapper_nocache_tags_str(self):

        # Insert flask record
        self._insert_data_flask_NN0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tags
        input_kv['tags'] = 'LN'

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.INITIAL)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertListEqual(flask.tags, ['NN'])

    def test_mapper_nocache_multiple_mol(self):

        # Insert flask record
        self._insert_data_flask_NN1()

        # Insert molecule record from previous runs
        self._insert_data_mol_NN0()

        # Get molecule result from the database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}))

        # Check molecule result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'C=O')
        self.assertEqual(mol1.priority, 30)
        self.assertEqual(mol1.generation, 0)
        self.assertListEqual(mol1.tags, ['test'])

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, '[CH2+][O-].C=O')
        self.assertEqual(flask.generation, 1)
        self.assertEqual(flask.generation_journal, [1])
        self.assertAlmostEqual(flask.precursor, -888.76256, places=4)
        self.assertEqual(len(flask.precursor_journal), 1)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.76256, places=4)
        self.assertListEqual(flask.tags, ['NN'])

        # Get molecule result from the database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}))

        # Check molecule result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'C=O')
        self.assertEqual(mol2.priority, 30)
        self.assertEqual(mol2.generation, 0)
        self.assertListEqual(mol2.tags, ['test', 'NN'])

        # Get molecule result from the database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"}))

        # Check molecule result
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, '[CH2+][O-]')
        self.assertEqual(mol3.priority, 30)
        self.assertEqual(mol3.generation, 1)
        self.assertListEqual(mol3.tags, ['NN'])

    def test_mapper_nocache_noinput(self):

        # Run FlaskMapperTask
        self._run_flaskmappertask()

        # No flasks are available
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_existing(self):

        # Insert flask record
        self._insert_data_flask_NN0_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.UNREACTIVE)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0, 2])
        self.assertAlmostEqual(flask.precursor, -888.76256, places=4)
        self.assertEqual(len(flask.precursor_journal), 1)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.76256, places=4)
        self.assertListEqual(flask.tags, ['NN'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_existing_norules_applied(self):

        # Insert flask record
        self._insert_data_flask_NN0_energy_norules_applied()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0, 2])
        self.assertAlmostEqual(flask.precursor, -888.76256, places=4)
        self.assertEqual(len(flask.precursor_journal), 1)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.76256, places=4)
        self.assertListEqual(flask.tags, ['NN'])

        # Get molecule result from the database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.formula.smiles, 'C=O')
        self.assertEqual(mol.priority, 30)
        self.assertEqual(mol.generation, 0)
        self.assertListEqual(mol.tags, ['NN'])

        # Molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

    def test_mapper_nocache_badchem(self):

        # Insert flask record
        self._insert_data_flask_badchem()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-WZTVMDNTKY"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_BADCHEM)
        self.assertEqual(flask.smiles, '[CH2][O].C=O')
        self.assertEqual(flask.generation, 2)
        self.assertListEqual(flask.generation_journal, [2])
        self.assertAlmostEqual(flask.precursor, -888.76256, places=4)
        self.assertEqual(len(flask.precursor_journal), 1)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.76256, places=4)
        self.assertEqual(flask.error, 'BadChemistry')
        self.assertListEqual(flask.tags, ['NN'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_error(self):

        # Insert flask record
        self._insert_data_flask_error()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.ERROR)
        self.assertEqual(flask.smiles, '[H+]')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.error, 'ExecutionError')
        self.assertListEqual(flask.tags, ['proton'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_invalid(self):

        # Insert flask record
        self._insert_data_flask_invalid()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check flask result
        self.assertEqual(flask.status, Status.INVALID)
        self.assertEqual(flask.smiles, '[OH3+]')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.error, 'ValidationError')
        self.assertListEqual(flask.tags, ['proton'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_hienergy(self):

        # Insert flask record
        self._insert_data_flask_hienergy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_HIENERGY)
        self.assertEqual(flask.smiles, '[CH2-][C+]=O.CO.O')
        self.assertEqual(flask.generation, 3)
        self.assertListEqual(flask.generation_journal, [3])
        self.assertEqual(flask.error, 'HiEnergy')
        self.assertListEqual(flask.tags, ['ES'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_nomaxgen_existing(self):

        # Insert flask record
        self._insert_data_flask_NN4_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max. generation
        input_kv['max_generation'] = 6

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask.status, Status.UNREACTIVE)
        self.assertEqual(flask.smiles, 'C1OCO1')
        self.assertEqual(flask.generation, 4)
        self.assertListEqual(flask.generation_journal, [4, 6])
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['NN'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_nomaxgen(self):

        # Insert flask record
        self._insert_data_flask_NN4()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max. generation
        input_kv['max_generation'] = 6

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'C1OCO1')
        self.assertEqual(flask.generation, 4)
        self.assertListEqual(flask.generation_journal, [4, 6])
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['NN'])

        # Get molecule result from the database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.smiles, 'C1OCO1')
        self.assertEqual(mol.generation, 4)
        self.assertListEqual(mol.tags, ['NN'])

    def test_mapper_nocache_maxgen_nomerge(self):

        # Insert flask record
        self._insert_data_flask_ES3_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max. generation
        input_kv['max_generation'] = 3

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_MAXGEN)
        self.assertEqual(flask.smiles, '[CH2-][C+]=O.CO.O')
        self.assertAlmostEqual(flask.energy, -1362.58152, places=4)
        self.assertEqual(flask.generation, 3)
        self.assertListEqual(flask.generation_journal, [3])
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['ES'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_maxgen_merge(self):

        # Insert flask record
        self._insert_data_flask_NN4_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max. generation
        input_kv['max_generation'] = 4

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_MAXGEN)
        self.assertEqual(flask.smiles, 'C1OCO1')
        self.assertAlmostEqual(flask.energy, -888.74776, places=4)
        self.assertEqual(flask.generation, 4)
        self.assertListEqual(flask.generation_journal, [4, 6])
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['NN'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_nomaxenchange(self):

        # Insert flask record
        self._insert_data_flask_NN13_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.UNREACTIVE)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertAlmostEqual(flask.energy, -878.40362, places=4)
        self.assertAlmostEqual(flask.energy_change, 5.37651, places=4)
        self.assertAlmostEqual(flask.precursor, -883.78013, places=4)
        self.assertEqual(len(flask.precursor_journal), 2)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.53291, places=4)
        self.assertAlmostEqual(
            flask.precursor_journal[1], -883.78013, places=4)
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['NN'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_maxenchange_nomerge(self):

        # Insert flask record
        self._insert_data_flask_ES3_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max. energy change
        input_kv['max_energy_change'] = -4.0

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_HIBARRIER)
        self.assertEqual(flask.smiles, '[CH2-][C+]=O.CO.O')
        self.assertAlmostEqual(flask.energy, -1362.58152, places=4)
        self.assertAlmostEqual(flask.energy_change, -1.36682, places=4)
        self.assertAlmostEqual(flask.precursor, -1361.2147, places=4)
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['ES'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_maxenchange_merge(self):

        # Insert flask record
        self._insert_data_flask_NN13_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max. energy change
        input_kv['max_energy_change'] = 3

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_HIBARRIER)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertAlmostEqual(flask.energy, -878.40362, places=4)
        self.assertAlmostEqual(flask.energy_change, 5.37651, places=4)
        self.assertAlmostEqual(flask.precursor, -883.78013, places=4)
        self.assertEqual(len(flask.precursor_journal), 2)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.53291, places=4)
        self.assertAlmostEqual(
            flask.precursor_journal[1], -883.78013, places=4)
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['NN'])

        # No molecules were inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mapper_nocache_batch1_nonapi(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tasks per calc
        input_kv['tasks_per_calc'] = 3

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 1

        # Batch size
        input_kv['batch_size'] = 1

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskMapperTask
        calc = self._run_flaskmappertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 3)

        # Retrieve completed task
        task1 = calc._completed_tasks[0]

        # Input molecules
        input_molecules1 = [f.key for f in task1._data.molecules]
        self.assertListEqual(
            input_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'] * 2)

        # Molecules to be submitted to storage
        storage_molecules1 = task1._molecules.keys()
        self.assertListEqual(
            storage_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules to be submitted to cache
        cache_molecules1 = list(task1._cache_molecules)
        self.assertListEqual(cache_molecules1, [])

        # Retrieve completed task
        task2 = calc._completed_tasks[1]

        # Input molecules
        input_molecules2 = [f.key for f in task2._data.molecules]
        self.assertListEqual(
            input_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules to be submitted to storage
        storage_molecules2 = task2._molecules.keys()
        self.assertListEqual(
            storage_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                 'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules to be submitted to cache
        cache_molecules2 = list(task2._cache_molecules)
        self.assertListEqual(cache_molecules2, [])

        # Retrieve completed task
        task3 = calc._completed_tasks[2]

        # Input molecules
        input_molecules3 = [f.key for f in task3._data.molecules]
        self.assertListEqual(
            input_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules to be submitted to storage
        storage_molecules3 = task3._molecules.keys()
        self.assertListEqual(
            storage_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'])

        # Molecules to be submitted to cache
        cache_molecules3 = list(task3._cache_molecules)
        self.assertListEqual(cache_molecules3, [])

    def test_mapper_nocache_batch2_nonapi(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tasks per calc
        input_kv['tasks_per_calc'] = 3

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 2

        # Batch size
        input_kv['batch_size'] = 2

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskMapperTask
        calc = self._run_flaskmappertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 2)

        # Retrieve completed task
        task1 = calc._completed_tasks[0]

        # Input molecules
        input_molecules1 = [f.key for f in task1._data.molecules]
        self.assertListEqual(
            input_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules to be submitted to storage
        storage_molecules1 = task1._molecules.keys()
        self.assertListEqual(
            storage_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                 'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules to be submitted to cache
        cache_molecules1 = list(task1._cache_molecules)
        self.assertListEqual(cache_molecules1, [])

        # Retrieve completed task
        task2 = calc._completed_tasks[1]

        # Input molecules
        input_molecules2 = [f.key for f in task2._data.molecules]
        self.assertListEqual(
            input_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules to be submitted to storage
        storage_molecules2 = task2._molecules.keys()
        self.assertListEqual(
            storage_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'])

        # Molecules to be submitted to cache
        cache_molecules2 = list(task2._cache_molecules)
        self.assertListEqual(cache_molecules2, [])

    def test_mapper_nocache_batch3_nonapi(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 3

        # Batch size
        input_kv['batch_size'] = 3

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskMapperTask
        calc = self._run_flaskmappertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 1)

        # Retrieve completed task
        task = calc._completed_tasks[0]

        # Input molecules
        input_molecules = [f.key for f in task._data.molecules]
        self.assertListEqual(
            input_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules to be submitted to storage
        storage_molecules = task._molecules.keys()
        self.assertListEqual(
            storage_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules to be submitted to cache
        cache_molecules = list(task._cache_molecules)
        self.assertListEqual(cache_molecules, [])

    def test_mapper_cache(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask record
        self._insert_data_flask_NN0()

        # Insert molecules into cache
        self._insert_cache_data_mol()

        # Dictionary of input options
        input_kv = {}

        # Enable cache
        input_kv['with_cache'] = True

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertListEqual(flask.tags, ['NN'])

        # No molecules were inserted into storeage
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

        # Check cache molecules
        self.assertDictEqual(self._cache.get(
            ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
             'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO']),
            {'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': '',
             'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': ''})

    def test_mapper_cache_batch1_nonapi(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Insert molecules into cache
        self._insert_cache_data_mol()

        # Dictionary of input options
        input_kv = {}

        # Enable cache
        input_kv['with_cache'] = True

        # Tasks per calc
        input_kv['tasks_per_calc'] = 3

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 1

        # Batch size
        input_kv['batch_size'] = 1

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskMapperTask
        calc = self._run_flaskmappertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 3)

        # Retrieve completed task
        task1 = calc._completed_tasks[0]

        # Input molecules
        input_molecules1 = [f.key for f in task1._data.molecules]
        self.assertListEqual(
            input_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'] * 2)

        # Molecules to be submitted to storage
        storage_molecules1 = task1._molecules.keys()
        self.assertListEqual(storage_molecules1, [])

        # Molecules to be submitted to cache
        cache_molecules1 = list(task1._cache_molecules)
        self.assertListEqual(
            cache_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Retrieve completed task
        task2 = calc._completed_tasks[1]

        # Input molecules
        input_molecules2 = [f.key for f in task2._data.molecules]
        self.assertListEqual(
            input_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules to be submitted to storage
        storage_molecules2 = task2._molecules.keys()
        self.assertListEqual(storage_molecules2, [])

        # Molecules to be submitted to cache
        cache_molecules2 = list(task2._cache_molecules)
        self.assertListEqual(
            cache_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Retrieve completed task
        task3 = calc._completed_tasks[2]

        # Input molecules
        input_molecules3 = [f.key for f in task3._data.molecules]
        self.assertListEqual(
            input_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules to be submitted to storage
        storage_molecules3 = task3._molecules.keys()
        self.assertListEqual(storage_molecules3, [])

        # Molecules to be submitted to cache
        cache_molecules3 = list(task3._cache_molecules)
        self.assertListEqual(
            cache_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'])

        # Check cache molecules
        self.assertDictEqual(self._cache.get(
            ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
             'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO']),
            {'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': '',
             'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': ''})

    def test_mapper_cache_batch3_nonapi(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Insert molecules into cache
        self._insert_cache_data_mol()

        # Dictionary of input options
        input_kv = {}

        # Enable cache
        input_kv['with_cache'] = True

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 3

        # Batch size
        input_kv['batch_size'] = 3

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskMapperTask
        calc = self._run_flaskmappertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 1)

        # Retrieve completed task
        task = calc._completed_tasks[0]

        # Input molecules
        input_molecules = [f.key for f in task._data.molecules]
        self.assertListEqual(
            input_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules to be submitted to storage
        storage_molecules = task._molecules.keys()
        self.assertListEqual(storage_molecules, [])

        # Molecules to be submitted to cache
        cache_molecules = list(task._cache_molecules)
        self.assertListEqual(
            cache_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                              'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Check cache molecules
        self.assertDictEqual(self._cache.get(
            ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
             'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO']),
            {'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': '',
             'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': ''})

    def test_mapper_nocache_batchsize(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_LL0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Batch size
        input_kv['batch_size'] = 2

        # Run FlaskMapperTask
        self._run_flaskmappertask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.RUNNING)
        self.assertEqual(flask1.smiles, 'C=O.C=O')

        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'CO.CO')

    def test_mapper_rollback(self):
        global _opts

        # Insert flask record
        self._insert_data_flask_NN0()

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'flask_mapper_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'FlaskMapperTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Batch size
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Locks
        input_kv['optimistic_lock'] = False
        input_kv['pessimistic_lock'] = True

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create Context object
        context = Context(opts)

        # Create new FlaskMapperTask
        task = colibri.task.FlaskMapperTask(context, opts)

        # Setup
        task.setup()

        # Retrieve
        task.retrieve()

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL_LOCKED)
        self.assertEqual(flask1.etag, calc_name)
        self.assertEqual(flask1.smiles, 'C=O.C=O')

        # Read initial flask
        task._read_initial_flask()

        # Rollback
        task.rollback()

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.etag, '')
        self.assertEqual(flask2.smiles, 'C=O.C=O')


class TestFlaskReducerTask(unittest.TestCase):
    """Test classes for FlaskReducerTask."""

    def setUp(self):
        global _tmpmongo, _tmpmemcached, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)
        if _tmpmemcached is not None:
            self._cache = _tmpmemcached.cache(key_prefix='RED:')
            self._logger.info('Connecting to %s' % _tmpmemcached)

    def tearDown(self):
        global _tmpmemcached
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})
        if _tmpmemcached is not None:
            self._logger.info('Flushing %s' % _tmpmemcached)
            self._cache.clear()

    def _insert_data_flask_NN0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O', generation=0, generation_journal=[0],
                  status=2100, tags=['NN']).todict())

    def _insert_data_flask_LL0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='CO.CO', status=2100, tags=['LL']).todict())

    def _insert_data_flask_NN1(self):

        # Insert new flask record (generation 1)
        self._client.test.flask.insert(
            Flask(smiles='[CH2+][O-].C=O', incoming=[{
                'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'}],
                generation=1, generation_journal=[1], status=2100,
                precursor=-888.76256, precursor_journal=[-888.76256],
                tags=['NN']).todict())

    def _insert_data_flask_L1(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C[O-].[H+]', generation=1, generation_journal=[1],
                  status=2100, tags=['L']).todict())

    def _insert_data_flask_NN2(self):

        # Insert new flask record (generation 2)
        self._client.test.flask.insert(
            Flask(smiles='[CH2+][O-].[CH2+][O-]',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE',
                      'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'}],
                  generation=2, generation_journal=[2], status=2100,
                  precursor=-888.76256, precursor_journal=[-888.76256],
                  tags=['NN']).todict())

    def _insert_data_flask_att3(self):

        # Insert new flask record (generation 2)
        self._client.test.flask.insert(
            Flask(smiles='[CH2+][O-].[CH2+][O-]',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE',
                      'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'}],
                  generation=2, generation_journal=[2], attempts=3,
                  status=2100, precursor=-888.76256,
                  precursor_journal=[-888.76256], tags=['NN']).todict())

    def _insert_data_flask_NN4(self):

        # Insert new flask record (generation 6)
        self._client.test.flask.insert(
            Flask(smiles='C1OCO1',
                  incoming=[{
                      'reactant': 'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX',
                      'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOX',
                      'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'},
                      {'reactant': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                       'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                       'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'}],
                  outgoing=[{
                      'reactant': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                      'product': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                      'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'}],
                  generation=4, generation_journal=[4, 6],
                  precursor=-888.93517,
                  precursor_journal=[-888.93517, -878.8571], status=2100,
                  tags=['NN']).todict())

    def _insert_data_flask_NN13(self):

        # Insert new flask record (generation 13)
        self._client.test.flask.insert(
            Flask(smiles='[C-]#CO.[H+].O',
                  incoming=[{
                      'reactant': 'MCIDLSNMEGIFCA-UHFFFAOYSA-N-UMUTKHSCDS',
                      'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                      'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-POAJXMCVAO',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'rule': '[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]'}],
                  outgoing=[{
                      'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                      'product': 'PXNRYTDTQNVFKV-UHFFFAOYSA-N-XYZTPCCCCS',
                      'rule': '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EQJNDBMSPY',
                       'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-PDNVFABJBM',
                       'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-POAJXMCVAO',
                       'rule': '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'MCIDLSNMEGIFCA-UHFFFAOYSA-N-UMUTKHSCDS',
                       'rule': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'},
                      {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                       'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-JJWWSAZBCQ',
                       'rule': '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'}],
                  generation=13, generation_journal=[13, 15],
                  precursor=-888.53291,
                  precursor_journal=[-888.53291, -883.78013],
                  energy_change=10.12929, status=2100, tags=['NN']).todict())

    def _insert_data_flask_ES3_energy(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='[CH2-][C+]=O.CO.O', generation=3,
                  generation_journal=[3], energy=-1362.58152,
                  precursor=-1361.2147, precursor_journal=[-1361.2147],
                  energy_change=-1.36682,
                  incoming=[{
                      'reactant': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-MUTKTNEQJD',
                      'product': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL',
                      'rule': '[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]'}],
                  outgoing=[{
                      'reactant': 'RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU',
                      'product': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL',
                      'rule': '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]'}],
                  status=2100, tags=['ES']).todict())

    def _insert_data_flask_badchem(self):

        # Insert new flask record (BadChemistry)
        self._client.test.flask.insert(
            Flask(smiles='[CH2][O].C=O',
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-WZTVMDNTKY',
                      'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'}],
                  outgoing=[], status=2100, priority=2200,
                  precursor=-888.76256, precursor_journal=[-888.76256],
                  generation=2, generation_journal=[2], error='',
                  tags=['NN'], energy=None, energy_change=None,
                  mult=3, charge=0).todict())

    def _insert_data_flask_badchem_unavailable(self):

        # Insert new flask record (BadChemistry)
        self._client.test.flask.insert(
            Flask(smiles='[CH2][O].CO', mult=3, charge=0,
                  status=2100).todict())

    def _insert_data_flask_error(self):

        # Insert new flask record (ExecutionError)
        self._client.test.flask.insert(
            Flask(smiles='[H+]', incoming=[], outgoing=[], status=2100,
                  generation=0, generation_journal=[0],
                  precursor=None, precursor_journal=[],
                  tags=['proton'], energy=None, energy_change=None,
                  mult=1, charge=1, error='ExecutionError').todict())

    def _insert_data_flask_invalid(self):

        # Insert new flask record (ValidationError)
        self._client.test.flask.insert(
            Flask(smiles='[OH3+]', incoming=[], outgoing=[], status=2100,
                  generation=0, generation_journal=[0],
                  precursor=None, precursor_journal=[],
                  tags=['proton'], energy=None, energy_change=None,
                  mult=1, charge=1, error='ValidationError').todict())

    def _insert_data_mol_NN0(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {"d": 30, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

C         -0.61340       -0.00000       -0.00000
O          0.60608        0.00000        0.00000
H         -1.15558       -0.93914        0.00000
H         -1.15564        0.93913        0.00000"""},
            "z": [], "d": 30, "g": {
                "e": -444.38128, "d": 30, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC", "7": True,
                "v": "2012", "x": """\
4

C    -0.602789     0.000065    -0.000000
O     0.603275    -0.000020     0.000000
H    -1.168300    -0.933944     0.000000
H    -1.168372     0.933787     0.000000"""},
            "f": {"i": None, "s": "C=O", "d": 30}, "l": 1200,
            "3": 0, "2": "", "5": "", "t": ["NN"],
            "4": "", "8": 0, "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"})

    def _insert_data_mol_LL0(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {"d": 32, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
6

C         -0.71578       -0.01298       -0.00001
O          0.68025        0.06949        0.00000
H         -1.14093        1.01168       -0.00001
H         -1.06852       -0.54668       -0.90915
H         -1.06850       -0.54658        0.90929
H          1.00941       -0.86674        0.00002"""},
            "z": [], "d": 32, "g": {
                "e": -472.05421, "d": 32, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC", "7": True,
                "v": "2012", "x": """\
6

C    -0.737045    -0.027591     0.000113
O     0.673805     0.065643    -0.000028
H    -1.068203     1.019087    -0.000064
H    -1.086355    -0.537412    -0.903378
H    -1.086328    -0.537247     0.903234
H     1.071436    -0.826984     0.000043"""},
            "f": {"i": None, "s": "CO", "d": 32}, "l": 1200,
            "3": 0, "2": "", "5": "",
            "4": "", "8": 0, "t": ["LL"],
            "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"})

    def _insert_data_mol_NN1(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {"d": 30, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

C         -0.69319       -0.00000        0.00000
O          0.67604       -0.00000       -0.00000
H         -1.23540        0.93913       -0.00002
H         -1.23548       -0.93908       -0.00002"""},
            "z": [], "d": 30, "g": {
                "e": -444.38128, "d": 30, "i": None,
                "h": "PM7", "m": 1, "o": "xyz", "q": 0, "w": "MOPAC",
                "7": True, "v": "2012", "x": """
4

C    -0.619529     0.000228    -0.000004
O     0.586739    -0.000073     0.000001
H    -1.184679     0.933778    -0.000016
H    -1.184689    -0.934228    -0.000016"""},
            "f": {"i": None, "s": "[CH2+][O-]", "d": 30},
            "l": 1200, "3": 1, "2": "", "4": "", "8": 0, "t": ["NN"],
            "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"})

    def _insert_data_mol_formula(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='C=O', tags=['NN']).todict())

    def _insert_data_mol_badchem(self):

        # Insert new molecule record (FORMULA_INVALID)
        self._client.test.mol.insert(
            Molecule(smiles='[CH2][O]', level=Level.FORMULA_INVALID).todict())

    def _insert_data_mol_error(self):

        # Insert new molecule record (GEOMETRY_ERROR)
        self._client.test.mol.insert(
            Molecule(smiles='[H+]', level=Level.GEOMETRY_ERROR).todict())

    def _insert_data_mol_invalid(self):

        # Insert new molecule record (GEOMETRY_INVALID)
        self._client.test.mol.insert(
            Molecule(smiles='[OH3+]', level=Level.GEOMETRY_INVALID).todict())

    def _insert_data_mol_noenergy(self):

        # Insert new molecule record (GEOMETRY, not converged, no energy)
        self._client.test.mol.insert({
            "c": {"d": 31, "i": None, "m": 1, "q": -1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

C         -0.68252        0.00000       -0.00001
O          0.71126        0.00000       -0.00000
H         -1.05230       -0.05091        1.04476
H         -1.05237        0.93020       -0.47824
H         -1.05233       -0.87932       -0.56640"""},
            "z": [], "d": 31, "g": {
                "d": 31, "i": None, "h": "PM7", "m": 1, "o": "xyz", "q": -1,
                "w": "MOPAC", "7": False, "v": "2012", "x": """\
5

C         -0.68252        0.00000       -0.00001
O          0.71126        0.00000       -0.00000
H         -1.05230       -0.05091        1.04476
H         -1.05237        0.93020       -0.47824
H         -1.05233       -0.87932       -0.56640"""},
            "f": {"i": None, "s": "C[O-]", "d": 31}, "l": 1200,
            "3": 5, "2": "", "4": "", "8": 0, "t": ["L"],
            "_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"})

        # Insert new molecule record
        self._client.test.mol.insert({
            "z": [], "d": 1, "g": {
                "e": -4.48975, "d": 1, "i": None, "m": 1, "q": 1,
                "x": """\
1

H 0. 0. 0."""},
            "f": {"i": None, "q": 1, "s": "[H+]", "m": 1, "d": 1},
            "l": 1200, "3": 0, "2": "", "5": "", "4": "", "8": 0,
            "t": ["L"], "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"})

    def _insert_data_mol_NN4(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {"d": 60, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
8

C         -0.00041        0.94625        0.00001
O          1.02858        0.00040       -0.00000
C          0.00041       -0.94625       -0.00001
O         -1.02858       -0.00040        0.00000
H         -0.00060        1.55355        0.93309
H         -0.00065        1.55363       -0.93309
H          0.00060       -1.55355       -0.93309
H          0.00065       -1.55363        0.93309"""},
            "z": [], "d": 60, "g": {
                "e": -888.74776, "d": 60, "i": None, "h": "PM7", "m": 1,
                "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                "x": """\
8

C    -0.000364     1.007533     0.000032
O     1.042777     0.000510     0.000058
C     0.000375    -1.007584    -0.000050
O    -1.042799    -0.000400     0.000070
H    -0.000619     1.582995     0.928946
H    -0.000641     1.582743    -0.929211
H     0.000644    -1.582836    -0.929217
H     0.000665    -1.583162     0.929033"""},
            "f": {"i": None, "s": "C1OCO1", "d": 60}, "l": 1200,
            "3": 4, "2": "", "4": "", "8": 0, "t": ["NN"],
            "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"})

    def _insert_data_mol_NN13(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {"d": 41, "i": None, "m": 1, "q": -1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

C          1.43105       -0.02176       -0.00000
C          0.22617        0.01461        0.00000
O         -1.13876        0.05605       -0.00000
H         -1.67162       -0.80447       -0.00000"""},
            "z": [], "d": 41, "g": {
                "e": -551.23467, "d": 41, "i": None, "h": "PM7", "m": 1,
                "o": "xyz", "q": -1, "w": "MOPAC", "7": True,
                "v": "2012", "x": """\
4

C     1.417206    -0.024175     0.000000
C     0.199487    -0.011374    -0.000000
O    -1.145485     0.086056     0.000000
H    -1.504923    -0.825982     0.000000"""},
            "f": {"i": None, "s": "[C-]#CO", "d": 41}, "l": 1200,
            "3": 12, "2": "", "4": "", "8": 0, "t": ["NN"],
            "_id": "FHVWGNRGWAFZIP-UHFFFAOYSA-N-OBKVRKGXIH"})

        # Insert new molecule record
        self._client.test.mol.insert({
            "z": [], "d": 1, "g": {
                "e": -4.48975, "d": 1, "i": None, "m": 1, "q": 1,
                "x": """\
1

H 0. 0. 0."""},
            "f": {"i": None, "q": 1, "s": "[H+]", "m": 1, "d": 1},
            "l": 1200, "3": 0, "2": "", "5": "", "4": "", "8": 0,
            "t": ["NN"], "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"})

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {"d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""},
            "z": [], "d": 18, "g": {
                "e": -322.6792, "d": 18, "i": None, "h": "PM7", "m": 1,
                "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                "x": """\
3

O    -0.000001     0.058320    -0.000000
H    -0.759932    -0.519445    -0.000000
H     0.760055    -0.519304     0.000000"""},
            "f": {"i": None, "s": "O", "d": 18}, "l": 1200, "3": 0,
            "2": "", "4": "", "8": 0, "t": ["NN"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_cache_data_mol(self):

        # Insert molecules into cache (generations 0--2)
        self._cache.set_blocking({
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': '-444.38128',
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': '-444.38128'})

    def _insert_cache_data_mol_error(self):

        # Insert molecules without energies into cache (generations 0--2)
        self._cache.set_blocking({
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': '',
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': ''})

    def _run_flaskreducertask(self, **kwargs):
        global _tmpmongo, _tmpmemcached, _opts, _logging_level

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'flask_reducer_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'FlaskReducerTask'}

        # Add storage hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Add cache hostname and port options
        if _tmpmemcached is not None:
            input_kv['cache_hostname'] = 'localhost'
            input_kv['cache_port'] = _tmpmemcached.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

        return calc

    def test_reducer_nocache(self):

        # Insert flask record
        self._insert_data_flask_NN0()

        # Insert molecule record
        self._insert_data_mol_NN0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertAlmostEqual(flask.energy, -888.76256, places=4)
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_nocache_tags(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_LL0()

        # Insert molecule records
        self._insert_data_mol_NN0()
        self._insert_data_mol_LL0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tags
        input_kv['tags'] = ['LL']

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.RUNNING)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.REACTIVE)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.precursor, None)
        self.assertListEqual(flask2.precursor_journal, [])
        self.assertAlmostEqual(flask2.energy, -944.10842, places=4)
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

    def test_reducer_nocache_multiple_mol(self):

        # Insert flask record
        self._insert_data_flask_NN1()

        # Insert molecule records
        self._insert_data_mol_NN0()
        self._insert_data_mol_NN1()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, '[CH2+][O-].C=O')
        self.assertEqual(flask.generation, 1)
        self.assertListEqual(flask.generation_journal, [1])
        self.assertEqual(flask.precursor, -888.76256)
        self.assertListEqual(flask.precursor_journal, [-888.76256])
        self.assertAlmostEqual(flask.energy, -888.76256, places=4)
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_nocache_noinput(self):

        # Run FlaskReducerTask
        self._run_flaskreducertask()

        # No flasks are available
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_reducer_nocache_incomplete(self):

        # Insert flask record
        self._insert_data_flask_NN1()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, '[CH2+][O-].C=O')
        self.assertEqual(flask.generation, 1)
        self.assertListEqual(flask.generation_journal, [1])
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_incomplete_retry(self):

        # Insert flask record
        self._insert_data_flask_NN1()

        # Insert molecule records, generation 1 is missing
        self._insert_data_mol_NN0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, '[CH2+][O-].C=O')
        self.assertEqual(flask.generation, 1)
        self.assertIsNone(flask.energy)
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_incomplete_unavailable(self):

        # Insert flask record
        self._insert_data_flask_NN1()

        # Insert molecule records, generation 1 is missing
        self._insert_data_mol_NN0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Number of attempts
        input_kv['reduce_attempts'] = 1

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"}))

        # Check flask result
        self.assertEqual(flask.status, Status.UNAVAILABLE)
        self.assertEqual(flask.smiles, '[CH2+][O-].C=O')
        self.assertEqual(flask.generation, 1)
        self.assertIsNone(flask.energy)
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_incomplete_retry_att3(self):

        # Insert flask record
        self._insert_data_flask_att3()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, '[CH2+][O-].[CH2+][O-]')
        self.assertEqual(flask.generation, 2)
        self.assertIsNone(flask.energy)
        self.assertEqual(flask.attempts, 4)

    def test_reducer_nocache_incomplete_unavailable_att3(self):

        # Insert flask record
        self._insert_data_flask_att3()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Number of attempts
        input_kv['reduce_attempts'] = 3

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"}))

        # Check flask result
        self.assertEqual(flask.status, Status.UNAVAILABLE)
        self.assertEqual(flask.smiles, '[CH2+][O-].[CH2+][O-]')
        self.assertEqual(flask.generation, 2)
        self.assertIsNone(flask.energy)
        self.assertEqual(flask.attempts, 4)

    def test_reducer_nocache_formula_retry(self):

        # Insert flask records
        self._insert_data_flask_NN0()

        # Insert molecule record
        self._insert_data_mol_formula()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_formula_unavailable(self):

        # Insert flask records
        self._insert_data_flask_NN0()

        # Insert molecule record
        self._insert_data_mol_formula()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Number of attempts
        input_kv['reduce_attempts'] = 1

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.UNAVAILABLE)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertEqual(flask.generation_journal, [0])
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_badchem(self):

        # Insert flask record
        self._insert_data_flask_badchem()

        # Insert molecule record
        self._insert_data_mol_NN0()
        self._insert_data_mol_badchem()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-WZTVMDNTKY"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_BADCHEM)
        self.assertEqual(flask.smiles, '[CH2][O].C=O')
        self.assertEqual(flask.generation, 2)
        self.assertEqual(flask.generation_journal, [2])
        self.assertEqual(flask.error, 'BadChemistry')
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_badchem_unavailable(self):

        # Insert flask record
        self._insert_data_flask_badchem_unavailable()

        # Insert molecule record
        self._insert_data_mol_badchem()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "REHUGJYJIZPQAV-UHFFFAOYSA-N-KGYXNESRFV"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_BADCHEM)
        self.assertEqual(flask.smiles, 'CO.[CH2][O]')
        self.assertEqual(flask.generation, 0)
        self.assertEqual(flask.generation_journal, [0])
        self.assertEqual(flask.error, 'BadChemistry')
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_error(self):

        # Insert flask record
        self._insert_data_flask_error()

        # Insert molecule record
        self._insert_data_mol_error()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.ERROR)
        self.assertEqual(flask.smiles, '[H+]')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.error, 'ExecutionError')
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_invalid(self):

        # Insert flask record
        self._insert_data_flask_invalid()

        # Insert molecule record
        self._insert_data_mol_invalid()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check flask result
        self.assertEqual(flask.status, Status.INVALID)
        self.assertEqual(flask.smiles, '[OH3+]')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.error, 'ValidationError')
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_noenergy(self):

        # Insert flask record
        self._insert_data_flask_L1()

        # Insert molecule records
        self._insert_data_mol_noenergy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-O-DBDCTMAYAP"}))

        # Check flask result
        self.assertEqual(flask.status, Status.ERROR)
        self.assertEqual(flask.smiles, '[H+].C[O-]')
        self.assertEqual(flask.generation, 1)
        self.assertListEqual(flask.generation_journal, [1])
        self.assertEqual(flask.error, 'ExecutionError')
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_nomaxgen(self):

        # Insert flask record
        self._insert_data_flask_NN4()

        # Insert molecule record
        self._insert_data_mol_NN4()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max generation
        input_kv['max_generation'] = 6

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, 'C1OCO1')
        self.assertEqual(flask.generation, 4)
        self.assertListEqual(flask.generation_journal, [4, 6])
        self.assertAlmostEqual(flask.precursor, -888.93517, places=4)
        self.assertEqual(len(flask.precursor_journal), 2)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.93517, places=4)
        self.assertAlmostEqual(
            flask.precursor_journal[1], -878.8571, places=4)
        self.assertAlmostEqual(flask.energy, -888.74776, places=4)
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_nocache_maxgen(self):

        # Insert flask record
        self._insert_data_flask_NN4()

        # Insert molecule record
        self._insert_data_mol_NN4()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max generation
        input_kv['max_generation'] = 4

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_MAXGEN)
        self.assertEqual(flask.smiles, 'C1OCO1')
        self.assertEqual(flask.generation, 4)
        self.assertListEqual(flask.generation_journal, [4, 6])
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_maxgen_failfast(self):

        # Insert new flask record
        self._insert_data_flask_ES3_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max generation
        input_kv['max_generation'] = 3

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_MAXGEN)
        self.assertEqual(flask.smiles, '[CH2-][C+]=O.CO.O')
        self.assertAlmostEqual(flask.energy, -1362.58152, places=4)
        self.assertEqual(flask.generation, 3)
        self.assertListEqual(flask.generation_journal, [3])
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['ES'])

    def test_reducer_nocache_nomaxen(self):

        # Insert flask records
        self._insert_data_flask_NN13()

        # Insert molecule record
        self._insert_data_mol_NN13()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max relative energy
        input_kv['max_rel_energy'] = 12.0

        # Reference flask
        input_kv['ref_energy'] = -888.76256

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertEqual(flask.generation, 13)
        self.assertListEqual(flask.generation_journal, [13, 15])
        self.assertAlmostEqual(flask.precursor, -888.53291, places=4)
        self.assertEqual(len(flask.precursor_journal), 2)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.53291, places=4)
        self.assertAlmostEqual(
            flask.precursor_journal[1], -883.78013, places=4)
        self.assertAlmostEqual(flask.energy, -878.40362, places=4)
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_nocache_maxen(self):

        # Insert flask records
        self._insert_data_flask_NN13()

        # Insert molecule record
        self._insert_data_mol_NN13()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max relative energy
        input_kv['max_rel_energy'] = 6.0

        # Reference flask
        input_kv['ref_energy'] = -888.76256

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_HIENERGY)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertEqual(flask.generation, 13)
        self.assertListEqual(flask.generation_journal, [13, 15])
        self.assertAlmostEqual(flask.energy, -878.40362, places=4)
        self.assertEqual(flask.attempts, 1)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_nocache_nomaxenchange(self):

        # Insert flask record
        self._insert_data_flask_NN13()

        # Insert molecule record
        self._insert_data_mol_NN13()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertEqual(flask.generation, 13)
        self.assertListEqual(flask.generation_journal, [13, 15])
        self.assertAlmostEqual(flask.precursor, -888.53291, places=4)
        self.assertEqual(len(flask.precursor_journal), 2)
        self.assertAlmostEqual(
            flask.precursor_journal[0], -888.53291, places=4)
        self.assertAlmostEqual(
            flask.precursor_journal[1], -883.78013, places=4)
        self.assertAlmostEqual(flask.energy, -878.40362, places=4)
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_nocache_maxenchange(self):

        # Insert flask record
        self._insert_data_flask_NN13()

        # Insert molecule record
        self._insert_data_mol_NN13()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max energy change
        input_kv['max_energy_change'] = 3

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_HIBARRIER)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertEqual(flask.generation, 13)
        self.assertListEqual(flask.generation_journal, [13, 15])
        self.assertEqual(flask.attempts, 1)

    def test_reducer_nocache_maxenchange_failfast(self):

        # Insert new flask record
        self._insert_data_flask_ES3_energy()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Max energy change
        input_kv['max_energy_change'] = -4.0

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_HIBARRIER)
        self.assertEqual(flask.smiles, '[CH2-][C+]=O.CO.O')
        self.assertAlmostEqual(flask.energy, -1362.58152, places=4)
        self.assertAlmostEqual(flask.energy_change, -1.36682, places=4)
        self.assertAlmostEqual(flask.precursor, -1361.2147, places=4)
        self.assertEqual(flask.generation, 3)
        self.assertListEqual(flask.generation_journal, [3])
        self.assertEqual(flask.error, '')
        self.assertListEqual(flask.tags, ['ES'])

    def test_reducer_nocache_batch1_nonapi(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Insert molecule records
        self._insert_data_mol_NN0()
        self._insert_data_mol_NN1()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tasks per calc
        input_kv['tasks_per_calc'] = 3

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 1

        # Batch size
        input_kv['batch_size'] = 1

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskReducerTask
        calc = self._run_flaskreducertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 3)

        # Retrieve completed task
        task1 = calc._completed_tasks[0]

        # Input molecules
        input_molecules1 = [f.key for f in task1._data.molecules]
        self.assertListEqual(
            input_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'] * 2)

        # Molecules retrieved from storage
        storage_molecules1 = task1._molecules.keys()
        self.assertListEqual(
            storage_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from cache
        cache_molecules1 = list(task1._cache_molecules)
        self.assertListEqual(cache_molecules1, [])

        # Retrieve completed task
        task2 = calc._completed_tasks[1]

        # Input molecules
        input_molecules2 = [f.key for f in task2._data.molecules]
        self.assertListEqual(
            input_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from storage
        storage_molecules2 = task2._molecules.keys()
        self.assertListEqual(
            storage_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                 'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from cache
        cache_molecules2 = list(task2._cache_molecules)
        self.assertListEqual(cache_molecules2, [])

        # Retrieve completed task
        task3 = calc._completed_tasks[2]

        # Input molecules
        input_molecules3 = [f.key for f in task3._data.molecules]
        self.assertListEqual(
            input_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules retrieved from storage or cache
        storage_molecules3 = task3._molecules.keys()
        self.assertListEqual(
            storage_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'])

        # Molecules retrieved from cache
        cache_molecules3 = list(task3._cache_molecules)
        self.assertListEqual(cache_molecules3, [])

    def test_reducer_nocache_batch2_nonapi(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Insert molecule records
        self._insert_data_mol_NN0()
        self._insert_data_mol_NN1()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tasks per calc
        input_kv['tasks_per_calc'] = 3

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 2

        # Batch size
        input_kv['batch_size'] = 2

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskReducerTask
        calc = self._run_flaskreducertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 2)

        # Retrieve completed task
        task1 = calc._completed_tasks[0]

        # Input molecules
        input_molecules1 = [f.key for f in task1._data.molecules]
        self.assertListEqual(
            input_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from storage
        storage_molecules1 = task1._molecules.keys()
        self.assertListEqual(
            storage_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                 'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from cache
        cache_molecules1 = list(task1._cache_molecules)
        self.assertListEqual(cache_molecules1, [])

        # Retrieve completed task
        task2 = calc._completed_tasks[1]

        # Input molecules
        input_molecules2 = [f.key for f in task2._data.molecules]
        self.assertListEqual(
            input_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules retrieved from storage or cache
        storage_molecules2 = task2._molecules.keys()
        self.assertListEqual(
            storage_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'])

        # Molecules retrieved from cache
        cache_molecules2 = list(task2._cache_molecules)
        self.assertListEqual(cache_molecules2, [])

    def test_reducer_nocache_batch3_nonapi(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Insert molecule records
        self._insert_data_mol_NN0()
        self._insert_data_mol_NN1()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 3

        # Batch size
        input_kv['batch_size'] = 3

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskReducerTask
        calc = self._run_flaskreducertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 1)

        # Retrieve completed task
        task = calc._completed_tasks[0]

        # Input molecules
        input_molecules = [f.key for f in task._data.molecules]
        self.assertListEqual(
            input_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules retrieved from storage or cache
        storage_molecules = task._molecules.keys()
        self.assertListEqual(
            storage_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from cache
        cache_molecules = list(task._cache_molecules)
        self.assertListEqual(cache_molecules, [])

    def test_reducer_cache(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask record
        self._insert_data_flask_NN0()

        # Insert molecule data into cache
        self._insert_cache_data_mol()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = True

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertAlmostEqual(flask.energy, -888.76256, places=4)
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_cache_error(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask record
        self._insert_data_flask_NN0()

        # Insert molecule record
        self._insert_data_mol_NN0()

        # Insert molecule data without energies into cache
        self._insert_cache_data_mol_error()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = True

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.attempts, 1)

    def test_reducer_cache_cleared(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask record
        self._insert_data_flask_NN0()

        # Insert molecule data without energies into cache
        self._insert_cache_data_mol_error()

        # Expire erroneous cached data
        self._cache.clear()

        # Insert molecule record
        self._insert_data_mol_NN0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = True

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertAlmostEqual(flask.energy, -888.76256, places=4)
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_reducer_cache_batch1_nonapi(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Insert molecule data into cache
        self._insert_cache_data_mol()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = True

        # Tasks per calc
        input_kv['tasks_per_calc'] = 3

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 1

        # Batch size
        input_kv['batch_size'] = 1

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskReducerTask
        calc = self._run_flaskreducertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 3)

        # Retrieve completed task
        task1 = calc._completed_tasks[0]

        # Input molecules
        input_molecules1 = [f.key for f in task1._data.molecules]
        self.assertListEqual(
            input_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'] * 2)

        # Molecules retrieved from storage or cache
        storage_molecules1 = task1._molecules.keys()
        self.assertListEqual(
            storage_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from cache
        cache_molecules1 = list(task1._cache_molecules)
        self.assertListEqual(
            cache_molecules1, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Retrieve completed task
        task2 = calc._completed_tasks[1]

        # Input molecules
        input_molecules2 = [f.key for f in task2._data.molecules]
        self.assertListEqual(
            input_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from storage or cache
        storage_molecules2 = task2._molecules.keys()
        self.assertListEqual(
            storage_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                 'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from cache
        cache_molecules2 = list(task2._cache_molecules)
        self.assertListEqual(
            cache_molecules2, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                               'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Retrieve completed task
        task3 = calc._completed_tasks[2]

        # Input molecules
        input_molecules3 = [f.key for f in task3._data.molecules]
        self.assertListEqual(
            input_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules retrieved from storage or cache
        storage_molecules3 = task3._molecules.keys()
        self.assertListEqual(
            storage_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'])

        # Molecules retrieved from cache
        cache_molecules3 = list(task3._cache_molecules)
        self.assertListEqual(
            cache_molecules3, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'])

    def test_reducer_cache_batch3_nonapi(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN2()

        # Insert molecule data into cache
        self._insert_cache_data_mol()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = True

        # Tasks per cycle
        input_kv['tasks_per_cycle'] = 3

        # Batch size
        input_kv['batch_size'] = 3

        # Save completed tasks
        input_kv['save_completed_tasks'] = True

        # Run FlaskReducerTask
        calc = self._run_flaskreducertask(**input_kv)

        # Number of completed tasks
        self.assertEqual(len(calc._completed_tasks), 1)

        # Retrieve completed task
        task = calc._completed_tasks[0]

        # Input molecules
        input_molecules = [f.key for f in task._data.molecules]
        self.assertListEqual(
            input_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU'] * 2)

        # Molecules retrieved from storage or cache
        storage_molecules = task._molecules.keys()
        self.assertListEqual(
            storage_molecules, ['WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU',
                                'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'])

        # Molecules retrieved from cache
        cache_molecules = list(task._cache_molecules)
        self.assertListEqual(cache_molecules, [])

    def test_reducer_nocache_batchsize(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_LL0()

        # Insert molecule records
        self._insert_data_mol_NN0()
        self._insert_data_mol_LL0()

        # Dictionary of input options
        input_kv = {}

        # Disable cache
        input_kv['with_cache'] = False

        # Batch size
        input_kv['batch_size'] = 2

        # Run FlaskReducerTask
        self._run_flaskreducertask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.REACTIVE)
        self.assertEqual(flask1.smiles, 'C=O.C=O')

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.RUNNING)
        self.assertEqual(flask2.smiles, 'CO.CO')

    def test_reducer_rollback(self):
        global _opts

        # Insert flask record
        self._insert_data_flask_NN0()

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'flask_reducer_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'FlaskReducerTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Batch size
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Locks
        input_kv['optimistic_lock'] = False
        input_kv['pessimistic_lock'] = True

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create Context object
        context = Context(opts)

        # Create new FlaskReducerTask
        task = colibri.task.FlaskReducerTask(context, opts)

        # Setup
        task.setup()

        # Retrieve
        task.retrieve()

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.RUNNING_LOCKED)
        self.assertEqual(flask1.etag, calc_name)
        self.assertEqual(flask1.smiles, 'C=O.C=O')

        # Read running flask
        task._read_running_flask()

        # Rollback
        task.rollback()

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.RUNNING)
        self.assertEqual(flask2.etag, '')
        self.assertEqual(flask2.smiles, 'C=O.C=O')


class TestFlaskCleanupTask(unittest.TestCase):
    """Test classes for FlaskCleanupTask."""

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.flask.remove({})

    def _insert_data_locked(self):

        # Insert INITIAL_LOCKED records
        self._client.test.flask.insert({
            "d": 1800, "k": [
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"},
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}],
            "j": 2001, "m": 1, "s": "C=O.C=O", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "5": "flask_mapper_WViqDy",
            "t": ["NN"], "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"})

        self._client.test.flask.insert({
            "d": 2048, "k": [
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"},
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}],
            "j": 2001, "m": 1, "s": "CO.CO", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "5": "flask_mapper_WViqDy",
            "t": ["LL"], "_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"})

        # Insert RUNNING_LOCKED record
        self._client.test.flask.insert({
            "b": [{"1": "[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]",
                   "p": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK",
                   "r": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-RDGEAJXOVL"}],
            "d": 8488, "3*": [8], "k": [
                {"s": "C=C=O",
                 "_id": "CCGKOQOJPYTBIH-UHFFFAOYSA-N-ACYGJWNMWD"},
                {"s": "O", "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2101, "m": 1, "s": "C=C=O.O", "q": 0, "0": -889.86602,
            "3": 8, "2": "", "4": "", "5": "flask_reducer_f4Ssb5",
            "0*": [-889.86602], "8": 0,
            "t": ["NN"], "_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"})

        # Insert REACTIVE_LOCKED record
        self._client.test.flask.insert({
            "9": -9.89066, "b": [
                {"1": "([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"},
                {"1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR"}],
            "e": -888.74776, "d": 5200, "3*": [4, 6],
            "k": [
                {"s": "C1OCO1",
                 "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}],
            "j": 2201, "m": 1, "s": "C1OCO1", "q": 0, "0": -878.8571,
            "3": 4, "2": "", "4": "", "0*": [-888.93686, -878.8571],
            "8": 0, "5": "rdkit_flask_react_Turnyt", "t": ["NN"],
            "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"})

    def _insert_data_unavailable(self):

        # Insert UNAVAILABLE record
        self._client.test.flask.insert({
            "9": 0.0, "b": [
                {"1": "[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]",
                 "p": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE",
                 "r": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"},
                {"1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                 "p": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE",
                 "r": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}],
            "e": -888.76256, "d": 2200, "3*": [2, 4],
            "k": [
                {"s": "[CH2+][O-]",
                 "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"},
                {"s": "[CH2+][O-]",
                 "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"}],
            "j": 2310, "m": 1, "s": "[CH2+][O-].[CH2+][O-]",
            "q": 0, "0": -888.76256, "3": 2, "2": "", "4": "",
            "0*": [-888.76256, -888.93686], "8": 0, "t": ["NN"],
            "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"})

    def _insert_data_maxgen(self):

        # Insert REACTIVE_MAXGEN record
        self._client.test.flask.insert(
            Flask(smiles='C1OCO1', incoming=[{
                'reactant': 'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX',
                'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOX',
                'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'},
                {'reactant': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                 'product': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                 'rule': '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]'}],
                outgoing=[{
                    'reactant': 'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD',
                    'product': 'QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR',
                    'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'}],
                generation=4, generation_journal=[4, 6], precursor=-888.93517,
                precursor_journal=[-888.93517, -878.8571], status=2212,
                tags=['NN']).todict())

    def _insert_data_maxenchange(self):

        # Insert REACTIVE_HIBARRIER record
        self._client.test.flask.insert(
            Flask(smiles='[C-]#CO.[H+].O', incoming=[{
                'reactant': 'MCIDLSNMEGIFCA-UHFFFAOYSA-N-UMUTKHSCDS',
                'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'},
                {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-POAJXMCVAO',
                 'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                 'rule': '[O+1:1]-[#1+0:2]>>[O+0:1].[#1+1:2]'}],
                outgoing=[{
                    'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                    'product': 'PXNRYTDTQNVFKV-UHFFFAOYSA-N-XYZTPCCCCS',
                    'rule': '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]'},
                    {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                     'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EQJNDBMSPY',
                     'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'},
                    {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                     'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-PDNVFABJBM',
                     'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'},
                    {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                     'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-POAJXMCVAO',
                     'rule': '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'},
                    {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                     'product': 'MCIDLSNMEGIFCA-UHFFFAOYSA-N-UMUTKHSCDS',
                     'rule': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'},
                    {'reactant': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN',
                     'product': 'WUFJODFZIXDDAD-UHFFFAOYSA-O-JJWWSAZBCQ',
                     'rule': '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'}],
                generation=13, generation_journal=[13, 15],
                precursor=-888.53291, status=2211,
                precursor_journal=[-888.53291, -883.78013],
                energy_change=10.12929, energy=-878.40362,
                tags=['NN']).todict())

    def _run_flaskcleanuptask(self, **kwargs):
        global _opts, _logging_level

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'flask_cleanup_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'FlaskCleanupTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1

        # Cleanup interval
        input_kv['cleanup_interval'] = 0

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_cleanup_locked(self):

        # Insert data
        self._insert_data_locked()

        # Run FlaskCleanupTask
        self._run_flaskcleanuptask()

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING)
        self.assertEqual(flask3.smiles, 'C=C=O.O')
        self.assertEqual(flask3.generation, 8)
        self.assertListEqual(flask3.generation_journal, [8])
        self.assertEqual(flask3.attempts, 0)
        self.assertListEqual(flask3.tags, ['NN'])

        # Get flask results from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.REACTIVE)
        self.assertEqual(flask4.smiles, 'C1OCO1')
        self.assertEqual(flask4.generation, 4)
        self.assertListEqual(flask4.generation_journal, [4, 6])
        self.assertEqual(flask4.attempts, 0)
        self.assertListEqual(flask4.tags, ['NN'])

    def test_cleanup_noinput(self):

        # Run FlaskCleanupTask with no input
        self._run_flaskcleanuptask()

        # No molecules present in the database
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_cleanup_locked_tags(self):

        # Insert data
        self._insert_data_locked()

        # Input options dict
        input_kv = {}

        # Tags
        input_kv['tags'] = ['NN']

        # Run FlaskCleanupTask
        self._run_flaskcleanuptask(**input_kv)

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL_LOCKED)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING)
        self.assertEqual(flask3.smiles, 'C=C=O.O')
        self.assertEqual(flask3.generation, 8)
        self.assertListEqual(flask3.generation_journal, [8])
        self.assertEqual(flask3.attempts, 0)
        self.assertListEqual(flask3.tags, ['NN'])

        # Get flask results from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.REACTIVE)
        self.assertEqual(flask4.smiles, 'C1OCO1')
        self.assertEqual(flask4.generation, 4)
        self.assertListEqual(flask4.generation_journal, [4, 6])
        self.assertEqual(flask4.attempts, 0)
        self.assertListEqual(flask4.tags, ['NN'])

    def test_cleanup_unavailable(self):

        # Insert data
        self._insert_data_unavailable()

        # Run FlaskCleanupTask
        self._run_flaskcleanuptask()

        # Get flask results from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, '[CH2+][O-].[CH2+][O-]')
        self.assertEqual(flask.generation, 2)
        self.assertListEqual(flask.generation_journal, [2, 4])
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_cleanup_maxgen(self):

        # Insert data
        self._insert_data_maxgen()

        # Input options dict
        input_kv = {}

        # Max generation
        input_kv['max_generation'] = 3

        # Run FlaskCleanupTask
        self._run_flaskcleanuptask(**input_kv)

        # Get flask results from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_MAXGEN)
        self.assertEqual(flask.smiles, 'C1OCO1')
        self.assertEqual(flask.generation, 4)
        self.assertListEqual(flask.generation_journal, [4, 6])
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_cleanup_nomaxgen(self):

        # Insert data
        self._insert_data_maxgen()

        # Input options dict
        input_kv = {}

        # Max generation
        input_kv['max_generation'] = 5

        # Run FlaskCleanupTask
        self._run_flaskcleanuptask(**input_kv)

        # Get flask results from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, 'C1OCO1')
        self.assertEqual(flask.generation, 4)
        self.assertListEqual(flask.generation_journal, [4, 6])
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_cleanup_maxenchange(self):

        # Insert data
        self._insert_data_maxenchange()

        # Input options dict
        input_kv = {}

        # Max energy change
        input_kv['max_energy_change'] = 12

        # Run FlaskCleanupTask
        self._run_flaskcleanuptask(**input_kv)

        # Get flask results from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertEqual(flask.generation, 13)
        self.assertListEqual(flask.generation_journal, [13, 15])
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])

    def test_cleanup_nomaxenchange(self):

        # Insert data
        self._insert_data_maxenchange()

        # Input options dict
        input_kv = {}

        # Max energy change
        input_kv['max_energy_change'] = 3

        # Run FlaskCleanupTask
        self._run_flaskcleanuptask(**input_kv)

        # Get flask results from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "WUFJODFZIXDDAD-UHFFFAOYSA-O-EYQFGKICWN"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE_HIBARRIER)
        self.assertEqual(flask.smiles, '[C-]#CO.[H+].O')
        self.assertEqual(flask.generation, 13)
        self.assertListEqual(flask.generation_journal, [13, 15])
        self.assertEqual(flask.attempts, 0)
        self.assertListEqual(flask.tags, ['NN'])


class TestFlaskSuspendTask(unittest.TestCase):
    """Test classes for FlaskSuspendTask."""

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.flask.remove({})

    def _insert_data(self):

        # Insert INITIAL records
        self._client.test.flask.insert({
            "d": 1800, "k": [
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"},
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}],
            "j": 2000, "m": 1, "s": "C=O.C=O", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "t": ["NN"],
            "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"})

        self._client.test.flask.insert({
            "d": 2048, "k": [
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"},
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}],
            "j": 2000, "m": 1, "s": "CO.CO", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "t": ["LL"],
            "_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"})

        # Insert RUNNING record
        self._client.test.flask.insert({
            "b": [{"1": "[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]",
                   "p": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK",
                   "r": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-RDGEAJXOVL"}],
            "d": 8488, "3*": [8], "k": [
                {"s": "C=C=O",
                 "_id": "CCGKOQOJPYTBIH-UHFFFAOYSA-N-ACYGJWNMWD"},
                {"s": "O", "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2100, "m": 1, "s": "C=C=O.O", "q": 0, "0": -889.86602,
            "3": 8, "2": "", "4": "", "5": "flask_reducer_f4Ssb5",
            "0*": [-889.86602], "8": 0,
            "_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"})

        # Insert REACTIVE record
        self._client.test.flask.insert({
            "9": -9.89066, "b": [
                {"1": "([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"},
                {"1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR"}],
            "e": -888.74776, "d": 5200, "3*": [4, 6],
            "k": [
                {"s": "C1OCO1",
                 "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}],
            "j": 2200, "m": 1, "s": "C1OCO1", "q": 0, "0": -878.8571,
            "3": 4, "2": "", "4": "", "0*": [-888.93686, -878.8571],
            "8": 0, "t": ["NN"],
            "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"})

    def _run_flasksuspendtask(self, **kwargs):
        global _opts, _logging_level

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'flask_suspend_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'FlaskSuspendTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1

        # Cleanup interval
        input_kv['suspend_interval'] = 0

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_suspend(self):

        # Insert flask records
        self._insert_data()

        # Run FlaskSuspendTask
        self._run_flasksuspendtask()

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL_SUSPENDED)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL_SUSPENDED)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING_SUSPENDED)
        self.assertEqual(flask3.smiles, 'C=C=O.O')
        self.assertEqual(flask3.generation, 8)
        self.assertListEqual(flask3.generation_journal, [8])
        self.assertEqual(flask3.attempts, 0)
        self.assertListEqual(flask3.tags, [])

        # Get flask results from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.REACTIVE_SUSPENDED)
        self.assertEqual(flask4.smiles, 'C1OCO1')
        self.assertEqual(flask4.generation, 4)
        self.assertListEqual(flask4.generation_journal, [4, 6])
        self.assertEqual(flask4.attempts, 0)
        self.assertListEqual(flask4.tags, ['NN'])

    def test_suspend_noinput(self):

        # Run FlaskSuspendTask
        self._run_flasksuspendtask()

        # No molecules present in the database
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_suspend_tags(self):

        # Insert flask records
        self._insert_data()

        # Input options dict
        input_kv = {}

        # Tags
        input_kv['tags'] = ['LL']

        # Run FlaskSuspendTask
        self._run_flasksuspendtask(**input_kv)

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL_SUSPENDED)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING)
        self.assertEqual(flask3.smiles, 'C=C=O.O')
        self.assertEqual(flask3.generation, 8)
        self.assertListEqual(flask3.generation_journal, [8])
        self.assertEqual(flask3.attempts, 0)
        self.assertListEqual(flask3.tags, [])

        # Get flask results from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.REACTIVE)
        self.assertEqual(flask4.smiles, 'C1OCO1')
        self.assertEqual(flask4.generation, 4)
        self.assertListEqual(flask4.generation_journal, [4, 6])
        self.assertEqual(flask4.attempts, 0)
        self.assertListEqual(flask4.tags, ['NN'])


class TestFlaskResumeTask(unittest.TestCase):
    """Test cases for FlaskResumeTask."""

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.flask.remove({})

    def _insert_data(self):

        # Insert INITIAL records
        self._client.test.flask.insert({
            "d": 1800, "k": [
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"},
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}],
            "j": 2002, "m": 1, "s": "C=O.C=O", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "t": ["NN"],
            "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"})

        self._client.test.flask.insert({
            "d": 2048, "k": [
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"},
                {"s": "CO", "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}],
            "j": 2002, "m": 1, "s": "CO.CO", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "t": ["LL"],
            "_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"})

        # Insert RUNNING record
        self._client.test.flask.insert({
            "b": [{"1": "[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]",
                   "p": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK",
                   "r": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-RDGEAJXOVL"}],
            "d": 8488, "3*": [8], "k": [
                {"s": "C=C=O",
                 "_id": "CCGKOQOJPYTBIH-UHFFFAOYSA-N-ACYGJWNMWD"},
                {"s": "O", "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2102, "m": 1, "s": "C=C=O.O", "q": 0, "0": -889.86602,
            "3": 8, "2": "", "4": "", "5": "flask_reducer_f4Ssb5",
            "0*": [-889.86602], "8": 0,
            "_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"})

        # Insert REACTIVE record
        self._client.test.flask.insert({
            "9": -9.89066, "b": [
                {"1": "([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"},
                {"1": "[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]",
                 "p": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD",
                 "r": "QZPLICXSKMZRMY-UHFFFAOYSA-O-JDPMNCOSDR"}],
            "e": -888.74776, "d": 5200, "3*": [4, 6],
            "k": [
                {"s": "C1OCO1",
                 "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}],
            "j": 2202, "m": 1, "s": "C1OCO1", "q": 0, "0": -878.8571,
            "3": 4, "2": "", "4": "", "0*": [-888.93686, -878.8571],
            "8": 0, "t": ["NN"],
            "_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"})

    def _run_flaskresumetask(self, **kwargs):
        global _opts, _logging_level

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'flask_resume_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'FlaskResumeTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1

        # Cleanup interval
        input_kv['resume_interval'] = 0

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_resume(self):

        # Insert flask records
        self._insert_data()

        # Run FlaskResumeTask
        self._run_flaskresumetask()

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING)
        self.assertEqual(flask3.smiles, 'C=C=O.O')
        self.assertEqual(flask3.generation, 8)
        self.assertListEqual(flask3.generation_journal, [8])
        self.assertEqual(flask3.attempts, 0)
        self.assertListEqual(flask3.tags, [])

        # Get flask results from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.REACTIVE)
        self.assertEqual(flask4.smiles, 'C1OCO1')
        self.assertEqual(flask4.generation, 4)
        self.assertListEqual(flask4.generation_journal, [4, 6])
        self.assertEqual(flask4.attempts, 0)
        self.assertListEqual(flask4.tags, ['NN'])

    def test_resume_noinput(self):

        # Run FlaskResumeTask
        self._run_flaskresumetask()

        # No molecules present in the database
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_resume_tags(self):

        # Insert flask records
        self._insert_data()

        # Input options dict
        input_kv = {}

        # Tags
        input_kv['tags'] = ['LL']

        # Run FlaskResumeTask
        self._run_flaskresumetask(**input_kv)

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL_SUSPENDED)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertEqual(flask2.attempts, 0)
        self.assertListEqual(flask2.tags, ['LL'])

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING_SUSPENDED)
        self.assertEqual(flask3.smiles, 'C=C=O.O')
        self.assertEqual(flask3.generation, 8)
        self.assertListEqual(flask3.generation_journal, [8])
        self.assertEqual(flask3.attempts, 0)
        self.assertListEqual(flask3.tags, [])

        # Get flask results from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.REACTIVE_SUSPENDED)
        self.assertEqual(flask4.smiles, 'C1OCO1')
        self.assertEqual(flask4.generation, 4)
        self.assertListEqual(flask4.generation_journal, [4, 6])
        self.assertEqual(flask4.attempts, 0)
        self.assertListEqual(flask4.tags, ['NN'])


if __name__ == '__main__':
    unittest.main()
