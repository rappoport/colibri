"""Test classes for the orcatask classes.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for orcatask classes

Test Classes:
    TestOrcaGeometryTask: Geometry task using Orca
    TestOrcaPropertyTask: Property task using Orca
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/31/2015'

#
# Standard library
#


import unittest
import logging


#
# colibri modules
#

from colibri.data import Molecule
from colibri.calc import Calculator, Context
from colibri.enums import Level
from colibri.utils import t2s, randomid
from colibri.exceptions import Structure3DError
import colibri.task

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_orcatask')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'


#
# Public functions
#


def get_suite():
    """Construct test suite for orcatask classes."""

    orcatask_suite = []

    # OrcaGeometryTask tests
    orca_geometry_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestOrcaGeometryTask)
    orcatask_suite.append(orca_geometry_suite)

    # OrcaPropertyTask tests
    orca_property_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestOrcaPropertyTask)
    orcatask_suite.append(orca_property_suite)

    return unittest.TestSuite(orcatask_suite)

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()


#
# Test classes
#


class TestOrcaGeometryTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_f(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 19, "i": None, "m": 1, "q": -1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
1

F          0.00000        0.00000        0.00000"""},
            "z": [], "d": 19, "f": {"i": None, "s": "[F-]", "d": 19},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "KRHYYFGTRYWZRS-UHFFFAOYSA-M-ZHCLDYPIPM"})

    def _insert_data_h2o(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""},
            "z": [], "d": 18,
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0, "t": ["oxygen"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_nh3(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17, "f": {"i": None, "s": "N", "d": 17},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0, "t": ["nitrogen"],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

    def _insert_data_proton_error(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 1, "i": None, "m": 1, "q": 1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
1

H          0.00000        0.00000        0.00000"""},
            "z": [], "d": 1, "f": {"i": None, "s": "[H+]", "d": 1},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"})

    def _insert_data_NN4_invalid_fragmentation(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 60, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
8

O         -1.44522        0.55657        0.13517
C         -0.73236       -0.58238       -0.23959
C          0.67590       -0.48147        0.25039
O          1.39884        0.42309       -0.14176
H         -0.73860       -0.67518       -1.34588
H         -1.21861       -1.47796        0.20032
H          1.05324       -1.21925        0.95474
H          2.31281        0.49873        0.16667"""},
            "z": [], "d": 60, "f": {"i": None, "s": "[O-]CC=[OH+]", "d": 60},
            "l": 1100, "3": 4, "2": "", "4": "", "8": 2, "t": ["NN"],
            "_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"})

    def _insert_data_methyloxonium(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 33, "i": None, "m": 1, "q": 1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
7

C         -0.74681       -0.00000       -0.01233
O          0.65351       -0.00000        0.07134
H         -1.15309       -0.90031        0.49724
H         -1.08520        0.00007       -1.07399
H         -1.15307        0.90028        0.49729
H          0.95849       -0.78531       -0.45307
H          0.95852        0.78535       -0.45298"""},
            "z": [], "d": 33, "f": {"i": None, "s": "C[OH2+]", "d": 33},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "OKKJLVBELUTLKV-UHFFFAOYSA-O-GNQMJBYIZY"})

    def _insert_data_bicycle_invalid_rearrangement(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 64, "i": None, "+": 0, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
9

C         -1.02078       -0.86929        0.00000
C          0.25857       -0.69275       -0.00001
C          1.45272       -0.01508        0.00000
C          0.27083        0.68383       -0.00000
C         -1.00521        0.88314       -0.00001
H         -1.72913       -1.68666        0.00006
H          1.97531        0.04732        0.97674
H          1.97533        0.04725       -0.97665
H         -1.69889        1.71294        0.00004"""},
            "z": [], "d": 64, "f": {
                "i": None, "s": "C1=C2CC2=C1", "d": 64, "+": 0},
            "+": 0, "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"})

    def _insert_data_ammonium_badxyz(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 18, "i": None, "m": 1, "q": 1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

N          0.00001        0.00000        0.00000
H         -0.76151        0.40218       -0.59098
H         -0.76151        0.40218       -0.59098
H         -0.00289        0.46483        0.93523"""},
            "z": [], "d": 18, "f": {"i": None, "s": "[NH4+]", "d": 18},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-O-WKXUEGBFRF"})

    def _insert_data_sucrose(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 342, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
45

O         -2.43177        3.43568       -0.31152
C         -1.86961        2.61954        0.68097
C         -1.76133        1.15945        0.20981
O         -0.98968        0.45246        1.16936
C         -0.73178       -0.88421        0.76575
O          0.02660       -0.91860       -0.43921
C          1.44018       -0.88636       -0.23829
C          1.97581       -2.33820       -0.14547
O          1.57853       -2.94392        1.05286
O          1.80401       -0.15758        0.92165
C          3.05737        0.44594        0.68074
C          3.09498        1.82674        1.34556
O          2.05000        2.62815        0.86681
C          3.31254        0.51384       -0.83227
O          4.48395       -0.19351       -1.15159
C          2.09348       -0.16791       -1.43312
O          1.22688        0.77936       -2.01403
C         -2.04389       -1.67691        0.59737
O         -1.76992       -2.96795        0.11673
C         -2.99053       -0.95740       -0.37438
O         -4.24712       -1.58504       -0.37077
C         -3.15547        0.51104        0.04680
O         -3.89062        1.19777       -0.93419
H         -1.72689        3.57752       -0.99627
H         -2.49966        2.67643        1.59416
H         -0.85872        3.00124        0.95271
H         -1.24507        1.16760       -0.77529
H         -0.19771       -1.38035        1.60473
H          1.58816       -2.92772       -1.00704
H          3.08770       -2.34070       -0.19657
H          1.92750       -3.87249        1.02290
H          3.83889       -0.18008        1.16825
H          2.99137        1.70000        2.44659
H          4.07427        2.31408        1.13862
H          2.13040        3.49695        1.33935
H          3.40403        1.56697       -1.19207
H          4.70464        0.03213       -2.09299
H          2.38513       -0.88685       -2.23158
H          1.00806        1.45962       -1.32490
H         -2.54955       -1.74624        1.59010
H         -1.38694       -3.48076        0.87576
H         -2.55593       -0.98407       -1.40297
H         -4.16880       -2.39086       -0.94531
H         -3.68631        0.55639        1.02830
H         -4.84947        1.00416       -0.76491"""},
            "z": [], "d": 342, "f": {
                "i": None,  "d": 342,
                "s": ("OC[C@H]1O[C@H](O[C@]2(CO)O[C@H](CO)[C@@H]" +
                      "(O)[C@@H]2O)[C@H](O)[C@@H](O)[C@@H]1O")},
                "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "CZMRCDWAGMRECN-UGDNZRGBSA-N-VOXHORWGWB"})

    def _run_orcageometrytask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'orca_geometry_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'OrcaGeometryTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Orca options
        input_kv['geometry_validation'] = True
        input_kv['geometry_attempts'] = 5

        # Geometry options for Orca
        input_kv['geometry_options_orca'] = {
            'key': 'geometry_options',
            'value': 'am1',
            'target': 'colibri.task.OrcaGeometryTask',
            'precedence': 'command'}

        # Geometry method
        input_kv['geometry_method_orca'] = {
            'key': 'geometry_method',
            'value': 'PM6 is not available',
            'target': 'colibri.task.OrcaGeometryTask',
            'precedence': 'command'}

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_orca_geometry(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Run OrcaGeometryTask
        self._run_orcageometrytask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -12.8095, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)
        self.assertListEqual(mol.tags, ['oxygen'])

    def test_orca_noinput(self):

        # Run OrcaGeometryTask without input
        self._run_orcageometrytask()

        # No molecules were produced
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_orca_tags(self):

        # Insert molecule records
        self._insert_data_h2o()
        self._insert_data_nh3()

        # Input options dict
        input_kv = {}

        # Tags
        input_kv['tags'] = 'nitrogen'

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.CONFIGURATION)
        self.assertEqual(mol1.configuration.charge, 0)
        self.assertEqual(mol1.configuration.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)
        self.assertListEqual(mol1.tags, ['oxygen'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.smiles, 'N')
        self.assertAlmostEqual(mol2.geometry.energy, -9.1356, places=4)
        self.assertTrue(mol2.converged)
        self.assertEqual(mol2.priority, 17)
        self.assertListEqual(mol2.tags, ['nitrogen'])

    def test_orca_atom(self):

        # Insert new molecule record
        self._insert_data_f()

        # Run OrcaGeometryTask
        self._run_orcageometrytask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "KRHYYFGTRYWZRS-UHFFFAOYSA-M-ZHCLDYPIPM"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.geometry.charge, -1)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -17.7485, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.formula.smiles, '[F-]')
        self.assertEqual(mol.priority, 19)

    def test_orca_raw(self):

        # Insert molecule records
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Raw write
        input_kv['raw_write'] = True

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.GEOMETRY)

        # Raw output key
        raw_output_key = mol.geometry.raw_output
        self.assertTrue(raw_output_key)

        # Raw output file metadata
        raw_output_metadata = self._client.test.raw.files.find_one(
            {'_id': raw_output_key})

        # Check molecule key
        self.assertEqual(
            raw_output_metadata['molkey'],
            'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')
        self.assertEqual(raw_output_metadata['filetype'], 'geometry')

    def test_orca_execution_retry(self):

        # Insert molecule record
        self._insert_data_proton_error()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['tasks_per_calc'] = 3

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, '[H+]')
        self.assertEqual(mol.priority, 1)
        with self.assertRaises(AttributeError):
            mol.geometry

    def test_orca_execution_error(self):

        # Insert molecule record
        self._insert_data_proton_error()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['tasks_per_calc'] = 5

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles, '[H+]')
        self.assertEqual(mol.priority, 1)
        with self.assertRaises(AttributeError):
            mol.geometry

    def test_orca_execution_timeout(self):

        # Insert molecule record
        self._insert_data_sucrose()

        # Input options dict
        input_kv = {}

        # Calculation timeout
        input_kv['geometry_timeout_orca'] = {
            'key': 'geometry_timeout',
            'value': 1,
            'target': 'colibri.task.OrcaGeometryTask',
            'precedence': 'command'}

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "CZMRCDWAGMRECN-UGDNZRGBSA-N-VOXHORWGWB"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles,
                         ('OC[C@H]1O[C@H](O[C@]2(CO)O[C@H](CO)[C@@H]' +
                          '(O)[C@@H]2O)[C@H](O)[C@@H](O)[C@@H]1O'))
        self.assertEqual(mol.priority, 342)
        with self.assertRaises(AttributeError):
            mol.geometry

    def test_orca_execution_noconvergence(self):

        # Insert molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Geometry options for Orca
        input_kv['geometry_options_orca'] = {
            'key': 'geometry_options',
            'value': 'am1\n%geom MaxIter 1\nend\n',
            'target': 'colibri.task.OrcaGeometryTask',
            'precedence': 'command'}

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)
        self.assertAlmostEqual(mol.geometry.energy, -12.8080, places=4)

    def test_orca_structure3d_error(self):

        # Insert molecule record
        self._insert_data_ammonium_badxyz()

        # Run OrcaGeometryTask
        self._run_orcageometrytask()

        # Get result from database
        input_data = self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-O-WKXUEGBFRF"})

        # Check result from database
        self.assertEqual(input_data[t2s['level']], Level.CONFIGURATION_ERROR)
        self.assertEqual(input_data[t2s['formula']][t2s['smiles']],
                         '[NH4+]')
        self.assertEqual(input_data[t2s['priority']], 18)
        self.assertNotIn(t2s['etag'], input_data)

        # Reading raises exception
        with self.assertRaises(Structure3DError):
            Molecule.fromdict(input_data)

    def test_orca_nofragmentation(self):

        # Insert new molecule record
        self._insert_data_methyloxonium()

        # Input options dict
        input_kv = {}

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-O-GNQMJBYIZY"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.formula.smiles, 'C[OH2+]')
        self.assertEqual(mol.formula.charge, 1)
        self.assertEqual(mol.formula.mult, 1)
        self.assertEqual(mol.priority, 33)
        self.assertTrue(mol.converged)
        self.assertAlmostEqual(mol.geometry.energy, -18.7125, places=4)

    def test_orca_fragmentation_retry(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['tasks_per_calc'] = 1

        # Number of attempts
        input_kv['geometry_attempts'] = 5

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.priority, 60)
        self.assertEqual(mol.generation, 4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -34.94982, places=4)
        self.assertListEqual(mol.tags, ['NN'])

    def test_orca_fragmentation_error(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['tasks_per_calc'] = 1

        # Number of attempts
        input_kv['geometry_attempts'] = 3

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -34.94982, places=4)

    def test_orca_fragmentation_novalidation(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['tasks_per_calc'] = 1

        # Number of attempts
        input_kv['geometry_validation'] = False

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.priority, 60)
        self.assertEqual(mol.generation, 4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -34.94982, places=4)
        self.assertListEqual(mol.tags, ['NN'])

    def test_orca_rearrangement_retry(self):

        # Insert new molecule record
        self._insert_data_bicycle_invalid_rearrangement()

        # Input options dict
        input_kv = {}

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.attempts, 1)
        self.assertEqual(mol.formula.smiles, 'C1=C2CC2=C1')
        self.assertEqual(mol.priority, 64)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -25.24946, places=4)

    def test_orca_noexecutable(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Executable path does not include Orca
        input_kv['executable_path'] = '/usr/bin:/bin'

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')

    def test_orca_badoptions(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Geometry options for Orca
        input_kv['geometry_options_orca'] = {
            'key': 'geometry_options',
            'value': 'pm6',
            'target': 'colibri.task.OrcaGeometryTask',
            'precedence': 'command'}

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')

    def test_orca_noscratchpath(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Scratch path does not exist or is not writable
        input_kv['scratch_path'] = '/DOESNTEXIST'

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run OrcaGeometryTask
        self._run_orcageometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')


class TestOrcaPropertyTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_h2o(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""},
            "z": [], "d": 18, "g": {
                "e": -322.6792, "d": 18, "i": None, "h": "PM7", "m": 1,
                "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                "x": """\
3

O    -0.000001     0.058320     0.000000
H    -0.759932    -0.519445    -0.000000
H     0.760055    -0.519304     0.000000"""},
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0, "t": ["oxygen"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_nh3(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17, "g": {
                "e": -226.7529, "d": 17, "i": None, "h": "PM7", "m": 1,
                "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                "x": """\
4

N    -0.000022    -0.000019     0.038027
H     0.569814    -0.749684    -0.283921
H    -0.934098    -0.118619    -0.283910
H     0.364387     0.868362    -0.283954"""},
            "f": {"i": None, "s": "N", "d": 17},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0, "t": ["nitrogen"],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

    def _run_orcapropertytask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'orca_property_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'OrcaPropertyTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Orca options
        input_kv['property_attempts'] = 5

        # Property options for Orca
        input_kv['property_options_orca'] = {
            'key': 'property_options',
            'value': """\
rhf zindo/s diis
%cis NRoots 1
  Maxdim 10
end""",
            'target': 'colibri.task.OrcaPropertyTask',
            'precedence': 'command'}

        # Property method
        input_kv['property_method_orca'] = {
            'key': 'property_method',
            'value': 'ZINDO/s',
            'target': 'colibri.task.OrcaPropertyTask',
            'precedence': 'command'}

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_orca_property(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Run OrcaPropertyTask
        self._run_orcapropertytask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))
        properties = mol.property.properties

        # Check result
        self.assertEqual(mol.level, Level.PROPERTY)
        self.assertAlmostEqual(mol.property.energy, -10.4134, places=4)
        self.assertListEqual(
            properties.keys(),
            ['transition_dipoles_z', 'transition_dipoles_y',
             'transition_dipoles_x', 'excitation_energies'])
        self.assertAlmostEqual(
            properties['excitation_energies'][0], 97016.6, places=1)
        self.assertAlmostEqual(
            properties['transition_dipoles_x'][0], 0.0, places=3)
        self.assertAlmostEqual(
            properties['transition_dipoles_y'][0], 0.0, places=3)
        self.assertAlmostEqual(
            abs(properties['transition_dipoles_z'][0]), 0.128, places=3)
        self.assertDictEqual(mol.property.units, {
            'transition_dipoles_z': 'au',
            'transition_dipoles_y': 'au',
            'transition_dipoles_x': 'au',
            'excitation_energies': 'cm^-1'})
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)
        self.assertListEqual(mol.tags, ['oxygen'])

    def test_orca_tags(self):

        # Insert new molecule records
        self._insert_data_h2o()
        self._insert_data_nh3()

        # Input options dict
        input_kv = {}

        # Tags
        input_kv['tags'] = 'nitrogen'

        # Run OrcaPropertyTask
        self._run_orcapropertytask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)
        self.assertListEqual(mol1.tags, ['oxygen'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))
        properties2 = mol2.property.properties

        # Check result
        self.assertEqual(mol2.level, Level.PROPERTY)
        self.assertAlmostEqual(mol2.property.energy, -6.6545, places=4)
        self.assertListEqual(
            properties2.keys(),
            ['transition_dipoles_z', 'transition_dipoles_y',
             'transition_dipoles_x', 'excitation_energies'])
        self.assertAlmostEqual(
            properties2['excitation_energies'][0], 81428.1, places=1)
        self.assertAlmostEqual(
            properties2['transition_dipoles_x'][0], 0.0, places=3)
        self.assertAlmostEqual(
            properties2['transition_dipoles_y'][0], 0.0, places=3)
        self.assertAlmostEqual(
            abs(properties2['transition_dipoles_z'][0]), 0.219, places=3)
        self.assertDictEqual(mol2.property.units, {
            'transition_dipoles_z': 'au',
            'transition_dipoles_y': 'au',
            'transition_dipoles_x': 'au',
            'excitation_energies': 'cm^-1'})
        self.assertEqual(mol2.formula.smiles, 'N')
        self.assertEqual(mol2.priority, 17)
        self.assertListEqual(mol2.tags, ['nitrogen'])

    def test_orca_raw(self):

        # Insert molecule records
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Raw write
        input_kv['raw_write'] = True

        # Run OrcaPropertyTask
        self._run_orcapropertytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.PROPERTY)

        # Raw output key
        raw_output_key = mol.property.raw_output
        self.assertTrue(raw_output_key)

        # Raw output file metadata
        raw_output_metadata = self._client.test.raw.files.find_one(
            {'_id': raw_output_key})

        # Check molecule key
        self.assertEqual(
            raw_output_metadata['molkey'],
            'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')
        self.assertEqual(raw_output_metadata['filetype'], 'property')

    def test_orca_execution_timeout(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Property options for Orca
        input_kv['property_options_orca'] = {
            'key': 'property_options',
            'value': """\
b3lyp def2-svp def2-svp/j tightscf
%cis NRoots 1
  Maxdim 10
end""",
            'target': 'colibri.task.OrcaPropertyTask',
            'precedence': 'command'}

        # Calculation timeout
        input_kv['property_timeout_orca'] = {
            'key': 'property_timeout',
            'value': 1,
            'target': 'colibri.task.OrcaPropertyTask',
            'precedence': 'command'}

        # Number of attempts
        input_kv['property_attempts'] = 1

        # Run OrcaPropertyTask
        self._run_orcapropertytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.PROPERTY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)
        with self.assertRaises(AttributeError):
            mol.property

    def test_orca_noexecutable(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Executable path does not include Orca
        input_kv['executable_path'] = '/usr/bin:/bin'

        # Number of attempts
        input_kv['property_attempts'] = 1

        # Run OrcaPropertyTask
        self._run_orcapropertytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.PROPERTY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')

    def test_orca_badoptions(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Geometry options for Orca
        input_kv['property_options_orca'] = {
            'key': 'property_options',
            'value': 'pm6',
            'target': 'colibri.task.OrcaPropertyTask',
            'precedence': 'command'}

        # Number of attempts
        input_kv['property_attempts'] = 1

        # Run OrcaPropertyTask
        self._run_orcapropertytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.PROPERTY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')

    def test_orca_noscratchpath(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Scratch path does not exist or is not writable
        input_kv['scratch_path'] = '/DOESNTEXIST'

        # Number of attempts
        input_kv['property_attempts'] = 1

        # Run OrcaPropertyTask
        self._run_orcapropertytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.PROPERTY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')

    def test_orca_rollback(self):
        global _opts

        # Insert molecule record
        self._insert_data_h2o()

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'orca_property_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'OrcaPropertyTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Batch size
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Locks
        input_kv['optimistic_lock'] = False
        input_kv['pessimistic_lock'] = True

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create Context object
        context = Context(opts)

        # Create new OrcaPropertyTask
        task = colibri.task.OrcaPropertyTask(context, opts)

        # Setup
        task.setup()

        # Retrieve
        task.retrieve()

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY_LOCKED)
        self.assertEqual(mol1.etag, calc_name)
        self.assertEqual(mol1.formula.smiles, 'O')

        # Read geometry output
        task._read_geometry_output()

        # Rollback
        task.rollback()

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.etag, '')
        self.assertEqual(mol2.formula.smiles, 'O')


if __name__ == '__main__':
    unittest.main()
