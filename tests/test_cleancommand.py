"""Test classes for colibri clean command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri clean command

Test Classes:
    TestCleanCommand: Test classes for CleanCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/01/2015'

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.api import clean
from colibri.data import Flask
from colibri.enums import Result
from colibri.exceptions import ExecutionError

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from tmpmemcached import TmpMemcachedClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Temporary Memcached instance
_tmpmemcached = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_cleancommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri clean command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCleanCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _tmpmemcached, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)

    # Temporary Memcached instance
    try:
        _tmpmemcached = TmpMemcachedClient.get_instance()
        _logger.info('Starting %s' % _tmpmemcached)
    except ExecutionError:
        _logger.warning('Cannot start Memcached')


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

    # Temporary Memcached instance
    if _tmpmemcached is not None:
        _logger.info('Destroying %s' % _tmpmemcached)
        _tmpmemcached.destroy()

#
# Test classes
#


class TestCleanCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _tmpmemcached, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)
        if _tmpmemcached is not None:
            self._map_cache = _tmpmemcached.cache(key_prefix='MAP:')
            self._red_cache = _tmpmemcached.cache(key_prefix='RED:')
            self._logger.info('Connecting to %s' % _tmpmemcached)

    def tearDown(self):
        global _tmpmemcached
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})
        if _tmpmemcached is not None:
            self._logger.info('Flushing %s' % _tmpmemcached)
            self._map_cache.clear()
            self._red_cache.clear()

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _tmpmemcached, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Add cache hostname and port options
        if _tmpmemcached is not None:
            input_kv['cache_hostname'] = 'localhost'
            input_kv['cache_port'] = _tmpmemcached.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_mol_h2o_formula(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 18, "f": {"i": None, "s": "O", "d": 18}, "l": 1000,
             "3": 0, "2": "", "5": "", "8": 0, "4": "",
             "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_flask_LL0_initial(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='CO.CO', tags=['LL']).todict())

    def _insert_map_cache_data_mol(self):

        # Insert molecules into mapping cache
        self._map_cache.set_blocking({
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': ''})

    def _insert_red_cache_data_mol(self):

        # Insert molecules into reducer cache
        self._red_cache.set_blocking({
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': '-444.38128'})

    def test_clean_mol(self):

        global _tmpmemcached

        # Insert molecule records
        self._insert_data_mol_h2o_formula()

        # Insert flask records
        self._insert_data_flask_LL0_initial()

        # Insert cache data
        if _tmpmemcached:
            self._insert_map_cache_data_mol()
            self._insert_red_cache_data_mol()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri clean command
        result = clean(molecules=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

        # Check mapping and reducer cache
        if _tmpmemcached:
            self.assertEqual(self._map_cache.get(
                'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'), '')
            self.assertEqual(self._red_cache.get(
                'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'), '-444.38128')

    def test_clean_flask(self):

        global _tmpmemcached

        # Insert molecule records
        self._insert_data_mol_h2o_formula()

        # Insert flask records
        self._insert_data_flask_LL0_initial()

        # Insert cache data
        if _tmpmemcached:
            self._insert_map_cache_data_mol()
            self._insert_red_cache_data_mol()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri clean command
        result = clean(flasks=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

        # Check mapping and reducer cache
        if _tmpmemcached:
            self.assertEqual(self._map_cache.get(
                'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'), '')
            self.assertEqual(self._red_cache.get(
                'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'), '-444.38128')

    def test_clean_cache(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert molecule records
        self._insert_data_mol_h2o_formula()

        # Insert flask records
        self._insert_data_flask_LL0_initial()

        # Insert cache data
        self._insert_map_cache_data_mol()
        self._insert_red_cache_data_mol()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri clean command
        result = clean(cache=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

        # Check mapping cache
        self.assertEqual(self._map_cache.get(
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'), None)

        # Check reducer cache
        self.assertEqual(self._red_cache.get(
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO'), None)


if __name__ == '__main__':
    unittest.main()
