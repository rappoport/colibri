"""Test classes for colib suspend console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib suspend console subcommand

Test Classes:
    TestSuspendConsoleCommand: Test classes for SuspendConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, SuspendConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_suspendconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib suspend console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestSuspendConsoleCommand)
    return suite

#
# Test classes
#


class TestSuspendConsoleCommand(unittest.TestCase):

    def _init_suspendconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        SuspendConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return SuspendConsoleCommand(args=args, opts=opts)

    def test_suspend_mol(self):

        # Initialize SuspendConsoleCommand
        console = self._init_suspendconsole(['colib', 'suspend', '-e'])

        # Check generated SuspendCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^SuspendCommand\(molecules=True, flasks=False, " +
            r"opts=Options\(.*\)\)")

    def test_suspend_mol_tags(self):

        # Initialize SuspendConsoleCommand
        console = self._init_suspendconsole(
            ['colib', 'suspend', '-e', '-@', 'alkane'])

        # Check generated SuspendCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^SuspendCommand\(molecules=True, flasks=False, " +
            r"opts=Options\(.*tags=OptionItem\(.*" +
            r"value=\['alkane'\].*\).*\).*\)")

    def test_suspend_mol_interval(self):

        # Initialize SuspendConsoleCommand
        console = self._init_suspendconsole(
            ['colib', 'suspend', '-e', '-V', '10'])

        # Check generated SuspendCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^SuspendCommand\(molecules=True, flasks=False, " +
            r"opts=Options\(.*suspend_interval=OptionItem\(.*" +
            r"value=10.*\).*\).*\)")

    def test_suspend_flask(self):

        # Initialize SuspendConsoleCommand
        console = self._init_suspendconsole(['colib', 'suspend', '-E'])

        # Check generated SuspendCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^SuspendCommand\(molecules=False, flasks=True, " +
            r"opts=Options\(.*\)\)")

    def test_suspend_nothing(self):

        # Initialize SuspendConsoleCommand
        with self.assertRaisesRegexp(SystemExit, 'Nothing to do'):
            self._init_suspendconsole(['colib', 'suspend'])


if __name__ == '__main__':
    unittest.main()
