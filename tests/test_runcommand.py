"""Test classes for colibri run command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri add command

Test Classes:
    TestRunCommand: Test classes for RunCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/29/2015'

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.api import run
from colibri.data import Molecule, Flask
from colibri.enums import Result, Level, Status
from colibri.exceptions import CommandError

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_runcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri run command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRunCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestRunCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Intervals
        input_kv['idle_interval'] = 0
        input_kv['action_delay'] = 0

        # Configuration options
        input_kv['configuration_validation'] = True
        input_kv['configuration_attempts'] = 5

        # Geometry options
        input_kv['geometry_validation'] = True
        input_kv['geometry_attempts'] = 5

        # Nudge geometry options
        input_kv['geometry_nudge_attempts'] = 5
        input_kv['geometry_nudge_strength'] = 500.0
        input_kv['geometry_nudge_factor'] = 2.0

        # Property options
        input_kv['property_attempts'] = 5

        # Geometry options for Orca
        input_kv['geometry_options_orca'] = {
            'key': 'geometry_options',
            'value': 'am1',
            'target': 'colibri.task.OrcaGeometryTask',
            'precedence': 'command'}

        # Geometry method
        input_kv['geometry_method_orca'] = {
            'key': 'geometry_method',
            'value': 'AM1',
            'target': 'colibri.task.OrcaGeometryTask',
            'precedence': 'command'}

        # Property options for Orca
        input_kv['property_options_orca'] = {
            'key': 'property_options',
            'value': """\
rhf zindo/s diis
%cis NRoots 1
  Maxdim 10
end""",
            'target': 'colibri.task.OrcaPropertyTask',
            'precedence': 'command'}

        # Property method
        input_kv['property_method_orca'] = {
            'key': 'property_method',
            'value': 'ZINDO/s',
            'target': 'colibri.task.OrcaPropertyTask',
            'precedence': 'command'}

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_mol_h2o_formula(self):

        # Insert molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 18, "f": {"i": None, "s": "O", "d": 18}, "l": 1000,
             "3": 0, "2": "", "5": "", "8": 0, "4": "",
             "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_ch4_formula(self):

        # Insert molecule record
        self._client.test.mol.insert(Molecule(smiles='C').todict())

    def _insert_data_mol_nh3_configuration(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17, "f": {"i": None, "s": "N", "d": 17},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0, "t": ["nitrogen"],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

    def _insert_data_mol_h2o_geometry(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""},
            "z": [], "d": 18, "g": {
                "e": -322.6792, "d": 18, "i": None, "h": "PM7", "m": 1,
                "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                "x": """\
3

O    -0.000001     0.058320     0.000000
H    -0.759932    -0.519445    -0.000000
H     0.760055    -0.519304     0.000000"""},
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0, "t": ["oxygen"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_mol_NN0(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {"d": 30, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

C         -0.61340       -0.00000       -0.00000
O          0.60608        0.00000        0.00000
H         -1.15558       -0.93914        0.00000
H         -1.15564        0.93913        0.00000"""},
            "z": [], "d": 30, "g": {
                "e": -444.38128, "d": 30, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC", "7": True,
                "v": "2012", "x": """\
4

C    -0.602789     0.000065    -0.000000
O     0.603275    -0.000020     0.000000
H    -1.168300    -0.933944     0.000000
H    -1.168372     0.933787     0.000000"""},
            "f": {"i": None, "s": "C=O", "d": 30}, "l": 1200,
            "3": 0, "2": "", "5": "", "t": ["NN"],
            "4": "", "8": 0, "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"})

    def _insert_data_NN4_configuration_nudge(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 60, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
8

O         -1.44522        0.55657        0.13517
C         -0.73236       -0.58238       -0.23959
C          0.67590       -0.48147        0.25039
O          1.39884        0.42309       -0.14176
H         -0.73860       -0.67518       -1.34588
H         -1.21861       -1.47796        0.20032
H          1.05324       -1.21925        0.95474
H          2.31281        0.49873        0.16667"""},
            "z": [], "d": 60, "f": {"i": None, "s": "[O-]CC=[OH+]", "d": 60},
            "l": 1110, "3": 4, "2": "", "4": "", "8": 2, "t": ["NN"],
            "_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"})

    def _insert_data_flask_LL0_initial(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='CO.CO', tags=['LL']).todict())

    def _insert_data_flask_NN0_running(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O', generation=0, generation_journal=[0],
                  status=2100, tags=['NN']).todict())

    def _insert_data_flask_NN0_reactive(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O', generation=0, generation_journal=[0],
                  status=2200, tags=['NN']).todict())

    def _insert_data_mol_locked(self):

        # Insert FORMULA_LOCKED record
        self._client.test.mol.insert({
            "z": [], "d": 44, "f": {"i": None, "s": "CCC", "d": 44},
            "l": 1001, "3": 0, "2": "", "5": "rdkit_build_i0b8hg",
            "t": ["organic"],
            "4": "", "8": 0, "_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"})

        # Insert CONFIGURATION_LOCKED record
        self._client.test.mol.insert({
            "c": {"d": 52, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

F         -1.12799       -0.23990       -0.00000
C         -0.00007        0.55775        0.00001
F          1.12804       -0.23986       -0.00000
H         -0.00006        1.19829        0.90579
H         -0.00004        1.19829       -0.90585"""},
            "z": [], "d": 52, "f": {"i": None, "s": "FCF", "d": 52},
            "l": 1101, "3": 0, "2": "", "5": "mopac_geometry_umIYYj",
            "t": ["organic", "halogenated"],
            "4": "", "8": 0, "_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"})

    def _insert_data_flask_locked(self):

        # Insert INITIAL_LOCKED records
        self._client.test.flask.insert({
            "d": 1800, "k": [
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"},
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}],
            "j": 2001, "m": 1, "s": "C=O.C=O", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "5": "flask_mapper_WViqDy",
            "t": ["NN"], "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"})

        # Insert RUNNING_LOCKED record
        self._client.test.flask.insert({
            "b": [{"1": "[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]",
                   "p": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK",
                   "r": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-RDGEAJXOVL"}],
            "d": 8488, "3*": [8], "k": [
                {"s": "C=C=O",
                 "_id": "CCGKOQOJPYTBIH-UHFFFAOYSA-N-ACYGJWNMWD"},
                {"s": "O", "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2101, "m": 1, "s": "C=C=O.O", "q": 0, "0": -889.86602,
            "3": 8, "2": "", "4": "", "5": "flask_reducer_f4Ssb5",
            "0*": [-889.86602], "8": 0,
            "t": ["NN"], "_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"})

    def _insert_data_flask_unavailable(self):

        # Insert UNAVAILABLE record
        self._client.test.flask.insert({
            "9": 0.0, "b": [
                {"1": "[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]",
                 "p": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE",
                 "r": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"},
                {"1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                 "p": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE",
                 "r": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}],
            "e": -888.76256, "d": 2200, "3*": [2, 4],
            "k": [
                {"s": "[CH2+][O-]",
                 "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"},
                {"s": "[CH2+][O-]",
                 "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"}],
            "j": 2310, "m": 1, "s": "[CH2+][O-].[CH2+][O-]",
            "q": 0, "0": -888.76256, "3": 2, "2": "", "4": "",
            "0*": [-888.76256, -888.93686], "8": 0, "t": ["NN"],
            "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"})

    def test_run_openbabel_build(self):

        # Insert molecule record
        self._insert_data_mol_h2o_formula()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(openbabel_build=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 0)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)

    def test_run_openbabel_build_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(openbabel_build=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_rdkit_build(self):

        # Insert molecule record
        self._insert_data_mol_h2o_formula()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(rdkit_build=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 0)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)

    def test_run_rdkit_build_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(rdkit_build=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_rdkit_build_nooptions(self):

        # Insert molecule record
        self._insert_data_mol_h2o_formula()

        # Run colibri run command
        with self.assertRaisesRegexp(CommandError, 'No input options'):
            run(rdkit_build=1)

    def test_run_mopac_geometry(self):

        # Insert molecule record
        self._insert_data_mol_nh3_configuration()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(mopac_geometry=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.formula.smiles, 'N')
        self.assertAlmostEqual(mol.geometry.energy, -226.7529, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.priority, 17)

    def test_run_mopac_geometry_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(mopac_geometry=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_mopac_nudge_geometry(self):

        # Insert molecule record
        self._insert_data_NN4_configuration_nudge()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(mopac_nudge_geometry=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertAlmostEqual(mol.geometry.energy, -886.42115, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.priority, 60)

    def test_run_mopac_nudge_geometry_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(mopac_nudge_geometry=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_orca_geometry(self):

        # Insert molecule record
        self._insert_data_mol_nh3_configuration()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(orca_geometry=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.formula.smiles, 'N')
        self.assertAlmostEqual(mol.geometry.energy, -9.13557158181, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.priority, 17)

    def test_run_orca_geometry_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(orca_geometry=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_orca_property(self):

        # Insert molecule record
        self._insert_data_mol_h2o_geometry()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(orca_property=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))
        properties = mol.property.properties

        # Check result
        self.assertEqual(mol.level, Level.PROPERTY)
        self.assertAlmostEqual(mol.property.energy, -10.4134, places=4)
        self.assertListEqual(
            properties.keys(),
            ['transition_dipoles_z', 'transition_dipoles_y',
             'transition_dipoles_x', 'excitation_energies'])
        self.assertAlmostEqual(
            properties['excitation_energies'][0], 97016.6, places=1)
        self.assertAlmostEqual(
            properties['transition_dipoles_x'][0], 0.0, places=3)
        self.assertAlmostEqual(
            properties['transition_dipoles_y'][0], 0.0, places=3)
        self.assertAlmostEqual(
            abs(properties['transition_dipoles_z'][0]), 0.128, places=3)
        self.assertDictEqual(mol.property.units, {
            'transition_dipoles_z': 'au',
            'transition_dipoles_y': 'au',
            'transition_dipoles_x': 'au',
            'excitation_energies': 'cm^-1'})
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)

    def test_run_orca_property_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(orca_property=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_flask_mapper(self):

        # Insert flask record
        self._insert_data_flask_LL0_initial()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(flask_mapper=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'CO.CO')
        self.assertListEqual(flask.tags, ['LL'])
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])

        # Get molecule result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.formula.smiles, 'CO')
        self.assertEqual(mol.priority, 32)

        # Total number of molecules inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

    def test_run_flask_mapper_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(flask_mapper=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_flask_reducer(self):

        # Insert flask record
        self._insert_data_flask_NN0_running()

        # Insert molecule record
        self._insert_data_mol_NN0()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(flask_reducer=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.REACTIVE)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertAlmostEqual(flask.energy, -888.76256, places=4)
        self.assertEqual(flask.attempts, 0)

    def test_run_flask_reducer_incomplete(self):

        # Insert flask record
        self._insert_data_flask_NN0_running()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(flask_reducer=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get flask result from the database
        flask = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask.status, Status.RUNNING)
        self.assertEqual(flask.smiles, 'C=O.C=O')
        self.assertEqual(flask.generation, 0)
        self.assertListEqual(flask.generation_journal, [0])
        self.assertEqual(flask.precursor, None)
        self.assertListEqual(flask.precursor_journal, [])
        self.assertEqual(flask.attempts, 1)

    def test_run_flask_reducer_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(flask_reducer=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_rdkit_react(self):

        # Insert molecule record
        self._insert_data_ch4_formula()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(rdkit_react=1, react_smarts=['[C:1]>>[C:1]C'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'C')
        self.assertEqual(mol1.priority, 16)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, 'CC')
        self.assertEqual(mol2.priority, 30)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 2)

    def test_run_rdkit_react_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(rdkit_react=1, react_smarts=['[C:1]>>[C:1]C'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # No molecules inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_run_rdkit_react_norules(self):

        # Insert molecule record
        self._insert_data_ch4_formula()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri add command
        result = run(rdkit_react=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

    def test_run_rdkit_flask_react(self):

        # Insert flask record
        self._insert_data_flask_NN0_reactive()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(rdkit_flask_react=1,
                     react_smarts=['[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'],
                     opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.tags, ['NN'])
        self.assertEqual(flask1.priority, 1800)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[CH-]=O.[H+].C=O')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.tags, ['NN'])
        self.assertEqual(flask2.priority, 1842)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_run_rdkit_flask_react_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(rdkit_flask_react=1,
                     react_smarts=['[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'],
                     opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # No flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_run_rdkit_flask_react_norules(self):

        # Insert flask record
        self._insert_data_flask_NN0_reactive()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri run command
        result = run(rdkit_flask_react=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # No flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_run_mol_cleanup(self):

        # Insert molecule records
        self._insert_data_mol_locked()

        # Get configuration options
        opts = self._update_conf_opts(cleanup_interval=0)

        # Run colibri run command
        result = run(mol_cleanup=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'CCC')
        self.assertEqual(mol1.etag, '')

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"}))

        # Check result
        self.assertEqual(mol2.level, Level.CONFIGURATION)
        self.assertEqual(mol2.formula.smiles, 'FCF')
        self.assertEqual(mol2.etag, '')

    def test_run_mol_cleanup_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts(cleanup_interval=0)

        # Run colibri run command
        result = run(mol_cleanup=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

    def test_run_flask_cleanup(self):

        # Insert flask records
        self._insert_data_flask_locked()
        self._insert_data_flask_unavailable()

        # Get configuration options
        opts = self._update_conf_opts(cleanup_interval=0)

        # Run colibri run command
        result = run(flask_cleanup=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get flask results from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertEqual(flask1.attempts, 0)

        # Get flask results from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.RUNNING)
        self.assertEqual(flask2.smiles, 'C=C=O.O')
        self.assertEqual(flask2.generation, 8)
        self.assertListEqual(flask2.generation_journal, [8])
        self.assertEqual(flask2.attempts, 0)

        # Get flask results from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.RUNNING)
        self.assertEqual(flask3.smiles, '[CH2+][O-].[CH2+][O-]')
        self.assertEqual(flask3.generation, 2)
        self.assertListEqual(flask3.generation_journal, [2, 4])
        self.assertEqual(flask3.attempts, 0)

    def test_run_flask_cleanup_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts(cleanup_interval=0)

        # Run colibri run command
        result = run(flask_cleanup=1, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)


if __name__ == '__main__':
    unittest.main()
