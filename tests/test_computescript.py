"""Test classes for colib compute script.

Copyright 2016 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib compute script

Test Classes:
    TestcomputeScript: Test classes for colib compute script
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/19/2016'

#
# Standard library
#

import os
import sys
import unittest
import logging
import subprocess

#
# colibri submodules
#

from colibri.utils import ensure_logger

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None


# Logging level
_logging_level = 'ERROR'

# Get logger
_logger = ensure_logger('computescript', _logging_level)
_logger.addHandler(logging.NullHandler())

#
# Public functions
#


def get_suite():
    """Construct test suite for colib compute script."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestComputeScript)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestComputeScript(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.flask.remove({})
        self._client.test.mol.remove({})

    def _update_script_args(self, args=[]):
        global _tmpmongo, _logging_level

        # List of script arguments
        script_args = []

        # Add hostname and port options
        script_args.extend(['--db-hostname', 'localhost'])
        script_args.extend(['--db-port', str(_tmpmongo.port)])

        # Logging level
        script_args.extend(['--logging-level', _logging_level])

        # Ignore all configuration files
        script_args.extend(['--no-config'])

        # Additional script arguments
        script_args.extend(args)

        return script_args

    def _run_computescript(self, script_args=[], cmd_args=[]):
        global _logging_level

        if _logging_level == 'DEBUG':
            stderr = sys.stderr
        else:
            stderr = open(os.devnull, 'w')

        try:
            output = subprocess.check_output(
                ['colib'] + script_args + ['compute'] + cmd_args,
                stderr=stderr)
            return (output, 0)

        except subprocess.CalledProcessError as e:
            return (e.output, e.returncode)

    def test_compute(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib compute script
        output, ret_value = self._run_computescript(script_args, cmd_args=[
            '-E', 'O=CO.CO'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Retrieve smiles and energy from script output
        smiles, energy = output.split()

        # Check result
        self.assertEqual(smiles, 'O=CO.CO')
        self.assertAlmostEqual(float(energy), -1212.34475, places=4)

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 2)

    def test_compute_multiple(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib compute script
        output, ret_value = self._run_computescript(script_args, cmd_args=[
            '-E', 'C=O.C=O', 'C=O.O', 'CO'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Retrieve smiles and energy from script output
        smiles1, energy1, smiles2, energy2, smiles3, energy3 = output.split()

        # Check result
        self.assertEqual(smiles1, 'CO')
        self.assertAlmostEqual(float(energy1), -472.05421, places=4)
        self.assertEqual(smiles2, 'C=O.O')
        self.assertAlmostEqual(float(energy2), -767.06048, places=4)
        self.assertEqual(smiles3, 'C=O.C=O')
        self.assertAlmostEqual(float(energy3), -888.76256, places=4)

        # Total number of flasks in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 3)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

    def test_compute_noinput(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib compute script
        output, ret_value = self._run_computescript(script_args)

        # Check script return value
        self.assertEqual(ret_value, 1)


if __name__ == '__main__':
    unittest.main()
