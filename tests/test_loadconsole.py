"""Test classes for colib load console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib load console subcommand

Test Classes:
    TestLoadConsoleCommand: Test classes for LoadConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, LoadConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_loadconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib load console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestLoadConsoleCommand)
    return suite

#
# Test classes
#


class TestLoadConsoleCommand(unittest.TestCase):

    def _init_loadconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        LoadConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return LoadConsoleCommand(args=args, opts=opts)

    def test_load(self):

        # Initialize LoadConsoleCommand
        console = self._init_loadconsole(
            ['colib', 'load', '-i', 'test.graphml'])

        # Check generated LoadCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^LoadCommand\(input_file='test.graphml', opts=Options\(.*\)\)")

    def test_load_noinput(self):

        # Initialize LoadConsoleCommand
        with self.assertRaisesRegexp(SystemExit, 'No input file'):
            self._init_loadconsole(['colib', 'load'])


if __name__ == '__main__':
    unittest.main()
