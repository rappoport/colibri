"""
Test classes for RDKitReactionTask

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for rdkitreact classes

Test Classes:
    TestRDKitReactionTask: Molecule generation task using RDKit.
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/10/2015'

#
# Standard library
#


import unittest
import logging


#
# colibri modules
#

from colibri.data import Molecule
from colibri.calc import Calculator
from colibri.enums import Level, Reactivity
from colibri.utils import randomid

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_rdkitreact')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for RDKitReactTask."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRDKitReactTask)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()


#
# Test classes
#

class TestRDKitReactTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_ch4_formula(self):

        # Insert molecule record
        self._client.test.mol.insert(Molecule(smiles='C').todict())

    def _insert_data_sec_alcohols_formula(self):

        # Insert molecule records
        self._client.test.mol.insert(
            Molecule(smiles='CC(O)CC', tags=['butanol']).todict())
        self._client.test.mol.insert(
            Molecule(smiles='CC(O)C', tags=['propanol']).todict())

    def _insert_data_prim_alcohols_formula(self):

        # Insert molecule records
        self._client.test.mol.insert(
            Molecule(smiles='CO', generation=1, tags=['methanol']).todict())
        self._client.test.mol.insert(
            Molecule(smiles='CCO', generation=2, tags=['ethanol']).todict())
        self._client.test.mol.insert(
            Molecule(smiles='CCCO', generation=2, tags=['propanol']).todict())

    def _insert_data_isopentane_formula(self):

        # Insert molecule record
        self._client.test.mol.insert(
            Molecule(smiles='CC(C)CC', generation=1).todict())

    def _insert_data_ethanediol_formula(self):

        # Insert molecule record
        self._client.test.mol.insert(Molecule(smiles='OCCO').todict())

    def _insert_data_diols_formula(self):

        # Insert molecule record
        self._client.test.mol.insert(
            Molecule(smiles='OCCCCO', tags=['butanediol']).todict())
        self._client.test.mol.insert(
            Molecule(smiles='OCCCCCO', tags=['pentanediol']).todict())

    def _insert_data_hydroxyacids_formula(self):

        # Insert molecule records
        self._client.test.mol.insert(
            Molecule(smiles='OCC(O)=O', generation=1).todict())
        self._client.test.mol.insert(
            Molecule(smiles='CC(O)C(O)=O', generation=1).todict())

    def _insert_data_cycloalkanes_formula(self):

        # Insert molecule records
        self._client.test.mol.insert(
            Molecule(smiles='C1CC1', tags=['cyclopropane']).todict())
        self._client.test.mol.insert(
            Molecule(smiles='C1CCC1', tags=['cyclobutane']).todict())

    def _insert_data_cycloether_ions_formula(self):

        # Insert molecule records
        self._client.test.mol.insert(
            Molecule(smiles='[O-]C[CH2+]').todict())
        self._client.test.mol.insert(
            Molecule(smiles='[O-]CC[CH2+]').todict())

    def _insert_data_ions_formula(self):

        # Insert molecule records
        self._client.test.mol.insert(Molecule(smiles='[OH-]').todict())
        self._client.test.mol.insert(Molecule(smiles='C[O-]').todict())
        self._client.test.mol.insert(Molecule(smiles='[CH3+]').todict())
        self._client.test.mol.insert(Molecule(smiles='C[CH2+]').todict())

    def _insert_data_alcohol_diol_formula(self):

        # Insert molecule records
        self._client.test.mol.insert(Molecule(smiles='CC(C)O').todict())
        self._client.test.mol.insert(Molecule(smiles='OCCCO').todict())

    def _insert_data_allene_formula(self):

        # Insert molecule record
        self._client.test.mol.insert(Molecule(smiles='C=C=C').todict())

    def _run_rdkitreacttask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task options
        calc_name = 'rdkit_react_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'RDKitReactionTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Tasks settings
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_rdkitreact_uni(self):

        # Insert molecule record
        self._insert_data_ch4_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule', 'value': '[C:1]>>[C:1]C',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Unimolecular reactions
        input_kv['reactivity'] = Reactivity.UNI

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'C')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 16)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'CC')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 30)
        self.assertEqual(mol2.generation, 1)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 2)

    def test_rdkitreact_uni_gen2(self):

        # Insert molecule record
        self._insert_data_ch4_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule', 'value': '[C:1]>>[C:1]C',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Unimolecular reactions
        input_kv['reactivity'] = Reactivity.UNI

        # Number of generations
        input_kv['max_generation'] = 2

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'C')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 16)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'CC')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 30)
        self.assertEqual(mol2.generation, 1)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"}))

        # Check result
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'CCC')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 44)
        self.assertEqual(mol3.generation, 2)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

    def test_rdkitreact_uni_tags(self):

        # Insert molecule records
        self._insert_data_sec_alcohols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH1:1][OH1:2]>>[C:1]=[O:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Unimolecular reactions
        input_kv['reactivity'] = Reactivity.UNI

        # Tags
        input_kv['tags'] = ['butanol']

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BTANRVKWQNVYAZ-UHFFFAOYSA-N-KFXKIYRIGO"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CCC(C)O')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 74)
        self.assertEqual(mol1.generation, 0)
        self.assertListEqual(mol1.tags, ['butanol'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "KFZMGEQAYNKOFK-UHFFFAOYSA-N-NFMMMTIBZH"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'CC(C)O')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 60)
        self.assertEqual(mol2.generation, 0)
        self.assertListEqual(mol2.tags, ['propanol'])

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ZWEHNKRNPOVVGH-UHFFFAOYSA-N-JJNZTBSKOY"}))

        # Check result
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'CCC(C)=O')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 72)
        self.assertEqual(mol3.generation, 1)
        self.assertListEqual(mol3.tags, ['butanol'])

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

    def test_rdkitreact_uni_limit(self):

        # Insert molecule records
        self._insert_data_sec_alcohols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH1:1][OH1:2]>>[C:1]=[O:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Unimolecular reactions
        input_kv['reactivity'] = Reactivity.UNI

        # Tags
        input_kv['limit'] = 1

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BTANRVKWQNVYAZ-UHFFFAOYSA-N-KFXKIYRIGO"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CCC(C)O')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 74)
        self.assertEqual(mol1.generation, 0)
        self.assertListEqual(mol1.tags, ['butanol'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "KFZMGEQAYNKOFK-UHFFFAOYSA-N-NFMMMTIBZH"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'CC(C)O')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 60)
        self.assertEqual(mol2.generation, 0)
        self.assertListEqual(mol2.tags, ['propanol'])

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ZWEHNKRNPOVVGH-UHFFFAOYSA-N-JJNZTBSKOY"}))

        # Check result
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'CCC(C)=O')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 72)
        self.assertEqual(mol3.generation, 1)
        self.assertListEqual(mol3.tags, ['butanol'])

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

    def test_rdkitreact_uni_multiple_rules(self):

        # Insert molecule records
        self._insert_data_sec_alcohols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH1:1][OH1:2]>>[C:1]=[O:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH3:1][CH1:2][OH1:3]>>[C:1]=[C:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH2:1][CH1:2][OH1:3]>>[C:1]=[C:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Unimolecular reactions
        input_kv['reactivity'] = Reactivity.UNI

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BTANRVKWQNVYAZ-UHFFFAOYSA-N-KFXKIYRIGO"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CCC(C)O')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 74)
        self.assertEqual(mol1.generation, 0)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "KFZMGEQAYNKOFK-UHFFFAOYSA-N-NFMMMTIBZH"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'CC(C)O')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 60)
        self.assertEqual(mol2.generation, 0)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QQONPFPTGQHPMA-UHFFFAOYSA-N-DEYWQGKXBH"}))

        # Check result
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'C=CC')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 42)
        self.assertEqual(mol3.generation, 1)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS"}))

        # Check result
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'CC(C)=O')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 58)
        self.assertEqual(mol4.generation, 1)

        # Get result from database
        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VXNZUUAINFGPBY-UHFFFAOYSA-N-RINLBARDWX"}))

        # Check result
        self.assertEqual(mol5.level, Level.FORMULA)
        self.assertEqual(mol5.formula.smiles, 'C=CCC')
        self.assertEqual(mol5.formula.charge, 0)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.priority, 56)
        self.assertEqual(mol5.generation, 1)

        # Get result from database
        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IAQRGUVFOMOMEM-UHFFFAOYSA-N-HOUZHABELW"}))

        # Check result
        self.assertEqual(mol6.level, Level.FORMULA)
        self.assertEqual(mol6.formula.smiles, 'CC=CC')
        self.assertEqual(mol6.formula.charge, 0)
        self.assertEqual(mol6.formula.mult, 1)
        self.assertEqual(mol6.priority, 56)
        self.assertEqual(mol6.generation, 1)

        # Get result from database
        mol7 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ZWEHNKRNPOVVGH-UHFFFAOYSA-N-JJNZTBSKOY"}))

        # Check result
        self.assertEqual(mol7.level, Level.FORMULA)
        self.assertEqual(mol7.formula.smiles, 'CCC(C)=O')
        self.assertEqual(mol7.formula.charge, 0)
        self.assertEqual(mol7.formula.mult, 1)
        self.assertEqual(mol7.priority, 72)
        self.assertEqual(mol7.generation, 1)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 7)

    def test_rdkitreact_uni_exact(self):

        # Insert molecule record
        self._insert_data_isopentane_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Unimolecular reactions
        input_kv['reactivity'] = Reactivity.UNI

        # Exact atom mapping
        input_kv['react_exact_mapping_rdkit'] = {
            'key': 'react_exact_mapping',
            'value': True,
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QWTDNUCVQCZILF-UHFFFAOYSA-N-OYZUXVXZGB"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CCC(C)C')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 72)
        self.assertEqual(mol1.generation, 1)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OHLYBPMISQIJRG-UHFFFAOYSA-N-OPYBBTRYPY"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, '[CH2-]CC(C)C')
        self.assertEqual(mol2.formula.charge, -1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 71)
        self.assertEqual(mol2.generation, 2)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VTPSVHCEYMUKKD-UHFFFAOYSA-N-DETMQSETWM"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'C[CH-]C(C)C')
        self.assertEqual(mol3.formula.charge, -1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 71)
        self.assertEqual(mol3.generation, 2)

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "PSNNZRWYVIJKQE-UHFFFAOYSA-N-PGUYCTLEBQ"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'CC[C-](C)C')
        self.assertEqual(mol4.formula.charge, -1)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 71)
        self.assertEqual(mol4.generation, 2)

        # Get results
        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AUWVOTBOQYAIMD-UHFFFAOYSA-N-NEMEJVAQPI"}))

        # Check results
        self.assertEqual(mol5.level, Level.FORMULA)
        self.assertEqual(mol5.formula.smiles, '[CH2-]C(C)CC')
        self.assertEqual(mol5.formula.charge, -1)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.priority, 71)
        self.assertEqual(mol5.generation, 2)

        # Get results
        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check results
        self.assertEqual(mol6.level, Level.FORMULA)
        self.assertEqual(mol6.formula.smiles, '[H+]')
        self.assertEqual(mol6.formula.charge, 1)
        self.assertEqual(mol6.formula.mult, 1)
        self.assertEqual(mol6.priority, 1)
        self.assertEqual(mol6.generation, 2)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 6)

    def test_rdkitreact_cycl_closing(self):

        # Insert molecule record
        self._insert_data_diols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Cyclization reactions
        input_kv['reactivity'] = Reactivity.CYCL

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WERYXYBDKMZEQL-UHFFFAOYSA-N-UBTEZPZJUY"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'OCCCCO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 90)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ALQSHHUCVQOPAS-UHFFFAOYSA-N-KVSSAKSEEC"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'OCCCCCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 104)
        self.assertEqual(mol2.generation, 0)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WYURNTSHIVDZCO-UHFFFAOYSA-N-LQLWNTVJGW"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'C1CCOC1')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 72)
        self.assertEqual(mol3.generation, 1)

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "DHXVGJBLRPWPCS-UHFFFAOYSA-N-HNMJHYWUYI"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'C1CCOCC1')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 86)
        self.assertEqual(mol4.generation, 1)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_rdkitreact_cycl_opening(self):

        # Insert molecule record
        self._insert_data_cycloalkanes_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][C:2]>>[C:1].[C:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Cyclization reactions
        input_kv['reactivity'] = Reactivity.CYCL

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LVZWSLJZHVFIQJ-UHFFFAOYSA-N-TKJPZVIWYP"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'C1CC1')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 42)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "PMPVIKIVABFJJI-UHFFFAOYSA-N-QBCNJLHECH"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'C1CCC1')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 56)
        self.assertEqual(mol2.generation, 0)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'CCC')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 44)
        self.assertEqual(mol3.generation, 1)

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IJDNQMDRQITEOD-UHFFFAOYSA-N-DOVKYLKHHV"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'CCCC')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 58)
        self.assertEqual(mol4.generation, 1)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_rdkitreact_cycl_tags(self):

        # Insert molecule record
        self._insert_data_diols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Cyclization reactions
        input_kv['reactivity'] = Reactivity.CYCL

        # Tags
        input_kv['tags'] = ['butanediol']

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WERYXYBDKMZEQL-UHFFFAOYSA-N-UBTEZPZJUY"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'OCCCCO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 90)
        self.assertEqual(mol1.generation, 0)
        self.assertListEqual(mol1.tags, ['butanediol'])

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ALQSHHUCVQOPAS-UHFFFAOYSA-N-KVSSAKSEEC"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'OCCCCCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 104)
        self.assertEqual(mol2.generation, 0)
        self.assertListEqual(mol2.tags, ['pentanediol'])

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WYURNTSHIVDZCO-UHFFFAOYSA-N-LQLWNTVJGW"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'C1CCOC1')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 72)
        self.assertEqual(mol3.generation, 1)
        self.assertListEqual(mol3.tags, ['butanediol'])

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

    def test_rdkitreact_cycl_closing_exact(self):

        # Insert molecule records
        self._insert_data_cycloether_ions_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Cyclization reactions
        input_kv['reactivity'] = Reactivity.CYCL

        # Exact atom mapping
        input_kv['react_exact_mapping_rdkit'] = {
            'key': 'react_exact_mapping',
            'value': True,
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ODVIVHCWPDKHAQ-UHFFFAOYSA-N-VZAGDMEEXE"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, '[CH2+]C[O-]')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 44)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VQNYUISONHJBNC-UHFFFAOYSA-N-UDOZSOTFRK"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, '[CH2+]CC[O-]')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 58)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IAYPIBMASNFSPL-UHFFFAOYSA-N-IGFFCQTDYI"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'C1CO1')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 44)
        self.assertEqual(mol3.generation, 1)

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AHHWIHXENZJRFG-UHFFFAOYSA-N-NUFHNADGKT"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'C1COC1')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 58)
        self.assertEqual(mol4.generation, 1)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_rdkitreact_cycl_opening_exact(self):

        # Insert molecule records
        self._insert_data_cycloalkanes_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[C+0:2]>>[C+1:1].[C-1:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Cyclization reactions
        input_kv['reactivity'] = Reactivity.CYCL

        # Exact atom mapping
        input_kv['react_exact_mapping_rdkit'] = {
            'key': 'react_exact_mapping',
            'value': True,
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LVZWSLJZHVFIQJ-UHFFFAOYSA-N-TKJPZVIWYP"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'C1CC1')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 42)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "PMPVIKIVABFJJI-UHFFFAOYSA-N-QBCNJLHECH"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'C1CCC1')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 56)
        self.assertEqual(mol2.generation, 0)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "CUJPFPXNDSIBPG-UHFFFAOYSA-N-BEJQVPMZLV"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, '[CH2+]C[CH2-]')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 42)
        self.assertEqual(mol3.generation, 1)

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OMIVCRYZSXDGAB-UHFFFAOYSA-N-MVLXVCUKWO"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, '[CH2+]CC[CH2-]')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 56)
        self.assertEqual(mol4.generation, 1)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_rdkitreact_bi(self):

        # Insert molecule records
        self._insert_data_prim_alcohols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Bimolecular reactions
        input_kv['reactivity'] = Reactivity.BI

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 32)
        self.assertEqual(mol1.generation, 1)
        self.assertListEqual(mol1.tags, ['methanol'])

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N-XUDWUARODM"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'CCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 46)
        self.assertEqual(mol2.generation, 2)
        self.assertListEqual(mol2.tags, ['ethanol'])

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BDERNNFJNOPAEC-UHFFFAOYSA-N-JZNXBLKBYT"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol3.formula.smiles, 'CCCO')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 60)
        self.assertEqual(mol3.generation, 2)
        self.assertListEqual(mol3.tags, ['propanol'])

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'COC')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 46)
        self.assertEqual(mol4.generation, 2)
        self.assertListEqual(mol4.tags, ['methanol'])

        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XOBKSJJDNFUZPF-UHFFFAOYSA-N-UHPOQXJNMV"}))

        # Check results
        self.assertEqual(mol5.level, Level.FORMULA)
        self.assertEqual(mol5.formula.smiles, 'CCOC')
        self.assertEqual(mol5.formula.charge, 0)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.priority, 60)
        self.assertEqual(mol5.generation, 3)
        self.assertListEqual(mol5.tags, ['methanol', 'ethanol'])

        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RTZKZFJDLAIYFH-UHFFFAOYSA-N-LIEOIGDBWZ"}))

        # Check results
        self.assertEqual(mol6.level, Level.FORMULA)
        self.assertEqual(mol6.formula.smiles, 'CCOCC')
        self.assertEqual(mol6.formula.charge, 0)
        self.assertEqual(mol6.formula.mult, 1)
        self.assertEqual(mol6.priority, 74)
        self.assertEqual(mol6.generation, 3)
        self.assertListEqual(mol6.tags, ['ethanol'])

        mol7 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VNKYTQGIUYNRMY-UHFFFAOYSA-N-YUFMAUKMJS"}))

        # Check results
        self.assertEqual(mol7.level, Level.FORMULA)
        self.assertEqual(mol7.formula.smiles, 'CCCOC')
        self.assertEqual(mol7.formula.charge, 0)
        self.assertEqual(mol7.formula.mult, 1)
        self.assertEqual(mol7.priority, 74)
        self.assertEqual(mol7.generation, 3)
        self.assertListEqual(mol7.tags, ['methanol', 'propanol'])

        mol8 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NVJUHMXYKCUMQA-UHFFFAOYSA-N-CTFZZMNEYG"}))

        # Check results
        self.assertEqual(mol8.level, Level.FORMULA)
        self.assertEqual(mol8.formula.smiles, 'CCCOCC')
        self.assertEqual(mol8.formula.charge, 0)
        self.assertEqual(mol8.formula.mult, 1)
        self.assertEqual(mol8.priority, 88)
        self.assertEqual(mol8.generation, 3)
        self.assertListEqual(mol8.tags, ['propanol', 'ethanol'])

        mol9 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "POLCUAVZOMRGSN-UHFFFAOYSA-N-FJANEVSPRG"}))

        # Check results
        self.assertEqual(mol9.level, Level.FORMULA)
        self.assertEqual(mol9.formula.smiles, 'CCCOCCC')
        self.assertEqual(mol9.formula.charge, 0)
        self.assertEqual(mol9.formula.mult, 1)
        self.assertEqual(mol9.priority, 102)
        self.assertEqual(mol9.generation, 3)
        self.assertListEqual(mol9.tags, ['propanol'])

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 9)

    def test_rdkitreact_bi_gen2(self):

        # Insert molecule record
        self._insert_data_ethanediol_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Bimolecular reactions
        input_kv['reactivity'] = Reactivity.BI

        # Generations
        input_kv['max_generation'] = 2

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LYCAIKOWRPUZTN-UHFFFAOYSA-N-ODXGMBXUFK"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'OCCO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 62)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MTHSVFCYNBDYFN-UHFFFAOYSA-N-CFVQVMVWCE"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'OCCOCCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 106)
        self.assertEqual(mol2.generation, 1)

        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ZIBGPFATKBEMQZ-UHFFFAOYSA-N-JJMASZULAO"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'OCCOCCOCCO')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 150)
        self.assertEqual(mol3.generation, 2)

        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "UWHCKJMYHZGTIT-UHFFFAOYSA-N-TVRRTMOPSH"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'OCCOCCOCCOCCO')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 194)
        self.assertEqual(mol4.generation, 2)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_rdkitreact_bi_gen2_noupdate(self):

        # Insert molecule record
        self._insert_data_ethanediol_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # To do options: bi
        input_kv['reactivity'] = Reactivity.BI

        # No update
        input_kv['react_update'] = {
            'key': 'react_update',
            'value': False,
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Generations
        input_kv['max_generation'] = 2

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LYCAIKOWRPUZTN-UHFFFAOYSA-N-ODXGMBXUFK"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'OCCO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 62)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "MTHSVFCYNBDYFN-UHFFFAOYSA-N-CFVQVMVWCE"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'OCCOCCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 106)
        self.assertEqual(mol2.generation, 1)

        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "UWHCKJMYHZGTIT-UHFFFAOYSA-N-TVRRTMOPSH"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'OCCOCCOCCOCCO')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 194)
        self.assertEqual(mol3.generation, 2)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

    def test_rdkitreact_bi_tags(self):

        # Insert molecule records
        self._insert_data_prim_alcohols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Bimolecular reactions
        input_kv['reactivity'] = Reactivity.BI

        # Tags
        input_kv['tags'] = ['methanol']

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 32)
        self.assertEqual(mol1.generation, 1)
        self.assertListEqual(mol1.tags, ['methanol'])

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N-XUDWUARODM"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'CCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 46)
        self.assertEqual(mol2.generation, 2)
        self.assertListEqual(mol2.tags, ['ethanol'])

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BDERNNFJNOPAEC-UHFFFAOYSA-N-JZNXBLKBYT"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'CCCO')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 60)
        self.assertEqual(mol3.generation, 2)
        self.assertListEqual(mol3.tags, ['propanol'])

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'COC')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 46)
        self.assertEqual(mol4.generation, 2)
        self.assertListEqual(mol4.tags, ['methanol'])

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_rdkitreact_bi_limit(self):

        # Insert molecule records
        self._insert_data_prim_alcohols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Bimolecular reactions
        input_kv['reactivity'] = Reactivity.BI

        # Tags
        input_kv['limit'] = 2

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 32)
        self.assertEqual(mol1.generation, 1)
        self.assertListEqual(mol1.tags, ['methanol'])

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N-XUDWUARODM"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'CCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 46)
        self.assertEqual(mol2.generation, 2)
        self.assertListEqual(mol2.tags, ['ethanol'])

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BDERNNFJNOPAEC-UHFFFAOYSA-N-JZNXBLKBYT"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'CCCO')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 60)
        self.assertEqual(mol3.generation, 2)
        self.assertListEqual(mol3.tags, ['propanol'])

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'COC')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 46)
        self.assertEqual(mol4.generation, 2)
        self.assertListEqual(mol4.tags, ['methanol'])

        # Get results
        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XOBKSJJDNFUZPF-UHFFFAOYSA-N-UHPOQXJNMV"}))

        # Check results
        self.assertEqual(mol5.level, Level.FORMULA)
        self.assertEqual(mol5.formula.smiles, 'CCOC')
        self.assertEqual(mol5.formula.charge, 0)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.priority, 60)
        self.assertEqual(mol5.generation, 3)
        self.assertListEqual(mol5.tags, ['methanol', 'ethanol'])

        # Get results
        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "RTZKZFJDLAIYFH-UHFFFAOYSA-N-LIEOIGDBWZ"}))

        # Check results
        self.assertEqual(mol6.level, Level.FORMULA)
        self.assertEqual(mol6.formula.smiles, 'CCOCC')
        self.assertEqual(mol6.formula.charge, 0)
        self.assertEqual(mol6.formula.mult, 1)
        self.assertEqual(mol6.priority, 74)
        self.assertEqual(mol6.generation, 3)
        self.assertListEqual(mol6.tags, ['ethanol'])

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 6)

    def test_rdkitreact_bi_multiple_rules(self):

        # Insert molecule records
        self._insert_data_hydroxyacids_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1]([OH1:2])=[O:3].[C&!$(C=O):4][OH1]' +
                     '>>[C:1]([O:2][C:4])=[O:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C&!$(C=O):1][OH1:2].[C&!$(C=O):3][OH1]' +
                     '>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # To do options: bi
        input_kv['reactivity'] = Reactivity.BI

        # Generations
        input_kv['max_generation'] = 1

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AEMRFAOFKBGASW-UHFFFAOYSA-N-IFMBGASCGR"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'O=C(O)CO')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 76)
        self.assertEqual(mol1.generation, 1)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "JVTAAEKCZFNVCJ-UHFFFAOYSA-N-ILNRIOMUSQ"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'CC(O)C(=O)O')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 90)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NYCQHRWCDWJIEG-UHFFFAOYSA-N-UGZTGSEVWW"}))

        # Check results
        self.assertEqual(mol3.formula.smiles, 'O=C(O)COC(=O)CO')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 134)

        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "HKVBZVITUCIVRZ-UHFFFAOYSA-N-EREPJYDCEW"}))

        # Check results
        self.assertEqual(mol4.formula.smiles, 'CC(OC(=O)CO)C(=O)O')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 148)

        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "DZOXUIJDHVERIU-UHFFFAOYSA-N-VHGWPYONJJ"}))

        # Check results
        self.assertEqual(mol5.formula.smiles, 'CC(O)C(=O)OCC(=O)O')
        self.assertEqual(mol5.formula.charge, 0)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.priority, 148)

        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OZZQHCBFUVFZGT-UHFFFAOYSA-N-PUCTACYANK"}))

        # Check results
        self.assertEqual(mol6.formula.smiles, 'CC(O)C(=O)OC(C)C(=O)O')
        self.assertEqual(mol6.formula.charge, 0)
        self.assertEqual(mol6.formula.mult, 1)
        self.assertEqual(mol6.priority, 162)

        mol7 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "FBYFHODQAUBIOO-UHFFFAOYSA-N-WMVTNPNKBT"}))

        # Check results
        self.assertEqual(mol7.formula.smiles, 'CC(OC(C)C(=O)O)C(=O)O')
        self.assertEqual(mol7.formula.charge, 0)
        self.assertEqual(mol7.formula.mult, 1)
        self.assertEqual(mol7.priority, 162)

        mol8 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "SZMWJECJLNOCKO-UHFFFAOYSA-N-LLVYIUOKLA"}))

        # Check results
        self.assertEqual(mol8.formula.smiles, 'CC(OCC(=O)O)C(=O)O')
        self.assertEqual(mol8.formula.charge, 0)
        self.assertEqual(mol8.formula.mult, 1)
        self.assertEqual(mol8.priority, 148)

        mol9 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QEVGZEDELICMKH-UHFFFAOYSA-N-ECOMIZURQR"}))

        # Check results
        self.assertEqual(mol9.formula.smiles, 'O=C(O)COCC(=O)O')
        self.assertEqual(mol9.formula.charge, 0)
        self.assertEqual(mol9.formula.mult, 1)
        self.assertEqual(mol9.priority, 134)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 9)

    def test_rdkitreact_bi_exact(self):

        # Insert molecule records
        self._insert_data_ions_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # To do options: bi
        input_kv['reactivity'] = Reactivity.BI

        # Exact atom mapping
        input_kv['react_exact_mapping_rdkit'] = {
            'key': 'react_exact_mapping',
            'value': True,
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, '[OH-]')
        self.assertEqual(mol1.formula.charge, -1)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 17)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'C[O-]')
        self.assertEqual(mol2.formula.charge, -1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 31)
        self.assertEqual(mol2.generation, 0)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "JUHDUIDUEUEQND-UHFFFAOYSA-N-CKOLEVABGL"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol3.formula.smiles, '[CH3+]')
        self.assertEqual(mol3.formula.charge, 1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 15)
        self.assertEqual(mol3.generation, 0)

        # Get results
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol4.formula.smiles, '[CH2+]C')
        self.assertEqual(mol4.formula.charge, 1)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 29)
        self.assertEqual(mol4.generation, 0)

        # Get results
        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check results
        self.assertEqual(mol5.level, Level.FORMULA)
        self.assertEqual(mol5.formula.smiles, 'CO')
        self.assertEqual(mol5.formula.charge, 0)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.priority, 32)
        self.assertEqual(mol5.generation, 1)

        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N-XUDWUARODM"}))

        # Check results
        self.assertEqual(mol6.level, Level.FORMULA)
        self.assertEqual(mol6.formula.smiles, 'CCO')
        self.assertEqual(mol6.formula.charge, 0)
        self.assertEqual(mol6.formula.mult, 1)
        self.assertEqual(mol6.priority, 46)
        self.assertEqual(mol6.generation, 1)

        mol7 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "LCGLNKUTAGEVQW-UHFFFAOYSA-N-GOADZRHIWV"}))

        # Check results
        self.assertEqual(mol7.level, Level.FORMULA)
        self.assertEqual(mol7.formula.smiles, 'COC')
        self.assertEqual(mol7.formula.charge, 0)
        self.assertEqual(mol7.formula.mult, 1)
        self.assertEqual(mol7.priority, 46)
        self.assertEqual(mol7.generation, 1)

        mol8 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XOBKSJJDNFUZPF-UHFFFAOYSA-N-UHPOQXJNMV"}))

        # Check results
        self.assertEqual(mol8.level, Level.FORMULA)
        self.assertEqual(mol8.formula.smiles, 'CCOC')
        self.assertEqual(mol8.formula.charge, 0)
        self.assertEqual(mol8.formula.mult, 1)
        self.assertEqual(mol8.priority, 60)
        self.assertEqual(mol8.generation, 1)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 8)

    def test_rdkitreact_all(self):

        # Insert molecule records
        self._insert_data_alcohol_diol_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[C:3][OH1]>>[C:1][O:2][C:3]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH3:1].[OH1:2]>>[C:1][O:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH:1][OH1:2]>>[C:1]=[O:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "KFZMGEQAYNKOFK-UHFFFAOYSA-N-NFMMMTIBZH"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CC(C)O')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 60)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "YPFDHNVEDLHUCE-UHFFFAOYSA-N-OKFNBHOIYP"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'OCCCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 76)
        self.assertEqual(mol2.generation, 0)

        # Get results
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ZAFNJMIOTHYJRJ-UHFFFAOYSA-N-OEWNUFEOVT"}))

        # Check results
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'CC(C)OC(C)C')
        self.assertEqual(mol3.formula.charge, 0)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.priority, 102)
        self.assertEqual(mol3.generation, 1)

        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AHHWIHXENZJRFG-UHFFFAOYSA-N-NUFHNADGKT"}))

        # Check results
        self.assertEqual(mol4.level, Level.FORMULA)
        self.assertEqual(mol4.formula.smiles, 'C1COC1')
        self.assertEqual(mol4.formula.charge, 0)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.priority, 58)
        self.assertEqual(mol4.generation, 1)

        mol5 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "SZXQTJUDPRGNJN-UHFFFAOYSA-N-NSDWWRWTCK"}))

        # Check results
        self.assertEqual(mol5.level, Level.FORMULA)
        self.assertEqual(mol5.formula.smiles, 'OCCCOCCCO')
        self.assertEqual(mol5.formula.charge, 0)
        self.assertEqual(mol5.formula.mult, 1)
        self.assertEqual(mol5.priority, 134)
        self.assertEqual(mol5.generation, 1)

        mol6 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QCEPAYDHIMIVTH-UHFFFAOYSA-N-KJTEIVPPCE"}))

        # Check results
        self.assertEqual(mol6.level, Level.FORMULA)
        self.assertEqual(mol6.formula.smiles, 'CC(O)COCCCO')
        self.assertEqual(mol6.formula.charge, 0)
        self.assertEqual(mol6.formula.mult, 1)
        self.assertEqual(mol6.priority, 134)
        self.assertEqual(mol6.generation, 1)

        mol7 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AFHJYKBGDDJSRR-UHFFFAOYSA-N-UMVWGZEAJI"}))

        # Check results
        self.assertEqual(mol7.level, Level.FORMULA)
        self.assertEqual(mol7.formula.smiles, 'CC(O)COC(C)C')
        self.assertEqual(mol7.formula.charge, 0)
        self.assertEqual(mol7.formula.mult, 1)
        self.assertEqual(mol7.priority, 118)

        mol8 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GOOHAUXETOMSMM-UHFFFAOYSA-N-SJYGZCHENI"}))

        # Check results
        self.assertEqual(mol8.level, Level.FORMULA)
        self.assertEqual(mol8.formula.smiles, 'CC1CO1')
        self.assertEqual(mol8.formula.charge, 0)
        self.assertEqual(mol8.formula.mult, 1)
        self.assertEqual(mol8.priority, 58)

        mol9 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AFHJYKBGDDJSRR-UHFFFAOYSA-N-UMVWGZEAJI"}))

        # Check results
        self.assertEqual(mol9.level, Level.FORMULA)
        self.assertEqual(mol9.formula.smiles, 'CC(O)COC(C)C')
        self.assertEqual(mol9.formula.charge, 0)
        self.assertEqual(mol9.formula.mult, 1)
        self.assertEqual(mol9.priority, 118)

        mol10 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QCEPAYDHIMIVTH-UHFFFAOYSA-N-KJTEIVPPCE"}))

        # Check results
        self.assertEqual(mol10.level, Level.FORMULA)
        self.assertEqual(mol10.formula.smiles, 'CC(O)COCCCO')
        self.assertEqual(mol10.formula.charge, 0)
        self.assertEqual(mol10.formula.mult, 1)
        self.assertEqual(mol10.priority, 134)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 10)

    def test_rdkitreact_nomols(self):

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule', 'value': '[C:1]>>[C:1]C',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # No molecules inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_rdkitreact_norules(self):

        # Insert molecule record
        self._insert_data_ch4_formula()

        # Task options dict
        input_kv = {}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'C')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 16)
        self.assertEqual(mol1.generation, 0)

        # Total number of molecules
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

    def test_rdkitreact_badrules(self):

        # Insert molecule records
        self._insert_data_sec_alcohols_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH1:][OH1:2]>>[C:1]=[O:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].>>[C:1][O:2]C',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1].[OH1:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[CH1:][OH1:2]>>[C:1][O:2]C',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BTANRVKWQNVYAZ-UHFFFAOYSA-N-KFXKIYRIGO"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CCC(C)O')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 74)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "KFZMGEQAYNKOFK-UHFFFAOYSA-N-NFMMMTIBZH"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'CC(C)O')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 60)
        self.assertEqual(mol2.generation, 0)

        # No additional molecules inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 2)

    def test_rdkitreact_product_error(self):

        # Insert molecule record
        self._insert_data_allene_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1]=[C:2]>>[C:1]#[C:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1].[C:2]>>[C:1]#[C:2]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "IYABWNGZIDDRAK-UHFFFAOYSA-N-YIFLNWIEQH"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'C=C=C')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 40)
        self.assertEqual(mol1.generation, 0)

        # No additional molecules inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 1)

    def test_rdkitreact_product_error_exact(self):

        # Insert molecule records
        self._insert_data_alcohol_diol_formula()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH3:1][OH1:2]>>[C:1][O:2]C',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][O:2].[C:3][O:4]>>[C:1][O:2][C:3].[O:4]',
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Exact atom mapping
        input_kv['react_exact_mapping_rdkit'] = {
            'key': 'react_exact_mapping',
            'value': True,
            'target': 'colibri.task.RDKitReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitreacttask(**input_kv)

        # Get results
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "KFZMGEQAYNKOFK-UHFFFAOYSA-N-NFMMMTIBZH"}))

        # Check results
        self.assertEqual(mol1.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol1.formula.smiles, 'CC(C)O')
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.priority, 60)
        self.assertEqual(mol1.generation, 0)

        # Get results
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "YPFDHNVEDLHUCE-UHFFFAOYSA-N-OKFNBHOIYP"}))

        # Check results
        self.assertEqual(mol2.level, Level.FORMULA_UNREACTIVE)
        self.assertEqual(mol2.formula.smiles, 'OCCCO')
        self.assertEqual(mol2.formula.charge, 0)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.priority, 76)
        self.assertEqual(mol2.generation, 0)

        # No additional molecules inserted
        self.assertEqual(self._client.test.mol.find({}).count(), 2)

if __name__ == '__main__':
    unittest.main()
