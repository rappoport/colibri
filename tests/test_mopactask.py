"""Test classes for the mopactask classes.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for mopactask classes

Test Classes:
    TestMOPACGeometryTask: Geometry task using MOPAC
    TestMOPACNudgeGeometryTask: Geometry task using MOPAC with nudge
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/29/2015'

#
# Standard library
#


import unittest
import logging


#
# colibri modules
#

from colibri.data import Molecule
from colibri.calc import Calculator, Context
from colibri.enums import Level
from colibri.utils import t2s, randomid
from colibri.exceptions import Structure3DError
import colibri.task

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_mopactask')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'


#
# Public functions
#


def get_suite():
    """Construct test suite for mopactask classes."""
    mopactask_suite = []

    # MOPACGeometryTask tests
    mopac_geometry_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestMOPACGeometryTask)
    mopactask_suite.append(mopac_geometry_suite)

    # MOPACNudgeGeometryTask tests
    mopac_nudge_geometry_suite = unittest.TestLoader().loadTestsFromTestCase(
        TestMOPACNudgeGeometryTask)
    mopactask_suite.append(mopac_nudge_geometry_suite)

    return unittest.TestSuite(mopactask_suite)

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()


#
# Test classes
#


class TestMOPACGeometryTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_h2o(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""},
            "z": [], "d": 18,
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0, "t": ["oxygen"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_nh3(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17, "f": {"i": None, "s": "N", "d": 17},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0, "t": ["nitrogen"],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

    def _insert_data_cl(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 35, "i": None, "+": 0, "m": 1, "q": -1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
1

Cl         0.00000        0.00000        0.00000"""},
            "z": [], "d": 35, "f": {"i": None, "s": "[Cl-]", "d": 35, "+": 0},
            "+": 0, "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "VEXZGXHMUGYJMC-UHFFFAOYSA-M-YSAZDIBDUY"})

    def _insert_data_proton_error(self):

        # Insert molecule record (misses the backslash in XYZ coords)
        self._client.test.mol.insert({
            "c": {"d": 1, "i": None, "m": 1, "q": 1, "w": "RDKit",
                  "v": "2015.3.1", "x": """
1

H          0.00000        0.00000        0.00000"""},
            "z": [], "d": 1, "f": {"i": None, "s": "[H+]", "d": 1},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"})

    def _insert_data_NN4_invalid_fragmentation(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 60, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
8

O         -1.44522        0.55657        0.13517
C         -0.73236       -0.58238       -0.23959
C          0.67590       -0.48147        0.25039
O          1.39884        0.42309       -0.14176
H         -0.73860       -0.67518       -1.34588
H         -1.21861       -1.47796        0.20032
H          1.05324       -1.21925        0.95474
H          2.31281        0.49873        0.16667"""},
            "z": [], "d": 60, "f": {"i": None, "s": "[O-]CC=[OH+]", "d": 60},
            "l": 1100, "3": 4, "2": "", "4": "", "8": 2, "t": ["NN"],
            "_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"})

    def _insert_data_h3o(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 19, "i": None, "m": 1, "q": 1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

O          0.00001        0.00001        0.06418
H         -0.90053        0.08188       -0.33952
H          0.37924       -0.82078       -0.33956
H          0.52114        0.73878       -0.33962"""},
            "z": [], "d": 19, "f": {"i": None, "s": "[OH3+]", "d": 19},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"})

    def _insert_data_bicycle_invalid_rearrangement(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 64, "i": None, "+": 0, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
9

C         -1.02078       -0.86929        0.00000
C          0.25857       -0.69275       -0.00001
C          1.45272       -0.01508        0.00000
C          0.27083        0.68383       -0.00000
C         -1.00521        0.88314       -0.00001
H         -1.72913       -1.68666        0.00006
H          1.97531        0.04732        0.97674
H          1.97533        0.04725       -0.97665
H         -1.69889        1.71294        0.00004"""},
            "z": [], "d": 64, "f": {
                "i": None, "s": "C1=C2CC2=C1", "d": 64, "+": 0},
            "+": 0, "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"})

    def _insert_data_oxetane_invalid_ring_opening(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 74, "i": None, "+": 0, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
11

O          1.61434        0.06531        0.33615
C          0.46889        0.06698       -0.46843
C         -0.47953        1.16157       -0.04444
C         -1.36111       -0.00031        0.34835
O         -0.41775       -0.96071       -0.07815
H          2.14178       -0.72513        0.05134
H          0.70972        0.07987       -1.56064
H         -0.89714        1.67107       -0.94034
H         -1.50667       -0.05024        1.44941
H         -2.32178       -0.03317       -0.22008
H         -0.77402       -1.36532       -0.91462"""},
            "z": [], "d": 74, "f": {
                "i": None, "s": "OC1[CH-]C[OH+]1", "d": 74, "+": 0},
            "+": 0, "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "SWYJFTMYVXUBDP-UHFFFAOYSA-O-FAHLOHWNTB"})

    def _insert_data_ammonium_badxyz(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 18, "i": None, "m": 1, "q": 1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

N          0.00001        0.00000        0.00000
H         -0.76151        0.40218       -0.59098
H         -0.76151        0.40218       -0.59098
H         -0.00289        0.46483        0.93523"""},
            "z": [], "d": 18, "f": {"i": None, "s": "[NH4+]", "d": 18},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-O-WKXUEGBFRF"})

    def _insert_data_sucrose(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 342, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
45

O         -2.43177        3.43568       -0.31152
C         -1.86961        2.61954        0.68097
C         -1.76133        1.15945        0.20981
O         -0.98968        0.45246        1.16936
C         -0.73178       -0.88421        0.76575
O          0.02660       -0.91860       -0.43921
C          1.44018       -0.88636       -0.23829
C          1.97581       -2.33820       -0.14547
O          1.57853       -2.94392        1.05286
O          1.80401       -0.15758        0.92165
C          3.05737        0.44594        0.68074
C          3.09498        1.82674        1.34556
O          2.05000        2.62815        0.86681
C          3.31254        0.51384       -0.83227
O          4.48395       -0.19351       -1.15159
C          2.09348       -0.16791       -1.43312
O          1.22688        0.77936       -2.01403
C         -2.04389       -1.67691        0.59737
O         -1.76992       -2.96795        0.11673
C         -2.99053       -0.95740       -0.37438
O         -4.24712       -1.58504       -0.37077
C         -3.15547        0.51104        0.04680
O         -3.89062        1.19777       -0.93419
H         -1.72689        3.57752       -0.99627
H         -2.49966        2.67643        1.59416
H         -0.85872        3.00124        0.95271
H         -1.24507        1.16760       -0.77529
H         -0.19771       -1.38035        1.60473
H          1.58816       -2.92772       -1.00704
H          3.08770       -2.34070       -0.19657
H          1.92750       -3.87249        1.02290
H          3.83889       -0.18008        1.16825
H          2.99137        1.70000        2.44659
H          4.07427        2.31408        1.13862
H          2.13040        3.49695        1.33935
H          3.40403        1.56697       -1.19207
H          4.70464        0.03213       -2.09299
H          2.38513       -0.88685       -2.23158
H          1.00806        1.45962       -1.32490
H         -2.54955       -1.74624        1.59010
H         -1.38694       -3.48076        0.87576
H         -2.55593       -0.98407       -1.40297
H         -4.16880       -2.39086       -0.94531
H         -3.68631        0.55639        1.02830
H         -4.84947        1.00416       -0.76491"""},
            "z": [], "d": 342, "f": {
                "i": None,  "d": 342,
                "s": ("OC[C@H]1O[C@H](O[C@]2(CO)O[C@H](CO)[C@@H]" +
                      "(O)[C@@H]2O)[C@H](O)[C@@H](O)[C@@H]1O")},
                "l": 1100, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "CZMRCDWAGMRECN-UGDNZRGBSA-N-VOXHORWGWB"})

    def _run_mopacgeometrytask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'mopac_geometry_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'MOPACGeometryTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # MOPAC options
        input_kv['geometry_validation'] = True
        input_kv['geometry_attempts'] = 5

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_mopac(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -322.6792, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)
        self.assertEqual(mol.quality, 0)
        self.assertListEqual(mol.tags, ['oxygen'])

    def test_mopac_tags(self):

        # Insert molecule records
        self._insert_data_h2o()
        self._insert_data_nh3()

        # Input options dict
        input_kv = {}

        # Tags
        input_kv['tags'] = 'nitrogen'

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.CONFIGURATION)
        self.assertEqual(mol1.configuration.charge, 0)
        self.assertEqual(mol1.configuration.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)
        self.assertListEqual(mol1.tags, ['oxygen'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.smiles, 'N')
        self.assertAlmostEqual(mol2.geometry.energy, -226.7529, places=4)
        self.assertTrue(mol2.converged)
        self.assertEqual(mol2.priority, 17)
        self.assertListEqual(mol2.tags, ['nitrogen'])

    def test_mopac_raw(self):

        # Insert molecule records
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Raw write
        input_kv['raw_write'] = True

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check molecule result
        self.assertEqual(mol.level, Level.GEOMETRY)

        # Raw output key
        raw_output_key = mol.geometry.raw_output
        self.assertTrue(raw_output_key)

        # Raw output file metadata
        raw_output_metadata = self._client.test.raw.files.find_one(
            {'_id': raw_output_key})

        # Check molecule key
        self.assertEqual(
            raw_output_metadata['molkey'],
            'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')

    def test_mopac_atom(self):

        # Insert molecule record
        self._insert_data_cl()

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VEXZGXHMUGYJMC-UHFFFAOYSA-M-YSAZDIBDUY"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.configuration.charge, -1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[Cl-]')
        self.assertAlmostEqual(mol.geometry.energy, -269.09445, places=4)
        self.assertEqual(mol.priority, 35)

    def test_mopac_noinput(self):

        # Run MOPACGeometryTask without input
        self._run_mopacgeometrytask()

        # No molecules were produced
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_mopac_execution_retry(self):

        # Insert molecule record
        self._insert_data_proton_error()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['tasks_per_calc'] = 3

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, '[H+]')
        self.assertEqual(mol.priority, 1)
        with self.assertRaises(AttributeError):
            mol.geometry

    def test_mopac_execution_error(self):

        # Insert molecule record
        self._insert_data_proton_error()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['tasks_per_calc'] = 5

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles, '[H+]')
        self.assertEqual(mol.priority, 1)
        with self.assertRaises(AttributeError):
            mol.geometry

    def test_mopac_execution_timeout(self):

        # Insert molecule record
        self._insert_data_sucrose()

        # Input options dict
        input_kv = {}

        # Calculation timeout
        input_kv['geometry_timeout'] = 1

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "CZMRCDWAGMRECN-UGDNZRGBSA-N-VOXHORWGWB"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles,
                         ('OC[C@H]1O[C@H](O[C@]2(CO)O[C@H](CO)[C@@H]' +
                          '(O)[C@@H]2O)[C@H](O)[C@@H](O)[C@@H]1O'))
        self.assertEqual(mol.priority, 342)
        with self.assertRaises(AttributeError):
            mol.geometry

    def test_mopac_execution_noconvergence(self):

        # Insert molecule record
        self._insert_data_sucrose()

        # Input options dict
        input_kv = {}

        # Geometry options for MOPAC
        input_kv['geometry_options_mopac'] = {
            'key': 'geometry_options',
            'value': 'xyz cycles=5',
            'target': 'colibri.task.MOPACGeometryTask',
            'precedence': 'command'}

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "CZMRCDWAGMRECN-UGDNZRGBSA-N-VOXHORWGWB"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol.formula.smiles,
                         ('OC[C@H]1O[C@H](O[C@]2(CO)O[C@H](CO)[C@@H]' +
                          '(O)[C@@H]2O)[C@H](O)[C@@H](O)[C@@H]1O'))
        self.assertEqual(mol.priority, 342)
        self.assertEqual(mol.quality, 3)
        self.assertEqual(mol.geometry.energy, 0)

    def test_mopac_structure3d_error(self):

        # Insert molecule record
        self._insert_data_ammonium_badxyz()

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask()

        # Get result from database
        input_data = self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-O-WKXUEGBFRF"})

        # Check result from database
        self.assertEqual(input_data[t2s['level']], Level.CONFIGURATION_ERROR)
        self.assertEqual(input_data[t2s['formula']][t2s['smiles']],
                         '[NH4+]')
        self.assertEqual(input_data[t2s['priority']], 18)
        self.assertNotIn(t2s['etag'], input_data)

        # Reading raises exception
        with self.assertRaises(Structure3DError):
            Molecule.fromdict(input_data)

    def test_mopac_nofragmentation(self):

        # Insert new molecule record
        self._insert_data_h3o()

        # Input options dict
        input_kv = {}

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.formula.smiles, '[OH3+]')
        self.assertEqual(mol.priority, 19)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -327.16895, places=4)

    def test_mopac_fragmentation_retry(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.priority, 60)
        self.assertEqual(mol.generation, 4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -887.23759, places=4)
        self.assertListEqual(mol.tags, ['NN'])

    def test_mopac_fragmentation_retry_nudge(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Use geometry nudge
        input_kv['geometry_nudge'] = True

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_NUDGE)
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.priority, 60)
        self.assertEqual(mol.generation, 4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -887.23759, places=4)
        self.assertListEqual(mol.tags, ['NN'])

    def test_mopac_fragmentation_error(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['geometry_attempts'] = 3

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -887.23759, places=4)

    def test_mopac_fragmentation_novalidation(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Number of tasks
        input_kv['geometry_attempts'] = 3

        # No geometry validation
        input_kv['geometry_validation'] = False

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -887.23759, places=4)

    def test_mopac_rearrangement_retry(self):

        # Insert new molecule record
        self._insert_data_bicycle_invalid_rearrangement()

        # Input options dict
        input_kv = {}

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.attempts, 1)
        self.assertEqual(mol.formula.smiles, 'C1=C2CC2=C1')
        self.assertEqual(mol.priority, 64)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -661.07153, places=4)

    def test_mopac_ring_opening_retry(self):

        # Insert new molecule record
        self._insert_data_oxetane_invalid_ring_opening()

        # Input options dict
        input_kv = {}

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "SWYJFTMYVXUBDP-UHFFFAOYSA-O-FAHLOHWNTB"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.attempts, 1)
        self.assertEqual(mol.formula.smiles, 'OC1[CH-]C[OH+]1')
        self.assertEqual(mol.priority, 74)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 0)
        self.assertAlmostEqual(mol.geometry.energy, -1039.6017, places=3)

    def test_mopac_noexecutable(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Executable path does not include MOPAC
        input_kv['executable_path'] = '/usr/bin:/bin'

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')

    def test_mopac_noscratchpath(self):

        # Insert new molecule record
        self._insert_data_h2o()

        # Input options dict
        input_kv = {}

        # Scratch path does not exist or is not writable
        input_kv['scratch_path'] = '/DOESNTEXIST'

        # Number of attempts
        input_kv['geometry_attempts'] = 1

        # Run MOPACGeometryTask
        self._run_mopacgeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_ERROR)
        self.assertEqual(mol.formula.smiles, 'O')

    def test_mopac_rollback(self):
        global _opts

        # Insert molecule record
        self._insert_data_h2o()

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'mopac_geometry_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'MOPACGeometryTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Batch size
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Locks
        input_kv['optimistic_lock'] = False
        input_kv['pessimistic_lock'] = True

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create Context object
        context = Context(opts)

        # Create new MOPACGeometryTask
        task = colibri.task.MOPACGeometryTask(context, opts)

        # Setup
        task.setup()

        # Retrieve
        task.retrieve()

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.CONFIGURATION_LOCKED)
        self.assertEqual(mol1.etag, calc_name)
        self.assertEqual(mol1.formula.smiles, 'O')

        # Read configuration output
        task._read_configuration_output()

        # Rollback
        task.rollback()

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol2.level, Level.CONFIGURATION)
        self.assertEqual(mol2.etag, '')
        self.assertEqual(mol2.formula.smiles, 'O')


class TestMOPACNudgeGeometryTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_NN4_invalid_fragmentation(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 60, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
8

O         -1.44522        0.55657        0.13517
C         -0.73236       -0.58238       -0.23959
C          0.67590       -0.48147        0.25039
O          1.39884        0.42309       -0.14176
H         -0.73860       -0.67518       -1.34588
H         -1.21861       -1.47796        0.20032
H          1.05324       -1.21925        0.95474
H          2.31281        0.49873        0.16667"""},
            "z": [], "d": 60, "f": {"i": None, "s": "[O-]CC=[OH+]", "d": 60},
            "l": 1110, "3": 4, "2": "", "4": "", "8": 2, "t": ["NN"],
            "_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"})

    def _insert_data_zwitterion_invalid_fragmentation(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 74, "i": None, "+": 0, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
15

C          1.79183        0.00002        0.52968
C          0.86941        0.00005       -0.70138
O         -0.53953       -0.00001       -0.43616
C         -0.93856        1.16646        0.27585
C         -0.93846       -1.16651        0.27584
H          1.68633        0.91893        1.14155
H          1.68633       -0.91889        1.14155
H          1.12971       -0.88137       -1.32812
H          1.12967        0.88140       -1.32803
H         -0.59191        1.16873        1.33382
H         -2.04792        1.24103        0.27756
H         -0.56668        2.08657       -0.22710
H         -0.56657       -2.08660       -0.22722
H         -0.59180       -1.16881        1.33386
H         -2.04786       -1.24108        0.27749"""},
            "z": [], "d": 74, "f": {
                "i": None, "s": "[CH2-]C[O+](C)C", "d": 74, "+": 0}, "+": 0,
            "l": 1110, "3": 0, "2": "", "4": "", "8": 0, "t": ["zwitterion"],
            "_id": "ZXALCXREIHRNNM-UHFFFAOYSA-N-RCKSAQMUVZ"})

    def _insert_data_bicycle_invalid_rearrangement(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 64, "i": None, "+": 0, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
9

C         -1.02078       -0.86929        0.00000
C          0.25857       -0.69275       -0.00001
C          1.45272       -0.01508        0.00000
C          0.27083        0.68383       -0.00000
C         -1.00521        0.88314       -0.00001
H         -1.72913       -1.68666        0.00006
H          1.97531        0.04732        0.97674
H          1.97533        0.04725       -0.97665
H         -1.69889        1.71294        0.00004"""},
            "z": [], "d": 64, "f": {
                "i": None, "s": "C1=C2CC2=C1", "d": 64, "+": 0},
            "+": 0, "l": 1110, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"})

    def _insert_data_oxetane_invalid_ring_opening(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 74, "i": None, "+": 0, "m": 1, "q": 0, "w": u"RDKit",
                  "v": u"2015.3.1", "x": """\
11

O          1.61434        0.06531        0.33615
C          0.46889        0.06698       -0.46843
C         -0.47953        1.16157       -0.04444
C         -1.36111       -0.00031        0.34835
O         -0.41775       -0.96071       -0.07815
H          2.14178       -0.72513        0.05134
H          0.70972        0.07987       -1.56064
H         -0.89714        1.67107       -0.94034
H         -1.50667       -0.05024        1.44941
H         -2.32178       -0.03317       -0.22008
H         -0.77402       -1.36532       -0.91462"""},
            "z": [], "d": 74, "f": {
                "i": None, "s": "OC1[CH-]C[OH+]1", "d": 74,  "+": 0},
            "+": 0, "l": 1110, "3": 0, "2": "", "4": "", "8": 1,
            "_id": "SWYJFTMYVXUBDP-UHFFFAOYSA-O-FAHLOHWNTB"})

    def _insert_data_cl(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 35, "i": None, "+": 0, "m": 1, "q": -1, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
1

Cl         0.00000        0.00000        0.00000"""},
            "z": [], "d": 35, "f": {"i": None, "s": "[Cl-]", "d": 35, "+": 0},
            "+": 0, "l": 1110, "3": 0, "2": "", "4": "", "8": 0,
            "_id": "VEXZGXHMUGYJMC-UHFFFAOYSA-M-YSAZDIBDUY"})

    def _run_mopacnudgegeometrytask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'mopac_nudge_geometry_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task',
            'value': 'MOPACNudgeGeometryTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # MOPAC geometry nudge options
        input_kv['geometry_validation'] = True
        input_kv['geometry_nudge_attempts'] = 5
        input_kv['geometry_nudge_strength'] = 500.0
        input_kv['geometry_nudge_factor'] = 2.0

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_mopac_nudge(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -886.42115, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.priority, 60)

    def test_mopac_nudge_tags(self):

        # Insert molecule records
        self._insert_data_NN4_invalid_fragmentation()
        self._insert_data_zwitterion_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Tags
        input_kv['tags'] = 'zwitterion'

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol1.level, Level.CONFIGURATION_NUDGE)
        self.assertEqual(mol1.configuration.charge, 0)
        self.assertEqual(mol1.configuration.mult, 1)
        self.assertEqual(mol1.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol1.priority, 60)
        self.assertEqual(mol1.quality, 0)
        self.assertEqual(mol1.attempts, 2)
        self.assertListEqual(mol1.tags, ['NN'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "ZXALCXREIHRNNM-UHFFFAOYSA-N-RCKSAQMUVZ"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.geometry.charge, 0)
        self.assertEqual(mol2.geometry.mult, 1)
        self.assertAlmostEqual(mol2.geometry.energy, -918.50899, places=4)
        self.assertTrue(mol2.converged)
        self.assertEqual(mol2.quality, 2)
        self.assertEqual(mol2.formula.smiles, '[CH2-]C[O+](C)C')
        self.assertEqual(mol2.priority, 74)
        self.assertEqual(mol2.attempts, 0)
        self.assertListEqual(mol2.tags, ['zwitterion'])

    def test_mopac_nudge_atom(self):

        # Insert molecule record
        self._insert_data_cl()

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VEXZGXHMUGYJMC-UHFFFAOYSA-M-YSAZDIBDUY"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.configuration.charge, -1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[Cl-]')
        self.assertAlmostEqual(mol.geometry.energy, -269.09445, places=4)
        self.assertEqual(mol.priority, 35)

    def test_mopac_nudge_fragmentation_retry(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 5.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 5

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_NUDGE)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -887.02122, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.priority, 60)

    def test_mopac_nudge_fragmentation_error(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 5.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 3

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -887.02122, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.priority, 60)

    def test_mopac_nudge_att3(self):

        # Insert new molecule record
        self._insert_data_NN4_invalid_fragmentation()

        # Input options dict
        input_kv = {}

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 3
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 5.0

        # Nudge factor
        input_kv['geometry_nudge_factor'] = 2.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 5

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XCUVZINPLNBHQY-UHFFFAOYSA-O-IKFDXCRCQW"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -886.78365, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, '[O-]CC=[OH+]')
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.priority, 60)

    def test_mopac_nudge_rearrangement(self):

        # Insert new molecule record
        self._insert_data_bicycle_invalid_rearrangement()

        # Input options dict
        input_kv = {}

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 500.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 3

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -659.93951, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, 'C1=C2CC2=C1')
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.priority, 64)

    def test_mopac_nudge_rearrangement_retry(self):

        # Insert new molecule record
        self._insert_data_bicycle_invalid_rearrangement()

        # Input options dict
        input_kv = {}

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 5.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 3

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_NUDGE)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -661.06543, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, 'C1=C2CC2=C1')
        self.assertEqual(mol.attempts, 1)
        self.assertEqual(mol.priority, 64)

    def test_mopac_nudge_rearrangement_error(self):

        # Insert new molecule record
        self._insert_data_bicycle_invalid_rearrangement()

        # Input options dict
        input_kv = {}

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 5.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 1

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -661.06543, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, 'C1=C2CC2=C1')
        self.assertEqual(mol.attempts, 1)
        self.assertEqual(mol.priority, 64)

    def test_mopac_nudge_rearrangement_att3(self):

        # Insert new molecule record
        self._insert_data_bicycle_invalid_rearrangement()

        # Input options dict
        input_kv = {}

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 5
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 5.0

        # Nudge factor
        input_kv['geometry_nudge_factor'] = 2.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 5

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "WCINGRVEIFOCQS-UHFFFAOYSA-N-TDJCBNRFZS"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY)
        self.assertEqual(mol.geometry.charge, 0)
        self.assertEqual(mol.geometry.mult, 1)
        self.assertAlmostEqual(mol.geometry.energy, -660.6110, places=4)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertEqual(mol.formula.smiles, 'C1=C2CC2=C1')
        self.assertEqual(mol.attempts, 0)
        self.assertEqual(mol.priority, 64)

    def test_mopac_nudge_ring_opening_invalid(self):

        # Insert new molecule record
        self._insert_data_oxetane_invalid_ring_opening()

        # Input options dict
        input_kv = {}

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 3
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Nudge strength
        input_kv['geometry_nudge_strength'] = 5.0

        # Nudge factor
        input_kv['geometry_nudge_factor'] = 2.0

        # Geometry attempts
        input_kv['geometry_nudge_attempts'] = 3

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask(**input_kv)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "SWYJFTMYVXUBDP-UHFFFAOYSA-O-FAHLOHWNTB"}))

        # Check result
        self.assertEqual(mol.level, Level.GEOMETRY_INVALID)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, 'OC1[CH-]C[OH+]1')
        self.assertEqual(mol.priority, 74)
        self.assertTrue(mol.converged)
        self.assertEqual(mol.quality, 2)
        self.assertAlmostEqual(mol.geometry.energy, -1038.59509, places=3)

    def test_mopac_nudge_noinput(self):

        # Run MOPACNudgeGeometryTask
        self._run_mopacnudgegeometrytask()

        # No molecules were produced
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

if __name__ == '__main__':
    unittest.main()
