"""Colbiri test suite.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Function:
    get_suite: Construct colibri test suite
"""

__all__ = ['get_suite']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/18/2015'

#
# Library modules
#

import unittest

#
# Test modules
#

# Task tests
import tests.test_flasktask
import tests.test_moltask
import tests.test_rdkittask
import tests.test_openbabeltask
import tests.test_mopactask
import tests.test_orcatask
import tests.test_rdkitreact
import tests.test_rdkitflasktask

# Command tests
import tests.test_initcommand
import tests.test_addcommand
import tests.test_fetchcommand
import tests.test_deletecommand
import tests.test_runcommand
import tests.test_statcommand
import tests.test_suspendcommand
import tests.test_resumecommand
import tests.test_loadcommand
import tests.test_dumpcommand
import tests.test_cleancommand

# Console tests
import tests.test_initconsole
import tests.test_addconsole
import tests.test_fetchconsole
import tests.test_deleteconsole
import tests.test_runconsole
import tests.test_statconsole
import tests.test_suspendconsole
import tests.test_resumeconsole
import tests.test_loadconsole
import tests.test_dumpconsole
import tests.test_cleanconsole
import tests.test_configconsole

# Script tests
import tests.test_protonscript
import tests.test_networkscript

#
# Auxiliary modules
#

import tests.tmpmongo
import tests.testopts


#
# Public functions
#


def get_suite():
    """Construct colibri test suite."""

    suite = []

    # Task tests
    flasktask_suite = tests.test_flasktask.get_suite()
    suite.append(flasktask_suite)

    moltask_suite = tests.test_moltask.get_suite()
    suite.append(moltask_suite)

    rdkittask_suite = tests.test_rdkittask.get_suite()
    suite.append(rdkittask_suite)

    openbabeltask_suite = tests.test_openbabeltask.get_suite()
    suite.append(openbabeltask_suite)

    mopactask_suite = tests.test_mopactask.get_suite()
    suite.append(mopactask_suite)

    orcatask_suite = tests.test_orcatask.get_suite()
    suite.append(orcatask_suite)

    rdkitreact_suite = tests.test_rdkitreact.get_suite()
    suite.append(rdkitreact_suite)

    rdkitflasktask_suite = tests.test_rdkitflasktask.get_suite()
    suite.append(rdkitflasktask_suite)

    # Command tests
    initcommand_suite = tests.test_initcommand.get_suite()
    suite.append(initcommand_suite)

    addcommand_suite = tests.test_addcommand.get_suite()
    suite.append(addcommand_suite)

    fetchcommand_suite = tests.test_fetchcommand.get_suite()
    suite.append(fetchcommand_suite)

    deletecommand_suite = tests.test_deletecommand.get_suite()
    suite.append(deletecommand_suite)

    runcommand_suite = tests.test_runcommand.get_suite()
    suite.append(runcommand_suite)

    statcommand_suite = tests.test_statcommand.get_suite()
    suite.append(statcommand_suite)

    suspendcommand_suite = tests.test_suspendcommand.get_suite()
    suite.append(suspendcommand_suite)

    resumecommand_suite = tests.test_resumecommand.get_suite()
    suite.append(resumecommand_suite)

    loadcommand_suite = tests.test_loadcommand.get_suite()
    suite.append(loadcommand_suite)

    dumpcommand_suite = tests.test_dumpcommand.get_suite()
    suite.append(dumpcommand_suite)

    cleancommand_suite = tests.test_cleancommand.get_suite()
    suite.append(cleancommand_suite)

    # Console tests
    initconsole_suite = tests.test_initconsole.get_suite()
    suite.append(initconsole_suite)

    addconsole_suite = tests.test_addconsole.get_suite()
    suite.append(addconsole_suite)

    fetchconsole_suite = tests.test_fetchconsole.get_suite()
    suite.append(fetchconsole_suite)

    deleteconsole_suite = tests.test_deleteconsole.get_suite()
    suite.append(deleteconsole_suite)

    runconsole_suite = tests.test_runconsole.get_suite()
    suite.append(runconsole_suite)

    statconsole_suite = tests.test_statconsole.get_suite()
    suite.append(statconsole_suite)

    suspendconsole_suite = tests.test_suspendconsole.get_suite()
    suite.append(suspendconsole_suite)

    resumeconsole_suite = tests.test_resumeconsole.get_suite()
    suite.append(resumeconsole_suite)

    loadconsole_suite = tests.test_loadconsole.get_suite()
    suite.append(loadconsole_suite)

    dumpconsole_suite = tests.test_dumpconsole.get_suite()
    suite.append(dumpconsole_suite)

    cleanconsole_suite = tests.test_cleanconsole.get_suite()
    suite.append(cleanconsole_suite)

    # Script tests
    protonscript_suite = tests.test_protonscript.get_suite()
    suite.append(protonscript_suite)

    networkscript_suite = tests.test_networkscript.get_suite()
    suite.append(networkscript_suite)

    return unittest.TestSuite(suite)
