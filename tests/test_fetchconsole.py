"""Test classes for colib fetch console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib fetch console subcommand

Test Classes:
    TestFetchConsoleCommand: Test classes for FetchConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, FetchConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_fetchconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib fetch console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestFetchConsoleCommand)
    return suite

#
# Test classes
#


class TestFetchConsoleCommand(unittest.TestCase):

    def _init_fetchconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        FetchConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return FetchConsoleCommand(args=args, opts=opts)

    def test_fetchconsole_mol(self):

        # Initialize FetchConsoleCommand
        console = self._init_fetchconsole(['colib', 'fetch', '-e'])

        # Check generated FetchCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^FetchCommand\(molecules=<generator .*>, molecule_ids=\[\], " +
            r"flasks=None, flask_ids=None, opts=Options\(.*\)\)")

    def test_fetchconsole_mol_ids(self):

        # Initialize FetchConsoleCommand
        console = self._init_fetchconsole(['colib', 'fetch', '-e', 'C'])

        # Check generated FetchCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^FetchCommand\(molecules=<generator .*>, " +
            r"molecule_ids=\['C'\], flasks=None, flask_ids=None, " +
            r"opts=Options\(.*\)\)")

    def test_fetchconsole_flask(self):

        # Initialize FetchConsoleCommand
        console = self._init_fetchconsole(['colib', 'fetch', '-E'])

        # Check generated FetchCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^FetchCommand\(molecules=None, molecule_ids=None, " +
            r"flasks=<generator .*>, flask_ids=\[\], opts=Options\(.*\)\)")

    def test_fetchconsole_flask_ids(self):

        # Initialize FetchConsoleCommand
        console = self._init_fetchconsole(['colib', 'fetch', '-E', 'CO.CO'])

        # Check generated FetchCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^FetchCommand\(molecules=None, molecule_ids=None, " +
            r"flasks=<generator .*>, flask_ids=\['CO\.CO'\], " +
            r"opts=Options\(.*\)\)")

    def test_fetchconsole_noreader(self):

        # Initialize FetchConsoleCommand
        with self.assertRaisesRegexp(SystemExit, 'Nothing to do'):
            self._init_fetchconsole(['colib', 'fetch'])


if __name__ == '__main__':
    unittest.main()
