"""Test classes for colib dump console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib dump console subcommand

Test Classes:
    TestDumpConsoleCommand: Test classes for DumpConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, DumpConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_dumpconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib dump console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestDumpConsoleCommand)
    return suite

#
# Test classes
#


class TestDumpConsoleCommand(unittest.TestCase):

    def _init_dumpconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        DumpConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return DumpConsoleCommand(args=args, opts=opts)

    def test_dump(self):

        # Initialize DumpConsoleCommand
        console = self._init_dumpconsole(
            ['colib', 'dump'])

        # Check generated DumpCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DumpCommand\(export_dep=False, export_var=False, " +
            r"export_default=False, exclude=None, " +
            r"output_file='test.graphml', opts=Options\(.*\)\)")

    def test_dump_export_dep_var_default(self):

        # Initialize DumpConsoleCommand
        console = self._init_dumpconsole(
            ['colib', 'dump', '--export-dep', '--export-var',
             '--export-default'])

        # Check generated DumpCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DumpCommand\(export_dep=True, export_var=True, " +
            r"export_default=True, exclude=None, " +
            r"output_file='test.graphml', opts=Options\(.*\)\)")

    def test_dump_export_exclude(self):

        # Initialize DumpConsoleCommand
        console = self._init_dumpconsole(
            ['colib', 'dump', '--exclude', 'chemistry', 'etag'])

        # Check generated DumpCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DumpCommand\(export_dep=False, export_var=False, " +
            r"export_default=False, exclude=\['chemistry', 'etag'\], " +
            r"output_file='test.graphml', opts=Options\(.*\)\)")

    def test_dump_output_file(self):

        # Initialize DumpConsoleCommand
        console = self._init_dumpconsole(
            ['colib', 'dump', '-o', 'output.graphml'])

        # Check generated DumpCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DumpCommand\(export_dep=False, export_var=False, " +
            r"export_default=False, exclude=None, " +
            r"output_file='output.graphml', opts=Options\(.*\)\)")

    def test_dump_tags(self):

        # Initialize DumpConsoleCommand
        console = self._init_dumpconsole(
            ['colib', 'dump', '-@', 'LL', 'LN'])

        # Check generated DumpCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^DumpCommand\(export_dep=False, export_var=False, " +
            r"export_default=False, exclude=None, " +
            r"output_file='test.graphml', "
            r"opts=Options\(.*tags=OptionItem\(.*value=\['LL', 'LN'\]" +
            r".*\).*\)\)")


if __name__ == '__main__':
    unittest.main()
