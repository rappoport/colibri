"""Test classes for colibri delete command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri delete command

Test Classes:
    TestDeleteCommand: Test classes for DeleteCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/06/2015'

#
# Standard library


import unittest
import logging

#
# colibri modules
#

from colibri.api import delete
from colibri.data import Molecule, Flask
from colibri.enums import Result
from colibri.exceptions import CommandError

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_deletecommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri delete command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDeleteCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestDeleteCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_mol_data(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {
                "d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""}, "z": [], "d": 18,
            "g": {
                "e": -322.6792, "d": 18, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC",
                "7": True, "v": "2012", "x": """\
3

O    -0.000001     0.058320     0.000000
H    -0.759932    -0.519445     0.000000
H     0.760055    -0.519304     0.000000"""},
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0,
            "t": ["liquid", "hydrophilic"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='CC(=O)C', generation=12,
                     tags=['liquid']).todict())

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='BrBr', generation=4,
                     tags=['liquid', 'halogen']).todict())

    def _insert_flask_data(self):

        # Insert new flask record
        self._client.test.flask.insert({
            "a": [], "9": None, "b": [], "e": -656.5736,
            "d": 656, "0": None, "k": [
                {"i": None, "s": "F",
                 "_id": "KRHYYFGTRYWZRS-UHFFFAOYSA-N-MMIJCGAIQJ"},
                {"i": None, "s": "C",
                 "_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}],
            "j": 2300, "m": 1, "s": "F.C", "q": 0, "3*": [0],
            "3": 0, "2": "", "4": "", "0*": [], "8": 0,
            "t": ["10-electron", "gas"],
            "_id": "UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS"})

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='N.O', tags=['liquid', '10-electron'],
                  generation=0).todict())

        # Insert new flask record
        self._client.test.flask.insert(Flask(
            smiles='[NH4+].[OH-].O', generation=1).todict())

        # Insert new flask record
        self._client.test.flask.insert(Flask(
            smiles='COC.O', generation=4).todict())

    def test_delete_mol_noop(self):

        # Insert molecule records
        self._insert_mol_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Run colibri delete command
        result = delete(molecules=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

    def test_delete_mol_tags(self):

        # Insert molecule records
        self._insert_mol_data()

        # Get configuration options
        opts = self._update_conf_opts(tags=['liquid'])

        # Check molecule results before deletion
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Run colibri delete command
        result = delete(molecules=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_delete_mol_level(self):

        # Insert molecule records
        self._insert_mol_data()

        # Get configuration options
        opts = self._update_conf_opts(level=['CONFIGURATION', 'GEOMETRY'])

        # Check molecule results before deletion
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Run colibri delete command
        result = delete(molecules=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 2)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'}).count(), 0)

    def test_delete_mol_min_max_generation(self):

        # Insert molecule records
        self._insert_mol_data()

        # Get configuration options
        opts = self._update_conf_opts(min_generation=10, max_generation=12)

        # Check molecule results before deletion
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Run colibri delete command
        result = delete(molecules=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 2)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS'}).count(), 0)

    def test_delete_mol_smiles(self):

        # Insert molecule records
        self._insert_mol_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Check molecule result before deletion
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP'}).count(), 1)

        # Run colibri delete command
        result = delete(molecules=True, molecule_ids=['BrBr'], opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 2)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP'}).count(), 0)

    def test_delete_mol_key(self):

        # Insert molecule records
        self._insert_mol_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Check molecule result before deletion
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'}).count(), 1)

        # Run colibri delete command
        result = delete(
            molecules=True,
            molecule_ids=['XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'], opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 2)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'}).count(), 0)

    def test_delete_flask_noop(self):

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

        # Run colibri delete command
        result = delete(flasks=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

    def test_delete_flask_tags(self):

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts(tags=['10-electron'])

        # Check flask results before deletion
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

        # Run colibri delete command
        result = delete(flasks=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 2)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'}).count(), 0)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ'}).count(), 0)

    def test_delete_flask_status(self):

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts(status=['UNREACTIVE'])

        # Check flask results before deletion
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

        # Run colibri delete command
        result = delete(flasks=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 3)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'}).count(), 0)

    def test_delete_flask_min_max_generation(self):

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts(min_generation=1, max_generation=3)

        # Check flask results before deletion
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

        # Run colibri delete command
        result = delete(flasks=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 3)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'ZFSFDELZPURLKD-UHFFFAOYSA-N-SBNUOHFJDP'}).count(), 0)

    def test_delete_flask_smiles(self):

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Check flask result before deletion
        self.assertEqual(self._client.test.flask.find(
            {'_id': 'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND'}).count(), 1)

        # Run colibri delete command
        result = delete(flasks=True, flask_ids=['COC.O'], opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 3)
        self.assertEqual(self._client.test.flask.find(
            {'_id': 'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND'}).count(), 0)

    def test_delete_flask_key(self):

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Check flask result before deletion
        self.assertEqual(self._client.test.flask.find(
            {'_id': 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'}).count(), 1)

        # Run colibri delete command
        result = delete(
            flasks=True, flask_ids=['UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'],
            opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 3)
        self.assertEqual(self._client.test.flask.find(
            {'_id': 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'}).count(), 0)

    def test_delete_noinput(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri delete command
        with self.assertRaisesRegexp(CommandError, 'Nothing to do'):
            delete(opts=opts)

    def test_delete_mol_flask_noop(self):

        # Insert molecule records
        self._insert_mol_data()

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri delete command
        result = delete(molecules=True, flasks=True, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 3)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

    def test_delete_mol_flask_key(self):

        # Insert molecule records
        self._insert_mol_data()

        # Insert flask records
        self._insert_flask_data()

        # Get configuration options
        opts = self._update_conf_opts()

        # Check molecule result before deletion
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'}).count(), 1)

        # Check flask result before deletion
        self.assertEqual(self._client.test.flask.find(
            {'_id': 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'}).count(), 1)

        # Run colibri delete command
        result = delete(
            molecules=True,
            molecule_ids=['XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'],
            flasks=True,
            flask_ids=['UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'], opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Check molecule results
        self.assertEqual(self._client.test.mol.find({}).count(), 2)
        self.assertEqual(self._client.test.mol.find(
            {'_id': 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'}).count(), 0)

        # Check flask results
        self.assertEqual(self._client.test.flask.find({}).count(), 3)
        self.assertEqual(self._client.test.flask.find(
            {'_id': 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS'}).count(), 0)

if __name__ == '__main__':
    unittest.main()
