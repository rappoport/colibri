"""Test classes for the rdkittask classes.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for rdkittask classes

Test Classes:
    TestRDKitBuildTask: Build task using RDKit
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/10/2015'

#
# Standard library
#


import unittest
import logging
import datetime


#
# colibri modules
#

from colibri.data import Molecule
from colibri.calc import Calculator, Context
from colibri.enums import Level
from colibri.utils import t2s, randomid
from colibri.exceptions import StructureError
import colibri.task

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_rdkittask')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'


#
# Public functions
#


def get_suite():
    """Construct test suite for rdkittask classes."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRDKitBuildTask)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()


#
# Test classes
#


class TestRDKitBuildTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_h2o(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 18, "f": {"i": None, "s": "O", "d": 18}, "l": 1000,
             "3": 0, "2": "", "5": "", "8": 0, "4": "",
             "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_so42(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='O=S([O-])([O-])=O').todict())

    def _insert_data_n2h62(self):

        # Insert new molecule record
        self._client.test.mol.insert(Molecule(smiles='[NH3+][NH3+]').todict())

    def _insert_data_cl(self):

        # Insert new molecule record
        self._client.test.mol.insert(Molecule(smiles='[Cl-]').todict())

    def _insert_data_o(self):

        # Insert new molecule record
        self._client.test.mol.insert(Molecule(smiles='[O-2]').todict())

    def _insert_data_na(self):

        # Insert new molecule record
        self._client.test.mol.insert(Molecule(smiles='[Na+]').todict())

    def _insert_data_br_cation(self):

        # Insert new molecule record
        self._client.test.mol.insert(Molecule(smiles='[Br+]').todict())

    def _insert_data_mg(self):

        # Insert new molecule record
        self._client.test.mol.insert(Molecule(smiles='[Mg+2]').todict())

    def _insert_data_nh2oh(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='NO', tags=['nitrogen']).todict())

    def _insert_data_structure_error(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 56, "f": {"i": None, "s": "[C-]12=C(O1)[O+]2",
             "d": 56}, "l": 1000, "3": 0, "2": "", "5": "", "8": 0, "4": "",
             "_id": "KOGZKTBHTGQYNX-UHFFFAOYSA-N-INQHAENHDC"})

    def _insert_data_error(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 78, "f": {"i": None, "s": "CB1B(C)B1C", "d": 78,
             "+": 0}, "+": 0, "l": 1000, "3": 0, "2": "",
             "4": datetime.datetime(2016, 7, 16, 15, 29, 29, 805718),
             "8": 0, "_id": "NUBAOKUNBMRSDW-UHFFFAOYSA-N-UGHVAKMJZV"})

    def _insert_data_h3o(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 19, "f": {"i": None, "s": "[OH3+]", "d": 19},
             "l": 1000, "3": 0, "2": "", "5": "", "4": "", "8": 0,
             "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"})

    def _insert_data_fragmentation(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='C[O+](C)C1C2=C1C2').todict())

    def _insert_data_rearrangement(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 50, "f": {"i": None, "s": "C1=C2C=C12", "d": 50},
             "l": 1000, "3": 0, "2": "", "5": "", "4": "", "8": 0,
             "_id": "QYGHRDRLUMAIGS-UHFFFAOYSA-N-NDUACUTQCK"})

    def _insert_data_badchem(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 50, "f": {"i": None, "s": "C12=C3C1C23", "d": 50},
             "l": 1000, "3": 0, "2": "", "5": "", "4": "", "8": 0,
             "_id": "NQFCANVBKCNLCO-UHFFFAOYSA-N-IFIDYPACWY"})

    def _run_rdkitbuildtask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'rdkit_build_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'RDKitBuildTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # RDKit options
        input_kv['configuration_validation'] = True
        input_kv['configuration_attempts'] = 5

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_rdkit(self):

        # Insert molecule record
        self._insert_data_h2o()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 0)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)

    def test_rdkit_dianion(self):

        # Insert molecule record
        self._insert_data_so42()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QAOWNCQODCNURD-UHFFFAOYSA-L-FKENSQEGUZ"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, -2)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'O=S(=O)([O-])[O-]')
        self.assertEqual(mol.priority, 96)

    def test_rdkit_dication(self):

        # Insert molecule record
        self._insert_data_n2h62()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "DIDREYHESYMPRP-UHFFFAOYSA-N-JRGVYPKFWF"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, +2)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[NH3+][NH3+]')
        self.assertEqual(mol.priority, 34)

    def test_rdkit_tags(self):

        # Insert molecule record
        self._insert_data_nh2oh()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(tags='nitrogen')

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AVXURJPOCDRRFD-UHFFFAOYSA-N-HDMTNUXKGO"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 0)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'NO')
        self.assertEqual(mol.priority, 33)
        self.assertListEqual(mol.tags, ['nitrogen'])

    def test_rdkit_atom_anion(self):

        # Insert molecule record
        self._insert_data_cl()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "VEXZGXHMUGYJMC-UHFFFAOYSA-M-YSAZDIBDUY"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, -1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[Cl-]')
        self.assertEqual(mol.priority, 35)

    def test_rdkit_atom_dianion(self):

        # Insert molecule record
        self._insert_data_o()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AHKZTVQIVOEVFO-UHFFFAOYSA-N-FIAVUHYKGN"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, -2)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[O-2]')
        self.assertEqual(mol.priority, 16)

    def test_rdkit_atom_cation(self):

        # Insert molecule record
        self._insert_data_na()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "FKNQFGJONOIPTF-UHFFFAOYSA-N-MIZVZQTOOH"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[Na+]')
        self.assertEqual(mol.priority, 23)

    def test_rdkit_atom_cation_sextet(self):

        # Insert molecule record
        self._insert_data_br_cation()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "SOUPRKFYMHZDHZ-UHFFFAOYSA-N-FZAOZBKPRQ"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 1)
        self.assertEqual(mol.configuration.mult, 3)
        self.assertEqual(mol.formula.smiles, '[Br+]')
        self.assertEqual(mol.priority, 80)

    def test_rdkit_atom_dication(self):

        # Insert molecule record
        self._insert_data_mg()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "JLVVSXFLKOJNIY-UHFFFAOYSA-N-QXICPCQITT"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 2)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[Mg+2]')
        self.assertEqual(mol.priority, 24)

    def test_rdkit_noinput(self):

        # Run RDKitBuildTask without input
        self._run_rdkitbuildtask()

        # No molecules were produced
        self.assertEqual(self._client.test.mol.find({}).count(), 0)

    def test_rdkit_structure_error(self):

        # Insert molecule record
        self._insert_data_structure_error()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result from database
        input_data = self._client.test.mol.find_one(
            {"_id": "KOGZKTBHTGQYNX-UHFFFAOYSA-N-INQHAENHDC"})

        # Check result from database
        self.assertEqual(input_data[t2s['level']], Level.FORMULA_ERROR)
        self.assertEqual(input_data[t2s['formula']][t2s['smiles']],
                         '[C-]12=C(O1)[O+]2')
        self.assertEqual(input_data[t2s['priority']], 56)
        self.assertNotIn(t2s['etag'], input_data)

        # Reading raises an exception
        with self.assertRaises(StructureError):
            Molecule.fromdict(input_data)

    def test_rdkit_execution_retry(self):

        # Insert molecule record
        self._insert_data_error()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(tasks_per_calc=3)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NUBAOKUNBMRSDW-UHFFFAOYSA-N-UGHVAKMJZV"}))

        # Check result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.attempts, 3)
        self.assertEqual(mol.formula.smiles, 'CB1B(C)B1C')
        self.assertEqual(mol.priority, 78)
        with self.assertRaises(AttributeError):
            mol.configuration

    def test_rdkit_execution_error(self):

        # Insert molecule record
        self._insert_data_error()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(tasks_per_calc=5)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NUBAOKUNBMRSDW-UHFFFAOYSA-N-UGHVAKMJZV"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_ERROR)
        self.assertEqual(mol.attempts, 5)

    def test_rdkit_nofragmentation(self):

        # Insert molecule record
        self._insert_data_h3o()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[OH3+]')
        self.assertEqual(mol.priority, 19)
        self.assertEqual(mol.attempts, 0)

    def test_rdkit_fragmentation_retry(self):

        # Insert molecule record
        self._insert_data_fragmentation()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(tasks_per_calc=3)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BGKBRNMZTHUGGD-UHFFFAOYSA-N-EAAHYIMYSU"}))

        # Check result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.attempts, 3)

    def test_rdkit_fragmentation_error(self):

        # Insert molecule record
        self._insert_data_fragmentation()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(tasks_per_calc=5)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BGKBRNMZTHUGGD-UHFFFAOYSA-N-EAAHYIMYSU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_INVALID)
        self.assertEqual(mol.attempts, 5)

    def test_rdkit_fragmentation_novalidation(self):

        # Insert molecule record
        self._insert_data_fragmentation()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(configuration_validation=False)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BGKBRNMZTHUGGD-UHFFFAOYSA-N-EAAHYIMYSU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'C[O+](C)C1C2=C1C2')
        self.assertEqual(mol.priority, 97)
        self.assertEqual(mol.attempts, 0)

    def test_rdkit_rearrangement_error(self):

        # Insert molecule record
        self._insert_data_rearrangement()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(tasks_per_calc=5)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QYGHRDRLUMAIGS-UHFFFAOYSA-N-NDUACUTQCK"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_INVALID)
        self.assertEqual(mol.configuration.charge, 0)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.attempts, 5)
        self.assertEqual(mol.formula.smiles, 'C1=C2C=C12')
        self.assertEqual(mol.priority, 50)

    def test_rdkit_badchem(self):

        # Insert molecule record
        self._insert_data_badchem()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask()

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NQFCANVBKCNLCO-UHFFFAOYSA-N-IFIDYPACWY"}))

        # Check result
        self.assertEqual(mol.level, Level.FORMULA_INVALID)
        self.assertEqual(mol.attempts, 1)
        self.assertEqual(mol.formula.smiles, "C12=C3C1C23")
        self.assertEqual(mol.priority, 50)
        with self.assertRaises(AttributeError):
            mol.configuration

    def test_rdkit_batchsize(self):

        # Insert molecule records
        self._insert_data_h2o()
        self._insert_data_nh2oh()

        # Run RDKitBuildTask
        self._run_rdkitbuildtask(batch_size=2)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AVXURJPOCDRRFD-UHFFFAOYSA-N-HDMTNUXKGO"}))

        # Check result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.formula.smiles, 'NO')
        self.assertEqual(mol.priority, 33)

    def test_rdkit_rollback(self):
        global _opts

        # Insert molecule record
        self._insert_data_nh2oh()

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'rdkit_build_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'RDKitBuildTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Batch size
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Locks
        input_kv['optimistic_lock'] = False
        input_kv['pessimistic_lock'] = True

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create Context object
        context = Context(opts)

        # Create new RDKitTask
        task = colibri.task.RDKitBuildTask(context, opts)

        # Setup
        task.setup()

        # Retrieve
        task.retrieve()

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AVXURJPOCDRRFD-UHFFFAOYSA-N-HDMTNUXKGO"}))

        # Check result
        self.assertEqual(mol1.level, Level.FORMULA_LOCKED)
        self.assertEqual(mol1.etag, calc_name)
        self.assertEqual(mol1.formula.smiles, 'NO')

        # Read formula output
        task._read_formula_output()

        # Rollback
        task.rollback()

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "AVXURJPOCDRRFD-UHFFFAOYSA-N-HDMTNUXKGO"}))

        # Check result
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.etag, '')
        self.assertEqual(mol2.formula.smiles, 'NO')


if __name__ == '__main__':
    unittest.main()
