"""Test classes for colib proton script.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib proton script

Test Classes:
    TestProtonScript: Test classes for colib proton script
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/08/2015'

#
# Standard library
#

import os
import sys
import unittest
import logging
import subprocess

#
# colibri submodules
#

from colibri.data import Molecule
from colibri.enums import Level
from colibri.utils import ensure_logger

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None


# Logging level
_logging_level = 'ERROR'

# Get logger
_logger = ensure_logger('protonscript', _logging_level)
_logger.addHandler(logging.NullHandler())

#
# Public functions
#


def get_suite():
    """Construct test suite for colib proton script."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestProtonScript)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestProtonScript(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _update_script_args(self, args=[]):
        global _tmpmongo, _logging_level

        # List of script arguments
        script_args = []

        # Add hostname and port options
        script_args.extend(['--db-hostname', 'localhost'])
        script_args.extend(['--db-port', str(_tmpmongo.port)])

        # Logging level
        script_args.extend(['--logging-level', _logging_level])

        # Ignore all configuration files
        script_args.extend(['--no-config'])

        # Additional script arguments
        script_args.extend(args)

        return script_args

    def _run_protonscript(self, script_args=[], cmd_args=[]):
        global _logging_level

        if _logging_level == 'DEBUG':
            stderr = sys.stderr
        else:
            stderr = open(os.devnull, 'w')

        return subprocess.call(
            ['colib'] + script_args + ['proton'] + cmd_args,
            stderr=stderr)

    def test_proton(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(script_args)

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)
        self.assertAlmostEqual(mol1.geometry.energy, -322.6792, places=4)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[OH3+]')
        self.assertEqual(mol2.priority, 19)
        self.assertAlmostEqual(mol2.geometry.energy, -327.16895, places=4)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, 1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[H+]')
        self.assertEqual(mol3.priority, 1)
        self.assertAlmostEqual(mol3.geometry.energy, -4.48975, places=4)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY)
        self.assertEqual(mol4.formula.charge, -1)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, '[OH-]')
        self.assertEqual(mol4.priority, 17)
        self.assertAlmostEqual(mol4.geometry.energy, -308.31139, places=4)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_proton_ph(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(script_args, cmd_args=[
            '--pH', '0'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)
        self.assertAlmostEqual(mol1.geometry.energy, -322.6792, places=4)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[OH3+]')
        self.assertEqual(mol2.priority, 19)
        self.assertAlmostEqual(mol2.geometry.energy, -327.16895, places=4)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, 1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[H+]')
        self.assertEqual(mol3.priority, 1)
        self.assertAlmostEqual(mol3.geometry.energy, -4.075637, places=4)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY)
        self.assertEqual(mol4.formula.charge, -1)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, '[OH-]')
        self.assertEqual(mol4.priority, 17)
        self.assertAlmostEqual(mol4.geometry.energy, -308.725503, places=4)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_proton_solvent_protonated_deprotonated(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(
            script_args, cmd_args=[
                '--solvent', 'N', '--protonated', '[NH4+]',
                '--deprotonated', '[NH2-]', '-@', 'ammonia'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'N')
        self.assertEqual(mol1.priority, 17)
        self.assertAlmostEqual(mol1.geometry.energy, -226.7529, places=4)
        self.assertListEqual(mol1.tags, ['ammonia'])

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-O-WKXUEGBFRF"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[NH4+]')
        self.assertEqual(mol2.priority, 18)
        self.assertAlmostEqual(mol2.geometry.energy, -233.18439, places=4)
        self.assertListEqual(mol2.tags, ['ammonia'])

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, 1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[H+]')
        self.assertEqual(mol3.priority, 1)
        self.assertAlmostEqual(mol3.geometry.energy, -6.43149, places=4)
        self.assertListEqual(mol3.tags, ['ammonia'])

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "HYGWNUKOUCZBND-UHFFFAOYSA-N-DERVRQTKYR"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY)
        self.assertEqual(mol4.formula.charge, -1)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, '[NH2-]')
        self.assertEqual(mol4.priority, 16)
        self.assertAlmostEqual(mol4.geometry.energy, -211.67141, places=4)
        self.assertListEqual(mol4.tags, ['ammonia'])

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_proton_nosolvent(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(
            script_args, cmd_args=['--solvent', '', '--protonated', '[NH4+]'])

        # Check script return value
        self.assertEqual(ret_value, 1)

    def test_proton_invalid(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(
            script_args, cmd_args=['--solvent', 'O', '--protonated', '[OH4+]'])

        # Check script return value
        self.assertEqual(ret_value, 1)

    def test_proton_incompatible(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(
            script_args, cmd_args=['--solvent', 'O', '--protonated', '[NH4+]'])

        # Check script return value
        self.assertEqual(ret_value, 1)

    def test_proton_present(self):

        # Insert water molecule
        self._client.test.mol.insert({
            "c": {
                "d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""}, "z": [], "d": 18,
            "g": {
                "e": -322.6792, "d": 18, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC",
                "7": True, "v": "2012", "x": """\
3

O    -0.000001     0.058320     0.000000
H    -0.759932    -0.519445     0.000000
H     0.760055    -0.519304     0.000000"""},
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0,
            "t": ["liquid", "hydrophilic"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(script_args)

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertEqual(mol1.priority, 18)
        self.assertAlmostEqual(mol1.geometry.energy, -322.6792, places=4)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, '[OH3+]')
        self.assertEqual(mol2.priority, 19)
        self.assertAlmostEqual(mol2.geometry.energy, -327.16895, places=4)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, 1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[H+]')
        self.assertEqual(mol3.priority, 1)
        self.assertAlmostEqual(mol3.geometry.energy, -4.48975, places=4)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY)
        self.assertEqual(mol4.formula.charge, -1)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, '[OH-]')
        self.assertEqual(mol4.priority, 17)
        self.assertAlmostEqual(mol4.geometry.energy, -308.31139, places=4)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 4)

    def test_proton_novalidation(self):

        # Get script arguments
        script_args = self._update_script_args()

        # Run colib proton script
        ret_value = self._run_protonscript(
            script_args, cmd_args=[
                '--solvent', 'CO', '--protonated', 'C[OH2+]',
                '--deprotonated', 'C[O-]',
                '--no-configuration-validation', '--no-geometry-validation'])

        # Check script return value
        self.assertEqual(ret_value, 0)

        # Get result from database
        mol1 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB"}))

        # Check result
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.charge, 0)
        self.assertEqual(mol1.formula.mult, 1)
        self.assertEqual(mol1.formula.smiles, 'CO')
        self.assertEqual(mol1.priority, 32)
        self.assertAlmostEqual(mol1.geometry.energy, -472.05421, places=4)

        # Get result from database
        mol2 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "OKKJLVBELUTLKV-UHFFFAOYSA-O-GNQMJBYIZY"}))

        # Check result
        self.assertEqual(mol2.level, Level.GEOMETRY)
        self.assertEqual(mol2.formula.charge, 1)
        self.assertEqual(mol2.formula.mult, 1)
        self.assertEqual(mol2.formula.smiles, 'C[OH2+]')
        self.assertEqual(mol2.priority, 33)
        self.assertAlmostEqual(mol2.geometry.energy, -477.00646, places=4)

        # Get result from database
        mol3 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"}))

        # Check result
        self.assertEqual(mol3.level, Level.GEOMETRY)
        self.assertEqual(mol3.formula.charge, 1)
        self.assertEqual(mol3.formula.mult, 1)
        self.assertEqual(mol3.formula.smiles, '[H+]')
        self.assertEqual(mol3.priority, 1)
        self.assertAlmostEqual(mol3.geometry.energy, -4.95225, places=4)

        # Get result from database
        mol4 = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NBTOZLQBSIZIKS-UHFFFAOYSA-N-MYYQSDNUUW"}))

        # Check result
        self.assertEqual(mol4.level, Level.GEOMETRY)
        self.assertEqual(mol4.formula.charge, -1)
        self.assertEqual(mol4.formula.mult, 1)
        self.assertEqual(mol4.formula.smiles, 'C[O-]')
        self.assertEqual(mol4.priority, 31)
        self.assertAlmostEqual(mol4.geometry.energy, -457.98934, places=4)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.mol.find({}).count(), 4)


if __name__ == '__main__':
    unittest.main()
