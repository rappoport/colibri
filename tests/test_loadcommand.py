"""Test classes for colibri load command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri load command

Test Classes:
    TestLoadCommand: Test classes for LoadCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/07/2015'


#
# Standard library
#

import os
import os.path
import unittest
import logging


#
# Third-party modules
#

try:
    import igraph
except ImportError:
    igraph = None

#
# colibri modules
#

from colibri.api import load
from colibri.data import Flask
from colibri.enums import Status, Result

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_loadcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'


#
# Public functions
#


def get_suite():
    """Construct test suite for colibri load command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLoadCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestLoadCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger

        # Need igraph
        if igraph is None:
            self.skipTest('igraph is not available')

        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def test_load(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Input file
        input_file = os.path.join(
            os.path.dirname(__file__), 'test_loadcommand.graphml')

        # Run colibri load command
        result = load(input_file=input_file, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Get result from database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check result
        self.assertEqual(flask1.smiles, 'CO.CO')
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.generation, 0)
        self.assertAlmostEqual(flask1.energy, -944.108, places=4)
        self.assertAlmostEqual(flask1.precursor, -932.513, places=4)
        self.assertAlmostEqual(flask1.energy_change, -11.5953, places=4)
        self.assertEqual(flask1.outgoing[0].reactant,
                         'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF')
        self.assertEqual(flask1.outgoing[0].product,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask1.outgoing[0].rule,
                         '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]')
        self.assertEqual(len(flask1.outgoing), 1)
        self.assertEqual(flask1.incoming[0].reactant,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask1.incoming[0].product,
                         'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF')
        self.assertEqual(flask1.incoming[0].rule,
                         '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]')
        self.assertEqual(len(flask1.incoming), 1)

        # Get result from database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX"}))

        # Check result
        self.assertEqual(flask2.smiles, 'C[OH+]C.[OH-]')
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.generation, 2)
        self.assertAlmostEqual(flask2.energy, -935.119, places=4)
        self.assertAlmostEqual(flask2.precursor, -932.513, places=4)
        self.assertAlmostEqual(flask2.energy_change, -2.60543, places=4)
        self.assertEqual(flask2.outgoing[0].reactant,
                         'DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX')
        self.assertEqual(flask2.outgoing[0].product,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask2.outgoing[0].rule,
                         '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]')
        self.assertEqual(len(flask2.outgoing), 1)
        self.assertEqual(flask2.incoming[0].reactant,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask2.incoming[0].product,
                         'DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX')
        self.assertEqual(flask2.incoming[0].rule,
                         '[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]')
        self.assertEqual(len(flask2.incoming), 1)

        # Get result from database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF"}))

        # Check result
        self.assertEqual(flask3.smiles, '[CH3+].CO.[OH-]')
        self.assertEqual(flask3.status, Status.UNREACTIVE)
        self.assertEqual(flask3.generation, 1)
        self.assertAlmostEqual(flask3.energy, -932.513, places=4)
        self.assertAlmostEqual(flask3.precursor, -935.119, places=4)
        self.assertAlmostEqual(flask3.energy_change, 2.60543, places=4)
        self.assertEqual(flask3.outgoing[0].reactant,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask3.outgoing[0].product,
                         'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF')
        self.assertEqual(flask2.outgoing[0].rule,
                         '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]')
        self.assertEqual(flask3.outgoing[1].reactant,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask3.outgoing[1].product,
                         'DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX')
        self.assertEqual(flask3.outgoing[1].rule,
                         '[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]')
        self.assertEqual(len(flask3.outgoing), 2)
        self.assertEqual(flask3.incoming[0].reactant,
                         'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF')
        self.assertEqual(flask3.incoming[0].product,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask2.incoming[0].rule,
                         '[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]')
        self.assertEqual(flask3.incoming[1].reactant,
                         'DRECBBFIAREDAS-UHFFFAOYSA-N-MVWRPQXOVX')
        self.assertEqual(flask3.incoming[1].product,
                         'JRUGLXICFJADHR-UHFFFAOYSA-M-SPTVTNEMSF')
        self.assertEqual(flask3.incoming[1].rule,
                         '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]')
        self.assertEqual(len(flask3.incoming), 2)

        # Total number of molecules in collection
        self.assertEqual(self._client.test.flask.find({}).count(), 3)


if __name__ == '__main__':
    unittest.main()
