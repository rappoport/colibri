"""Test classes for colib config console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib config console subcommand

Test Classes:
    TestConfigConsoleCommand: Test classes for ConfigConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, ConfigConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_configconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib config console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestConfigConsoleCommand)
    return suite

#
# Test classes
#


class TestConfigConsoleCommand(unittest.TestCase):

    def _init_configconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        ConfigConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Logging level option
        opts = Options(no_config=True, logging_level=_logging_level)

        return ConfigConsoleCommand(args=args, opts=opts)

    def test_configconsole(self):

        # Initialize ConfigConsoleCommand
        console = self._init_configconsole(
            ['colib', 'config', '--key', 'db_hostname', '--value',
             'localhost'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ConfigCommand\(config_file='/.*/colibri\.json', " +
            r"opt=OptionItem\(group='db_hostname', key='db_hostname', " +
            r"value='localhost'\), append=False, opts=Options\(.*\)\)")

    def test_configconsole_group(self):

        # Initialize ConfigConsoleCommand
        console = self._init_configconsole(
            ['colib', 'config', '--group', 'db_hostname', '--value',
             'localhost'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ConfigCommand\(config_file='/.*/colibri\.json', " +
            r"opt=OptionItem\(group='db_hostname', value='localhost'\), " +
            r"append=False, opts=Options\(.*\)\)")

    def test_configconsole_group_key(self):

        # Initialize ConfigConsoleCommand
        console = self._init_configconsole(
            ['colib', 'config', '--key', 'rdkit_react', '--value',
             'RDKitReactionTask', '--group', 'task'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ConfigCommand\(config_file='/.*/colibri\.json', " +
            r"opt=OptionItem\(group='task', key='rdkit_react', " +
            r"value='RDKitReactionTask'\), append=False, opts=Options\(.*\)\)")

    def test_configconsole_type(self):

        # Initialize ConfigConsoleCommand
        console = self._init_configconsole(
            ['colib', 'config', '--key', 'db_port', '--value',
             '27017', '--type', 'int'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ConfigCommand\(config_file='/.*/colibri\.json', " +
            r"opt=OptionItem\(group='db_port', key='db_port', " +
            r"value=27017\), append=False, opts=Options\(.*\)\)")

    def test_configconsole_badtype(self):

        # Initialize ConfigConsoleCommand
        with self.assertRaisesRegexp(
                CommandError, 'Option type double invalid'):
            self._init_configconsole(
                ['colib', 'config', '--key', 'db_port', '--value',
                 '27017', '--type', 'double'])

    def test_configconsole_config_file(self):

        # Initialize ConfigConsoleCommand
        console = self._init_configconsole(
            ['colib', 'config', '--key', 'db_hostname', '--value',
             'localhost', '--config-file', 'test.json'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ConfigCommand\(config_file='test\.json', " +
            r"opt=OptionItem\(group='db_hostname', key='db_hostname', " +
            r"value='localhost'\), append=False, opts=Options\(.*\)\)")

    def test_configconsole_append(self):

        # Initialize ConfigConsoleCommand
        console = self._init_configconsole(
            ['colib', 'config', '--key', 'db_hostname', '--value',
             'localhost', '--append'])

        # Check generated RunCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^ConfigCommand\(config_file='/.*/colibri\.json', " +
            r"opt=OptionItem\(group='db_hostname', key='db_hostname', " +
            r"value='localhost'\), append=True, opts=Options\(.*\)\)")

    def test_configconsole_incomplete(self):

        # Initialize ConfigConsoleCommand
        with self.assertRaisesRegexp(
                CommandError, 'OptionItem needs group or key specification'):
            self._init_configconsole(['colib', 'config'])


if __name__ == '__main__':
    unittest.main()
