"""Test classes for colib init console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib init console subcommand

Test Classes:
    TestInitConsoleCommand: Test classes for InitConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, InitConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_initconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib init console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestInitConsoleCommand)
    return suite

#
# Test classes
#


class TestInitConsoleCommand(unittest.TestCase):

    def _init_initconsole(self, argv):

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        InitConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files
        opts = Options(no_config=True)

        return InitConsoleCommand(args=args, opts=opts)

    def test_initconsole(self):

        # Initialize InitConsoleCommand
        console = self._init_initconsole(['colib', 'init'])

        # Check generated AddCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^InitCommand\(tags=False, opts=Options\(.*\)\)")

    def test_initconsole_tags(self):

        # Initialize InitConsoleCommand
        console = self._init_initconsole(['colib', 'init', '-@'])

        # Check generated AddCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^InitCommand\(tags=True, opts=Options\(.*\)\)")

if __name__ == '__main__':
    unittest.main()
