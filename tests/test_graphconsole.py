"""Test classes for colib graph console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib graph console subcommand

Test Classes:
    TestGraphConsoleCommand: Test classes for GraphConsoleCommand
"""

#
# Standard library
#

import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, GraphConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_graphconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib graph console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestGraphConsoleCommand)
    return suite

#
# Test classes
#


class TestGraphConsoleCommand(unittest.TestCase):

    def _init_graphconsole(self, argv):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        GraphConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Ignore configuration files, set logging level
        opts = Options(no_config=True, logging_level=_logging_level)

        return GraphConsoleCommand(args=args, opts=opts)

    def test_graph(self):

        # Initialize GraphConsoleCommand
        console = self._init_graphconsole(
            ['colib', 'graph'])

        # Check generated GraphCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^GraphCommand\(labels=None, keys=None, opts=Options\(.*\)\)")

    def test_graph_labels(self):

        # Initialize GraphConsoleCommand
        console = self._init_graphconsole(
            ['colib', 'graph', '-B', 'LL', 'LN'])

        # Check generated GraphCommand
        self.assertRegexpMatches(
            repr(console.cmd), r"^GraphCommand\(labels=\['LL', 'LN'\]")

    def test_graph_keys(self):

        # Initialize GraphConsoleCommand
        console = self._init_graphconsole(
            ['colib', 'graph', '-Z', '[A-N]'])

        # Check generated GraphCommand
        self.assertRegexpMatches(
            repr(console.cmd), r"^GraphCommand\(labels=None, keys='\[A-N\]'")


if __name__ == '__main__':
    unittest.main()
