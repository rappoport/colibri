"""Test classes for the openbabeltask classes.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for openbabeltask classes

Test Classes:
    TestOpenBabelBuildTask: Build task using OpenBabel
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/10/2015'

#
# Standard library
#


import unittest
import logging

#
# colibri modules
#

from colibri.data import Molecule
from colibri.calc import Calculator
from colibri.enums import Level
from colibri.utils import t2s, randomid

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_openbabeltask')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'


#
# Public functions
#


def get_suite():
    """Construct test suite for openbabeltask classes."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestOpenBabelBuildTask)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()


#
# Test classes
#


class TestOpenBabelBuildTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})

    def _insert_data_h2o(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 18, "f": {"i": None, "s": "O", "d": 18}, "l": 1000,
             "3": 0, "2": "", "5": "", "8": 0, "4": "",
             "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_nh2oh(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='NO', tags=['nitrogen']).todict())

    def _insert_data_structure_error(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 56, "f": {"i": None, "s": "[C-]12=C(O1)[O+]2",
             "d": 56}, "l": 1000, "3": 0, "2": "", "5": "", "8": 0, "4": "",
             "_id": "KOGZKTBHTGQYNX-UHFFFAOYSA-N-INQHAENHDC"})

    def _insert_data_error(self):

        # Insert new molecule record
        self._client.test.mol.insert(
           {"z": [], "d": 356, "f": {
            "i": None, "d": 356,
            "s": "CC1=CC=CC(N=O)=C2C=CC=CC(=C(NO)C=CC=C(C)C#CC#C1)C2"},
            "l": 1000, "3": 0, "2": "", "5": "", "4": "",
            "8": 0, "_id": "FYDXAOQHBJUFLK-UHFFFAOYSA-N-XIAYNVFPJQ"})

    def _insert_data_h3o(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 19, "f": {"i": None, "s": "[OH3+]", "d": 19},
             "l": 1000, "3": 0, "2": "", "5": "", "4": "", "8": 0,
             "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"})

    def _insert_data_fragmentation(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='C[O+](C)C1C2=C1C2').todict())

    def _insert_data_rearrangement(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 50, "f": {"i": None, "s": "C1=C2C=C12", "d": 50},
             "l": 1000, "3": 0, "2": "", "5": "", "4": "", "8": 0,
             "_id": "QYGHRDRLUMAIGS-UHFFFAOYSA-N-NDUACUTQCK"})

    def _insert_data_badchem(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 50, "f": {"i": None, "s": "C12=C3C1C23", "d": 50},
             "l": 1000, "3": 0, "2": "", "5": "", "4": "", "8": 0,
             "_id": "NQFCANVBKCNLCO-UHFFFAOYSA-N-IFIDYPACWY"})

    def _run_openbabelbuildtask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task option
        calc_name = 'openbabel_build_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task', 'value': 'OpenBabelBuildTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # OpenBabel options
        input_kv['configuration_validation'] = True
        input_kv['configuration_attempts'] = 5

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def test_openbabel(self):

        # Insert molecule record
        self._insert_data_h2o()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask()

        # Get result from database
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 0)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'O')
        self.assertEqual(mol.priority, 18)

    def test_openbabel_structure_error(self):

        # Insert new molecule record
        self._insert_data_structure_error()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask()

        # Get result from database
        input_data = self._client.test.mol.find_one(
            {"_id": "KOGZKTBHTGQYNX-UHFFFAOYSA-N-INQHAENHDC"})

        # Check result from database
        self.assertEqual(input_data[t2s['level']], Level.FORMULA_ERROR)
        self.assertEqual(input_data[t2s['formula']][t2s['smiles']],
                         '[C-]12=C(O1)[O+]2')
        self.assertEqual(input_data[t2s['priority']], 56)
        self.assertNotIn(t2s['etag'], input_data)

    def test_openbabel_nofragmentation(self):

        # Insert molecule record
        self._insert_data_h3o()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask()

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-O-ESETVCSGLL"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, '[OH3+]')
        self.assertEqual(mol.priority, 19)
        self.assertEqual(mol.attempts, 0)

    def test_openbabel_fragmentation_retry(self):

        # Insert new molecule record
        self._insert_data_fragmentation()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask(tasks_per_calc=3)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BGKBRNMZTHUGGD-UHFFFAOYSA-N-EAAHYIMYSU"}))

        # Check result
        self.assertEqual(mol.level, Level.FORMULA)
        self.assertEqual(mol.attempts, 3)

    def test_openbabel_fragmentation_error(self):

        # Insert new molecule record
        self._insert_data_fragmentation()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask(tasks_per_calc=5)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BGKBRNMZTHUGGD-UHFFFAOYSA-N-EAAHYIMYSU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_INVALID)
        self.assertEqual(mol.attempts, 5)

    def test_openbabel_fragmentation_novalidation(self):

        # Insert molecule record
        self._insert_data_fragmentation()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask(configuration_validation=False)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "BGKBRNMZTHUGGD-UHFFFAOYSA-N-EAAHYIMYSU"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION)
        self.assertEqual(mol.configuration.charge, 1)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.formula.smiles, 'C[O+](C)C1C2=C1C2')
        self.assertEqual(mol.priority, 97)
        self.assertEqual(mol.attempts, 0)

    def test_openbabel_rearrangement_error(self):

        # Insert new molecule record
        self._insert_data_rearrangement()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask(tasks_per_calc=5)

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "QYGHRDRLUMAIGS-UHFFFAOYSA-N-NDUACUTQCK"}))

        # Check result
        self.assertEqual(mol.level, Level.CONFIGURATION_INVALID)
        self.assertEqual(mol.configuration.charge, 0)
        self.assertEqual(mol.configuration.mult, 1)
        self.assertEqual(mol.attempts, 5)
        self.assertEqual(mol.formula.smiles, 'C1=C2C=C12')
        self.assertEqual(mol.priority, 50)

    @unittest.expectedFailure
    def test_openbabel_badchem(self):

        # Insert new molecule record
        self._insert_data_badchem()

        # Run OpenBabelBuildTask
        self._run_openbabelbuildtask()

        # Get result
        mol = Molecule.fromdict(self._client.test.mol.find_one(
            {"_id": "NQFCANVBKCNLCO-UHFFFAOYSA-N-IFIDYPACWY"}))

        # Check result
        self.assertEqual(mol.level, Level.FORMULA_INVALID)
        self.assertEqual(mol.attempts, 1)
        self.assertEqual(mol.formula.smiles, "C12=C3C1C23")
        self.assertEqual(mol.priority, 50)
        with self.assertRaises(AttributeError):
            mol.configuration


if __name__ == '__main__':
    unittest.main()
