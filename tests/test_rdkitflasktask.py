"""Test classes for RDKitFlaskReactionTask

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for rdkitflasktask classes

Test Classes:
    TestRDKitFlaskReactionTask: Flask generation task using RDKit.
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/10/2015'

#
# Standard library
#


import unittest
import logging


#
# colibri modules
#

from colibri.data import Flask
from colibri.calc import Calculator, Context
from colibri.enums import Status, Reactivity
from colibri.utils import randomid
import colibri.task

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_rdkitflasktask')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for RDKitFlaskReactionTask."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestRDKitFlaskReactionTask)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()


#
# Test classes
#

class TestRDKitFlaskReactionTask(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.flask.remove({})

    def _run_rdkitflaskreacttask(self, **kwargs):
        global _opts

        # Dictionary of input options
        input_kv = {}

        # Add task options
        calc_name = 'rdkit_flask_react_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task',
            'value': 'RDKitFlaskReactionTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Calculator and batch options
        input_kv['tasks_per_calc'] = 1
        input_kv['tasks_per_cycle'] = 1
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create and start Calculator instance
        calc = Calculator(opts)
        calc.run()

    def _insert_data_flask_NN0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O', generation=0, generation_journal=[0],
                  status=2200, tags=['NN']).todict())

    def _insert_data_flask_NN0_unreactive(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='C=O.C=O', generation=0, generation_journal=[0, 2],
                  outgoing=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'product': 'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA',
                      'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'},
                      {'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                       'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                       'rule': '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'}],
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB',
                      'product': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'rule': '[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]'}],
                  rules_applied=['[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
                                 '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'],
                  status=2300, tags=['NN']).todict())

    def _insert_data_flask_NN1(self):

        # Insert new flask record (generation 1)
        self._client.test.flask.insert(
            Flask(smiles='[CH-]=O.[H+].C=O', generation=1,
                  generation_journal=[1], status=2200,
                  incoming=[{
                      'reactant': 'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ',
                      'product': 'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA',
                      'rule': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'}],
                  tags=['test']).todict())

    def _insert_data_flask_NN3(self):

        # Insert new flask record (generation 3)
        self._client.test.flask.insert(
            Flask(smiles='[CH2+]OC[O-]', generation=3, generation_journal=[3],
                  status=2200, tags=['NN']).todict())

    def _insert_data_flask_NN4(self):

        # Insert new flask record (generation 4)
        self._client.test.flask.insert(
            Flask(smiles='C1OCO1', generations=4, generation_journal=[4],
                  status=2200, tags=['NN']).todict())

    def _insert_data_flask_halogens(self):

        # Insert new flask records
        self._client.test.flask.insert(
            Flask(smiles='F', status=2200).todict())
        self._client.test.flask.insert(
            Flask(smiles='Cl', status=2200).todict())

    def _insert_data_flask_aromatic(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='c1ccccc1', status=2200).todict())

    def _insert_data_flask_oxirane(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='C1OC1', status=2200).todict())

    def _insert_data_flask_duplicate(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='[CH2+]C[O-].[CH2+]C[O-]',
                  status=2200).todict())

    def _insert_data_flask_degenerate(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='[CH2+]C(C[O-])C[O-].[H+].[H+].[OH-]',
                  status=2200).todict())

    def _insert_data_flask_LL0(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='CO.CO', status=2200, tags=['LL']).todict())

    def _insert_data_flask_ES2(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='[CH2-]C(=O)[OH2+].CO', generation=2,
                  generation_journal=[2], energy=-1361.2147,
                  precursor=-1361.72669, precursor_journal=[-1361.72669],
                  energy_change=0.51199,
                  incoming=[{
                      'reactant': 'RLARTBJTCPYRSV-UHFFFAOYSA-O-WUGNMLDCBK',
                      'product': 'RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU',
                      'rule': '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'}],
                  status=2200, tags=['ES']).todict())

    def _insert_data_flask_ES3_maxgen(self):

        # Insert new flask record (REACTIVE_MAXGEN)
        self._client.test.flask.insert(
            Flask(smiles='[CH2-][C+]=O.CO.O', generation=3,
                  generation_journal=[3], energy=-1362.58152,
                  precursor=-1361.2147, precursor_journal=[-1361.2147],
                  energy_change=-2.11286,
                  incoming=[{
                      'reactant': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-MUTKTNEQJD',
                      'product': 'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL',
                      'rule': '[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]'}],
                  status=2212, tags=['ES']).todict())

    def _insert_data_flask_cycl_product_error(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='[CH2+][CH2-]', status=2200).todict())

    def _insert_data_flask_bi_product_error(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='CC(C)(C)C.C(C)(C)C', status=2200).todict())

    def test_rdkitflask_uni(self):

        # Insert flask record
        self._insert_data_flask_NN0()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'])
        self.assertListEqual(flask1.tags, ['NN'])
        self.assertEqual(flask1.priority, 1800)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[CH-]=O.[H+].C=O')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertListEqual(flask2.tags, ['NN'])
        self.assertEqual(flask2.priority, 1842)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_rdkitflask_uni_tags(self):

        # Insert flask records
        self._insert_data_flask_LL0()
        self._insert_data_flask_NN0()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Tags
        input_kv['tags'] = ['LL']

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.REACTIVE)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertListEqual(flask1.tags, ['NN'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, 'CO.CO')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertListEqual(flask2.rules_applied, [
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'])
        self.assertListEqual(flask2.tags, ['LL'])
        self.assertEqual(flask2.priority, 2048)

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DBCQSZHWQGOSTM-UHFFFAOYSA-O-FXYSLWNJLA"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[H+].CO.[CH2-]O')
        self.assertEqual(flask3.generation, 1)
        self.assertListEqual(flask3.generation_journal, [1])
        self.assertListEqual(flask3.rules_applied, [])
        self.assertListEqual(flask3.tags, ['LL'])
        self.assertEqual(flask3.priority, 2086)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 3)

    def test_rdkitflask_uni_multiple_rules(self):

        # Insert flask record
        self._insert_data_flask_LL0()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Tags
        input_kv['tags'] = 'LL'

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'CO.CO')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'])
        self.assertListEqual(flask1.tags, ['LL'])
        self.assertEqual(flask1.priority, 2048)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "NPRPHDSQZBFINE-UHFFFAOYSA-O-WCSFEUPZHB"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[H+].C[O-].CO')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertListEqual(flask2.tags, ['LL'])
        self.assertEqual(flask2.priority, 2086)

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "DBCQSZHWQGOSTM-UHFFFAOYSA-O-FXYSLWNJLA"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[H+].CO.[CH2-]O')
        self.assertEqual(flask3.generation, 1)
        self.assertListEqual(flask3.generation_journal, [1])
        self.assertListEqual(flask3.rules_applied, [])
        self.assertListEqual(flask3.tags, ['LL'])
        self.assertEqual(flask3.priority, 2086)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 3)

    def test_rdkitflask_uni_compound_rule(self):

        # Insert flask record
        self._insert_data_flask_halogens()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[F+0,Cl+0,Br+0,I+0:1]-[#1+0:2]>>[*-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Calculator and batch options
        input_kv['tasks_per_cycle'] = 2
        input_kv['batch_size'] = 2

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "KRHYYFGTRYWZRS-UHFFFAOYSA-N-MMIJCGAIQJ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'F')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '[F+0,Cl+0,Br+0,I+0:1]-[#1+0:2]>>[*-1:1].[#1+1:2]'])
        self.assertEqual(flask1.priority, 400)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "VEXZGXHMUGYJMC-UHFFFAOYSA-N-RJMUNSFIBS"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, 'Cl')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0])
        self.assertListEqual(flask2.rules_applied, [
            '[F+0,Cl+0,Br+0,I+0:1]-[#1+0:2]>>[*-1:1].[#1+1:2]'])
        self.assertEqual(flask2.priority, 1296)

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "KRHYYFGTRYWZRS-UHFFFAOYSA-N-NSRCSJSXUY"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[H+].[F-]')
        self.assertEqual(flask3.generation, 1)
        self.assertListEqual(flask3.generation_journal, [1])
        self.assertListEqual(flask3.rules_applied, [])
        self.assertEqual(flask3.priority, 462)

        # Get flask result from the database
        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "VEXZGXHMUGYJMC-UHFFFAOYSA-N-XLFVIECCAM"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.INITIAL)
        self.assertEqual(flask4.smiles, '[H+].[Cl-]')
        self.assertEqual(flask4.generation, 1)
        self.assertListEqual(flask4.generation_journal, [1])
        self.assertListEqual(flask4.rules_applied, [])
        self.assertEqual(flask4.priority, 1326)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

    def test_rdkitflask_uni_aromatic(self):

        # Insert flask record
        self._insert_data_flask_aromatic()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]=[C+0:2]>>[C-1:1][C+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "UHOVQNZJYSORNB-UHFFFAOYSA-N-NZHCCSVAQJ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'c1ccccc1')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[C+0:1]=[C+0:2]>>[C-1:1][C+1:2]'])
        self.assertEqual(flask1.priority, 6084)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "BJISZDCMBMQNIY-UHFFFAOYSA-O-PGPNNMDALT"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[c-]1ccccc1.[H+]')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertEqual(flask2.priority, 6030)

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "UHOVQNZJYSORNB-UHFFFAOYSA-N-OZLUSOZLRL"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, 'c1cc[cH-][cH+]c1')
        self.assertEqual(flask3.generation, 1)
        self.assertListEqual(flask3.generation_journal, [1])
        self.assertListEqual(flask3.rules_applied, [])
        self.assertEqual(flask3.priority, 6184)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 3)

    def test_rdkitflask_uni_batchsize(self):

        # Insert flask records
        self._insert_data_flask_NN0()
        self._insert_data_flask_LL0()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Batch size
        input_kv['batch_size'] = 2

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertListEqual(flask1.rules_applied, [
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]'])

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.REACTIVE)
        self.assertEqual(flask2.smiles, 'CO.CO')

        # Get flask result from the database
        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[CH-]=O.[H+].C=O')

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 3)

    def test_rdkitflask_uni_maxgen(self):

        # Insert flask records
        self._insert_data_flask_ES2()
        self._insert_data_flask_ES3_maxgen()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, '[CH2-]C(=O)[OH2+].CO')
        self.assertEqual(flask1.generation, 2)
        self.assertListEqual(flask1.generation_journal, [2])
        self.assertEqual(len(flask1.outgoing), 1)
        self.assertEqual(flask1.outgoing[0].reactant,
                         'RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU')
        self.assertEqual(flask1.outgoing[0].product,
                         'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL')
        self.assertEqual(flask1.outgoing[0].rule,
                         '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]')
        self.assertEqual(len(flask1.incoming), 1)
        self.assertEqual(flask1.incoming[0].reactant,
                         'RLARTBJTCPYRSV-UHFFFAOYSA-O-WUGNMLDCBK')
        self.assertEqual(flask1.incoming[0].product,
                         'RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU')
        self.assertEqual(flask1.incoming[0].rule,
                         '[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]')
        self.assertListEqual(flask1.rules_applied, [
            '[C+0:1]-[O+1:2]>>([C+1:1].[O+0:2])',
            '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]'])
        self.assertListEqual(flask1.tags, ['ES'])
        self.assertEqual(flask1.priority, 5024)

        # # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[CH2-][C+]=O.CO.O')
        self.assertEqual(flask2.generation, 3)
        self.assertListEqual(flask2.generation_journal, [3])
        self.assertEqual(len(flask2.outgoing), 0)
        self.assertEqual(len(flask2.incoming), 2)
        self.assertEqual(flask2.incoming[0].reactant,
                         'LZTNNLWJVFNVGT-UHFFFAOYSA-N-MUTKTNEQJD')
        self.assertEqual(flask2.incoming[0].product,
                         'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL')
        self.assertEqual(flask2.incoming[0].rule,
                         '[O-1:1].[#1+1:2]>>[O+0:1]-[#1+0:2]')
        self.assertEqual(flask2.incoming[1].reactant,
                         'RLARTBJTCPYRSV-UHFFFAOYSA-O-BXNTYWCSDU')
        self.assertEqual(flask2.incoming[1].product,
                         'LZTNNLWJVFNVGT-UHFFFAOYSA-N-OHRAUUOYDL')
        self.assertEqual(flask2.incoming[1].rule,
                         '[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]')
        self.assertEqual(len(flask2.rules_applied), 0)
        self.assertListEqual(flask2.tags, ['ES'])
        self.assertEqual(flask2.priority, 4012)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_rdkitflask_cycl_closing(self):

        # Insert flask record
        self._insert_data_flask_NN3()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, '[CH2+]OC[O-]')
        self.assertEqual(flask1.generation, 3)
        self.assertListEqual(flask1.generation_journal, [3])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])
        self.assertListEqual(flask1.tags, ['NN'])
        self.assertEqual(flask1.priority, 4500)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'C1OCO1')
        self.assertEqual(flask2.generation, 4)
        self.assertListEqual(flask2.generation_journal, [4])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertListEqual(flask2.tags, ['NN'])
        self.assertEqual(flask2.priority, 5200)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_rdkitflask_cycl_opening(self):

        # Insert molecule record
        self._insert_data_flask_oxirane()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "IAYPIBMASNFSPL-UHFFFAOYSA-N-IGFFCQTDYI"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'C1CO1')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '[C+0:1]-[O+0:2]>>([C+1:1].[O-1:2])',
            '[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]'])
        self.assertEqual(flask1.priority, 1936)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "ODVIVHCWPDKHAQ-UHFFFAOYSA-N-VZAGDMEEXE"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[CH2+]C[O-]')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertEqual(flask2.priority, 2036)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_rdkitflask_bi(self):

        # Insert flask records
        self._insert_data_flask_NN1()

        # Task options dict
        input_kv = {}

        # Bimolecular reaction
        input_kv['reactivity'] = Reactivity.BI

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, '[CH-]=O.[H+].C=O')
        self.assertEqual(flask1.generation, 1)
        self.assertListEqual(flask1.generation_journal, [1])
        self.assertListEqual(flask1.rules_applied, [
            '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'])
        self.assertListEqual(flask1.tags, ['test'])
        self.assertEqual(flask1.priority, 1842)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'C=O.C=O')
        self.assertEqual(flask2.generation, 2)
        self.assertListEqual(flask2.generation_journal, [2])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertListEqual(flask2.tags, ['test'])
        self.assertEqual(flask2.priority, 2200)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_rdkitflask_bi_existing(self):

        # Insert flask records
        self._insert_data_flask_NN0_unreactive()
        self._insert_data_flask_NN1()

        # Task options dict
        input_kv = {}

        # Bimolecular reactions
        input_kv['reactivity'] = Reactivity.BI

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, '[CH-]=O.[H+].C=O')
        self.assertEqual(flask1.generation, 1)
        self.assertListEqual(flask1.generation_journal, [1])
        self.assertEqual(len(flask1.outgoing), 1)
        self.assertEqual(flask1.outgoing[0].reactant,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask1.outgoing[0].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask1.outgoing[0].rule,
                         '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]')
        self.assertEqual(len(flask1.incoming), 1)
        self.assertEqual(flask1.incoming[0].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask1.incoming[0].product,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask1.incoming[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertListEqual(flask1.rules_applied, [
            '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'])
        self.assertListEqual(flask1.tags, ['test'])
        self.assertEqual(flask1.priority, 1842)

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, 'C=O.C=O')
        self.assertEqual(flask2.generation, 0)
        self.assertListEqual(flask2.generation_journal, [0, 2])
        self.assertEqual(len(flask2.outgoing), 2)
        self.assertEqual(flask2.outgoing[0].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask2.outgoing[0].product,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask2.outgoing[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertEqual(flask2.outgoing[1].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask2.outgoing[1].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB')
        self.assertEqual(flask2.outgoing[1].rule,
                         '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]')
        self.assertEqual(len(flask2.incoming), 2)
        self.assertEqual(flask2.incoming[0].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB')
        self.assertEqual(flask2.incoming[0].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask2.incoming[0].rule,
                         '[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]')
        self.assertEqual(flask2.incoming[1].reactant,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask2.incoming[1].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask2.incoming[1].rule,
                         '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]')
        self.assertListEqual(flask2.rules_applied, [
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'])
        self.assertListEqual(flask2.tags, ['NN', 'test'])
        self.assertEqual(flask2.priority, 1800)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 2)

    def test_rdkitflask_all_reactivity(self):

        # Insert flask records
        self._insert_data_flask_NN0_unreactive()
        self._insert_data_flask_NN1()
        self._insert_data_flask_NN3()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Number of tasks
        input_kv['tasks_per_cycle'] = 2
        input_kv['batch_size'] = 2

        # Run RDKitFlaskReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'C=O.C=O')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0, 2])
        self.assertEqual(len(flask1.outgoing), 2)
        self.assertEqual(flask1.outgoing[0].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask1.outgoing[0].product,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask1.outgoing[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertEqual(flask1.outgoing[1].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask1.outgoing[1].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB')
        self.assertEqual(flask1.outgoing[1].rule,
                         '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]')
        self.assertEqual(len(flask1.incoming), 2)
        self.assertEqual(flask1.incoming[0].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB')
        self.assertEqual(flask1.incoming[0].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask1.incoming[0].rule,
                         '[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]')
        self.assertEqual(flask1.incoming[1].reactant,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask1.incoming[1].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask1.incoming[1].rule,
                         '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]')
        self.assertListEqual(flask1.rules_applied, [
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]'])
        self.assertListEqual(flask1.tags, ['NN', 'test'])
        self.assertEqual(flask1.priority, 1800)

        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, '[CH-]=O.[H+].C=O')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertEqual(len(flask2.outgoing), 2)
        self.assertEqual(flask2.outgoing[0].reactant,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask2.outgoing[0].product,
                         'AIXFNXIOVVCILF-UHFFFAOYSA-P-MVPZQLQRFO')
        self.assertEqual(flask2.outgoing[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertEqual(flask2.outgoing[1].reactant,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask2.outgoing[1].product,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask2.outgoing[1].rule,
                         '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]')
        self.assertEqual(len(flask2.incoming), 1)
        self.assertEqual(flask2.incoming[0].reactant,
                         'CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ')
        self.assertEqual(flask2.incoming[0].product,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask2.incoming[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertListEqual(flask2.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'])
        self.assertListEqual(flask2.tags, ['test'])
        self.assertEqual(flask2.priority, 1842)

        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "AIXFNXIOVVCILF-UHFFFAOYSA-P-MVPZQLQRFO"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[CH-]=O.[CH-]=O.[H+].[H+]')
        self.assertEqual(flask3.generation, 2)
        self.assertListEqual(flask3.generation_journal, [2])
        self.assertEqual(len(flask3.outgoing), 0)
        self.assertEqual(len(flask3.incoming), 1)
        self.assertEqual(flask3.incoming[0].reactant,
                         'IYJGNZSCJWTRBW-UHFFFAOYSA-O-BEKGVSKXXA')
        self.assertEqual(flask3.incoming[0].product,
                         'AIXFNXIOVVCILF-UHFFFAOYSA-P-MVPZQLQRFO')
        self.assertEqual(flask3.incoming[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertListEqual(flask3.rules_applied, [])
        self.assertListEqual(flask3.tags, ['test'])
        self.assertEqual(flask3.priority, 2084)

        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.UNREACTIVE)
        self.assertEqual(flask4.smiles, '[CH2+]OC[O-]')
        self.assertEqual(flask4.generation, 3)
        self.assertListEqual(flask4.generation_journal, [3])
        self.assertEqual(len(flask4.outgoing), 2)
        self.assertEqual(flask4.outgoing[0].reactant,
                         'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX')
        self.assertEqual(flask4.outgoing[0].product,
                         'HZIXCDRQDCNIKZ-UHFFFAOYSA-O-VULHHZTNKX')
        self.assertEqual(flask4.outgoing[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertEqual(flask4.outgoing[1].reactant,
                         'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX')
        self.assertEqual(flask4.outgoing[1].product,
                         'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD')
        self.assertEqual(flask4.outgoing[1].rule,
                         '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]')
        self.assertEqual(len(flask4.incoming), 0)
        self.assertListEqual(flask4.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'])
        self.assertListEqual(flask4.tags, ['NN'])
        self.assertEqual(flask4.priority, 4500)

        flask5 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD"}))

        # Check flask result
        self.assertEqual(flask5.status, Status.INITIAL)
        self.assertEqual(flask5.smiles, 'C1OCO1')
        self.assertEqual(flask5.generation, 4)
        self.assertListEqual(flask5.generation_journal, [4])
        self.assertEqual(len(flask5.outgoing), 0)
        self.assertEqual(len(flask5.incoming), 1)
        self.assertEqual(flask5.incoming[0].reactant,
                         'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX')
        self.assertEqual(flask5.incoming[0].product,
                         'GFAJOMHUNNCCJQ-UHFFFAOYSA-N-GUPNWICOXD')
        self.assertEqual(flask5.incoming[0].rule,
                         '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]')
        self.assertListEqual(flask5.rules_applied, [])
        self.assertListEqual(flask5.tags, ['NN'])
        self.assertEqual(flask5.priority, 5200)

        flask6 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "HZIXCDRQDCNIKZ-UHFFFAOYSA-O-VULHHZTNKX"}))

        # Check flask result
        self.assertEqual(flask6.status, Status.INITIAL)
        self.assertEqual(flask6.smiles, '[H+].[CH2+]O[CH-][O-]')
        self.assertEqual(flask6.generation, 4)
        self.assertListEqual(flask6.generation_journal, [4])
        self.assertEqual(len(flask6.outgoing), 0)
        self.assertEqual(len(flask6.incoming), 1)
        self.assertEqual(flask6.incoming[0].reactant,
                         'OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX')
        self.assertEqual(flask6.incoming[0].product,
                         'HZIXCDRQDCNIKZ-UHFFFAOYSA-O-VULHHZTNKX')
        self.assertEqual(flask6.incoming[0].rule,
                         '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]')
        self.assertListEqual(flask6.rules_applied, [])
        self.assertListEqual(flask6.tags, ['NN'])
        self.assertEqual(flask6.priority, 5082)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 6)

    def test_rdkitflask_duplicate_reactants(self):

        # Insert flask record
        self._insert_data_flask_duplicate()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask no input flasks
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "BVYFDTOHHAQGCO-UHFFFAOYSA-N-HFAGYZWUZH"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, '[CH2+]C[O-].[CH2+]C[O-]')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'])
        self.assertEqual(flask1.priority, 3872)

        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "FMPQLDBANPWEQG-UHFFFAOYSA-O-MZTHAQTPJJ"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[H+].[CH2+][CH-][O-].[CH2+]C[O-]')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertEqual(flask2.priority, 3886)

        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "GMJZLHZNZYVTCX-UHFFFAOYSA-N-BGSZAKIGXA"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, 'C1CO1.[CH2+]C[O-]')
        self.assertEqual(flask3.generation, 1)
        self.assertListEqual(flask3.generation_journal, [1])
        self.assertListEqual(flask3.rules_applied, [])
        self.assertEqual(flask3.priority, 3972)

        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "NFPCQNCBRDZKLE-UHFFFAOYSA-N-WNZCUEKGDV"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.INITIAL)
        self.assertEqual(flask4.smiles, '[CH2+]COCC[O-]')
        self.assertEqual(flask4.generation, 1)
        self.assertListEqual(flask4.generation_journal, [1])
        self.assertListEqual(flask4.rules_applied, [])
        self.assertEqual(flask4.priority, 7844)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 4)

    def test_rdkitflask_duplicate_products(self):

        # Insert flask record
        self._insert_data_flask_degenerate()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask no input flasks
        self._run_rdkitflaskreacttask(**input_kv)

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "NIGSDUUENKCZBR-UHFFFAOYSA-O-CTLWSNUJEO"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, '[H+].[H+].[CH2+]C(C[O-])C[O-].[OH-]')
        self.assertEqual(flask1.generation, 0)
        self.assertListEqual(flask1.generation_journal, [0])
        self.assertListEqual(flask1.rules_applied, [
            '([C+1:1].[O-1:2])>>[C+0:1]-[O+0:2]',
            '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]',
            '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]'])
        self.assertEqual(flask1.priority, 7860)

        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "QIVQEGIIWHBAKA-UHFFFAOYSA-P-SLNPHZZQJH"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(
            flask2.smiles, '[H+].[H+].[H+].[CH2+][C-](C[O-])C[O-].[OH-]')
        self.assertEqual(flask2.generation, 1)
        self.assertListEqual(flask2.generation_journal, [1])
        self.assertListEqual(flask2.rules_applied, [])
        self.assertEqual(flask2.priority, 7788)

        flask3 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "AEZLCMTYTBRXOP-UHFFFAOYSA-P-SVEVHAMBDH"}))

        # Check flask result
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[O-]CC(C[O-])CO.[H+].[H+]')
        self.assertEqual(flask3.generation, 1)
        self.assertListEqual(flask3.generation_journal, [1])
        self.assertListEqual(flask3.rules_applied, [])
        self.assertEqual(flask3.priority, 10918)

        flask4 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "LKLGEIYCPKLCCG-UHFFFAOYSA-O-ALVNHACYNT"}))

        # Check flask result
        self.assertEqual(flask4.status, Status.INITIAL)
        self.assertEqual(flask4.smiles, '[H+].[H+].[O-]CC1COC1.[OH-]')
        self.assertEqual(flask4.generation, 1)
        self.assertListEqual(flask4.generation_journal, [1])
        self.assertListEqual(flask4.rules_applied, [])
        self.assertEqual(flask4.priority, 7960)

        flask5 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "FEAKCIJZQQMUOM-UHFFFAOYSA-P-SIINSHQKGH"}))

        # Check flask result
        self.assertEqual(flask5.status, Status.INITIAL)
        self.assertEqual(
            flask5.smiles, '[H+].[H+].[H+].[CH2+]C([CH-][O-])C[O-].[OH-]')
        self.assertEqual(flask5.generation, 1)
        self.assertListEqual(flask5.generation_journal, [1])
        self.assertListEqual(flask5.rules_applied, [])
        self.assertEqual(flask5.priority, 7788)

        # Number of flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 5)

    def test_rdkitflask_noinput(self):

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask no input flasks
        self._run_rdkitflaskreacttask(**input_kv)

        # No flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 0)

    def test_rdkitflask_norules(self):

        # Insert flask records
        self._insert_data_flask_NN1()

        # Run RDKitFlaskReactionTask with no input rules
        self._run_rdkitflaskreacttask()

        # No additional flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_rdkitflask_noexact(self):

        # Insert flask records
        self._insert_data_flask_NN1()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C-1:1].[#1+1:2]>>[C+0:1]-[#1+0:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Disable exact atom mapping raises exception
        input_kv['react_exact_mapping_rdkit_flask'] = {
            'key': 'react_exact_mapping',
            'value': False,
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask no input flasks
        self._run_rdkitflaskreacttask(**input_kv)

        # No additional flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_rdkitflask_badrules(self):

        # Insert flask records
        self._insert_data_flask_NN1()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[CH1:][OH1:2]>>[C:1]=[O:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].>>[C:1][O:2]C',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1].[OH1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C:1][OH1:2].[CH1:][OH1:2]>>[C:1][O:2]C',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        key = 'reac_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[c+0:1]1[c+0:2]cccc1>>[C-1:1]1[C+1:2]1',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitReactionTask
        self._run_rdkitflaskreacttask(**input_kv)

        # No additional flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_rdkitflask_uni_product_error(self):

        # Insert flask record
        self._insert_data_flask_NN4()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[O+0:2]>>[C+1:1].[C-1:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask no input flasks
        self._run_rdkitflaskreacttask(**input_kv)

        # No additional flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_rdkitflask_cycl_product_error(self):

        # Insert flask record
        self._insert_data_flask_cycl_product_error()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+1:1].[C-1:2]>>[C+0:1]-[C+2:2]',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask no input flasks
        self._run_rdkitflaskreacttask(**input_kv)

        # No additional flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_rdkitflask_bi_product_error(self):

        # Insert flask record
        self._insert_data_flask_bi_product_error()

        # Task options dict
        input_kv = {}

        # Rule options
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': 'C.C>>CC',
            'target': 'colibri.task.RDKitFlaskReactionTask',
            'precedence': 'command'}

        # Run RDKitFlaskReactionTask no input flasks
        self._run_rdkitflaskreacttask(**input_kv)

        # No additional flasks inserted
        self.assertEqual(self._client.test.flask.find({}).count(), 1)

    def test_rdkitflask_rollback(self):
        global _opts

        # Insert flask record
        self._insert_data_flask_NN0()

        # Dictionary of input options
        input_kv = {}

        # Add task options
        calc_name = 'rdkit_flask_react_' + randomid(pos=6)
        input_kv[calc_name] = {
            'key': calc_name, 'group': 'task',
            'value': 'RDKitFlaskReactionTask'}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Batch size
        input_kv['batch_size'] = 1

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Locks
        input_kv['optimistic_lock'] = False
        input_kv['pessimistic_lock'] = True

        # Rule options, target is 'colibri'
        key = 'react_rule_' + randomid(pos=6)
        input_kv[key] = {
            'key': key, 'group': 'react_rule',
            'value': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
            'target': 'colibri'}

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        # Create Context object
        context = Context(opts)

        # Create new RDKitFlaskReactionTask
        task = colibri.task.RDKitFlaskReactionTask(context, opts)

        # Setup
        task.setup()

        # Retrieve
        task.retrieve()

        # Get flask result from the database
        flask1 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask1.status, Status.REACTIVE_LOCKED)
        self.assertEqual(flask1.etag, calc_name)
        self.assertEqual(flask1.smiles, 'C=O.C=O')

        # Read reactive flask
        task._read_reactive_flask()

        # Rollback
        task.rollback()

        # Get flask result from the database
        flask2 = Flask.fromdict(self._client.test.flask.find_one(
            {"_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"}))

        # Check flask result
        self.assertEqual(flask2.status, Status.REACTIVE)
        self.assertEqual(flask2.etag, '')
        self.assertEqual(flask2.smiles, 'C=O.C=O')


if __name__ == '__main__':
    unittest.main()
