"""Temporary Memcached instance for colibri testing.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    TmpMemcachedClient: Singleton to manage a temporary Memcached instance

"""

__all__ = ['TmpMemcachedClient']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/26/2015'


#
# Library modules
#

import os
import time
import subprocess
import atexit

#
# Third-party modules
#

try:
    import port_for
except ImportError:
    port_for = None

#
# colibri modules
#

from colibri.memcachedcache import MemcachedClient
from colibri.exceptions import ConnectionError

#
# Local variables
#

TEST_PORT_MIN = 42189
TEST_PORT_MAX = 42199


#
# Classes
#


class TmpMemcachedClient(object):
    """
    Singleton to manage a temporary Memcached instance at localhost
    for testing. The database instance is destroyed at exit.
    Wraps the memcachedcache.MemcachedClient API.

    """

    _instance = None

    @classmethod
    def get_instance(cls):
        """Ensure a singleton instance."""
        if cls._instance is None:
            cls._instance = cls()

        # Register with atexit[]
        atexit.register(cls._instance.destroy)

        return cls._instance

    def __init__(self):

        # Iterate over test ports
        for port in xrange(TEST_PORT_MIN, TEST_PORT_MAX):

            # if port_for is not None:
            if port_for is not None:

                # Check port for availability using port_for
                if not port_for.is_available(port):
                    continue

            # Try to connect directly using mongo
            # Slower but has no additional requirements
            else:

                # Check if a mongod instance is already listening
                try:
                    memcachedclient = MemcachedClient(
                            host='localhost', port=port)
                except ConnectionError:
                    pass
                else:
                    continue

            # Start Memcached daemon process
            try:

                process = subprocess.Popen(
                    ['memcached', '-l', 'localhost', '-p', str(port),
                     '-m', '64'], stdout=open(os.devnull, 'wb'),
                    stderr=subprocess.STDOUT)

            except (OSError, IOError):
                raise RuntimeError('Cannot start subprocess')

            # Sleep a bit
            time.sleep(1)

            # Open MemcachedClient instance
            try:
                memcachedclient = MemcachedClient(host='localhost', port=port)
            except ConnectionError:
                process.terminate()
                process.wait()
            else:
                # Save local variables
                self._memcachedclient = memcachedclient
                self._process = process
                self._host = 'localhost'
                self._port = port
                break
        else:
            raise RuntimeError('Cannot find suitable port for testing')

    def __str__(self):
        return 'TmpMemcachedClient %s:%d' % (self._host, self._port)

    def __getattr__(self, attr):
        """Delegate to the MemcachedClient API."""
        return getattr(self.memcachedclient, attr)

    @property
    def memcachedclient(self):
        return self._memcachedclient

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    def destroy(self):
        """Shut down Memcached daemon instance."""

        # Terminate the Memcached daemon process
        if getattr(self, '_process', None):
            self._process.terminate()
            self._process.wait()
            self._process = None

        # Remove the temporary Memcached instance
        try:
            cls = type(self)
            if getattr(cls, '_instance', None) is not None:
                cls._instance = None
        except RuntimeError:
            pass
