"""Test classes for colibri stat command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri stat command

Test Classes:
    TestInitCommand: Test classes for InitCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/30/2015'

#
# Standard library
#

import os
import unittest
import logging
import tempfile

#
# colibri modules
#

from colibri.api import stat
from colibri.data import Flask
from colibri.enums import Result
from colibri.exceptions import ExecutionError

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from tmpmemcached import TmpMemcachedClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Temporary Memcached instance
_tmpmemcached = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_statcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri stat command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestStatCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _tmpmemcached, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)

    # Temporary Memcached instance
    try:
        _tmpmemcached = TmpMemcachedClient.get_instance()
        _logger.info('Starting %s' % _tmpmemcached)
    except ExecutionError:
        _logger.warning('Cannot start Memcached')


def tearDownModule():
    global _tmpmongo, _tmpmemcached, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

    # Temporary Memcached instance
    if _tmpmemcached is not None:
        _logger.info('Destroying %s' % _tmpmemcached)
        _tmpmemcached.destroy()

#
# Test classes
#


class TestStatCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)
        if _tmpmemcached is not None:
            self._map_cache = _tmpmemcached.cache(key_prefix='MAP:')
            self._red_cache = _tmpmemcached.cache(key_prefix='RED:')
            self._logger.info('Connecting to %s' % _tmpmemcached)

    def tearDown(self):
        global _tmpmemcached
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})
        if _tmpmemcached is not None:
            self._logger.info('Flushing %s' % _tmpmemcached)
            self._map_cache.clear()
            self._red_cache.clear()

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _tmpmemcached, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Add cache hostname and port options
        if _tmpmemcached is not None:
            input_kv['cache_hostname'] = 'localhost'
            input_kv['cache_port'] = _tmpmemcached.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_mol_h2o_formula(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            {"z": [], "d": 18, "f": {"i": None, "s": "O", "d": 18}, "l": 1000,
             "3": 0, "2": "", "5": "", "8": 0, "4": "",
             "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_mol_nh3_configuration(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 17, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
4

N         -0.00000        0.00000        0.06983
H          0.58552       -0.77033       -0.32344
H         -0.95990       -0.12189       -0.32345
H          0.37440        0.89221       -0.32345"""},
            "z": [], "d": 17, "f": {"i": None, "s": "N", "d": 17},
            "l": 1100, "3": 0, "2": "", "4": "", "8": 0, "t": ["nitrogen"],
            "_id": "QGZKDVFQNNGYKY-UHFFFAOYSA-N-MAXWVLNDLJ"})

    def _insert_data_mol_ch4_geometry(self):

        # Insert molecule record
        self._client.test.mol.insert({
            "c": {"d": 16, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

C          0.00000       -0.00000       -0.00000
H          0.14053        0.96103        0.53604
H         -0.74194       -0.62110        0.54270
H          0.96783       -0.53995       -0.05093
H         -0.36642        0.20002       -1.02781"""},
            "z": [], "d": 16, "g": {
                "e": -177.04321, "d": 16, "i": None, "h": "PM7", "m": 1,
                "o": "xyz", "q": 0, "w": "MOPAC", "7": True, "v": "2012",
                "x": """
5

C    -0.000007     0.000047     0.000006
H     0.137522     0.940504     0.524617
H    -0.726023    -0.607873     0.531084
H     0.947084    -0.528475    -0.049863
H    -0.358559     0.195681    -1.005860"""},
            "f": {"i": None, "s": "C", "d": 16}, "l": 1200, "3": 6,
            "2": "", "4": "", "8": 0, "t": ["test"],
            "_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"})

    def _insert_data_mol_locked(self):

        # Insert FORMULA_LOCKED record
        self._client.test.mol.insert({
            "z": [], "d": 44, "f": {"i": None, "s": "CCC", "d": 44},
            "l": 1001, "3": 2, "2": "", "5": "rdkit_build_i0b8hg",
            "t": ["organic"],
            "4": "", "8": 0, "_id": "ATUOYWHBWRKTHZ-UHFFFAOYSA-N-GWQPKENYAK"})

        # Insert CONFIGURATION_LOCKED record
        self._client.test.mol.insert({
            "c": {"d": 52, "i": None, "m": 1, "q": 0, "w": "RDKit",
                  "v": "2015.3.1", "x": """\
5

F         -1.12799       -0.23990       -0.00000
C         -0.00007        0.55775        0.00001
F          1.12804       -0.23986       -0.00000
H         -0.00006        1.19829        0.90579
H         -0.00004        1.19829       -0.90585"""},
            "z": [], "d": 52, "f": {"i": None, "s": "FCF", "d": 52},
            "l": 1101, "3": 2, "2": "", "5": "mopac_geometry_umIYYj",
            "t": ["organic", "halogenated"],
            "4": "", "8": 0, "_id": "RWRIWBAIICGTTQ-UHFFFAOYSA-N-TLSEZQASAI"})

    def _insert_data_mol_invalid(self):

        # Insert FORMULA_INVALID record
        self._client.test.mol.insert({
            "z": [], "d": 28, "f": {"i": None, "s": "[CH2][CH2]", "d": 28},
            "l": 1091, "3": 6, "2": "", "4": "", "8": 1,
            "_id": "VGGSQFUCUMXWEO-UHFFFAOYSA-N-ZYOCEWJBAV"})

    def _insert_data_flask_LL0_initial(self):

        # Insert new flask record (generation 0)
        self._client.test.flask.insert(
            Flask(smiles='CO.CO', tags=['LL']).todict())

    def _insert_data_flask_NN1_running(self):

        # Insert new flask record (generation 1)
        self._client.test.flask.insert(
            Flask(smiles='[CH-]=O.[H+].C=O', generation=1,
                  generation_journal=[1], status=2100,
                  tags=['test']).todict())

    def _insert_data_flask_NN4_reactive(self):

        # Insert new flask record (generation 4)
        self._client.test.flask.insert(
            Flask(smiles='C1OCO1', generations=4, generation_journal=[4],
                  status=2200, tags=['NN']).todict())

    def _insert_data_flask_locked(self):

        # Insert INITIAL_LOCKED records
        self._client.test.flask.insert({
            "d": 1800, "k": [
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"},
                {"s": "C=O", "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO"}],
            "j": 2001, "m": 1, "s": "C=O.C=O", "q": 0, "3*": [0], "3": 0,
            "2": "", "4": "", "0*": [], "8": 0, "5": "flask_mapper_WViqDy",
            "t": ["NN"], "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-DNLMFHERKQ"})

        # Insert RUNNING_LOCKED record
        self._client.test.flask.insert({
            "b": [{"1": "[C+1:1]-[C-1:2]>>[C+0:1]=[C+0:2]",
                   "p": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK",
                   "r": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-RDGEAJXOVL"}],
            "d": 8488, "3*": [8], "k": [
                {"s": "C=C=O",
                 "_id": "CCGKOQOJPYTBIH-UHFFFAOYSA-N-ACYGJWNMWD"},
                {"s": "O", "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"}],
            "j": 2101, "m": 1, "s": "C=C=O.O", "q": 0, "0": -889.86602,
            "3": 8, "2": "", "4": "", "5": "flask_reducer_f4Ssb5",
            "0*": [-889.86602], "8": 0,
            "t": ["NN"], "_id": "HKDNMKSZNAFAKR-UHFFFAOYSA-N-HNTINFTYTK"})

    def _insert_data_flask_badchem(self):

        # Insert REACTIVE_BADCHEM record
        self._client.test.flask.insert({
            "a": [], "9": None, "b": [
                {"1": "([C+1:1].[C-1:2])>>[C+0:1]-[C+0:2]",
                 "p": "NNTJCCALZXXKME-UHFFFAOYSA-N-HACIWYXXPZ",
                 "r": "NNTJCCALZXXKME-UHFFFAOYSA-N-INQLCSNSZH"}],
            "e": None, "d": 4964, "3*": [6],
            "k": [{"i": None, "s": "[H+]",
                   "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"},
                  {"i": None, "s": "[H+]",
                   "_id": "GPRLSGONYQIRFK-UHFFFAOYSA-N-HDTWLUOJFQ"},
                  {"i": None, "s": "[CH2][CH2]",
                   "_id": "VGGSQFUCUMXWEO-UHFFFAOYSA-N-ZYOCEWJBAV"},
                  {"i": None, "s": "[OH-]",
                   "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"},
                  {"i": None, "s": "[OH-]",
                   "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-M-JWANHJLYAR"}],
            "j": 2213, "m": 3, "s": "[H+].[H+].[CH2][CH2].[OH-].[OH-]",
            "q": 0, "0": -924.52009, "3": 6, "2": "BadChemistry",
            "4": "", "0*": [-924.52009], "8": 1,
            "_id": "NNTJCCALZXXKME-UHFFFAOYSA-N-HACIWYXXPZ"})

    def _insert_data_flask_unavailable(self):

        # Insert UNAVAILABLE record
        self._client.test.flask.insert({
            "9": 0.0, "b": [
                {"1": "[C+0:1]=[O+0:2]>>[C+1:1]-[O-1:2]",
                 "p": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE",
                 "r": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-JPUGZBKVUB"},
                {"1": "[C+0:1]-[O+0:2]>>[C+1:1].[O-1:2]",
                 "p": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE",
                 "r": "OLZYYGXDSFMNJQ-UHFFFAOYSA-N-DXCQSPEOYX"}],
            "e": -888.76256, "d": 2200, "3*": [2, 4],
            "k": [
                {"s": "[CH2+][O-]",
                 "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"},
                {"s": "[CH2+][O-]",
                 "_id": "WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU"}],
            "j": 2310, "m": 1, "s": "[CH2+][O-].[CH2+][O-]",
            "q": 0, "0": -888.76256, "3": 2, "2": "", "4": "",
            "0*": [-888.76256, -888.93686], "8": 0, "t": ["NN"],
            "_id": "CUHVTYCUTYWQOR-UHFFFAOYSA-N-ZJONNSIWIE"})

    def _insert_map_cache_data_mol(self):

        # Insert molecules into mapping cache
        self._map_cache.set_blocking({
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': '',
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': ''})

    def _insert_red_cache_data_mol(self):

        # Insert molecules into reducer cache
        self._red_cache.set_blocking({
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DQENYVTZYO': '-444.38128',
            'WSFSSNUMVMOOMR-UHFFFAOYSA-N-DCKFFISXAU': '-444.38128'})

    def test_stat(self):

        global _tmpmemcached
        if _tmpmemcached is None:
            self.skipTest('Memcached is not available')

        # Insert molecule records
        self._insert_data_mol_h2o_formula()
        self._insert_data_mol_nh3_configuration()
        self._insert_data_mol_ch4_geometry()
        self._insert_data_mol_locked()
        self._insert_data_mol_invalid()

        # Insert flask records
        self._insert_data_flask_LL0_initial()
        self._insert_data_flask_NN1_running()
        self._insert_data_flask_NN4_reactive()
        self._insert_data_flask_locked()
        self._insert_data_flask_badchem()
        self._insert_data_flask_unavailable()

        # Insert cache data
        self._insert_map_cache_data_mol()
        self._insert_red_cache_data_mol()

        # Get configuration options
        opts = self._update_conf_opts()

        # Output file
        _, output_file = tempfile.mkstemp()

        # Run colibri stat command
        result = stat(output_file=output_file, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Read statistics output
        output = open(output_file).read()

        # Check statistics output
        self.assertRegexpMatches(output, r"FORMULA\.+ 1")
        self.assertRegexpMatches(output, r"INITIAL\.+ 1")
        self.assertRegexpMatches(output, r"FORMULA_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"INITIAL_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"FORMULA_INVALID\.+ 1")
        self.assertRegexpMatches(output, r"RUNNING\.+ 1")
        self.assertRegexpMatches(output, r"CONFIGURATION\.+ 1")
        self.assertRegexpMatches(output, r"RUNNING_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"CONFIGURATION_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"REACTIVE\.+ 1")
        self.assertRegexpMatches(output, r"GEOMETRY\.+ 1")
        self.assertRegexpMatches(output, r"REACTIVE_BADCHEM\.+ 1")
        self.assertRegexpMatches(output, r"UNAVAILABLE\.+ 1")
        self.assertRegexpMatches(
            output, r"MAX GENERATION\.+ 6\s+MAX GENERATION\.+ 8")
        self.assertRegexpMatches(
            output, r"DATABASE\s+test\s+DATABASE\s+test")
        self.assertRegexpMatches(
            output, r"COLLECTION\s+mol\s+COLLECTION\s+flask")
        self.assertRegexpMatches(output, r"TAGS \[\]")
        self.assertRegexpMatches(output, r"TOTAL ITEMS\.+ 4")
        self.assertRegexpMatches(output, r"CURRENT ITEMS\.+ 4")
        self.assertRegexpMatches(output, r"SET COMMANDS\.+ 4")

        # Remove temporary file
        os.remove(output_file)

    def test_stat_cachedown(self):

        global _tmpmemcached
        if _tmpmemcached is not None:
            self.skipTest('testing Memcached being down')

        # Insert molecule records
        self._insert_data_mol_h2o_formula()
        self._insert_data_mol_nh3_configuration()
        self._insert_data_mol_ch4_geometry()
        self._insert_data_mol_locked()
        self._insert_data_mol_invalid()

        # Insert flask records
        self._insert_data_flask_LL0_initial()
        self._insert_data_flask_NN1_running()
        self._insert_data_flask_NN4_reactive()
        self._insert_data_flask_locked()
        self._insert_data_flask_badchem()
        self._insert_data_flask_unavailable()

        # Get configuration options
        opts = self._update_conf_opts()

        # Output file
        _, output_file = tempfile.mkstemp()

        # Run colibri stat command
        result = stat(output_file=output_file, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Read statistics output
        output = open(output_file).read()

        # Check statistics output
        self.assertRegexpMatches(output, r"FORMULA\.+ 1")
        self.assertRegexpMatches(output, r"INITIAL\.+ 1")
        self.assertRegexpMatches(output, r"FORMULA_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"INITIAL_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"FORMULA_INVALID\.+ 1")
        self.assertRegexpMatches(output, r"RUNNING\.+ 1")
        self.assertRegexpMatches(output, r"CONFIGURATION\.+ 1")
        self.assertRegexpMatches(output, r"RUNNING_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"CONFIGURATION_LOCKED\.+ 1")
        self.assertRegexpMatches(output, r"REACTIVE\.+ 1")
        self.assertRegexpMatches(output, r"GEOMETRY\.+ 1")
        self.assertRegexpMatches(output, r"REACTIVE_BADCHEM\.+ 1")
        self.assertRegexpMatches(output, r"UNAVAILABLE\.+ 1")
        self.assertRegexpMatches(
            output, r"MAX GENERATION\.+ 6\s+MAX GENERATION\.+ 8")
        self.assertRegexpMatches(
            output, r"DATABASE\s+test\s+DATABASE\s+test")
        self.assertRegexpMatches(
            output, r"COLLECTION\s+mol\s+COLLECTION\s+flask")
        self.assertRegexpMatches(output, r"TAGS \[\]")
        self.assertRegexpMatches(
            output, r"CACHE Memcached HOST \*\* DOWN \*\* PORT \*\* DOWN")

        # Remove temporary file
        os.remove(output_file)


    def test_stat_tags(self):

        # Insert molecule records
        self._insert_data_mol_h2o_formula()
        self._insert_data_mol_nh3_configuration()
        self._insert_data_mol_ch4_geometry()
        self._insert_data_mol_locked()
        self._insert_data_mol_invalid()

        # Insert flask records
        self._insert_data_flask_LL0_initial()
        self._insert_data_flask_NN1_running()
        self._insert_data_flask_NN4_reactive()
        self._insert_data_flask_locked()
        self._insert_data_flask_badchem()
        self._insert_data_flask_unavailable()

        # Get configuration options
        opts = self._update_conf_opts(tags=['test'])

        # Output file
        _, output_file = tempfile.mkstemp()

        # Run colibri stat command
        result = stat(output_file=output_file, opts=opts)

        # Check return value
        self.assertEqual(result, Result.OK)

        # Read statistics output
        output = open(output_file).read()

        # Check statistics output
        self.assertRegexpMatches(output, r"RUNNING\.+ 1")
        self.assertRegexpMatches(output, r"GEOMETRY\.+ 1")
        self.assertRegexpMatches(
            output, r"MAX GENERATION\.+ 6\s+MAX GENERATION\.+ 1")
        self.assertRegexpMatches(output, r"TAGS \['test'\]")

        # Remove temporary file
        os.remove(output_file)

if __name__ == '__main__':
    unittest.main()
