"""Test classes for colibri fetch command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colibri fetch command

Test Classes:
    TestFetchCommand: Test classes for FetchCommand
"""

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/13/2015'

#
# Standard library
#

import json
import unittest
import logging

#
# colibri modules
#

from colibri.api import fetch
from colibri.data import Molecule, Flask
from colibri.enums import Level, Status, Result
from colibri.exceptions import CommandError
from colibri.utils import object_hook, list_accumulator

#
# Auxiliary modules
#

from tmpmongo import TmpMongoClient
from testopts import get_test_options

#
# Module variables
#

# Temporary MongoClient instance
_tmpmongo = None

# Get test options
_opts = get_test_options()

# Get logger
_logger = logging.getLogger('test_fetchcommand')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colibri fetch command."""
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFetchCommand)
    return suite

#
# Module functions
#


def setUpModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _tmpmongo = TmpMongoClient.get_instance()
    _logger.info('Starting %s' % _tmpmongo)


def tearDownModule():
    global _tmpmongo, _logger

    # Temporary MongoDB instance
    _logger.info('Destroying %s' % _tmpmongo)
    _tmpmongo.destroy()

#
# Test classes
#


class TestFetchCommand(unittest.TestCase):

    def setUp(self):
        global _tmpmongo, _logger
        self._client = _tmpmongo.mongoclient
        self._logger = _logger
        self._logger.info('Connecting to %s' % _tmpmongo)

    def tearDown(self):
        self._logger.info('Flushing %s' % _tmpmongo)
        self._client.test.mol.remove({})
        self._client.test.flask.remove({})

    def _update_conf_opts(self, **kwargs):
        global _tmpmongo, _opts

        # Dictionary of input options
        input_kv = {}

        # Add hostname and port options
        input_kv['db_hostname'] = 'localhost'
        input_kv['db_port'] = _tmpmongo.port

        # Logging level
        input_kv['logging_level'] = _logging_level

        # Additional options
        input_kv.update(kwargs)

        # Create Options object
        opts = _opts.update(precedence='command', **input_kv)

        return opts

    def _insert_data_mol_h2o_geometry(self):

        # Insert new molecule record
        self._client.test.mol.insert({
            "c": {
                "d": 18, "i": None, "m": 1, "q": 0, "w": "RDKit",
                "v": "2015.3.1", "x": """\
3

O         -0.00001        0.06783        0.00000
H         -0.78296       -0.53838        0.00000
H          0.78312       -0.53826        0.00000"""}, "z": [], "d": 18,
            "g": {
                "e": -322.6792, "d": 18, "i": None, "h": "PM7",
                "m": 1, "o": "xyz", "q": 0, "w": "MOPAC",
                "7": True, "v": "2012", "x": """\
3

O    -0.000001     0.058320     0.000000
H    -0.759932    -0.519445     0.000000
H     0.760055    -0.519304     0.000000"""},
            "f": {"i": None, "s": "O", "d": 18},
            "l": 1200, "3": 0, "2": "", "4": "", "8": 0,
            "t": ["liquid", "hydrophilic"],
            "_id": "XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU"})

    def _insert_data_mol_acetone(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='CC(=O)C', generation=12,
                     tags=['liquid']).todict())

    def _insert_data_mol_br2(self):

        # Insert new molecule record
        self._client.test.mol.insert(
            Molecule(smiles='BrBr', generation=4,
                     tags=['liquid', 'halogen']).todict())

    def _insert_data_flask_LL4_initial(self):

        # Insert new flask record
        self._client.test.flask.insert(Flask(
            smiles='COC.O', generation=4).todict())

    def _insert_data_flask_C_F_unreactive(self):

        # Insert new flask record
        self._client.test.flask.insert({
            "a": [], "9": None, "b": [], "e": -656.5736,
            "d": 656, "0": None, "k": [
                {"i": None, "s": "F",
                 "_id": "KRHYYFGTRYWZRS-UHFFFAOYSA-N-MMIJCGAIQJ"},
                {"i": None, "s": "C",
                 "_id": "VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB"}],
            "j": 2300, "m": 1, "s": "F.C", "q": 0, "3*": [0],
            "3": 0, "2": "", "4": "", "0*": [], "8": 0,
            "t": ["10-electron", "gas"],
            "_id": "UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS"})

    def _insert_data_flask_N_O0_initial(self):

        # Insert new flask record
        self._client.test.flask.insert(
            Flask(smiles='N.O', tags=['liquid', '10-electron'],
                  generation=0).todict())

    def _insert_data_flask_N_O1_initial(self):

        # Insert new flask record
        self._client.test.flask.insert(Flask(
            smiles='[NH4+].[OH-]', generation=1).todict())

    def test_fetch_mol_json(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts()

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertAlmostEqual(mol1.geometry.energy, -322.6792, places=4)
        self.assertListEqual(mol1.tags, ['liquid', 'hydrophilic'])

        # Get results
        mol2 = Molecule.fromdict(
            json.loads(molecules[1], object_hook=object_hook))

        # Check results
        self.assertEqual(mol2.key, 'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS')
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'CC(C)=O')
        self.assertListEqual(mol2.tags, ['liquid'])

        # Get results
        mol3 = Molecule.fromdict(
            json.loads(molecules[2], object_hook=object_hook))

        # Check results
        self.assertEqual(mol3.key, 'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP')
        self.assertEqual(mol3.level, Level.FORMULA)
        self.assertEqual(mol3.formula.smiles, 'BrBr')
        self.assertListEqual(mol3.tags, ['liquid', 'halogen'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 3)

    def test_fetch_mol_json_level(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts(level=['GEOMETRY'])

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertAlmostEqual(mol1.geometry.energy, -322.6792, places=4)
        self.assertListEqual(mol1.tags, ['liquid', 'hydrophilic'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 1)

    def test_fetch_mol_json_min_generation(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts(min_generation=5)

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS')
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.generation, 12)
        self.assertEqual(mol1.formula.smiles, 'CC(C)=O')
        self.assertListEqual(mol1.tags, ['liquid'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 1)

    def test_fetch_mol_json_max_generation(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts(max_generation=5)

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.generation, 0)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertAlmostEqual(mol1.geometry.energy, -322.6792, places=4)
        self.assertListEqual(mol1.tags, ['liquid', 'hydrophilic'])

        # Get results
        mol2 = Molecule.fromdict(
            json.loads(molecules[1], object_hook=object_hook))

        # Check results
        self.assertEqual(mol2.key, 'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP')
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.generation, 4)
        self.assertEqual(mol2.formula.smiles, 'BrBr')
        self.assertListEqual(mol2.tags, ['liquid', 'halogen'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 2)

    def test_fetch_mol_json_tags(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts(tags=['halogen'])

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP')
        self.assertEqual(mol1.level, Level.FORMULA)
        self.assertEqual(mol1.formula.smiles, 'BrBr')
        self.assertListEqual(mol1.tags, ['liquid', 'halogen'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 1)

    def test_fetch_mol_json_level_tags(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts(level=['FORMULA'], tags=['liquid'])

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS')

        # Get results
        mol2 = Molecule.fromdict(
            json.loads(molecules[1], object_hook=object_hook))

        # Check results
        self.assertEqual(mol2.key, 'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP')

        # Number of molecules fetched
        self.assertEqual(len(molecules), 2)

    def test_fetch_mol_json_smiles(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts()

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, molecule_ids=['O=C(C)C'],
                       opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS')
        self.assertEqual(mol1.formula.smiles, 'CC(C)=O')
        self.assertListEqual(mol1.tags, ['liquid'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 1)

    def test_fetch_mol_json_key(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts()

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, molecule_ids=[
            'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS',
            'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP'],
            opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS')
        self.assertEqual(mol1.formula.smiles, 'CC(C)=O')
        self.assertListEqual(mol1.tags, ['liquid'])

        # Get results
        mol2 = Molecule.fromdict(
            json.loads(molecules[1], object_hook=object_hook))

        # Check results
        self.assertEqual(mol2.key, 'GDTBXPJZTBHREO-UHFFFAOYSA-N-JDBSFNOHFP')
        self.assertEqual(mol2.level, Level.FORMULA)
        self.assertEqual(mol2.formula.smiles, 'BrBr')
        self.assertListEqual(mol2.tags, ['liquid', 'halogen'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 2)

    def test_fetch_mol_json_level_smiles(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts(
            level=['FORMULA_INVALID', 'CONFIGURATION_INVALID'])

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, molecule_ids=['O=C(C)C'],
                       opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # No molecules fetched
        self.assertEqual(len(molecules), 0)

    def test_fetch_mol_json_limit(self):

        # Insert molecule records
        self._insert_data_mol_h2o_geometry()
        self._insert_data_mol_acetone()
        self._insert_data_mol_br2()

        # Get configuration options
        opts = self._update_conf_opts(limit=2)

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(molecules=mol_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')

        # Get results
        mol2 = Molecule.fromdict(
            json.loads(molecules[1], object_hook=object_hook))

        # Check results
        self.assertEqual(mol2.key, 'CSCPPACGZOOCGX-UHFFFAOYSA-N-HMUQENCRPS')

        # Number of molecules fetched
        self.assertEqual(len(molecules), 2)

    def test_fetch_flask_json(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts()

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'N.O')
        self.assertEqual(flask1.priority, 613)
        self.assertListEqual(flask1.tags, ['liquid', '10-electron'])

        # Get results
        flask2 = Flask.fromdict(
            json.loads(flasks[1], object_hook=object_hook))

        # Check results
        self.assertEqual(flask2.key, 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS')
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, 'F.C')
        self.assertAlmostEqual(flask2.energy, -656.5736, places=4)
        self.assertEqual(flask2.priority, 656)
        self.assertListEqual(flask2.tags, ['10-electron', 'gas'])

        # Get results
        flask3 = Flask.fromdict(
            json.loads(flasks[2], object_hook=object_hook))

        # Check results
        self.assertEqual(flask3.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-VRSMLPXASU')
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, '[NH4+].[OH-]')
        self.assertEqual(flask3.priority, 713)
        self.assertListEqual(flask3.tags, [])

        # Get results
        flask4 = Flask.fromdict(
            json.loads(flasks[3], object_hook=object_hook))

        # Check results
        self.assertEqual(flask4.key, 'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND')
        self.assertEqual(flask4.status, Status.INITIAL)
        self.assertEqual(flask4.smiles, 'COC.O')
        self.assertEqual(flask4.priority, 4040)
        self.assertListEqual(flask4.tags, [])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 4)

    def test_fetch_flask_json_status(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts(status=['INITIAL'])

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'N.O')
        self.assertEqual(flask1.priority, 613)
        self.assertListEqual(flask1.tags, ['liquid', '10-electron'])

        # Get results
        flask2 = Flask.fromdict(
            json.loads(flasks[1], object_hook=object_hook))

        # Check results
        self.assertEqual(flask2.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-VRSMLPXASU')
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[NH4+].[OH-]')
        self.assertEqual(flask2.priority, 713)
        self.assertListEqual(flask2.tags, [])

        # Get results
        flask3 = Flask.fromdict(
            json.loads(flasks[2], object_hook=object_hook))

        # Check results
        self.assertEqual(flask3.key, 'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND')
        self.assertEqual(flask3.status, Status.INITIAL)
        self.assertEqual(flask3.smiles, 'COC.O')
        self.assertEqual(flask3.priority, 4040)
        self.assertListEqual(flask3.tags, [])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 3)

    def test_fetch_flask_json_min_generation(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts(min_generation=3)

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'COC.O')
        self.assertEqual(flask1.priority, 4040)
        self.assertEqual(flask1.generation, 4)
        self.assertListEqual(flask1.tags, [])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 1)

    def test_fetch_flask_json_max_generation(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts(max_generation=0)

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'N.O')
        self.assertEqual(flask1.generation, 0)
        self.assertEqual(flask1.priority, 613)
        self.assertListEqual(flask1.tags, ['liquid', '10-electron'])

        # Get results
        flask2 = Flask.fromdict(
            json.loads(flasks[1], object_hook=object_hook))

        # Check results
        self.assertEqual(flask2.key, 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS')
        self.assertEqual(flask2.status, Status.UNREACTIVE)
        self.assertEqual(flask2.smiles, 'F.C')
        self.assertEqual(flask2.generation, 0)
        self.assertAlmostEqual(flask2.energy, -656.5736, places=4)
        self.assertEqual(flask2.priority, 656)
        self.assertListEqual(flask2.tags, ['10-electron', 'gas'])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 2)

    def test_fetch_flask_json_tags(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts(tags=['gas'])

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS')
        self.assertEqual(flask1.status, Status.UNREACTIVE)
        self.assertEqual(flask1.smiles, 'F.C')
        self.assertListEqual(flask1.tags, ['10-electron', 'gas'])
        self.assertAlmostEqual(flask1.energy, -656.5736, places=4)

        # Number of flasks fetched
        self.assertEqual(len(flasks), 1)

    def test_fetch_flask_json_status_tags(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts(status=['INITIAL'], tags=['10-electron'])

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'N.O')
        self.assertListEqual(flask1.tags, ['liquid', '10-electron'])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 1)

    def test_fetch_flask_json_smiles(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts()

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, flask_ids=['[OH-].[NH4+]'],
                       opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-VRSMLPXASU')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, '[NH4+].[OH-]')
        self.assertListEqual(flask1.tags, [])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 1)

    def test_fetch_flask_json_key(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts()

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, flask_ids=[
            'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ',
            'VHUUQVKOLVNVRT-UHFFFAOYSA-N-VRSMLPXASU'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'N.O')
        self.assertListEqual(flask1.tags, ['liquid', '10-electron'])

        # Get results
        flask2 = Flask.fromdict(
            json.loads(flasks[1], object_hook=object_hook))

        # Check results
        self.assertEqual(flask2.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-VRSMLPXASU')
        self.assertEqual(flask2.status, Status.INITIAL)
        self.assertEqual(flask2.smiles, '[NH4+].[OH-]')
        self.assertListEqual(flask2.tags, [])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 2)

    def test_fetch_flask_json_status_smiles(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts(status=['INITIAL'])

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()
        # Run colibri fetch command
        result = fetch(flasks=flask_reader, flask_ids=['C.F'], opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Number of flasks fetched
        self.assertEqual(len(flasks), 0)

    def test_fetch_flask_json_limit(self):

        # Insert flask records
        self._insert_data_flask_LL4_initial()
        self._insert_data_flask_C_F_unreactive()
        self._insert_data_flask_N_O0_initial()
        self._insert_data_flask_N_O1_initial()

        # Get configuration options
        opts = self._update_conf_opts(limit=2)

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'VHUUQVKOLVNVRT-UHFFFAOYSA-N-IXQRQTNVFQ')

        # Get results
        flask2 = Flask.fromdict(
            json.loads(flasks[1], object_hook=object_hook))

        # Check results
        self.assertEqual(flask2.key, 'UUXZFMKOCRKVDG-UHFFFAOYSA-N-XHBVKWJPTS')

        # Number of flasks fetched
        self.assertEqual(len(flasks), 2)

    def test_fetch_mol_flask_json(self):

        # Insert molecule record
        self._insert_data_mol_h2o_geometry()

        # Insert flask record
        self._insert_data_flask_LL4_initial()

        # Get configuration options
        opts = self._update_conf_opts()

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Create flask accumulator
        flasks, flask_reader = list_accumulator()

        # Run colibri fetch command
        result = fetch(
            molecules=mol_reader, flasks=flask_reader, opts=opts)

        # Check command return value
        self.assertEqual(result, Result.OK)

        # Get results
        mol1 = Molecule.fromdict(
            json.loads(molecules[0], object_hook=object_hook))

        # Check results
        self.assertEqual(mol1.key, 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU')
        self.assertEqual(mol1.level, Level.GEOMETRY)
        self.assertEqual(mol1.generation, 0)
        self.assertEqual(mol1.formula.smiles, 'O')
        self.assertAlmostEqual(mol1.geometry.energy, -322.6792, places=4)
        self.assertListEqual(mol1.tags, ['liquid', 'hydrophilic'])

        # Number of molecules fetched
        self.assertEqual(len(molecules), 1)

        # Get results
        flask1 = Flask.fromdict(
            json.loads(flasks[0], object_hook=object_hook))

        # Check results
        self.assertEqual(flask1.key, 'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND')
        self.assertEqual(flask1.status, Status.INITIAL)
        self.assertEqual(flask1.smiles, 'COC.O')
        self.assertEqual(flask1.priority, 4040)
        self.assertListEqual(flask1.tags, [])

        # Number of flasks fetched
        self.assertEqual(len(flasks), 1)

    def test_fetch_noreader(self):

        # Get configuration options
        opts = self._update_conf_opts()

        # Run colibri fetch command
        with self.assertRaisesRegexp(CommandError, 'Nothing to do'):
            fetch(opts=opts)

    def test_fetch_nooptions(self):

        # Insert molecule record
        self._insert_data_mol_h2o_geometry()

        # Create molecule accumulator
        molecules, mol_reader = list_accumulator()

        # Run colibri fetch command
        with self.assertRaisesRegexp(CommandError, 'No options'):
            fetch(molecules=mol_reader)


if __name__ == '__main__':
    unittest.main()
