"""Test classes for colib clean console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    get_suite: Construct test suite for colib clean console subcommand

Test Classes:
    TestCleanConsoleCommand: Test classes for CleanConsoleCommand
"""

#
# Standard library
#

import os
import sys
import StringIO
import unittest
import logging

#
# colibri modules
#

from colibri.console import ConsoleProgram, CleanConsoleCommand
from colibri.options import Options

#
# Auxiliary modules
#

# Get logger
_logger = logging.getLogger('test_cleanconsole')
_logger.addHandler(logging.NullHandler())

# Logging level
_logging_level = 'ERROR'

#
# Public functions
#


def get_suite():
    """Construct test suite for colib clean console subcommand."""
    suite = unittest.TestLoader().loadTestsFromTestCase(
        TestCleanConsoleCommand)
    return suite

#
# Test classes
#


class TestCleanConsoleCommand(unittest.TestCase):

    def _init_cleanconsole(self, argv, inp):
        global _logging_level

        # Construct ConsoleProgram
        prg = ConsoleProgram()

        # Add parser
        parser = prg.add_parser()

        # Add subparsers
        subparsers = prg.add_subparsers()

        # Add subparser
        CleanConsoleCommand.add_subparser(subparsers)

        # Args
        args = vars(parser.parse_args(argv[1:]))

        # Logging level option
        opts = Options(no_config=True, logging_level=_logging_level)

        # Redirect standard input and output
        sys.stdin.close()
        sys.stdin = StringIO.StringIO(inp)
        sys.stdout.close()
        sys.stdout = open(os.devnull, 'w')

        return CleanConsoleCommand(args=args, opts=opts)

    def test_cleanconsole_default(self):

        # Initialize CleanConsoleCommand
        console = self._init_cleanconsole(['colib', 'clean'], '\n\n')

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^CleanCommand\(molecules=False, flasks=False, cache=True, " +
            r"opts=Options\(.*\)\)")

    def test_cleanconsole_mol(self):

        # Initialize CleanConsoleCommand
        console = self._init_cleanconsole(['colib', 'clean'], 'y\nn\n')

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^CleanCommand\(molecules=True, flasks=False, cache=True, " +
            r"opts=Options\(.*\)\)")

    def test_cleanconsole_flask(self):

        # Initialize CleanConsoleCommand
        console = self._init_cleanconsole(['colib', 'clean'], 'n\ny\n')

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^CleanCommand\(molecules=False, flasks=True, cache=True, " +
            r"opts=Options\(.*\)\)")

    def test_cleanconsole_all(self):

        # Initialize CleanConsoleCommand
        console = self._init_cleanconsole(['colib', 'clean'], 'y\ny\n')

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^CleanCommand\(molecules=True, flasks=True, cache=True, " +
            r"opts=Options\(.*\)\)")

    def test_cleanconsole_badinput(self):

        # Initialize CleanConsoleCommand
        console = self._init_cleanconsole(['colib', 'clean'], 't\n\n\n')

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^CleanCommand\(molecules=False, flasks=False, cache=True, " +
            r"opts=Options\(.*\)\)")

    def test_cleanconsole_force(self):

        # Initialize CleanConsoleCommand
        console = self._init_cleanconsole(['colib', 'clean', '-f'], '')

        # Check genererated StatCommand
        self.assertRegexpMatches(
            repr(console.cmd),
            r"^CleanCommand\(molecules=True, flasks=True, cache=True, " +
            r"opts=Options\(.*\)\)")


if __name__ == '__main__':
    unittest.main()
