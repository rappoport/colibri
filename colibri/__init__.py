"""colibri is your lightweight and gregarious chemistry explorer.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""

__all__ = []

__package__ = 'colibri'
__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/01/2015'

#
# Submodule imports
#

import colibri.api
import colibri.chemistry
import colibri.console
import colibri.task
import colibri.calc
import colibri.data
import colibri.enums
import colibri.exceptions
import colibri.memcachedcache
import colibri.mongostorage
import colibri.options
import colibri.utils

assert colibri
