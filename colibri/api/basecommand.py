"""API commands for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    Command: Base class for colibri API commands
"""

__all__ = ['Command']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/11/2015'


#
# colibri submodules
#

from colibri.exceptions import CommandError


#
# Classes
#

class Command(object):
    """Base Command class."""

    def __init__(self, opts=None):
        """Command class constructor.

        Args:
            opts (Options, optional): Command options

        Returns:
            Result: Execution result

        Raises:
            CommandError: Exception in command execution
        """
        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __str__(self):
        """Return string representation

        Returns:
            str: Command type as string
        """
        return type(self).__name__

    def execute(self):
        """Perform the command.

        Abstract Class:
            Subclasses must define a specific implementation.

        Returns:
            Result: Execution result
        """
        raise NotImplementedError
