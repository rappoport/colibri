"""API for colibri init command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    init: colibri init functional API

Classes:
    InitCommand: colibri init command API
"""

__all__ = ['init', 'InitCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/16/2015'


#
# Third-party modules
#

import pymongo

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.enums import Result, Sort
from colibri.exceptions import CommandError
from colibri.utils import t2s

#
# Public functions
#


def init(**kwargs):
    return InitCommand(**kwargs).execute()

#
# Classes
#


class InitCommand(Command):
    """colibri init functional API."""

    def __init__(self, tags=False, opts=None):
        """InitCommand class constructor.

        Args:
            tags (bool, optional): Add index on tags field
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Store arguments
        self._tags = tags

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(tags=%r, opts=%r)' % (self._tags, self._opts))

    def execute(self):
        """Execute colibri init command.

        Returns:
            Result: Execution result
        """

        # Connect to the MongoDB database using pymongo
        mongoclient = pymongo.MongoClient(
            host=self._opts['db_hostname'], port=self._opts['db_port'])

        # Molecule collection
        mol_collection = mongoclient[
            self._opts['mol_database']][self._opts['mol_collection']]

        # Create level-priority compound index
        mol_collection.create_index(
            [(t2s['level'], Sort.ASCENDING),
             (t2s['priority'], Sort.ASCENDING)])

        # Create tags multi-key index
        if self._tags:
            mol_collection.create_index([(t2s['tags'], Sort.ASCENDING)])

        # Flask collection
        flask_collection = mongoclient[
            self._opts['flask_database']][self._opts['flask_collection']]

        # Create level-priority index
        flask_collection.create_index(
            [(t2s['status'], Sort.ASCENDING),
             (t2s['priority'], Sort.ASCENDING)])

        # Create tags multi-key index
        if self._tags:
            flask_collection.create_index([(t2s['tags'], Sort.ASCENDING)])

        return Result.OK
