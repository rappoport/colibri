"""API for colibri stat command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    stat: colibri stat functional API

Classes:
    StatCommand: colibri stat command API
"""

__all__ = ['stat', 'StatCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/16/2015'

#
# Standard library
#

import sys
import socket
import string

#
# Third-party modules
#

import pymongo
try:
    from pymemcache import client as pymemcache_client
except:
    pymemcache_client = None

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.enums import Level, Status, Result, Sort
from colibri.exceptions import CommandError
from colibri.utils import t2s

#
# Public functions
#


def stat(**kwargs):
    return StatCommand(**kwargs).execute()

#
# Classes
#


class StatCommand(Command):
    """colibri stat command API."""

    def __init__(self, output_file=None, opts=None):
        """StatCommand class constructor.

        Args:
            output_file (str, optional): File for statistics output
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exeption in command execution
        """

        # Output file
        self._output_file = output_file
        if output_file is not None:
            self._output = open(output_file, 'w')
        else:
            self._output = sys.stdout

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(output_file=%r, opts=%r)' % (self._output_file, self._opts))

    def execute(self):
        """Execute colib stat command.

        Returns:
            Result: Execution result
        """

        # Connect to the MongoDB database using pymongo
        mongoclient = pymongo.MongoClient(
            host=self._opts['db_hostname'], port=self._opts['db_port'])

        # Connect to molecule and flask collection
        has_storage = True
        try:
            mol_collection = mongoclient[
                self._opts['mol_database']][self._opts['mol_collection']]
            flask_collection = mongoclient[
                self._opts['flask_database']][self._opts['flask_collection']]
        except pymongo.errors.ConnectionFailure:
            has_storage = False

        # Cache stats
        has_cache = True

        # Connect to Memcached cache using pymemcache
        if pymemcache_client is not None:
            cache_client = pymemcache_client.Client(
                (self._opts['cache_hostname'], self._opts['cache_port']))
            try:
                cache_stats = cache_client.stats()
            except socket.error:
                has_cache = False
        else:
            has_cache = False

        # Tags
        tags = self._opts.get('tags', [])

        # Output dictionary
        output = {}

        # Storage parameters
        if has_storage:
            output['shost'] = self._opts['db_hostname']
            output['sport'] = '%-6d' % self._opts['db_port']
            output['sversion'] = str(mongoclient.server_info()['version'])
            output['stags'] = tags
            output['mdb'] = self._opts['mol_database']
            output['mcoll'] = self._opts['mol_collection']
            output['fdb'] = self._opts['flask_database']
            output['fcoll'] = self._opts['flask_collection']
        else:
            output['shost'] = '** DOWN **'
            output['sport'] = '** DOWN **'
            output['sversion'] = '** DOWN **'
            output['stags'] = ''
            output['mdb'] = ''
            output['mcoll'] = ''
            output['fdb'] = ''
            output['fcoll'] = ''

        # Cache parameters
        if has_cache:
            output['chost'] = self._opts['cache_hostname']
            output['cport'] = '%-6d' % self._opts['cache_port']
            output['cversion'] = cache_stats['version']
        else:
            output['chost'] = '** DOWN **'
            output['cport'] = '** DOWN **'
            output['cversion'] = '** DOWN **'

        # Check molecule documents
        for level, num in sorted(
                [(l, getattr(Level, l)) for l in dir(Level)
                 if l[0] in string.uppercase], key=lambda t: t[1]):
            if not isinstance(num, int):
                continue
            key = 'l%d' % num
            output[key] = ''
            if has_storage:
                query_doc = {t2s['level']: num}
                if tags:
                    query_doc[t2s['tags']] = {'$all': tags}
                try:
                    count = mol_collection.find(query_doc).count()
                except KeyboardInterrupt as e:
                    raise SystemExit(e)
                if count:
                    output[key] = '%-6d' % count

        # Max generation
        if has_storage:
            if tags:
                try:
                    gen_cursor = mol_collection.find(
                        {t2s['tags']: {'$all': tags}},
                        {t2s['generation']: True},
                        sort=[(t2s['generation'], Sort.DESCENDING)],
                        limit=1)
                except KeyboardInterrupt as e:
                    raise SystemExit(e)
            else:
                try:
                    gen_cursor = mol_collection.find(
                        {}, {t2s['generation']: True, t2s['key']: False},
                        sort=[(t2s['generation'], Sort.DESCENDING)],
                        limit=1)
                except KeyboardInterrupt as e:
                    raise SystemExit(e)
            try:
                gen = list(gen_cursor)[0][t2s['generation']]
                output['mg'] = '%-6d' % gen
            except (IndexError, KeyError):
                output['mg'] = ''
        else:
            output['mg'] = ''

        # Check flask documents
        for status, num in sorted(
                [(s, getattr(Status, s)) for s in dir(Status)
                 if s[0] in string.uppercase], key=lambda t: t[1]):
            if not isinstance(num, int):
                continue
            key = 's%d' % num
            output[key] = ''
            if has_storage:
                query_doc = {t2s['status']: num}
                if tags:
                    query_doc[t2s['tags']] = {'$all': tags}
                try:
                    count = flask_collection.find(query_doc).count()
                except KeyboardInterrupt as e:
                    raise SystemExit(e)
                if count:
                    output[key] = '%-6d' % count

        # Max generation
        if has_storage:
            if tags:
                try:
                    gen_cursor = flask_collection.find(
                        {t2s['tags']: {'$all': tags}},
                        {t2s['generation']: True},
                        sort=[(t2s['generation'], Sort.DESCENDING)],
                        limit=1)
                except KeyboardInterrupt as e:
                    raise SystemExit(e)
            else:
                try:
                    gen_cursor = flask_collection.find(
                        {}, {t2s['generation']: True, t2s['key']: False},
                        sort=[(t2s['generation'], Sort.DESCENDING)],
                        limit=1)
                except KeyboardInterrupt as e:
                    raise SystemExit(e)
            try:
                gen = list(gen_cursor)[0][t2s['generation']]
                output['fg'] = '%-6d' % gen
            except (IndexError, KeyError):
                output['fg'] = ''
        else:
            output['fg'] = ''

        # Cache statistics
        for key in ['total_items', 'curr_items', 'get_hits',
                    'get_misses', 'cmd_set']:
            output[key] = ''
        if has_cache:
            for key in ['total_items', 'curr_items', 'get_hits',
                        'get_misses', 'cmd_set']:
                count = cache_stats[key]
                if count:
                    output[key] = '%-8d' % count

        # Output template
        template = '''\
STORAGE MongoDB HOST {shost:s} PORT {sport:s} VERSION {sversion:s}
========================================================
M  FORMULA................. {l1000:<8s}  F  INITIAL................. {s2000:<8s}
O  FORMULA_LOCKED.......... {l1001:<8s}  L  INITIAL_LOCKED.......... {s2001:<8s}
L  FORMULA_SUSPENDED....... {l1002:<8s}  A  INITIAL_SUSPENDED....... {s2002:<8s}
E  FORMULA_UNREACTIVE...... {l1003:<8s}  S  INITIAL_ERROR........... {s2090:<8s}
C  FORMULA_ERROR........... {l1090:<8s}  K  RUNNING................. {s2100:<8s}
U  FORMULA_INVALID......... {l1091:<8s}  S  RUNNING_LOCKED.......... {s2101:<8s}
L  CONFIGURATION........... {l1100:<8s}     RUNNING_SUSPENDED....... {s2102:<8s}
E  CONFIGURATION_LOCKED.... {l1101:<8s}     REACTIVE................ {s2200:<8s}
S  CONFIGURATION_SUSPENDED. {l1102:<8s}     REACTIVE_LOCKED......... {s2201:<8s}
   CONFIGURATION_NUDGE..... {l1110:<8s}     REACTIVE_SUSPENDED...... {s2202:<8s}
   CONFIGURATION_ERROR..... {l1190:<8s}     REACTIVE_HIENERGY....... {s2210:<8s}
   CONFIGURATION_INVALID... {l1191:<8s}     REACTIVE_HIBARRIER.......{s2211:<8s}
   GEOMETRY................ {l1200:<8s}     REACTIVE_MAXGEN......... {s2212:<8s}
   GEOMETRY_LOCKED......... {l1201:<8s}     REACTIVE_BADCHEM........ {s2213:<8s}
   GEOMETRY_SUSPENDED...... {l1202:<8s}     UNREACTIVE.............. {s2300:<8s}
   GEOMETRY_ERROR.......... {l1290:<8s}     UNAVAILABLE............. {s2310:<8s}
   GEOMETRY_INVALID........ {l1291:<8s}     ERROR................... {s2390:<8s}
   PROPERTY................ {l1300:<8s}     INVALID................. {s2391:<8s}
   PROPERTY_LOCKED......... {l1301:<8s}
   PROPERTY_SUSPENDED...... {l1302:<8s}
   PROPERTY_ERROR.......... {l1390:<8s}
   PROPERTY_INVALID........ {l1391:<8s}

   MAX GENERATION.......... {mg:<8s}     MAX GENERATION.......... {fg:<8s}

   DATABASE   {mdb:<24s}   DATABASE   {fdb:<24s}
   COLLECTION {mcoll:<24s}   COLLECTION {fcoll:<24s}
   TAGS {stags:<60s}

CACHE Memcached HOST {chost:s} PORT {cport:s} VERSION {cversion:s}
=========================================================
M  TOTAL ITEMS............. {total_items:<8s}     CURRENT ITEMS........... {curr_items:<8s}
O  GET HITS................ {get_hits:<8s}     SET COMMANDS............ {cmd_set:<8s}
L  GET MISSES.............. {get_misses:<8s}
'''

        # Format and print output
        self._output.write(template.format(**output))

        return Result.OK
