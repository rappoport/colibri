"""API for colibri graph command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    graph: colibri graph functional API

Classes:
    GraphCommand: colibri graph command API
"""

__all__ = ['graph', 'GraphCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/17/2015'


#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.data import Flask
from colibri.mongostorage import MongoClient
from colibri.neo4jstorage import Neo4JClient
from colibri.enums import Status, Result, Sort
from colibri.exceptions import MoleculeError, FlaskError, CommandError
from colibri.utils import t2s, ensure_logger


#
# Public functions
#


def graph(**kwargs):
    """colibri graph functional API

    Args:
        **kwargs: See GraphCommand constructor

    Returns:
        Result: Execution result
    """
    return GraphCommand(**kwargs).execute()

#
# Classes
#


class GraphCommand(Command):
    """colibri graph command API."""

    def __init__(self, labels=None, keys=None, opts=None):
        """GraphCommand class constructor.

        Args:
            label (str/list, optional): Label to attach to graph nodes
            keys (str, optional): Regex for flask keys to retrieve
            opts (Options): Command options

        Raises:
            CommandError: Exception in command execution
        """

        # Label to attach to graph nodes upon export
        self._labels = labels

        # Regex for flask keys to retrieve (for sharding)
        self._keys = keys

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(labels=%r, keys=%r, opts=%r)' % (
                self._labels, self._keys, self._opts))

    def execute(self):
        """Execute colibri graph command.

        Returns:
            Result: Execution result
        """

        # Get logger
        logger = ensure_logger('graphcommand', self._opts['logging_level'])

        # Connect to MongoDB database
        mongoclient = MongoClient(
            host=self._opts['db_hostname'], port=self._opts['db_port'])

        # MongoDB flask storage
        flask_storage = mongoclient.storage(
            db=self._opts['flask_database'],
            collection=self._opts['flask_collection'])

        # Min and max generations
        min_generation = self._opts.get('min_generation', None)
        max_generation = self._opts.get('max_generation', None)

        # Maximum energy change
        max_energy_change = self._opts.get('max_energy_change', None)

        # Maximum energy above reference
        max_rel_energy = self._opts.get('max_rel_energy', None)

        # Reference flask
        ref_flask = self._opts.get('ref_flask', None)

        # Reference energy
        ref_energy = self._opts.get('ref_energy', None)

        # Get reference flask
        if max_rel_energy is not None:

            # Reference energy not available, retrieve reference flask
            if ref_energy is None:

                # Construct MongoDB query for reference flask
                ref_query = {}

                # Tag selection
                tags_select = self._opts.get('tags', [])
                if tags_select:
                    if isinstance(tags_select, basestring):
                        ref_query[t2s['tags']] = tags_select
                    else:
                        ref_query[t2s['tags']] = {'$all': tags_select}

                # Reference flask key
                if ref_flask is not None:

                    # Interpret as SMILES string
                    try:
                        ref_query[t2s['key']] = Flask(smiles=ref_flask).key
                    except (MoleculeError, FlaskError):
                        # Interpret as flask key
                        ref_query[t2s['key']] = ref_flask

                    # No sorting necessary
                    ref_sort = None

                else:

                    # Get lowest-energy flask of generation 0
                    ref_query[t2s['generation']] = 0

                    # Sort by energy
                    ref_sort = [[t2s['energy'], Sort.ASCENDING]]

                # MongoDB query request
                ref_request = mongoclient.query_request(
                    query=ref_query, sort=ref_sort)

                # Get reference flask data
                try:
                    ref_data = next(flask_storage.find(ref_request))
                except StopIteration:
                    return Result.COMMAND_ERROR

                # Reference flask energy
                try:
                    ref_energy = ref_data['e']
                except KeyError:
                    return Result.COMMAND_ERROR

        # Connect to Neo4J database
        neo4jclient = Neo4JClient(
            host=self._opts['graph_hostname'], port=self._opts['graph_port'])

        # Neo4J Graph storage
        graph_storage = neo4jclient.graph_storage()

        # Add constraints and indexes on the Flask label
        graph_storage.run(
            'CREATE CONSTRAINT ON (f:Flask) ASSERT f.key IS UNIQUE;')
        graph_storage.run('CREATE INDEX ON :Flask(smiles);')
        graph_storage.run('CREATE INDEX ON :Flask(generation);')

        # Flask count
        count = 0
        prev_count = 0

        # Flask label
        flask_label = ':Flask'
        if self._labels:
            if isinstance(self._labels, str):
                flask_label += (':' + self._labels)
            else:
                flask_label += (':' + ':'.join(self._labels))

        # Transformation type
        trans_type = ':Transformation'

        # Construct MongoDB query
        query = {}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                query[t2s['tags']] = tags_select
            else:
                query[t2s['tags']] = {'$all': tags_select}

        # Regex for flask keys (for sharding), used as prefix
        if self._keys:
            query[t2s['key']] = {'$regex': r'^' + self._keys}

        # MongoDB query request
        request = mongoclient.query_request(query=query)

        # Start Cypher transaction
        tx = graph_storage.begin()
        logger.debug('Cypher transaction started')

        try:

            # Get all flasks
            cursor = flask_storage.find(request, no_cursor_timeout=True)
            for data in cursor:

                # Flask status
                status = data['j']

                # Skip if flask does not have energy
                if status not in Status.HAS_ENERGY:
                    continue

                if not data['e']:
                    continue

                # Flask key
                key = data['_id']

                # Generation
                generation = data['3']

                # Min generation
                if min_generation is not None and generation < min_generation:
                    continue

                # Max generation
                if max_generation is not None and generation > max_generation:
                    continue

                # Max energy change
                if (max_energy_change is not None and
                        data['9'] > max_energy_change):
                    continue

                # Max relative energy
                if (max_rel_energy is not None and
                        (data['e'] - ref_energy) > max_rel_energy):
                    continue

                # Tag selection
                for tag in tags_select:
                    if tag not in data['t']:
                            continue

                # Flask properties
                flask_properties = (
                    ('{key: "%s", smiles: "%s", ' +
                     'generation: %d, energy: %s, status: %d}') % (
                        key, data['s'], generation, data['e'], status))

                # Cypher MERGE command for the flask
                cmd = ('MERGE (f:Flask {key: "%s"}) SET f = %s, f %s;') % (
                    key, flask_properties, flask_label)
                logger.debug('Cypher command issued: %s' % cmd)
                tx.evaluate(cmd)

                # Incoming transformations
                for trans in data['b']:

                    # Cypher MERGE command for reactant flask
                    reactant = trans['r']
                    cmd = ('MERGE (r:Flask {key: "%s"}) ' +
                           'SET r %s;') % (reactant, flask_label)
                    logger.debug('Cypher command issued: %s' % cmd)
                    tx.run(cmd)

                    # Transformation properties
                    trans_properties = '{rule: "%s"}' % (trans['1'])

                    # Cypher MERGE command for the incoming transformation
                    cmd = ('MATCH (r%s {key: "%s"}), (p%s {key: "%s"}) ' +
                           'MERGE (r)-[t%s %s]->(p);') % (
                        flask_label, reactant, flask_label, key,
                        trans_type, trans_properties)
                    logger.debug('Cypher command issued: %s' % cmd)
                    tx.run(cmd)

                # Outgoing transformations
                for trans in data['a']:

                    # Cypher MERGE command for product flask
                    product = trans['p']
                    cmd = ('MERGE (p:Flask {key: "%s"}) ' +
                           'SET p %s;') % (product, flask_label)
                    logger.debug('Cypher command issued: %s' % cmd)
                    tx.run(cmd)

                    # Transformation properties
                    trans_properties = '{rule: "%s"}' % (trans['1'])

                    # Cypher MERGE command for the outgoing transformation
                    cmd = ('MATCH (r%s {key: "%s"}), (p%s {key: "%s"}) ' +
                           'MERGE (r)-[t%s %s]->(p);') % (
                        flask_label, key, flask_label, product,
                        trans_type, trans_properties)
                    logger.debug('Cypher command issued: %s' % cmd)
                    tx.run(cmd)

                # Increase flask count
                count += 1

                # Commit transations and start new transaction
                if count > prev_count and count % 10 == 0:
                    tx.commit()
                    logger.debug('Cypher transaction committed')
                    tx = graph_storage.begin()
                    prev_count = count

                # Log output
                if count > prev_count and count % 100 == 0:
                    logger.info('Flasks written: %d' % count)

        finally:

            try:
                # Commit last transaction
                tx.commit()
                logger.debug('Cypher transaction finished')
            except Exception as e:
                logger.warning('Cypher transaction failed: %s' % e)

            # Delete all flasks not having a SMILES string (should use index)
            try:
                graph_storage.run(
                    ('MATCH (f%s) WHERE NOT EXISTS (f.smiles) ' +
                     'DETACH DELETE f;') % (flask_label))
            except Exception as e:
                logger.warning('Removal of empty nodes failed: %s' % e)

            # Log output
            logger.info('Total flasks written: %d' % count)

            # Close cursor
            cursor.close()

        return Result.OK
