"""API for colibri run command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    run: colibri run functional API

Classes:
    RunCommand: colibri run command API
"""

__all__ = ['run', 'RunCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/16/2015'

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.calc import Dispatcher
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.task import task_names
from colibri.utils import randomid

#
# Public functions
#


def run(**kwargs):
    return RunCommand(**kwargs).execute()

#
# Classes
#


class RunCommand(Command):
    """colibri run command API."""

    def __init__(self, openbabel_build=0, rdkit_build=0, mopac_geometry=0,
                 mopac_nudge_geometry=0, orca_geometry=0, orca_property=0,
                 flask_mapper=0, flask_reducer=0, rdkit_react=0,
                 rdkit_flask_react=0, mol_cleanup=0, flask_cleanup=0,
                 react_smarts=None, react_json=None, opts=None):

        """RunCommand class constructor.

        Args:
            openbabel_build (int, optional): OpenBabelBuildTask calculators
            rdkit_build (int, optional): RDKitBuildTask calculators
            mopac_geometry (int, optional): MOPACGeometryTask calculators
            mopac_nudge_geometry (int, optional): MOPACNudgeGeometryTask calcs
            orca_geometry (int, optional): OrcaGeometryTask calculators
            orca_property (int, optional): OrcaPropertyTask calculators
            flask_mapper (int, optional): FlaskMapperTask calculators
            flask_reducer (int, optional): FlaskReducerTask calculators
            rdkit_react (int, optional): RDKitReactionTask calculators
            rdkit_flask_react (int, optional): RDKitFlaskReactionTask calcs
            mol_cleanup (int, optional): CleanupTask calculators
            flask_cleanup (int, optional): FlaskCleanupTask calculators
            react_smarts (list, optional): Reaction SMARTS strings
            react_json (list, optional): Reaction JSON strings
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Task options
        task_kv = {}

        # OpenBabelBuildTask
        for _ in xrange(openbabel_build):
            task_name = 'openbabel_build_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['openbabel_build']}
            task_kv[task_name] = task

        # RDKitBuildTask
        for _ in xrange(rdkit_build):
            task_name = 'rdkit_build_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['rdkit_build']}
            task_kv[task_name] = task

        # MOPACGeometryTask
        for _ in xrange(mopac_geometry):
            task_name = 'mopac_geometry_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['mopac_geometry']}
            task_kv[task_name] = task

        # MOPACNudgeGeometryTask
        for _ in xrange(mopac_nudge_geometry):
            task_name = 'mopac_nudge_geometry_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['mopac_nudge_geometry']}
            task_kv[task_name] = task

        # OrcaGeometryTask
        for _ in xrange(orca_geometry):
            task_name = 'orca_geometry_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['orca_geometry']}
            task_kv[task_name] = task

        # OrcaPropertyTask
        for _ in xrange(orca_property):
            task_name = 'orca_property_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['orca_property']}
            task_kv[task_name] = task

        # FlaskMapperTask
        for _ in xrange(flask_mapper):
            task_name = 'flask_mapper_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['flask_mapper']}
            task_kv[task_name] = task

        # FlaskReducerTask
        for _ in xrange(flask_reducer):
            task_name = 'flask_reducer_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['flask_reducer']}
            task_kv[task_name] = task

        # RDKitReactionTask
        for _ in xrange(rdkit_react):
            task_name = 'rdkit_react_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['rdkit_react']}
            task_kv[task_name] = task

        # RDKitFlaskReactionTask
        for _ in xrange(rdkit_flask_react):
            task_name = 'rdkit_flask_react_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['rdkit_flask_react']}
            task_kv[task_name] = task

        # CleanupTask
        for _ in xrange(mol_cleanup):
            task_name = 'mol_cleanup_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['mol_cleanup']}
            task_kv[task_name] = task

        # FlaskCleanupTask
        for _ in xrange(flask_cleanup):
            task_name = 'flask_cleanup_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['flask_cleanup']}
            task_kv[task_name] = task

        # Raise CommandError if no tasks given
        if not task_kv:
            raise CommandError('Nothing to run')

        # Add reaction rules to dictionary
        rule_kv = {}
        if react_smarts:
            for rule in react_smarts:
                key = 'react_rule_' + randomid(pos=6)
                rule_kv[key] = {
                    'key': key,
                    'group': 'react_rule',
                    'value': rule,
                    'precedence': 'command'}

        # DEBUG
        # Need to add extra option for reaction rule as JSON string
        # DEBUG

        # Read reaction rules from list or stream, one rule per line
        if react_json:
            for rule in react_json:
                key = 'react_rule_' + randomid(pos=6)
                rule_kv[key] = {
                    'key': key,
                    'group': 'react_rule',
                    'value': rule,
                    'precedence': 'command'}

        # Additional input options
        input_kv = {}

        # Add task options
        input_kv.update(task_kv)

        # Add reaction rule options
        input_kv.update(rule_kv)

        # Construct Options object for arguments
        self._args = Options(precedence='command', **input_kv)

        # Configuration options
        if opts is None:
            raise CommandError('No input options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """

        # OpenBabelBuildTask
        openbabel_build = len([t for t in self._args.list('task')
                               if t.value == 'OpenBabelBuildTask'])

        # RDKitBuildTask
        rdkit_build = len([t for t in self._args.list('task')
                           if t.value == 'RDKitBuildTask'])

        # MOPACGeometryTask
        mopac_geometry = len([t for t in self._args.list('task')
                              if t.value == 'MOPACGeometryTask'])

        # MOPACNudgeGeometryTask
        mopac_nudge_geometry = len([t for t in self._args.list('task')
                                    if t.value == 'MOPACNudgeGeometryTask'])

        # OrcaGeometryTask
        orca_geometry = len([t for t in self._args.list('task')
                             if t.value == 'OrcaGeometryTask'])

        # OrcaPropertyTask
        orca_property = len([t for t in self._args.list('task')
                             if t.value == 'OrcaPropertyTask'])

        # FlaskMapperTask
        flask_mapper = len([t for t in self._args.list('task')
                            if t.value == 'FlaskMapperTask'])

        # FlaskReducerTask
        flask_reducer = len([t for t in self._args.list('task')
                             if t.value == 'FlaskReducerTask'])

        # RDKitReactionTask
        rdkit_react = len([t for t in self._args.list('task')
                           if t.value == 'RDKitReactionTask'])

        # RDKitFlaskReactionTask
        rdkit_flask_react = len([t for t in self._args.list('task')
                                 if t.value == 'RDKitFlaskReactionTask'])

        # CleanupTask
        mol_cleanup = len([t for t in self._args.list('task')
                          if t.value == 'CleanupTask'])

        # FlaskCleanupTask
        flask_cleanup = len([t for t in self._args.list('task')
                             if t.value == 'FlaskCleanupTask'])

        # Reaction rules as SMARTS strings
        if self._args.get('react_rule'):
            react_smarts = [r.value for r in self._args.list('react_rule')]
        else:
            react_smarts = []

        # Reaction rules as JSON strings
        react_json = []

        return type(self).__name__ + (
            ('(openbabel_build=%r, rdkit_build=%r, mopac_geometry=%r, ' +
             'mopac_nudge_geometry=%r, orca_geometry=%r, orca_property=%r, ' +
             'flask_mapper=%r, flask_reducer=%r, rdkit_react=%r, ' +
             'rdkit_flask_react=%r, mol_cleanup=%r, flask_cleanup=%r, ' +
             'react_smarts=%r, react_json=%r, opts=%r)') % (
                openbabel_build, rdkit_build, mopac_geometry,
                mopac_nudge_geometry, orca_geometry, orca_property,
                flask_mapper, flask_reducer, rdkit_react, rdkit_flask_react,
                mol_cleanup, flask_cleanup, react_smarts, react_json,
                self._opts))

    def execute(self):
        """Execute colibri run command.

        Returns:
            Result: Execution result
        """

        # Total options object
        opts = self._opts.update(self._args)

        # Create Dispatcher instance
        disp = Dispatcher(opts)

        # Start Dispatcher instance
        return disp.run()
