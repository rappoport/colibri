"""API for colibri resume command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    resume: colibri resume functional API

Classes:
    ResumeCommand: colibri resume command API
"""

__all__ = ['resume', 'ResumeCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/17/2015'

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.calc import Calculator
from colibri.enums import Result
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.task import task_names
from colibri.utils import randomid

#
# Public functions
#


def resume(**kwargs):
    return ResumeCommand(**kwargs).execute()

#
# Classes
#


class ResumeCommand(Command):
    """colibri resume command API."""

    def __init__(self, molecules=False, flasks=False, opts=None):
        """ResumeCommand class constructor.

        Args:
            molecules (bool, optional): Resume molecules
            flasks (bool, optional): Resume flasks
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Perform ResumeTask
        self._molecules = molecules

        # Perform FlaskResumeTask
        self._flasks = flasks

        # Raise CommandError if there is nothing to do
        if not self._molecules and not self._flasks:
            raise CommandError('Nothing to do')

        # Resume molecules
        if self._molecules:

            # Additional input options to ResumeTask
            input_kv = {}

            # Add ResumeTask option
            task_name = 'mol_resume_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['mol_resume']}
            input_kv[task_name] = task

            # Add tasks_per_calc option
            input_kv['tasks_per_calc'] = 1

            # Add resume_interval options
            input_kv['resume_interval'] = 0

            # Construct Options object for ResumeTask options
            self._molecule_opts = Options(precedence='command', **input_kv)

        # Resume flasks
        if self._flasks:

            # Additional input options to FlaskResumeTask
            input_kv = {}

            # Add FlaskResumeTask option
            task_name = 'flask_resume_' + randomid(pos=6)
            task = {
                'key': task_name, 'group': 'task',
                'value': task_names['flask_resume']}
            input_kv[task_name] = task

            # Add tasks_per_calc option
            input_kv['tasks_per_calc'] = 1

            # Add resume_interval options
            input_kv['resume_interval'] = 0

            # Construct Options object for FlaskResumeTask options
            self._flask_opts = Options(precedence='command', **input_kv)

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(molecules=%r, flasks=%r, opts=%r)' % (
                self._molecules, self._flasks, self._opts))

    def execute(self):
        """Execute colib resume command.

        Returns:
            Result: Execution result
        """

        # Result variable
        result = Result.OK

        # ResumeTask instance
        if self._molecules:

            # Create combined Options object
            opts = self._opts.update(self._molecule_opts)

            # Start Calculator instance
            mol_calc = Calculator(opts)
            result |= mol_calc.run()

        # FlaskResumeTask instance
        if self._flasks:

            # Create combined Options object
            opts = self._opts.update(self._flask_opts)

            # Start Calculator instance
            flask_calc = Calculator(opts)
            result |= flask_calc.run()

        # Rsturn value
        return result
