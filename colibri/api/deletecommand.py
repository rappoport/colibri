"""API for colibri delete command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    delete: colibri delete functional API

Classes:
    DeleteCommand: colibri delete command API
"""

__all__ = ['delete', 'DeleteCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/02/2015'

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.data import Molecule, Flask
from colibri.enums import Level, Status, Result
from colibri.exceptions import MoleculeError, FlaskError, CommandError, \
    ConnectionError
from colibri.mongostorage import MongoClient
from colibri.utils import t2s, ensure_logger

#
# Public functions
#


def delete(**kwargs):
    """colibri delete functional API.

    Args:
        **kwargs: See DeleteCommand constructor

    Returns:
        Result: Execution result
    """
    return DeleteCommand(**kwargs).execute()

#
# Classes
#


class DeleteCommand(Command):
    """colibri delete command API."""

    def __init__(self, molecules=False, molecule_ids=None, flasks=False,
                 flask_ids=None, opts=None):
        """DeleteCommand class constructor.

        Args:

            molecules (bool, optional): Delete molecules
            molecule_ids (list, optional): List of molecule SMILES/keys
            flasks (bool, optional): Delete flasks
            flask_ids (list, optional): List of flask SMILES/keys
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Delete molecules
        self._molecules = molecules

        # List of molecule SMILES/keys
        self._molecule_ids = molecule_ids

        # Delete flasks
        self._flasks = flasks

        # List of flask SMILES/keys
        self._flask_ids = flask_ids

        # Raise CommandError nothing to do
        if not self._molecules and not self._flasks:
            raise CommandError('Nothing to do')

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            ('(molecules=%s, molecule_ids=%r, flasks=%s, ' +
             'flask_ids=%r, opts=%r)') % (
                self._molecules, self._molecule_ids, self._flasks,
                self._flask_ids, self._opts))

    def execute(self):
        """Execute colibri delete command.

        Returns:
            Result: Execution result
        """

        # Create logger if necessary
        logger = ensure_logger('deletecommand', self._opts['logging_level'])

        # Connect to MongoDB database
        if self._molecules or self._flasks:
            try:
                mongoclient = MongoClient(
                    host=self._opts['db_hostname'],
                    port=self._opts['db_port'])
            except ConnectionError:
                return Result.COMMAND_ERROR

        # Delete molecules
        if self._molecules:

            try:

                mol_storage = mongoclient.storage(
                    db=self._opts['mol_database'],
                    collection=self._opts['mol_collection'])

                # Prepare delete query parameters
                delete_query = {}

                # Use delete multi command for level, generation, tags
                # Select levels to delete
                level_select = self._opts.get('level', [])
                if isinstance(level_select, basestring):
                    level_select = [level_select]
                if level_select:
                    delete_query[t2s['level']] = {
                        '$in': [getattr(Level, l) for l in level_select]}

                # Max and min generation to delete
                max_generation = self._opts.get('max_generation', None)
                min_generation = self._opts.get('min_generation', None)
                if max_generation is not None or min_generation is not None:
                    delete_query[t2s['generation']] = {}
                    if max_generation is not None:
                        delete_query[t2s['generation']][
                            '$lte'] = max_generation
                    if min_generation is not None:
                        delete_query[t2s['generation']][
                            '$gte'] = min_generation

                # Tag selection
                tags_select = self._opts.get('tags', [])
                if tags_select:
                    if isinstance(tags_select, basestring):
                        delete_query[t2s['tags']] = tags_select
                    else:
                        delete_query[t2s['tags']] = {'$all': tags_select}

                # Multi delete request
                if delete_query:
                    delete_request = mongoclient.query_request(
                        query=delete_query)
                    mol_storage.delete([delete_request], multi=True)

                # Molecule SMILES/keys
                if self._molecule_ids:
                    requests = []
                    for data in self._molecule_ids:
                        # Interpret as SMILES string
                        try:
                            query = {t2s['key']: Molecule(smiles=data).key}
                        # Interpret as database key
                        except MoleculeError:
                            query = {t2s['key']: data}
                        requests.append(mongoclient.query_request(query=query))

                    # Issue delete command for individual molecule records
                    if requests:
                        mol_storage.delete(requests)

            except ConnectionError:
                return Result.COMMAND_ERROR

            # Logging output
            logger.info('Deleted molecules')

        # Delete flasks
        if self._flasks:

            try:

                flask_storage = mongoclient.storage(
                    db=self._opts['flask_database'],
                    collection=self._opts['flask_collection'])

                # Prepare delete query parameters
                delete_query = {}

                # Use delete multi command for status, generation, tags
                # Select status to delete
                status_select = self._opts.get('status', [])
                if isinstance(status_select, basestring):
                    status_select = [status_select]
                if status_select:
                    delete_query[t2s['status']] = {
                        '$in': [getattr(Status, s) for s in status_select]}

                # Max and min generation to delete
                max_generation = self._opts.get('max_generation', None)
                min_generation = self._opts.get('min_generation', None)
                if max_generation is not None or min_generation is not None:
                    delete_query[t2s['generation']] = {}
                    if max_generation is not None:
                        delete_query[t2s['generation']][
                            '$lte'] = max_generation
                    if min_generation is not None:
                        delete_query[t2s['generation']][
                            '$gte'] = min_generation

                # Tag selection
                tags_select = self._opts.get('tags', [])
                if tags_select:
                    if isinstance(tags_select, basestring):
                        delete_query[t2s['tags']] = tags_select
                    else:
                        delete_query[t2s['tags']] = {'$all': tags_select}

                # Multi delete request
                if delete_query:
                    delete_request = mongoclient.query_request(
                        query=delete_query)
                    flask_storage.delete([delete_request], multi=True)

                # Flask SMILES/keys
                if self._flask_ids:
                    requests = []
                    for data in self._flask_ids:
                        # Interpret as SMILES string
                        try:
                            query = {t2s['key']: Flask(smiles=data).key}
                        # Interpet as database key
                        except (MoleculeError, FlaskError):
                            query = {t2s['key']: data}
                        requests.append(mongoclient.query_request(query=query))

                    # Issue delete command for individual flask records
                    if requests:
                        flask_storage.delete(requests)

            except ConnectionError:
                return Result.COMMAND_ERROR

            # Logging output
            logger.info('Deleted flasks')

        return Result.OK
