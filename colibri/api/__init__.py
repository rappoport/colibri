"""API commands for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Attributes:
    cmd_names (OrderedDict): Translation subcommands to command class names

Functions:
    init: colibri init functional API
    add: colibri add functional API
    fetch: colibri fetch functional API
    delete: colibri delete functional API
    run: colibri run functional API
    stat: colibri stat functional API
    suspend: colibri suspend functional API
    resume: colibri resume funcitonal API
    load: colibri load functional API
    dump: colibri dump functional API
    graph: colibri graph functional API
    clean: colibri clean functional API
    config: colibri config functional API

Classes:
    Command:  Base class for colibri API commands
    InitCommand: colibri init command API
    AddCommand: colibri add command API
    FetchCommand: colibri fetch command API
    DeleteCommand: colibri delete command API
    RunCommand: colibri run command API
    StatCommand: colibri stat command API
    SuspendCommand: colibri suspend command API
    ResumeCommand: colibri resume command API
    LoadCommand: colibri load command API
    DumpCommand: colibri dump command API
    GraphCommand: colibri graph command API
    CleanCommand: colibri clean command API
    ConfigCommand: colibri config command API
"""

__all__ = ['cmd_names', 'Command',
           'init', 'add', 'fetch', 'delete', 'run', 'stat', 'suspend',
           'resume', 'load',  'dump', 'graph', 'clean', 'config',
           'InitCommand', 'AddCommand', 'FetchCommand', 'DeleteCommand',
           'RunCommand', 'StatCommand', 'SuspendCommand', 'ResumeCommand',
           'LoadCommand', 'DumpCommand', 'GraphCommand', 'CleanCommand',
           'ConfigCommand']

__module__ = 'colibri.api'
__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/11/2015'

#
# Library modules
#

from collections import OrderedDict

#
# Variables

# Translation table from command names to command class names
cmd_names = OrderedDict([
    ('init', 'InitCommand'),
    ('add', 'AddCommand'),
    ('fetch', 'FetchCommand'),
    ('delete', 'DeleteCommand'),
    ('run', 'RunCommand'),
    ('stat', 'StatCommand'),
    ('suspend', 'SuspendCommand'),
    ('resume', 'ResumeCommand'),
    ('load', 'LoadCommand'),
    ('dump', 'DumpCommand'),
    ('graph', 'GraphCommand'),
    ('clean', 'CleanCommand'),
    ('config', 'ConfigCommand')])

#
#
# Classes
#

# Base command
from colibri.api.basecommand import Command

# colibri init command API
from colibri.api.initcommand import init, InitCommand

# colibri add command API
from colibri.api.addcommand import add, AddCommand

# colibri fetch command API
from colibri.api.fetchcommand import fetch, FetchCommand

# colibri delete command API
from colibri.api.deletecommand import delete, DeleteCommand

# colibri run command API
from colibri.api.runcommand import run, RunCommand

# colibri stat command API
from colibri.api.statcommand import stat, StatCommand

# colibri suspend command API
from colibri.api.suspendcommand import suspend, SuspendCommand

# colibri resume command API
from colibri.api.resumecommand import resume, ResumeCommand

# colibri load command API
from colibri.api.loadcommand import load, LoadCommand

# colibri dump command API
from colibri.api.dumpcommand import dump, DumpCommand

# colibri graph command API
from colibri.api.graphcommand import graph, GraphCommand

# colibri clean command API
from colibri.api.cleancommand import clean, CleanCommand

# colibri config command API
from colibri.api.configcommand import config, ConfigCommand
