"""API for colibri clean command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    clean: colibri clean functional API

Classes:
    CleanCommand: colibri clean command API
"""

__all__ = ['clean', 'CleanCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/17/2015'

#
# Library modules
#

import os
import subprocess

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.enums import Result
from colibri.exceptions import ConnectionError, ExecutionError, CommandError
from colibri.mongostorage import MongoClient
from colibri.memcachedcache import MemcachedClient
from colibri.utils import ensure_logger


#
# Public functions
#


def clean(**kwargs):
    """colibri clean functional API.

    Args:
        **kwargs: See CleanCommand constructor

    Returns:
        Result: Execution result
    """
    return CleanCommand(**kwargs).execute()

#
# Classes
#


class CleanCommand(Command):
    """colibri clean command API."""

    def __init__(self, molecules=False, flasks=False, cache=False,
                 opts=None):
        """CleanCommand class constructor.

        Args:
            molecules (bool, optional): Clean molecules
            flasks (bool, optional): Clean flasks
            cache (bool, optional): Clean cache
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Clean molecule collection
        self._molecules = molecules

        # Clean flask collection
        self._flasks = flasks

        # Clean cache
        self._cache = cache

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(molecules=%r, flasks=%r, cache=%r, opts=%r)' % (
                self._molecules, self._flasks, self._cache, self._opts))

    def execute(self):
        """Execute colibri clean command.

        Returns:
            Result: Execution result
        """

        # Create logger if necessary
        logger = ensure_logger('cleancommand', self._opts['logging_level'])

        # Connect to MongoDB database
        if self._molecules or self._flasks:
            try:
                mongoclient = MongoClient(
                    host=self._opts['db_hostname'],
                    port=self._opts['db_port'])
            except ConnectionError:
                return Result.COMMAND_ERROR

        # Clean molecule collection
        if self._molecules:

            try:

                mol_storage = mongoclient.storage(
                    db=self._opts['mol_database'],
                    collection=self._opts['mol_collection'])
                mol_storage.clear()

                # Logging output
                logger.info('Cleaned molecule storage')

            except ConnectionError:
                return Result.COMMAND_ERROR

        # Clean flask collection
        if self._flasks:

            try:

                mongoclient = MongoClient(
                    host=self._opts['db_hostname'],
                    port=self._opts['db_port'])

                flask_storage = mongoclient.storage(
                    db=self._opts['flask_database'],
                    collection=self._opts['flask_collection'])
                flask_storage.clear()

                # Logging output
                logger.info('Cleaned flask storage')

            except ConnectionError:
                return Result.COMMAND_ERROR

        # Clean cache
        if self._cache:

            # Connect to Memcached cache
            try:
                memcachedclient = MemcachedClient(
                    host=self._opts['cache_hostname'],
                    port=self._opts['cache_port'])

                # Expire molecule cache
                mol_cache = memcachedclient.cache()
                mol_cache.clear()

                # Logging output
                logger.info('Cleaned Memcached cache')
                dev_null = open(os.devnull, 'w')

                # Dirty hack: Use telnet to reset stats
                subprocess.call(
                    '(sleep 2; echo stats reset; sleep 2; echo quit) | ' +
                    'telnet ' + self._opts['cache_hostname'] + ' ' +
                    str(self._opts['cache_port']),
                    stdout=dev_null, stderr=subprocess.STDOUT, shell=True)

                # Logging output
                logger.info('Reset Memcached stats')

            except ExecutionError:
                pass

            except ConnectionError:
                return Result.COMMAND_ERROR

        return Result.OK
