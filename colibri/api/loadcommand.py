"""API for colibri load command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    load: colibri load functional API

Classes:
    LoadCommand: colibri load command API
"""

__all__ = ['load', 'LoadCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/02/2015'

#
# Third-party modules
#

import pymongo
try:
    import igraph
except ImportError:
    igraph = None

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.data import Flask, Transformation
from colibri.enums import Result
from colibri.exceptions import CommandError
from colibri.utils import t2l, ensure_logger

#
# Public functions
#


def load(**kwargs):
    """colibri load functional API.

    Args:
        **kwargs: See LoadCommand constructor

    Returns:
        Result: Execution result
    """
    return LoadCommand(**kwargs).execute()

#
# Classes
#


class LoadCommand(Command):
    """colibri load command API."""

    def __init__(self, input_file=None, opts=None):
        """LoadCommand class constructor.

        Args:
            input_file (str, optional): Network input file
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Input file
        if input_file is None:
            raise CommandError('No input file')
        self._input_file = input_file

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(input_file=%r, opts=%r)' % (self._input_file, self._opts))

    def execute(self):
        """Execute colibri load command.

        Returns:
            Result: Execution result
        """

        # Create logger if necessary
        logger = ensure_logger('loadcommand', self._opts['logging_level'])

        # Connect to the MongoDB database using pymongo
        mongoclient = pymongo.MongoClient(
            host=self._opts['db_hostname'], port=self._opts['db_port'])

        # Flask collection
        flask_collection = mongoclient[
            self._opts['flask_database']][self._opts['flask_collection']]

        # Read network using igraph, auto-detect format
        g = igraph.Graph.Read(self._input_file, format=None)

        # Set vertex count
        vcount = 0
        prev_vcount = 0

        # Add nodes and edges
        for n in g.vs:

            # Create flask
            flask = Flask.fromdict(
                {t2l[k]: v for k, v in n.attributes().items() if k in t2l})

            # Flask key
            key = flask.key

            # Find outgoing edges
            for e in g.incident(n, mode=igraph.OUT):
                product = g.vs[g.es[e].target]['key']
                trans = Transformation(
                    reactant=key, product=product, rule=g.es[e]['rule'])
                flask.outgoing.append(trans)

            # Find incoming edges
            for e in g.incident(n, mode=igraph.IN):
                reactant = g.vs[g.es[e].source]['key']
                trans = Transformation(
                    reactant=reactant, product=key, rule=g.es[e]['rule'])
                flask.incoming.append(trans)

            # Upsert into flask collection
            flask_collection.update(
                {'_id': key},
                {'$set': flask.todict(exclude='key')}, upsert=True)

            # Increase vertex count
            vcount += 1

            # Log output
            vcount = g.vcount()
            if vcount > prev_vcount and vcount % 100 == 0:
                logger.info('Flasks loaded: %d' % vcount)
                prev_vcount = vcount

        # Log output
        logger.info('Total flasks loaded: %d' % vcount)
        return Result.OK
