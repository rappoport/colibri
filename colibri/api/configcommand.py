"""API for colibri config command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    config: colibri config functional API

Classes:
    ConfigCommand: colibri config command API
"""

__all__ = ['config', 'ConfigCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/08/2015'


#
# Standard library
#

import os.path
import json

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.enums import Result
from colibri.exceptions import CommandError
from colibri.options import Options

#
# Public functions
#


def config(**kwargs):
    """colibri config functional API.

    Args:
        **kwargs: See ConfigCommand constructor

    Returns:
        Result: Execution result
    """
    return ConfigCommand(**kwargs).execute()

#
# Classes
#


class ConfigCommand(Command):

    def __init__(self, config_file=None, opt=None, append=False, opts=None):
        """ConfigCommand class constructor.

        Args:
            config_file (str, optional): Configuration file
            opt (OptionItem, optional): New option data
            append (bool, optional): Append option
            opts (Options, optional): Configuration options object

        Raises:
            CommandError: Exception in command execution
        """

        # Configuration file
        if config_file is None:
            raise CommandError('No configuration file')
        self._config_file = config_file

        # New option data
        self._opt = opt

        # Append option
        self._append = append

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(config_file=%r, opt=%r, append=%r, opts=%r)' % (
                self._config_file, self._opt, self._append, self._opts))

    def execute(self):
        """Execute colibri config command.

        Returns:
            Result: Execution result
        """

        # Read existing configuration file
        if os.path.exists(self._config_file):
            with open(self._config_file, 'r') as f:
                prev_opts = Options.fromdict(**json.load(f))
        # Create empty Options object
        else:
            prev_opts = Options()

        # Key
        key = self._opt.key

        # Append option
        if self._append:
            updated_opts = prev_opts.update(**{key: self._opt})

        # Set option
        else:
            updated_opts = prev_opts.set(**{key: self._opt})

        # Write out updated configuration file
        with open(self._config_file, 'w') as f:
            json.dump(updated_opts.todict(nondata=False), f, indent=4,
                      sort_keys=True)

        return Result.OK
