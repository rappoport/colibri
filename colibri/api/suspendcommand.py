"""API for colibri suspend command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    suspend: colibri suspend functional API

Classes:
    SuspendCommand: colibri suspend command API
"""

__all__ = ['suspend', 'SuspendCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/17/2015'

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.calc import Calculator
from colibri.enums import Result
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.task import task_names
from colibri.utils import randomid

#
# Public functions
#


def suspend(**kwargs):
    """colibri suspend functional API.

    Args:
        **kwargs: See SuspendCommand constructor

    Returns:
        colibri.enums.Result: Execution result
    """
    return SuspendCommand(**kwargs).execute()

#
# Classes
#


class SuspendCommand(Command):
    """colibri suspend command API."""

    def __init__(self, molecules=False, flasks=False, opts=None):
        """SuspendCommand class constructor.

        Args:
            molecules (bool, optional): Suspend molecules
            flasks (bool, optional): Suspend flasks
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Perform SuspendTask
        self._molecules = molecules

        # Perform FlaskSuspendTask
        self._flasks = flasks

        # Raise CommandError if there is nothing to do
        if not self._molecules and not self._flasks:
            raise CommandError('Nothing to do')

        # Suspend molecules
        if self._molecules:

            # Additional input options to SuspendTask
            input_kv = {}

            # Add SuspendTask option
            task_name = 'mol_suspend_' + randomid(pos=6)
            task = {
                'key': task_name,
                'group': 'task',
                'value': task_names['mol_suspend']}
            input_kv[task_name] = task

            # Add tasks_per_calc option
            input_kv['tasks_per_calc'] = 1

            # Add suspend_interval options
            input_kv['suspend_interval'] = 0

            # Construct Options object for SuspendTask options
            self._molecule_opts = Options(precedence='command', **input_kv)

        # Suspend flasks
        if self._flasks:

            # Additional input options to FlaskSuspendTask
            input_kv = {}

            # Add FlaskSuspendTask option
            task_name = 'flask_suspend_' + randomid(pos=6)
            task = {
                'key': task_name, 'group': 'task',
                'value': task_names['flask_suspend']}
            input_kv[task_name] = task

            # Add tasks_per_calc option
            input_kv['tasks_per_calc'] = 1

            # Add suspend_interval options
            input_kv['suspend_interval'] = 0

            # Construct Options object for FlaskSuspendTask options
            self._flask_opts = Options(precedence='command', **input_kv)

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(molecules=%r, flasks=%r, opts=%r)' % (
                self._molecules, self._flasks, self._opts))

    def execute(self):
        """Execute colib suspend command.

        Returns:
            Result: Execution result
        """

        # Result variable
        result = Result.OK

        # SuspendTask instance
        if self._molecules:

            # Create combined Options object
            opts = self._opts.update(self._molecule_opts)

            # Start Calculator instance
            mol_calc = Calculator(opts)
            result |= mol_calc.run()

        # FlaskSuspendTask instance
        if self._flasks:

            # Create combined Options object
            opts = self._opts.update(self._flask_opts)

            # Start Calculator instance
            flask_calc = Calculator(opts)
            result |= flask_calc.run()

        # Rsturn value
        return result
