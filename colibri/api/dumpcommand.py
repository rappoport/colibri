"""API for colibri dump command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    dump: colibri dump functional API

Classes:
    DumpCommand: colibri dump command API
"""

__all__ = ['dump', 'DumpCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/17/2015'

#
# Third-party modules
#

try:
    import igraph
except ImportError:
    igraph = None

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.data import Flask
from colibri.mongostorage import MongoClient
from colibri.enums import Status, Result, Sort
from colibri.exceptions import MoleculeError, FlaskError, CommandError
from colibri.utils import t2s, t2l, ensure_logger


#
# Public functions
#


def dump(**kwargs):
    """colibri dump functional API

    Args:
        **kwargs: See DumpCommand constructor

    Returns:
        Result: Execution result
    """
    return DumpCommand(**kwargs).execute()

#
# Classes
#


class DumpCommand(Command):
    """colibri dump command API."""

    def __init__(self, export_dep=False, export_var=True,
                 export_default=False, exclude=[], output_file=None,
                 opts=None):
        """DumpCommand class constructor.

        Args:
            export_dep (bool, optional): Export dependent attributes
            export_var (bool, optional): Export variable attributes
            export_default (bool, optional): Export default variable values
            exclude (list, optional): List of exclusions
            output_file (str, optional): Output file name
            opts (Options): Command options

        Raises:
            CommandError: Exception in command execution
        """

        # List of dependent attributes (read-only)
        self._attr_dep = ['inchi', 'inchikey', 'smileskey', 'sumformula']

        # List of variable attributes (writable, computed default)
        self._attr_var = ['charge', 'mult', 'priority', 'chemistry', 'ts',
                          'etag', 'energy', 'status', 'tags', 'attempts',
                          'generation', 'generation_journal', 'precursor',
                          'precursor_journal', 'energy_change', 'error']

        # Export dependent attributes
        self._export_dep = export_dep

        # Export variable attributes
        self._export_var = export_var

        # Export default variable values
        self._export_default = export_default

        # List of exclusions
        if isinstance(exclude, basestring):
            exclude = [exclude]
        self._exclude = exclude

        # Output file
        self._output_file = (
            output_file if output_file is not None else 'test.graphml')

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            ('(export_dep=%r, export_var=%r, export_default=%r, '
             'exclude=%r, output_file=%r, opts=%r)') % (
                self._export_dep, self._export_var, self._export_default,
                self._exclude, self._output_file, self._opts))

    def execute(self):
        """Execute colibri dump command.

        Returns:
            Result: Execution result
        """

        # Get logger
        logger = ensure_logger('dumpcommand', self._opts['logging_level'])

        # Connect to MongoDB database
        mongoclient = MongoClient(
            host=self._opts['db_hostname'], port=self._opts['db_port'])

        # MongoDB flask storage
        flask_storage = mongoclient.storage(
            db=self._opts['flask_database'],
            collection=self._opts['flask_collection'])

        # Min and max generations
        min_generation = self._opts.get('min_generation', None)
        max_generation = self._opts.get('max_generation', None)

        # Maximum energy change
        max_energy_change = self._opts.get('max_energy_change', None)

        # Maximum energy above reference
        max_rel_energy = self._opts.get('max_rel_energy', None)

        # Reference flask
        ref_flask = self._opts.get('ref_flask', None)

        # Reference energy
        ref_energy = self._opts.get('ref_energy', None)

        # Get reference flask
        if max_rel_energy is not None:

            # Reference energy not available, retrieve reference flask
            if ref_energy is None:

                # Construct MongoDB query for reference flask
                ref_query = {}

                # Tag selection
                tags_select = self._opts.get('tags', [])
                if tags_select:
                    if isinstance(tags_select, basestring):
                        ref_query[t2s['tags']] = tags_select
                    else:
                        ref_query[t2s['tags']] = {'$all': tags_select}

                # Reference flask key
                if ref_flask is not None:

                    # Interpret as SMILES string
                    try:
                        ref_query[t2s['key']] = Flask(smiles=ref_flask).key
                    except (MoleculeError, FlaskError):
                        # Interpret as flask key
                        ref_query[t2s['key']] = ref_flask

                    # No sorting necessary
                    ref_sort = None

                else:

                    # Get lowest-energy flask of generation 0
                    ref_query[t2s['generation']] = 0

                    # Sort by energy
                    ref_sort = [[t2s['energy'], Sort.ASCENDING]]

                # MongoDB query request
                ref_request = mongoclient.query_request(
                    query=ref_query, sort=ref_sort)

                # Get reference flask data
                try:
                    ref_data = next(flask_storage.find(ref_request))
                except StopIteration:
                    return Result.COMMAND_ERROR

                # Reference flask energy
                try:
                    ref_energy = ref_data['e']
                except KeyError:
                    return Result.COMMAND_ERROR

        # Create igraph instance
        g = igraph.Graph(directed=True)

        # Read flasks with UNREACTIVE status from MongoDB and add them as nodes
        unreactive = set()
        edges = {}

        # Set counts
        vcount = 0
        prev_vcount = 0
        ecount = 0
        prev_ecount = 0

        # Flask energies
        energies = {}

        # Construct MongoDB query
        query = {}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                query[t2s['tags']] = tags_select
            else:
                query[t2s['tags']] = {'$all': tags_select}

        # MongoDB query request
        request = mongoclient.query_request(query=query)

        # Get all flasks
        cursor = flask_storage.find(request, no_cursor_timeout=True)
        for data in cursor:

            # Convert to flask
            flask = Flask.fromdict(data)

            # INChI key
            key = flask.key

            # Generation
            generation = flask.generation

            # Min generation
            if min_generation is not None and generation < min_generation:
                continue

            # Max generation
            if max_generation is not None and generation > max_generation:
                continue

            # Max energy change
            if (max_energy_change is not None and
                    flask.energy_change > max_energy_change):
                continue

            # Max energy
            if (max_rel_energy is not None and
                    (data['e'] - ref_energy) > max_rel_energy):
                continue

            # Tag selection
            for tag in tags_select:
                if tag not in flask.tags:
                        continue

            # Add only UNREACTIVE flasks
            if flask.status == Status.UNREACTIVE:

                # Add to set of UNREACTIVE flasks
                unreactive.add(key)

                # Energy
                energy = flask.energy
                energies[key] = energy

                for outgoing in flask.outgoing:
                    edges[str(outgoing)] = outgoing

                for incoming in flask.incoming:
                    edges[str(incoming)] = incoming

                # Compile data
                data = {
                    'key': key,
                    'energy': energy,
                    'generation': generation
                }

                data.update({t2l[k]: v for k, v in flask.todict(
                    export_dep=self._export_dep,
                    export_var=self._export_var,
                    export_default=self._export_default,
                    exclude=self._exclude).items()})

                # Add vertex and attributes to graph
                g.add_vertex(**data)

            # Log output
            vcount = g.vcount()
            if vcount > prev_vcount and vcount % 100 == 0:
                logger.info('Flasks dumped: %d' % vcount)
                prev_vcount = vcount

        # Log output
        logger.info('Total flasks dumped: %d' % vcount)

        # Loop over all edges
        for key, edge in edges.iteritems():
            reactant = edge.reactant
            product = edge.product
            rule = edge.rule

            if reactant in unreactive and product in unreactive:
                energy_change = energies[product] - energies[reactant]
                data = {
                    'key': key,
                    'rule': rule,
                    'energy_change': energy_change
                }
                g.add_edge(g.vs.find(key_eq=reactant),
                           g.vs.find(key_eq=product), **data)

            # Log output
            ecount = g.ecount()
            if ecount > prev_ecount and ecount % 100 == 0:
                logger.info('Transformations dumped: %d' % ecount)
                prev_ecount = ecount

        # Log output
        logger.info('Total transformations dumped: %d' % ecount)

        # Output file
        f = open(self._output_file, 'w')

        # Write output
        g.write(f)

        return Result.OK
