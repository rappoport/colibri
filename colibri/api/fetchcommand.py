"""API for colibri fetch command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    fetch: colibri fetch functional API

Classes:
    FetchCommand: colibri fetch command API
"""

__all__ = ['fetch', 'FetchCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/17/2015'

#
# Library modules
#

import json
import itertools as it

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.data import Molecule, Flask
from colibri.enums import Level, Status, Result, Sort
from colibri.exceptions import MoleculeError, FlaskError, CommandError,\
    ConnectionError
from colibri.mongostorage import MongoClient
from colibri.utils import ensure_logger, default, t2s

#
# Public functions
#


def fetch(**kwargs):
    """colibri fetch functional API.

    Args:
        **kwargs: See FetchCommand constructor

    Returns:
        Result: Execution result
    """
    return FetchCommand(**kwargs).execute()

#
# Classes
#


class FetchCommand(Command):
    """colibri fetch command API."""

    def __init__(self, molecules=None, molecule_ids=None, flasks=None,
                 flask_ids=None, opts=None):
        """FetchCommand class constructor.

        Molecules or flasks reader should implement the send() method
        (Generator-style) and accept SMILES/JSON-formatted strings.

        Args:
            molecules (Generator, optional): Molecule output reader
            molecule_ids (list, optional): List of molecule SMILES/keys
            flasks (Generator, optional): Flask output reader
            flask_ids (list, optional): List of flask SMILES/keys
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Reader for molecule input
        self._molecules = molecules

        # List of molecule SMILES/keys
        self._molecule_ids = molecule_ids

        # Reader for flask input
        self._flasks = flasks

        # List of flask SMILES/keys
        self._flask_ids = flask_ids

        # Raise CommandError if no reader is specified
        if not self._molecules and not self._flasks:
            raise CommandError('Nothing to do')

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            ('(molecules=%s, molecule_ids=%r, flasks=%s, ' +
             'flask_ids=%r, opts=%r)') % (
                self._molecules, self._molecule_ids, self._flasks,
                self._flask_ids, self._opts))

    def execute(self):
        """Execute colibri fetch command.

        Returns:
            Result: Execution result
        """

        # Create logger if necessary
        logger = ensure_logger('fetchcommand', self._opts['logging_level'])

        # Connect to MongoDB database
        if self._molecules or self._flasks:
            try:
                mongoclient = MongoClient(
                    host=self._opts['db_hostname'],
                    port=self._opts['db_port'])
            except ConnectionError:
                return Result.COMMAND_ERROR

        # Batch size
        batch_size = self._opts.get('batch_size', 1)

        # Molecule output reader
        if self._molecules:

            try:

                mol_storage = mongoclient.storage(
                    db=self._opts['mol_database'],
                    collection=self._opts['mol_collection'])

            except ConnectionError:
                return Result.COMMAND_ERROR

            # Prepare input query parameters
            fetch_query = {}
            fetch_limit = self._opts.get('limit', 0)
            fetch_sort = [[t2s['priority'], Sort.ASCENDING]]

            # Select levels to retrieve
            level_select = self._opts.get('level', [])
            if isinstance(level_select, basestring):
                level_select = [level_select]
            if level_select:
                fetch_query[t2s['level']] = {
                    '$in': [getattr(Level, l) for l in level_select]}

            # Max and min generation to retrieve
            max_generation = self._opts.get('max_generation', None)
            min_generation = self._opts.get('min_generation', None)
            if max_generation is not None or min_generation is not None:
                fetch_query[t2s['generation']] = {}
                if max_generation is not None:
                    fetch_query[t2s['generation']][
                        '$lte'] = max_generation
                if min_generation is not None:
                    fetch_query[t2s['generation']][
                        '$gte'] = min_generation

            # Tag selection
            tags_select = self._opts.get('tags', [])
            if tags_select:
                if isinstance(tags_select, basestring):
                    fetch_query[t2s['tags']] = tags_select
                else:
                    fetch_query[t2s['tags']] = {'$all': tags_select}

            # Molecule SMILES/keys
            if self._molecule_ids:
                keys = []
                for data in self._molecule_ids:
                    # Interpret as SMILES string
                    try:
                        keys.append(Molecule(smiles=data).key)
                    # Interpret as database key
                    except MoleculeError:
                        keys.append(data)
                fetch_query[t2s['key']] = {'$in': keys}

            # Create query request object
            fetch_request = mongoclient.query_request(
                query=fetch_query, limit=fetch_limit, sort=fetch_sort)

            # Get query cursor
            cursor = mol_storage.find(fetch_request)

            # Set cursor batch size
            cursor.batch_size(batch_size)

            # Read query cursor
            count = 0
            for count, data in it.izip(it.count(1), cursor):
                self._molecules.send(json.dumps(data, default=default))

                logger.debug(
                    'Fetching molecule from storage: %s',
                    Molecule.fromdict(data))

            logger.info('Retrieved %d molecules' % count)

        # Flask output reader
        if self._flasks:

            try:

                flask_storage = mongoclient.storage(
                    db=self._opts['flask_database'],
                    collection=self._opts['flask_collection'])

            except ConnectionError:
                return Result.COMMAND_ERROR

            # Prepare input query parameters
            fetch_query = {}
            fetch_limit = self._opts.get('limit', 0)
            fetch_sort = [[t2s['priority'], Sort.ASCENDING]]

            # Select status to retrieve
            status_select = self._opts.get('status', [])
            if isinstance(status_select, basestring):
                status_select = [status_select]
            if status_select:
                fetch_query[t2s['status']] = {
                    '$in': [getattr(Status, l) for l in status_select]}

            # Max and min generation to retrieve
            max_generation = self._opts.get('max_generation', None)
            min_generation = self._opts.get('min_generation', None)
            if max_generation is not None or min_generation is not None:
                fetch_query[t2s['generation']] = {}
                if max_generation is not None:
                    fetch_query[t2s['generation']][
                        '$lte'] = max_generation
                if min_generation is not None:
                    fetch_query[t2s['generation']][
                        '$gte'] = min_generation

            # Tag selection
            tags_select = self._opts.get('tags', [])
            if tags_select:
                if isinstance(tags_select, basestring):
                    fetch_query[t2s['tags']] = tags_select
                else:
                    fetch_query[t2s['tags']] = {'$all': tags_select}

            # Flask SMILES/keys
            if self._flask_ids:
                keys = []
                for data in self._flask_ids:
                    # Interpret as SMILES string
                    try:
                        keys.append(Flask(smiles=data).key)
                    # Interpret as database key
                    except (MoleculeError, FlaskError):
                        keys.append(data)
                fetch_query[t2s['key']] = {'$in': keys}

            # Create query request object
            fetch_request = mongoclient.query_request(
                query=fetch_query, limit=fetch_limit, sort=fetch_sort)

            # Get query cursor
            cursor = flask_storage.find(fetch_request)

            # Set cursor batch size
            cursor.batch_size(batch_size)

            # Read query cursor
            count = 0
            for count, data in it.izip(it.count(1), cursor):
                self._flasks.send(json.dumps(data, default=default))

                logger.debug(
                    'Fetching flask from storage: %s',
                    Flask.fromdict(data))

            logger.info('Retrieved %d flasks' % count)

        return Result.OK
