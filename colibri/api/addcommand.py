"""API for colibri add command.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    add: colibri add functional API

Classes:
    AddCommand: colibri add command API
"""

__all__ = ['add', 'AddCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/11/2015'


#
# Library modules
#

import json
import itertools as it

#
# colibri submodules
#

from colibri.api.basecommand import Command
from colibri.data import Molecule, Flask
from colibri.enums import Result
from colibri.exceptions import MoleculeError, FlaskError, CommandError, \
    ConnectionError
from colibri.mongostorage import MongoClient
from colibri.utils import ensure_logger, object_hook, t2s

#
# Public functions
#


def add(**kwargs):
    """colibri add functional API.

    Args:
        **kwargs: See AddCommand constructor

    Returns:
        Result: Execution result
    """
    return AddCommand(**kwargs).execute()

#
# Classes
#


class AddCommand(Command):
    """colibri add command API."""

    def __init__(self, molecules=None, flasks=None, opts=None):
        """AddCommand class constructor.

        Molecules or flasks are supplied as SMILES/JSON-formatted strings.

        Args:
            molecules (Iterator, optional): Molecule input iterator
            flasks (Iterator, optional): Flask input iterator
            opts (Options, optional): Configuration options

        Raises:
            CommandError: Exception in command execution
        """

        # Iterator over molecule input
        self._molecules = molecules

        # Iterator over flask input
        self._flasks = flasks

        # Raise CommandError if no input data
        if not self._molecules and not self._flasks:
            raise CommandError('No input data')

        # Configuration options
        if opts is None:
            raise CommandError('No options')
        self._opts = opts

    def __repr__(self):
        """Return evaluatable string representation.

        Returns:
            str: Evaluatable string representation
        """
        return type(self).__name__ + (
            '(molecules=%r, flasks=%r, opts=%r)' % (
                self._molecules, self._flasks, self._opts))

    def execute(self):
        """Execute colibri add command.

        Returns:
            Result: Execution result
        """

        # Create logger if necessary
        logger = ensure_logger('addcommand', self._opts['logging_level'])

        # Connect to MongoDB database
        if self._molecules or self._flasks:
            try:
                mongoclient = MongoClient(
                    host=self._opts['db_hostname'],
                    port=self._opts['db_port'])
            except ConnectionError:
                return Result.COMMAND_ERROR

        # Overwrite/update flags
        overwrite_on_insert = self._opts.get('overwrite_on_insert', False)
        update_on_insert = self._opts.get('update_on_insert', False)

        # Additional tags
        add_tags = self._opts.get('tags', [])

        # Batch size
        batch_size = self._opts.get('batch_size', 1)

        # Molecule input iterator
        if self._molecules:

            try:

                mol_storage = mongoclient.storage(
                    db=self._opts['mol_database'],
                    collection=self._opts['mol_collection'])

            except ConnectionError:
                return Result.COMMAND_ERROR

            # Pad molecule input stream with None values
            molecules = it.chain(self._molecules, iter([None] * batch_size))

            while True:

                try:

                    # Accumulator list for molecule update requests
                    mol_requests = []

                    for _ in xrange(batch_size):

                        # Retrieve data from input stream
                        data = next(molecules)

                        # Remove padding values
                        if data is None:
                            raise StopIteration

                        # Interpret as SMILES string
                        try:
                            mol = Molecule(smiles=data)
                        except MoleculeError as e1:

                            # Interpret as JSON string
                            try:
                                mol = Molecule.fromdict(json.loads(
                                    data, object_hook=object_hook))
                            except (ValueError, MoleculeError) as e2:
                                e = e1 if data[0] != '{' else e2
                                logger.warning(
                                    'Completed unsuccessfully: %s' % e)
                                continue

                        # Create update request
                        add_update = {'query': {t2s['key']: mol.key}}

                        # Combibe molecule tags and new tags
                        tags = mol.tags
                        if add_tags:
                            tags = list(set(tags) | set(add_tags))

                        # Check overwrite/update flags
                        if overwrite_on_insert:
                            mol.tags = tags
                            add_update['overwrite'] = mol.todict(exclude='key')
                        elif update_on_insert:
                            add_update['replace'] = mol.todict(
                                exclude=['key', 'tags'])
                            add_update['union'] = {t2s['tags']: tags}
                        else:
                            add_update['add'] = mol.todict(
                                exclude=['key', 'tags'])
                            add_update['union'] = {t2s['tags']: tags}

                        logger.debug(
                            'Adding molecule to storage: %s' % mol)

                        # Update request
                        add_request = mongoclient.update_request(**add_update)

                        # Append update request to list
                        mol_requests.append(add_request)

                except StopIteration:
                    break

                finally:

                    # Submit update requests to molecule storage
                    if mol_requests:

                        try:
                            mol_storage.update(mol_requests)
                            logger.info(
                                'Submitted %d molecules' % len(mol_requests))

                        except ConnectionError as e:
                            logger.error('Connection error: %s' % e)
                            return Result.COMMAND_ERROR

        # Flask input iterator
        if self._flasks:

            try:

                flask_storage = mongoclient.storage(
                    db=self._opts['flask_database'],
                    collection=self._opts['flask_collection'])

            except ConnectionError:
                return Result.COMMAND_ERROR

            # Pad flask input stream with None values
            flasks = it.chain(self._flasks, iter([None] * batch_size))

            while True:

                try:

                    # Accumulator list for flask update requests
                    flask_requests = []

                    for _ in xrange(batch_size):

                        # Retrieve data from input stream
                        data = next(flasks)

                        # Remove padding values
                        if data is None:
                            raise StopIteration

                        # Interpret as SMILES string
                        try:
                            flask = Flask(smiles=data)
                        except FlaskError as e1:

                            # Interpret as JSON string
                            try:
                                flask = Flask.fromdict(json.loads(
                                    data, object_hook=object_hook))
                            except (ValueError, FlaskError) as e2:
                                e = e1 if data[0] != '{' else e2
                                logger.warning(
                                    'Completed unsuccessfully: %s' % e)
                                continue

                        # Create update request
                        add_update = {'query': {t2s['key']: flask.key}}

                        # Combibe flask tags and new tags
                        tags = flask.tags
                        if add_tags:
                            tags = list(set(tags) | set(add_tags))

                        # Check overwrite/update flags
                        if overwrite_on_insert:
                            flask.tags = tags
                            add_update['overwrite'] = flask.todict(
                                exclude='key')
                        elif update_on_insert:
                            add_update['replace'] = flask.todict(
                                exclude=['key', 'tags'])
                            add_update['union'] = {t2s['tags']: tags}
                        else:
                            add_update['add'] = flask.todict(
                                exclude=['key', 'tags'])
                            add_update['union'] = {t2s['tags']: tags}

                        logger.debug(
                            'Adding flask to storage: %s' % flask)

                        # Update request
                        add_request = mongoclient.update_request(**add_update)

                        # Append update request to list
                        flask_requests.append(add_request)

                except StopIteration:
                    break

                finally:

                    # Submit update requests to flask storage
                    if flask_requests:

                        try:
                            flask_storage.update(flask_requests)
                            logger.info(
                                'Submitted %d flasks' % len(flask_requests))

                        except ConnectionError:
                            logger.error('Connection error: %s' % e)
                            return Result.COMMAND_ERROR

        return Result.OK
