"""Implementations for data types for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    canonicalize: Canonicalize molecule/flask SMILES representation
    xyz_to_adjacencylist: Compute adjacency list from XYZ coordinates
    xyz_to_mol: Convert XYZ coordinates to MOL format
    xyz_to_atoms: Get atomic composition dict from XYZ coordinates

Classes:
    Structure: Base class for chemical structures defined by connectivities
    Formula: Chemical structures defined by connectivities (SMILES)
    Structure3D: Base class for chemical structures defined by XYZ coordinates
    Configuration: Chemical structures defined by XYZ coordinates
                    as created by heuristic builders (no energies)
    Geometry: Chemical structures defined by XYZ coordinates
                    as created by structure optimizations (including energies)
    Property: Chemical structure augmented by computed properties
    Molecule: Molecular structure and properties
    CacheMolecule: String representation of Molecule level/energy for caching
    Flask: Collection of molecules and their properties
    Transformation:  Flask transformation object
"""

__all__ = ['canonicalize', 'xyz_to_adjacencylist', 'xyz_to_mol',
           'xyz_to_atoms',
           'Formula', 'Configuration', 'Geometry', 'Property',
           'Molecule', 'CacheMolecule', 'Flask', 'Transformation']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '01/26/2015'


#
# Library modules
#

import re
from math import sqrt
from copy import copy
from collections import Counter
import datetime

#
# Third-party modules
#

import pybel
import openbabel as ob
from rdkit import Chem, rdBase
from rdkit.Chem import AllChem, Descriptors

# Shut up OpenBabel
ob.obErrorLog.SetOutputLevel(ob.obError)

# Shut up RDKit
rdBase.DisableLog('rdApp.warning')
rdBase.DisableLog('rdApp.error')

#
# colibri submodules
#

from colibri.chemistry import Element
from colibri.exceptions import StructureError, Structure3DError, \
    MoleculeError, FlaskError
from colibri.utils import t2l, t2s, controlhash, adjcompress, composition
from colibri.enums import Level, Status

#
# Internal variables
#

# XYZ atom regex
_re_line = re.compile(
    r'^\s*([a-zA-z]+)\s+([-]?\d+\.\d+)\s+([-]?\d+\.\d+)\s+([-]?\d+\.\d+)\s*$')

# Atomic unit (Bohr) to AA conversion
_au2aa = 0.52917721092

# Distance slack in AA (From Meng and Lewis, 1991)
_dist_slack = 0.4

#
# Public functions
#


def canonicalize(smiles):
    """Canonicalize SMILES representation of a molecule or flask.

    Args:
        smiles (str): Molecule/flask SMILES string

    Returns:
        str: Canonicalized molecule/flask SMILES string

    Examples:
        >>> print canonicalize('O(C=O)CCCCO')
        O=COCCCCO
        >>> print canonicalize('S.F.N#N')
        N#N.F.S
    """

    # Treat as flask
    if '.' in smiles:
        flask = Flask(smiles=smiles)
        return flask.smiles
    else:
        mol = Molecule(smiles=smiles)
        return mol.smiles


def xyz_to_adjacencylist(xyz):
    """Compute adjacency list from XYZ coordinates.

    Args:
        xyz (str): Coordinates in XYZ format

    Returns:
        list of lists: Adjacency list

    Examples:
        >>> print xyz_to_adjacencylist('''7
        ...
        ... O         -1.18352       -0.29841       -0.00000
        ... C         -0.07770        0.55871       -0.00000
        ... O          1.12039       -0.17536       -0.00000
        ... H         -0.11847        1.20906       -0.90011
        ... H         -0.11848        1.20904        0.90017
        ... H          1.08253       -0.77762       -0.78803
        ... H          1.08251       -0.77765        0.78805''')
        ...                     # doctest: +NORMALIZE_WHITESPACE
        [[1], [0, 2, 3, 4], [1, 5, 6], [1], [1], [2], [2]]

        >>> print xyz_to_adjacencylist('''7
        ...
        ... O    -1.294178    -0.191945     0.003233
        ... C    -0.530687     0.755257    -0.001301
        ... O     1.499079    -0.359271     0.002008
        ... H    -0.204299     1.197987    -0.944913
        ... H    -0.194589     1.199218     0.938363
        ... H     1.425883    -0.945717    -0.765433
        ... H     1.396950    -0.958591     0.755674''')
        ...                     # doctest: +NORMALIZE_WHITESPACE
        [[1], [0, 3, 4], [5, 6], [1], [1], [2], [2]]
    """

    # Skip the first two lines
    count, _, lines = xyz.split('\n', 2)

    # Iterate over lines
    atoms = []
    for line in lines.splitlines():
        if not line.strip():
            break

        # Extract element name and atom coordinates
        m = _re_line.match(line)
        if m:
            el = Element(m.group(1))
            num = el.number
            x, y, z = float(m.group(2)), float(m.group(3)), float(m.group(4))
            cov = el.covalent_radius

        # Append to atoms list, lengths in AA
        atoms.append((num, x, y, z, cov))

    # Find bonded atom pairs
    adj = []
    for i, at_i in enumerate(atoms):
        num_i, x_i, y_i, z_i, cov_i = at_i
        adj_i = []
        for j, at_j in enumerate(atoms):
            if j == i:
                continue
            num_j, x_j, y_j, z_j, cov_j = at_j
            if (sqrt((x_i - x_j) ** 2 + (y_i - y_j) ** 2 + (z_i - z_j) ** 2) <
                    cov_i + cov_j + _dist_slack):
                adj_i.append(j)
        adj.append(adj_i)

    return adj


def xyz_to_mol(xyz):
    """Convert XYZ coordinates to MOL format.
       Assigns all bonds as single bonds.

    Args:
        xyz (str): Coordinates in XYZ format

    Returns:
        str: Coordinates in MOL format

    Examples:
        >>> print xyz_to_mol('''10
        ...
        ... C          0.88583       -0.04084        0.06398
        ... C          2.22342       -0.04719        0.04543
        ... C          2.98928       -0.41335       -1.11991
        ... C          4.32705       -0.42053       -1.13914
        ... H          0.34936        0.24200        0.96425
        ... H          0.29383       -0.31314       -0.80393
        ... H          2.76673        0.23498        0.94490
        ... H          2.44575       -0.69503       -2.01943
        ... H          4.91952       -0.14890       -0.27135
        ... H          4.86291       -0.70346       -2.03974''')
        ...                     # doctest: +NORMALIZE_WHITESPACE
        <BLANKLINE>
        <BLANKLINE>
        <BLANKLINE>
         10  9  0  0  0  0            999 V2000
            0.4688   -0.0216    0.0339 C   0  0  0  0  0  0  0  0  0  0  0  0
            1.1766   -0.0250    0.0240 C   0  0  0  0  0  0  0  0  0  0  0  0
            1.5819   -0.2187   -0.5926 C   0  0  0  0  0  0  0  0  0  0  0  0
            2.2898   -0.2225   -0.6028 C   0  0  0  0  0  0  0  0  0  0  0  0
            0.1849    0.1281    0.5103 H   0  0  0  0  0  0  0  0  0  0  0  0
            0.1555   -0.1657   -0.4254 H   0  0  0  0  0  0  0  0  0  0  0  0
            1.4641    0.1243    0.5000 H   0  0  0  0  0  0  0  0  0  0  0  0
            1.2942   -0.3678   -1.0686 H   0  0  0  0  0  0  0  0  0  0  0  0
            2.6033   -0.0788   -0.1436 H   0  0  0  0  0  0  0  0  0  0  0  0
            2.5733   -0.3723   -1.0794 H   0  0  0  0  0  0  0  0  0  0  0  0
          2  1  1  0  0  0  0
          3  2  1  0  0  0  0
          4  3  1  0  0  0  0
          5  1  1  0  0  0  0
          6  1  1  0  0  0  0
          7  2  1  0  0  0  0
          8  3  1  0  0  0  0
          9  4  1  0  0  0  0
         10  4  1  0  0  0  0
        M  END
        <BLANKLINE>
    """

    # Skip the first two lines
    count, _, lines = xyz.split('\n', 2)

    # Iterate over lines
    atoms = []
    for line in lines.splitlines():
        if not line.strip():
            break

        # Extract element name and atom coordinates
        m = _re_line.match(line)
        if m:
            el = Element(m.group(1))
            name = el.name
            x, y, z = float(m.group(2)), float(m.group(3)), float(m.group(4))
            cov = el.covalent_radius

        # Append to atoms list, lengths in AA
        atoms.append((name, x, y, z, cov))

    # Find bonded atom pairs
    bonds = []
    for i, at_i in enumerate(atoms):
        name_i, x_i, y_i, z_i, cov_i = at_i
        for j, at_j in enumerate(atoms):
            if j == i:
                break
            name_j, x_j, y_j, z_j, cov_j = at_j
            if (sqrt((x_i - x_j) ** 2 + (y_i - y_j) ** 2 + (z_i - z_j) ** 2) <
                    cov_i + cov_j + _dist_slack):
                bonds.append((i, j, 1))

    # Format the MOL file
    mol = '\n\n\n'

    # Counts line
    mol += ('{natoms:3d}{nbonds:3d}  0  0{chiral:3d}  ' +
            '0            999 V2000\n').format(
                natoms=len(atoms), nbonds=len(bonds), chiral=0)

    # Atom block
    for at_i in atoms:
        name_i, x_i, y_i, z_i, cov_i = at_i
        mol += ('{x:10.4f}{y:10.4f}{z:10.4f} {name:3s} 0' +
                '{charge:3d}  0  0  0  0  0  0  0  0  0  0\n').format(
            x=x_i * _au2aa, y=y_i * _au2aa, z=z_i * _au2aa,
            name=name_i, charge=0)

    # Bond block
    # Bond order is equal to bond type (1=single, 2=double, 3=triple)
    for bd_i in bonds:
        at1_i, at2_i, order_i = bd_i
        mol += '{at1:3d}{at2:3d}{type:3d}{stereo:3d}  0  0  0\n'.format(
            at1=at1_i + 1, at2=at2_i + 1, type=order_i, stereo=0)

    mol += 'M  END\n'

    return mol


def xyz_to_atoms(xyz):
    """Get atomic composition dict from XYZ coordinates.

    Args:
        xyz (str): Coordinates in XYZ format

    Returns:
        dict: Atomic composition dict

    Examples:
        >>> print xyz_to_atoms('''7
        ...
        ... O         -1.18352       -0.29841       -0.00000
        ... C         -0.07770        0.55871       -0.00000
        ... O          1.12039       -0.17536       -0.00000
        ... H         -0.11847        1.20906       -0.90011
        ... H         -0.11848        1.20904        0.90017
        ... H          1.08253       -0.77762       -0.78803
        ... H          1.08251       -0.77765        0.78805''')
        ...                     # doctest: +NORMALIZE_WHITESPACE
        {'H': 4, 'C': 1, 'O': 2}
     """

    # Skip the first two lines
    count, _, lines = xyz.split('\n', 2)

    # Iterate over lines
    atoms = {}
    for line in lines.splitlines():
        if not line.strip():
            break

        # Extract element name and atom coordinates
        m = _re_line.match(line)
        if m:
            el = Element(m.group(1))
            atoms[el.name] = atoms.setdefault(el.name, 0) + 1

    return atoms

#
# Classes
#


class Structure(object):
    """Base class for chemical structures defined by connectivities.

    Minimal attributes:
        smiles (str): SMILES string (required)
        index (str/int): Index (for conformers and similar)

    Read-only attributes, computed from SMILES string:
        inchi (str): INChI descriptor
        key (str): INChI key + SMILES key
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula
        atoms (dict): Atomic composition dict
        atomkey (str): Atom composition key
        adjacency (list of lists): Adjacency list
        adjkey (str): Adjacency key

    Writable attributes with default values:
        charge (int): Molecular charge
        mult (int): Molecular spin multiplicity
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        program (str): Program name used for calculation
        version (str): Program version
        method (str): Computational method
        options (str): Computational options

    Examples:
    Initialization uses SMILES strings, canonicalized automatically
        >>> struct = Structure(smiles='CC')
        >>> print struct
        CC (OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG/30)
        >>> struct = Structure(smiles='OCC(O)CO')
        >>> print struct
        OCC(O)CO (PEDCQBHIVMGVHV-UHFFFAOYSA-N-RPKLSCWIRG/92)
        >>> struct = Structure('[N]=O')
        >>> print struct
        [N]=O (MWUXSHHQAYIFBG-UHFFFAOYSA-N-VAVKJDJKPO/30)
        >>> struct = Structure()
        Traceback (most recent call last):
        StructureError: SMILES string missing
        >>> struct = Structure('XY')
        Traceback (most recent call last):
        StructureError: Reading SMILES string failed
        >>> struct = Structure('N#O')
        Traceback (most recent call last):
        StructureError: Reading SMILES string failed
        >>> struct = Structure('C.C')
        Traceback (most recent call last):
        StructureError: Multiple molecules in SMILES string

    Canonicalization tests
        >>> struct = Structure(smiles='C1(N=CN2)=C2N=CN=C1')
        >>> print struct
        c1ncc2nc[nH]c2n1 (KDCGOANMDULRCW-UHFFFAOYSA-N-RGDAMILTMU/120)
        >>> struct = Structure(smiles='c12nc[nH]c1ncnc2')
        >>> print struct
        c1ncc2nc[nH]c2n1 (KDCGOANMDULRCW-UHFFFAOYSA-N-RGDAMILTMU/120)
        >>> struct = Structure(smiles='c1ncc2nc[nH]c2n1')
        >>> print struct
        c1ncc2nc[nH]c2n1 (KDCGOANMDULRCW-UHFFFAOYSA-N-RGDAMILTMU/120)
        >>> struct = Structure(smiles='NC1C2=NC3=C(N12)N3')
        >>> print struct
        NC1c2nc3c(n21)N3 (QRWGLOOUBCLHCC-UHFFFAOYSA-N-TRCVYMLYMD/108)

    Index tests
        >>> struct = Structure(smiles='CCCC', index=149)
        >>> print struct
        CCCC (IJDNQMDRQITEOD-UHFFFAOYSA-N-0000000149/58)
        >>> struct = Structure(smiles='C(=O)O', index='AEQR')
        Traceback (most recent call last):
        StructureError: Invalid structure index

    Read-only attributes
        >>> struct = Structure('OO')
        >>> print struct.smiles
        OO
        >>> struct.smiles = 'OOO'
        Traceback (most recent call last):
        AttributeError
        >>> print struct.inchi
        InChI=1S/H2O2/c1-2/h1-2H
        >>> print struct.atoms
        {'H': 2, 'O': 2}
        >>> print struct.atomkey
        H 2,O 2
        >>> print struct.adjacency
        [[1, 2], [0, 3], [0], [1]]
        >>> print struct.adjkey
        ;0;0;1

    Writable attributes with default values
        >>> struct = Structure(smiles='ClC(C(Cl)(Cl)Cl)(Cl)Cl')
        >>> print struct.priority
        237
        >>> struct.priority = 300
        >>> print struct.priority
        300
        >>> struct = Structure(smiles='[O][O]')
        >>> print struct.mult
        3
        >>> struct = Structure('[N]=O')
        >>> print struct.mult
        2
        >>> struct = Structure('O', quality=3)
        >>> print struct.quality
        3

    Structure objects are hashable and allow rich comparisons
        >>> struct1 = Structure('COC')
        >>> hash(struct1)
        -5416564952799542019
        >>> struct2 = Structure('COC')
        >>> print struct1 == struct2
        True
        >>> struct1 is struct2
        False
        >>> struct3 = Structure('CCO')
        >>> print struct1 < struct3
        True
        >>> print struct1.sumformula
        C2H6O
        >>> print struct3.sumformula
        C2H6O
    """

    #
    # Structure class variables
    #

    # Minimal attributes (read-only)
    _attr_min = ['smiles', 'index']

    # Dependent attributes (read-only)
    _attr_dep = ['key', 'inchi', 'inchikey', 'smileskey', 'sumformula',
                 'atoms', 'atomkey', 'adjacency', 'adjkey']

    # Variable attributes (writable, computed default)
    _attr_var = ['charge', 'mult', 'priority', 'program', 'version', 'method',
                 'options', 'quality']

    #
    # Structure internal methods
    #

    # SMILES string and index
    def _set_struct(self, smiles, index=None):

        # Check for multiple molecules in SMILES
        if '.' in smiles:
            raise StructureError('Multiple molecules in SMILES string')

        # Sanitize and save as RDKit molecule
        self._rdmol = Chem.MolFromSmiles(str(smiles), sanitize=True)

        # Raise exception
        if self._rdmol is None:
            raise StructureError('Reading SMILES string failed')

        # Write canonicalized SMILES
        self._smiles = Chem.MolToSmiles(self._rdmol, isomericSmiles=True)

        # Set index (for conformers and similar)
        if index is not None:
            try:
                index = int(index)
            except ValueError:
                raise StructureError('Invalid structure index')

            if index >= 0 and index <= 9999999999:
                self._index = '%010d' % index
            else:
                raise StructureError('Invalid structure index')
        else:
            self._index = None

    # Lazy computation of dependent or variable attributes
    def _attr_compute(self, attr):

        # Compute default values from pymol object
        if attr == 'key':
            if self.index is not None:
                return self.inchikey + '-' + self.index
            else:
                return self.inchikey + '-' + self.smileskey
        elif attr == 'inchi':
            return Chem.MolToInchi(self._rdmol)
        elif attr == 'inchikey':
            return Chem.InchiToInchiKey(Chem.MolToInchi(self._rdmol))
        elif attr == 'smileskey':
            return controlhash(self.smiles)
        elif attr == 'sumformula':
            return AllChem.CalcMolFormula(self._rdmol)
        elif attr == 'adjacency':
            return [[j for j, conn_ij in enumerate(conn_i) if conn_ij]
                    for i, conn_i in enumerate(
                    Chem.GetAdjacencyMatrix(Chem.AddHs(self._rdmol)).tolist())]
        elif attr == 'atoms':
            return dict(Counter(
                at.GetSymbol() for at in Chem.AddHs(self._rdmol).GetAtoms()))
        elif attr == 'atomkey':
            return composition(self.atoms)
        elif attr == 'adjkey':
            return adjcompress(self.adjacency)
        elif attr == 'charge':
            return AllChem.GetFormalCharge(self._rdmol)
        elif attr == 'mult':
            return int(Descriptors.NumRadicalElectrons(self._rdmol)) + 1
        elif attr == 'priority':
            return int(Descriptors.MolWt(self._rdmol) + 0.5)
        elif attr == 'quality':
            return 0
        else:
            return

    #
    # Structure magic methods
    #

    def __init__(self, smiles=None, index=None, **kwargs):
        """Initializer, requires valid SMILES string."""

        # SMILES string provided as argument
        if smiles is not None:
            self._set_struct(smiles, index)
        else:
            raise StructureError('SMILES string missing')

        # Set variable arguments
        for attr, value in kwargs.iteritems():
            if attr in self._attr_var:
                setattr(self, attr, value)

    def __getattr__(self, attr):

        # Field names, needed to stop infinite recursion
        if attr[0] == '_':
            raise AttributeError

        # Computable attributes
        else:
            # Corresponding field name
            field = '_' + attr

            # Minimal attributes
            if attr in self._attr_min:
                return getattr(self, field)

            # Dependent attributes
            elif attr in self._attr_dep:
                return self._attr_compute(attr)

            # Variable attributes
            elif attr in self._attr_var:
                try:
                    return getattr(self, field)
                except AttributeError:
                    value = self._attr_compute(attr)
                    setattr(self, field, value)
                    return value
            else:
                raise AttributeError

    def __setattr__(self, attr, value):

        # Field names are just set
        if attr[0] == '_':
            super(Structure, self).__setattr__(attr, value)

        # Computable attributes
        else:
            # Corresponding field name
            field = '_' + attr

            # Minimal or dependent attributes cannot be set
            if attr in self._attr_min or attr in self._attr_dep:
                raise AttributeError

            # Variable attributes
            elif attr in self._attr_var:
                setattr(self, field, value)
            else:
                raise AttributeError

    def __delattr__(self, attr):

        # Field names are directly deleted
        if attr[0] == '_':
            del self.__dict__[attr]

        # Computable attributes
        else:

            # Corresponding field name
            field = '_' + attr

            # Minimal or dependent attributes cannot be deleted
            if attr in self._attr_min or attr in self._attr_dep:
                raise AttributeError

            # Variable attributes, returns to computed default value
            elif attr in self._attr_var:
                delattr(self, field)

            else:
                raise AttributeError

    def __str__(self):
        """String representation."""
        return '%s (%s/%d)' % (self.smiles, self.key, self.priority)

    def __eq__(self, other):
        """Equality is based on INChI keys."""
        return self.key == other.key

    def __ne__(self, other):
        return self.key != other.key

    def __lt__(self, other):
        return self.key < other.key

    def __le__(self, other):
        return self.key <= other.key

    def __gt__(self, other):
        return self.key > other.key

    def __ge__(self, other):
        return self.key >= other.key

    def __hash__(self):
        """Key is used for hashing."""
        return hash(self.key)

    #
    # Structure public methods
    #

    @classmethod
    def fromdict(cls, inp):
        """Structure constructor from dict representation.

        Args:
            inp (dict): Structure dict representation

        Returns:
            Structure: New Structure object

        Examples:
            >>> struct = Structure.fromdict({"smiles": "CCO"})
            >>> print struct
            CCO (LFQSCWFLJHTTHZ-UHFFFAOYSA-N-XUDWUARODM/46)
        """
        return cls(**{t2l[k]: v for k, v in inp.items()})

    def todict(self, export_dep=False, export_var=True,
               export_default=True, export_key=False, exclude=[]):
        """Export Structure to dict representation.

        Args:
            export_dep (bool, optional): Export dependent attributes
            export_var (bool, optional): Export variable attributes
            export_default (bool, optional): Export default variable values
            export_key (bool, optional): Export key
            exclude (list, optional): Keys to exclude from export

        Returns:
            dict: Structure in dict representation

        Examples:
            >>> outp = Structure(smiles='ICI').todict(exclude='priority')
            >>> print outp
            {'i': None, 'q': 0, 's': 'ICI', 'm': 1, '+': 0}
        """

        # List of exclusions
        if isinstance(exclude, basestring):
            exclude = [exclude]

        # Export data dict
        data = {}

        # Add minimal attributes
        for attr in self._attr_min:
            if attr in exclude:
                continue
            data[t2s[attr]] = copy(getattr(self, attr))

        # Add dependent attributes
        if export_dep:
            for attr in self._attr_dep:
                if attr in exclude:
                    continue
                value = copy(getattr(self, attr, None))
                if value is not None:
                    data[t2s[attr]] = value

        # Add variable attributes
        if export_var:
            for attr in self._attr_var:
                if attr in exclude:
                    continue
                value = copy(getattr(self, attr, None))
                if value is not None:
                    # Export all values
                    if export_default:
                        data[t2s[attr]] = value
                    # Only export non-default values
                    else:
                        value_default = self._attr_compute(attr)
                        if value != value_default:
                            data[t2s[attr]] = value

        # Add key attribute
        if export_key:
            data[t2s['key']] = self.key

        return data

    def copy(self, **kwargs):
        """Make deep copy of Structure object.

        Args:
            **kwargs: See todict() method

        Returns:
            Structure: Deep copy of current object

        Examples:
            >>> struct1 = Structure(smiles='O1CC1')
            >>> struct2 = struct1.copy()
            >>> print struct2
            C1CO1 (IAYPIBMASNFSPL-UHFFFAOYSA-N-IGFFCQTDYI/44)
            >>> print struct1 == struct2
            True
            >>> print struct1 is struct2
            False
        """

        return self.fromdict(self.todict(**kwargs))

    def update(self, other):
        """Update Structure with data from other object.

        Args:
            other (Structure/dict): Structure to use

        Raises:
            StructureError: Invalid Structure input

        Examples:
            >>> struct1 = Structure(smiles='CC(C(=O)O)N', priority=100)
            >>> print struct1
            CC(N)C(=O)O (QNAYBMKLOCPYGJ-UHFFFAOYSA-N-SKYMXEEXCM/100)
            >>> struct2 = Structure(smiles='CC(C(=O)O)N')
            >>> struct1.update(struct2)
            >>> print struct1
            CC(N)C(=O)O (QNAYBMKLOCPYGJ-UHFFFAOYSA-N-SKYMXEEXCM/89)
            >>> struct3 = Structure(smiles='C[C@@H](C(=O)O)N')
            >>> struct1.update(struct3)
            Traceback (most recent call last):
            StructureError: Update cannot change SMILES string
        """

        if isinstance(other, Structure):
            pass
        elif isinstance(other, dict):
            other = Structure.fromdict(other)
        else:
            raise StructureError('Invalid structure input in update')

        # Check if SMILES string changes
        if self.smiles != other.smiles:
            raise StructureError('Update cannot change SMILES string')

        # Overwrite old variable attributes with new ones
        for attr in other._attr_var:
            try:
                setattr(self, attr, (getattr(other, attr)))
            except AttributeError:
                pass


class Structure3D(Structure):
    """Base class for chemical structures defined by XYZ coordinates.

    Minimal attributes:
        xyz (str): Atomic coordinates in XYZ format (required)
        index (str/int): Index (for conformers and similar)

    Read-only attributes, computed from XYZ string:
        smiles (str): SMILES string
        inchi (str): INChI descriptor
        key (str): INChI key + SMILES key
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula
        atoms (dict): Atomic composition dict
        atomkey (str): Atom composition key
        adjacency (list of lists): Adjacency list
        adjkey (str): Adjacency key

    Writable attributes with default values:
        charge (int): Molecular charge
        mult (int): Molecular spin multiplicity
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        program (str): Program name used for calculation
        version (str): Program version
        method (str): Computational method
        options (str): Computational options

    Examples:
        >>> struct3d = Structure3D(xyz='''3
        ...
        ... N          1.06466        0.02749       -0.07330
        ... H          0.73267       -0.59812        0.66957
        ... H          2.09099        0.02058       -0.06510''')
        >>> print struct3d
        [NH2] (MDFFNEOEWAXZRQ-UHFFFAOYSA-N-LVELFWPLOP/16)
        >>> print struct3d.mult
        2
        >>> print struct3d.charge
        0
        >>> print struct3d.sumformula
        H2N
        >>> print struct3d.atoms
        {'H': 2, 'N': 1}
        >>> print struct3d.atomkey
        H 2,N 1
        >>> print struct3d.adjacency
        [[1, 2], [0], [0]]
        >>> print struct3d.adjkey
        ;0;0
    """

    #
    # Structure3D class variables
    #

    # Minimal attributes (read-only)
    _attr_min = ['xyz', 'index']

    # Dependent attributes (read-only)
    _attr_dep = ['smiles', 'key', 'inchi', 'inchikey', 'smileskey',
                 'sumformula', 'atoms', 'atomkey', 'adjacency', 'adjkey']

    # Variable attributes (writable, computed default,
    #                      exported only if non-default)
    _attr_var = ['charge', 'mult', 'priority', 'program', 'version',
                 'method', 'options', 'quality']

    #
    # Structure3D internal methods
    #

    # XYZ coordinates and index
    def _set_struct(self, xyz, index=None):

        # Set XYZ coordinates
        self._xyz = str(xyz)

        # Convert to MOL format using OpenBabel
        try:
            mdlmol = pybel.readstring('xyz', self._xyz).write('mol')

        # Use local code if OpenBabel conversion fails
        except IOError:
            mdlmol = xyz_to_mol(self._xyz)

        # Sanitize and read into RDKit
        self._rdmol = Chem.MolFromMolBlock(mdlmol, sanitize=True)

        # Raise exception
        if self._rdmol is None:
            raise Structure3DError('Reading MOL format failed')

        # Set index (for conformers and similar)
        if index is not None:
            try:
                index = int(index)
            except ValueError:
                raise Structure3DError('Invalid structure index')

            if index >= 0 and index <= 9999999999:
                self._index = '%010d' % index
            else:
                raise Structure3DError('Invalid structure index')
        else:
            self._index = None

    # Lazy computation of dependent or variable attributes
    def _attr_compute(self, attr):

        # Compute default values from pymol object
        if attr == 'smiles':
            return Chem.MolToSmiles(self._rdmol, isomericSmiles=True)
        elif attr == 'key':
            if self.index is not None:
                return self.inchikey + '-' + self.index
            else:
                return self.inchikey + '-' + self.smileskey
        elif attr == 'inchi':
            return Chem.MolToInchi(self._rdmol)
        elif attr == 'inchikey':
            return Chem.InchiToInchiKey(Chem.MolToInchi(self._rdmol))
        elif attr == 'smileskey':
            return controlhash(self.smiles)
        elif attr == 'sumformula':
            return AllChem.CalcMolFormula(self._rdmol)
        elif attr == 'charge':
            return AllChem.GetFormalCharge(self._rdmol)
        elif attr == 'mult':
            return int(Descriptors.NumRadicalElectrons(self._rdmol)) + 1
        elif attr == 'priority':
            return int(Descriptors.MolWt(self._rdmol) + 0.5)
        elif attr == 'quality':
            return 0
        elif attr == 'atoms':
                return xyz_to_atoms(self._xyz)
        elif attr == 'atomkey':
            return composition(self.atoms)
        elif attr == 'adjacency':
            return xyz_to_adjacencylist(self._xyz)
        elif attr == 'adjkey':
            return adjcompress(self.adjacency)
        else:
            return

    #
    # Structure3D magic methods
    #

    def __init__(self, xyz=None, index=None, **kwargs):
        """Initializer, requires atomic coordinates in XYZ format."""

        # Set coordinates, charge, multiplicity, index
        if xyz is not None:
            self._set_struct(xyz, index)
        else:
            raise Structure3DError('XYZ coordinates missing')

        # Set variable arguments
        for attr, value in kwargs.iteritems():
            if attr in self._attr_var:
                setattr(self, attr, value)


class Formula(Structure):
    """Chemical structures defined by connectivities (SMILES).

    Minimal attributes:
        smiles (str): SMILES string (required)
        charge (int): Molecular charge
        mult (int): Molecular spin multiplicity
        index (str/int): Index (for conformers and similar)

    Read-only attributes, computed from SMILES string:
        inchi (str): INChI descriptor
        key (str): INChI key + SMILES key
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula
        atoms (dict): Atomic composition dict
        atomkey (str): Atom composition key
        adjacency (list of lists): Adjacency list
        adjkey (str): Adjacency key

    Writable attributes with default values:
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        program (str): Program name used for calculation
        version (str): Program version
        method (str): Computational method
        options (str): Computational options
    """
    pass


class Configuration(Structure3D):
    """Chemical structures defined by XYZ coordinates (no energies).

    Minimal attributes:
        xyz (str): Atomic coordinates in XYZ format (required)
        charge (int): Molecular charge
        mult (str): Molecular spin multiplicity
        index (str/int): Index (for conformers and similar)

    Read-only attributes, computed from XYZ string:
        smiles (str): SMILES string
        inchi (str): INChI descriptor
        key (str): INChI key + SMILES key
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula
        atoms (dict): Atomic composition dict
        atomkey (str): Atom composition key
        adjacency (list of lists): Adjacency list
        adjkey (str): Adjacency key

    Writable attributes with default values:
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        program (str): Program name used for calculation
        version (str): Program version
        method (str): Computational method
        options (str): Computational options

    """

    pass


class Geometry(Structure3D):
    """Chemical structures defined by XYZ coordinates (including energies).

    Minimal Attributes:
        xyz (str): Atomic coordinates in XYZ format (required)
        charge (int): Molecular charge
        mult (str): Molecular spin multiplicity
        index (str/int): Index (for conformers and similar)

    Read-only attributes, computed from XYZ string:
        smiles (str): SMILES string
        inchi (str): INChI descriptor
        key (str): INChI key + SMILES key
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula
        atoms (dict): Atomic composition dict
        atomkey (str): Atom composition key
        adjacency (list of lists): Adjacency list
        adjkey (str): Adjacency key

    Writable attributes with default values:
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        program (str): Program name used for calculation
        version (str): Program version
        method (str): Computational method
        options (str): Computational options
        energy (float): Computed energy
        converged (bool): Convergence flag
        raw_output (str): Computational output
    """

    # Writable attributes
    _attr_var = Structure3D._attr_var + ['energy', 'converged', 'raw_output']


class Property(Structure3D):
    """Chemical structures defined by XYZ coordinates augmented by molecular
    properties.

    Minimal attributes:
        xyz (str): Atomic coordinates in XYZ format (required)
        charge (int): Molecular charge
        mult (str): Molecular spin multiplicity
        index (str/int): Index (for conformers and similar)

    Read-only attributes, computed from XYZ string:
        smiles (str): SMILES string
        inchi (str): INChI descriptor
        key (str): INChI key + SMILES key
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula
        atoms (dict): Atomic composition dict
        atomkey (str): Atom composition key
        adjacency (list of lists): Adjacency list
        adjkey (str): Adjacency key

    Writable attributes with default values:
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        program (str): Program name used for calculation
        version (str): Program version
        method (str): Computational method
        options (str): Computational options
        energy (float): Computed energy
        converged (bool): Convergence flag
        raw_output (str): Computational output
        properties (dict): Property dict
        units (dict): Property units dict
    """

    # Writable attributes
    _attr_var = Structure3D._attr_var + ['energy', 'raw_output',
                                         'properties', 'units']


class Molecule(object):
    """Molecular structure and properties.

    Minimal attributes:
        key (str): INChI key + SMILES key

    Structure attributes:
        formula (Formula): Formula data
        configuration (Configuration): Configuration data
        geometry (Geometry): Geometry data
        property (Property): Property data

    Read-only attributes, computed from structure attributes:
        smiles (str): SMILES string
        xyz (str): Atomic coordinates in XYZ format
        charge (int): Molecular charge
        mult (int): Molecular spin multiplicity
        index (str/int): Index (for conformers and similar)
        inchi (str): INChI descriptor
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula
        atoms (dict): Atomic composition dict
        atomkey (str): Atom composition key
        adjacency (list of lists): Adjacency list
        adjkey (str): Adjacency key
        energy (float): Computed energy
        converged (bool): Convergence flag
        raw_output (str): Computational output
        properties (dict): Property dict
        units (dict): Property units dict

    Writable attributes with default values:
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        chemistry (list): Chemical classification
        ts (datetime.datetime): Timestamp
        etag (str): External/entity tag
        level (Level): Computational level
        tags (list): List of tags
        attempts (int): Number of computation attempts
        generation (int): Molecule generation
        error (str): Error description

    Examples:
        >>> mol = Molecule(configuration=Configuration(xyz='''5
        ...
        ... C          1.03679       -0.05763       -0.00158
        ... O          0.31404       -1.02766       -0.13596
        ... O          2.36496       -0.12371        0.20849
        ... H          0.69232        0.98769       -0.03555
        ... H          2.76543        0.76272        0.28991''',
        ... index=1234))
        >>> print mol
        O=CO (BDAGIHXWWSANSR-UHFFFAOYSA-N-0000001234/46)
        >>> print mol.sumformula
        CH2O2
        >>> print mol.atoms
        {'H': 2, 'C': 1, 'O': 2}
        >>> print mol.atomkey
        C 1,H 2,O 2
        >>> print mol.adjkey
        ;0;0;0;2
        >>> mol = Molecule(smiles='S', property=Property(xyz='''3
        ...
        ... S          0.863103      -0.113163       0.086821
        ... H          2.200456       0.021323      -0.066751
        ... H          0.610001       0.762160      -0.912711''',
        ... energy=-399.213887041283, converged=True,
        ... properties={'dipole_moment': 1.23090},
        ... units={'dipole_moment': 'D'}))
        >>> print mol.sumformula
        H2S
        >>> print mol.adjkey
        ;0;0
        >>> print '%s %s' % (mol.properties['dipole_moment'],
        ... mol.units['dipole_moment'])
        1.2309 D
    """

    #
    # Molecule class variables
    #

    # Minimal attributes
    _attr_min = ['key']

    # Structure attributes in order of resolution
    _attr_struct = ['formula', 'configuration', 'geometry', 'property']

    # Dependent attributes (read-only)
    _attr_dep = ['smiles', 'xyz', 'charge', 'mult', 'index', 'inchi',
                 'inchikey', 'smileskey', 'sumformula', 'atoms', 'atomkey',
                 'adjacency', 'adjkey', 'energy', 'converged', 'raw_output',
                 'properties', 'units']

    # Variable attributes (writable, computed default)
    _attr_var = ['priority', 'quality', 'chemistry', 'ts', 'etag', 'level',
                 'tags', 'attempts', 'generation', 'error']

    #
    # Molecule internal methods
    #

    # INChI key
    def _set_key(self):
        self._key = self._search_attr('key')

    # Search for structure attributes in order of resolution
    #  property - geometry - configuration - formula
    def _search_attr(self, attr):
        for struct in self._attr_struct:
            try:
                return getattr(getattr(self, struct), attr)
            except AttributeError:
                pass

    # Variable attributes
    def _attr_compute(self, attr):

        # Compute default values

        if attr in ['smiles', 'xyz', 'charge', 'mult', 'index', 'inchi',
                    'inchikey', 'smileskey', 'sumformula', 'atoms', 'atomkey',
                    'adjacency', 'adjkey', 'energy', 'converged', 'raw_output',
                    'priority', 'quality', 'properties', 'units']:
            return self._search_attr(attr)
        elif attr == 'ts':
            return datetime.datetime.utcnow()
        elif attr == 'etag':
            return ''
        elif attr == 'level':
            for struct in reversed(self._attr_struct):
                try:
                    getattr(self, struct)
                    return getattr(Level, struct.upper())
                except AttributeError:
                    pass
            return
        elif attr == 'chemistry':
            return []
        elif attr == 'tags':
            return []
        elif attr == 'attempts':
            return 0
        elif attr == 'generation':
            return 0
        elif attr == 'error':
            return ''
        else:
            return

    #
    # Molecule magic methods
    #

    def __init__(self, formula=None, configuration=None, geometry=None,
                 property=None, smiles=None, charge=None, mult=None,
                 index=None, **kwargs):

        # Property provided
        if property is not None:
            if isinstance(property, Property):
                self.property = property
            elif isinstance(property, dict):
                self.property = Property(**{
                    t2l[k]: v for k, v in property.items()})
            else:
                raise MoleculeError('Invalid property input')

        # Geometry provided
        if geometry is not None:
            if isinstance(geometry, Geometry):
                self.geometry = geometry
            elif isinstance(geometry, dict):
                self.geometry = Geometry(**{
                    t2l[k]: v for k, v in geometry.items()})
            else:
                raise MoleculeError('Invalid geometry input')

        # Configuration provided
        if configuration is not None:
            if isinstance(configuration, Configuration):
                self.configuration = configuration
            elif isinstance(configuration, dict):
                self.configuration = Configuration(**{
                    t2l[k]: v for k, v in
                    configuration.items()})
            else:
                raise MoleculeError('Invalid configuration input')

        # Formula provided
        if formula is not None:
            if isinstance(formula, Formula):
                self.formula = formula
            elif isinstance(formula, dict):
                self.formula = Formula(**{
                    t2l[k]: v for k, v in formula.items()})
            else:
                raise MoleculeError('Invalid formula input')

        # Set SMILES string if not provided otherwise
        if smiles is not None and getattr(self, 'smiles', None) is None:
            self.formula = Formula(smiles=smiles, charge=charge, mult=mult,
                                   index=index, **kwargs)

        # Set INChI key
        self._set_key()
        if not hasattr(self, 'key'):
            raise MoleculeError('Cannot compute molecule key')

        # Set variable arguments
        for attr, value in kwargs.iteritems():
            if attr in attr in self._attr_var:
                setattr(self, attr, value)

        # Set creation timestamp
        if 'ts' not in kwargs:
            self.ts = datetime.datetime.utcnow()

    def __getattr__(self, attr):

        # Field names, needed to stop infinite recursion
        if attr[0] == '_':
            raise AttributeError

        # Computable attributes
        else:

            # Corresponding field name
            field = '_' + attr

            # Minimal or structure attributes
            if attr in self._attr_min or attr in self._attr_struct:
                return getattr(self, field)

            # Dependent attributes
            elif attr in self._attr_dep:
                return self._attr_compute(attr)

            # Variable attributes
            elif attr in self._attr_var:
                try:
                    return getattr(self, field)
                except AttributeError:
                    setattr(self, field, self._attr_compute(attr))
                    return getattr(self, field)

            else:
                raise AttributeError

    def __setattr__(self, attr, value):

        # Field names are just set
        if attr[0] == '_':
            self.__dict__[attr] = value

        # Computable attributes
        else:

            # Corresponding field name
            field = '_' + attr

            # Minimal or dependent attributes cannot be set
            if attr in self._attr_min or attr in self._attr_dep:
                raise AttributeError

            # Structure and variable attributes
            elif attr in self._attr_struct or attr in self._attr_var:
                setattr(self, field, value)

            else:
                raise AttributeError

    def __delattr__(self, attr):

        # Field names are directly deleted
        if attr[0] == '_':
            del self.__dict__[attr]

        # Computable attributes
        else:

            # Corresponding field name
            field = '_' + attr

            # Minimal or dependent attributes cannot be deleted
            if attr in self._attr_min or attr in self._attr_dep:
                raise AttributeError

            # Variable attributes, reverts to computed default value
            elif attr in self._attr_struct or attr in self._attr_var:
                delattr(self, field)

            else:
                raise AttributeError

    def __str__(self):
        """String representation."""
        return '%s (%s/%d)' % (self.smiles, self.key, self.priority)

    def __eq__(self, other):
        return self.key == other.key

    def __ne__(self, other):
        return self.key != other.key

    def __lt__(self, other):
        return self.key < other.key

    def __le__(self, other):
        return self.key <= other.key

    def __gt__(self, other):
        return self.key > other.key

    def __ge__(self, other):
        return self.key >= other.key

    def __hash__(self):
        return hash(self.key)

    #
    # Molecule public methods
    #

    @classmethod
    def fromdict(cls, inp):
        """Molecule constructor from dict representation.

        Args:
            inp (dict): Molecule dict representation

        Returns:
            Molecule: New Molecule object

        Examples:
            >>> mol = Molecule.fromdict({"smiles": "OS(=O)(=O)O"})
            >>> print mol
            O=S(=O)(O)O (QAOWNCQODCNURD-UHFFFAOYSA-N-VMQUDPACAO/98)
        """
        return cls(**{t2l[k]: v for k, v in inp.items()})

    def todict(self, export_struct=True, export_dep=False,
               export_var=True, export_default=True, exclude=[]):
        """Export Molecule to dict representation.

        Args:
            export_struct (bool, optional): Export structure attributes
            export_dep (bool, optional): Export dependent variables
            export_var (bool, optional): Export variable attributes
            export_default (bool, optional): Export default variable values
            exclude (list, optional): Keys to exclude from export

        Returns:
            dict: Molecule in dict representation

        Examples:
            >>> outp = Molecule(smiles='CN', chemistry=['amine']).todict()
            >>> print outp # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
            {'z': ['amine'], 'd': 31, 'f': {'i': None, 's': 'CN', 'd': 31,
             '+': 0}, '+': 0, 'l': 1000, '3': 0, '2': '', '5': '',
             '4': ..., '8': 0, '_id': 'BAVYZALUXZFZLV-UHFFFAOYSA-N-NXJQXGBTJO',
            't': []}
        """

        # List of exclusions
        if isinstance(exclude, basestring):
            exclude = [exclude]

        # Export data dict
        data = {}

        # Set key
        if 'key' not in exclude:
            data[t2s['key']] = self.key

        # Add dependent attributes
        if export_dep:
            for attr in self._attr_dep:
                if attr in exclude:
                    continue
                value = copy(getattr(self, attr, None))
                if value is not None:
                    data[t2s[attr]] = value

        # Add structure attributes
        if export_struct:
            for struct in self._attr_struct:
                if struct in exclude:
                    continue
                if hasattr(self, struct):
                    value = getattr(self, struct).todict()
                    if value is not None:
                        data[t2s[struct]] = value

        # Add variable attributes
        if export_var:
            for attr in self._attr_var:
                if attr in exclude:
                    continue
                value = getattr(self, attr, None)
                if value is not None:
                    # Export all values
                    if export_default:
                        data[t2s[attr]] = value
                    # Only export non-default values
                    else:
                        value_default = self._attr_compute(attr)
                        if value != value_default:
                            data[t2s[attr]] = value

        return data

    def copy(self, **kwargs):
        """Make deep copy of Molecule object.

        Args:
            **kwargs: See todict() method

        Returns:
            Molecule: Deep copy of current object

        Examples:
            >>> mol1 = Molecule(smiles='FF')
            >>> mol2 = mol1.copy()
            >>> print mol1 == mol2
            True
            >>> print mol1 is mol2
            False

        """
        return self.fromdict(self.todict(**kwargs))

    def update(self, other):
        """Update Molecule with data from other object.

        Args:
            other (Molecule/dict): Molecule to use

        Raises:
            MoleculeError: Invalid Molecule input

        Examples:
            >>> mol1 = Molecule(formula=Formula(smiles='NC=O'))
            >>> print mol1.level == Level.FORMULA
            True
            >>> mol2 = Molecule(configuration=Configuration(xyz='''6
            ...
            ... N          1.09540       -0.03680       -0.06149
            ... C          2.45290       -0.00487       -0.10980
            ... O          3.07897        0.57285       -0.98385
            ... H          0.58688       -0.51784        0.66628
            ... H          0.58191        0.44381       -0.78862
            ... H          2.92794       -0.55370        0.72053'''))
            >>> mol1.update(mol2)
            >>> print mol1.level == Level.CONFIGURATION
            True
            >>> print mol1
            NC=O (ZHNUHDYFZUAESO-UHFFFAOYSA-N-HQNVVWHFRC/45)
            >>> mol1.level = Level.CONFIGURATION_ERROR
            >>> print mol1.level == Level.CONFIGURATION_ERROR
            True
            >>> mol3 = Molecule(smiles='NC=O', mult=3)
            >>> mol1.update(mol3)
            >>> print mol3.mult
            3
            >>> mol4 = Molecule(smiles='NCO')
            >>> mol1.update(mol4)
            Traceback (most recent call last):
            MoleculeError: Update cannot change key
        """

        if isinstance(other, Molecule):
            pass
        elif isinstance(other, dict):
            other = Molecule.fromdict(other)
        else:
            raise MoleculeError('Invalid molecule input in update')

        # Check if key has changed
        if self.key != other.key:
            raise MoleculeError('Update cannot change key')

        # Update structure attributes
        for attr in other._attr_struct:
            if hasattr(other, attr):
                if hasattr(self, attr):
                    getattr(self, attr).update(getattr(other, attr))
                else:
                    setattr(self, attr, getattr(other, attr).copy(
                            export_default=False))

        # Overwrite old variable attributes with new ones
        for attr in other._attr_var:
            try:
                setattr(self, attr, (getattr(other, attr)))
            except AttributeError:
                pass


class CacheMolecule(object):
    """String representation of Molecule level/energy for caching.
    Contains either the energy or the molecule level prefixed by 'L'.
    The string representation is designed for caching.

    Attributes (readable/writable):
        level (Level): Molecule level
        energy (float): Molecule energy if available

    Examples:
        >>> cmol1 = CacheMolecule('L1000')
        >>> print cmol1.level == Level.FORMULA
        True
        >>> print cmol1.energy
        None
        >>> cmol2 = CacheMolecule('-23.34034')
        >>> print cmol2.level == Level.GEOMETRY
        True
        >>> print cmol2.energy
        -23.34034
        >>> cmol3 = CacheMolecule()
        >>> print cmol3.level
        None
        >>> print cmol3.energy
        None
    """

    def __init__(self, inp=None):

        # Set molecule level
        self.level = None

        # Set molecule energy
        self.energy = None

        if not inp:
            return

        # Energy not available
        if inp[0] == 'L':
            self.level = int(inp[1:])

        # Geometry level, energy available
        else:
            self.level = Level.GEOMETRY
            self.energy = float(inp)

    @classmethod
    def fromdict(cls, inp):
        """CacheMolecule constructor from Molecule dict representation.

        Args:
            inp (dict): Molecule dict representation

        Returns:
            CacheMolecule: New CacheMolecule object

        Examples:
            >>> inp1 = Molecule(smiles='CS').todict()
            >>> print CacheMolecule.fromdict(inp1)
            L1000
            >>> inp2 = Molecule(smiles='S', geometry=Geometry(xyz='''3
            ...
            ... S          0.863103      -0.113163       0.086821
            ... H          2.200456       0.021323      -0.066751
            ... H          0.610001       0.762160      -0.912711''',
            ... energy=-399.213887041283, converged=True)).todict()
            >>> print CacheMolecule.fromdict(inp2)
            -399.213887041
        """

        # Create new CacheMolecule object
        cmol = cls()

        # Store level
        cmol.level = inp[t2s['level']]

        # Store energy if available
        if cmol.level in Level.HAS_ENERGY:
            cmol.energy = inp.get(t2s['geometry'], {}).get(t2s['energy'])
        else:
            cmol.energy = None

        return cmol

    @classmethod
    def from_molecule(cls, mol):
        """CacheMolecule constructor from Molecule object.

        Args:
            mol (Molecule): Input Molecule object

        Returns:
            CacheMolecule: New CacheMolecule object

        Examples:
            >>> mol1 = Molecule(smiles='CS')
            >>> print CacheMolecule.from_molecule(mol1)
            L1000
            >>> print hash(mol1)
            5381175847441722005
            >>> mol2 = Molecule(smiles='S', geometry=Geometry(xyz='''3
            ...
            ... S          0.863103      -0.113163       0.086821
            ... H          2.200456       0.021323      -0.066751
            ... H          0.610001       0.762160      -0.912711''',
            ... energy=-399.213887041283, converged=True))
            >>> print CacheMolecule.from_molecule(mol2)
            -399.213887041
            >>> print hash(mol2)
            9068707403735103017
        """

        # Create new CacheMolecule object
        cmol = cls()

        # Store level
        cmol.level = mol.level

        # Store energy if available
        if cmol.level == Level.GEOMETRY:
            cmol.energy = mol.geometry.energy
        else:
            cmol.energy = None

        return cmol

    def todict(self):
        """Export CacheMolecule to dict representation.

        Returns:
            dict: CacheMolecule dict representation

        Examples:
            >>> inp1 = Molecule(smiles='CS').todict()
            >>> cmol1 = CacheMolecule.fromdict(inp1)
            >>> print cmol1.todict()
            {'energy': None, 'level': 1000}
            >>> mol2 = Molecule(smiles='S', geometry=Geometry(xyz='''3
            ...
            ... S          0.863103      -0.113163       0.086821
            ... H          2.200456       0.021323      -0.066751
            ... H          0.610001       0.762160      -0.912711''',
            ... energy=-399.213887041283, converged=True))
            >>> cmol2 = CacheMolecule.from_molecule(mol2)
            >>> print cmol2.todict()
            {'energy': -399.213887041283, 'level': 1200}
        """

        outp = {
            'level': self.level,
            'energy': self.energy
        }

        return outp

    def empty(self):
        """Check if CacheMolecule is empty.

        Returns:
            bool: True if CacheMolecule is empty

        Examples:
            >>> CacheMolecule().empty()
            True
            >>> CacheMolecule.fromdict({'l': Level.CONFIGURATION}).empty()
            False
            >>> CacheMolecule.fromdict(
            ...     {'g': {'e': -24.39482}, 'l': Level.GEOMETRY}).empty()
            False
        """
        return self.level is None and self.energy is None

    def __str__(self):
        """CacheMolecule string representation.

        Returns:
            str: energy if available, or level prefixed by 'L'

        Examples:
            >>> json_doc1 = Molecule(smiles='CS').todict()
            >>> cmol1 = CacheMolecule.fromdict(json_doc1)
            >>> print cmol1
            L1000
            >>> mol2 = Molecule(smiles='S', geometry=Geometry(xyz='''3
            ...
            ... S          0.863103      -0.113163       0.086821
            ... H          2.200456       0.021323      -0.066751
            ... H          0.610001       0.762160      -0.912711''',
            ... energy=-399.213887041283, converged=True))
            >>> cmol2 = CacheMolecule.from_molecule(mol2)
            >>> print cmol2
            -399.213887041
        """

        if self.level == Level.GEOMETRY:
            return str(self.energy)
        else:
            return 'L' + str(self.level)

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __ne__(self, other):
        return hash(self) != hash(other)


class Flask(object):
    """Collection of molecules and their properties.

    Minimal attributes:
        key (str): INChI key + SMILES key

    Structure attributes:
        smiles (str): Sorted SMILES strings contained in the flask
        molecules (list): List of Formula objects for constituent molecules
        incoming (list): List of incoming Transformation objects
        outgoing (list): List of outgoing Transformation objects

    Dependent attributes (read-only):
        inchi (str): INChI descriptor
        inchikey (str): INChI key
        smileskey (str): SMILES key
        sumformula (str): Sum formula

    Variable attributes (writable, computed default):
        priority (int): Smaller values mean higher priority
        quality (int): Smaller values mean higher quality
        chemistry (list): Chemical classification
        ts (datetime.datetime): Timestamp
        etag (str): External/entity tag
        energy (float): Flask energy
        status (Status): Flask status
        tags (list): List of tags
        attempts (int): Number of computation attempts
        generation (int): Flask generation
        generation_journal (list): Append-only journal for flask generations
        precursor (float): Highest energy among parent flasks
        generation_journal (list): Append-only journal for precursor energies
        energy_change (float): Smallest energy change from precursor
        error (str): Error description
        rules_applied (list): List of reaction rules applied

    Examples:
        >>> flask1 = Flask(['[CH3-]', '[CH3]', '[CH3+]'], chemistry=['methyl'])
        >>> print flask1
        [CH3+].[CH3-].[CH3] (WBCXBGDXOGTVKX-UHFFFAOYSA-N-IBOPVQJNMH/675)
        >>> print flask1.sumformula
        C3H9
        >>> print flask1.priority
        675
        >>> print flask1.chemistry
        ['methyl']
        >>> print flask1.rules_applied
        []
        >>> flask2 = Flask('[C]=O.O.O.C')
        >>> print flask2
        [C]=O.C.O.O (NDOIIPYSKJDDJJ-UHFFFAOYSA-N-TKUJRVBAKG/1688)
        >>> flask3 = Flask('O.C.[C]=O.O')
        >>> print flask3
        [C]=O.C.O.O (NDOIIPYSKJDDJJ-UHFFFAOYSA-N-TKUJRVBAKG/1688)
        >>> flask4 = Flask(['[N]=O', '[O-]N=O', '[CH3]'])
        >>> print flask4
        O=N[O-].[N]=O.[CH3] (RPWSXAJCTKRGAR-UHFFFAOYSA-M-YZSIFGFUHE/3241)
        >>> print flask4.charge
        -1
        >>> print flask4.mult
        3
        >>> print flask4.priority
        3241
        >>> flask5 = Flask(['[N]=O', '[O-]N=O', '[CH3]'], generation=2)
        >>> print flask5.priority
        3641
        >>> flask6 = Flask(['[N]=O', '[O-]N=O', '[CH3]'], attempts=5)
        >>> print flask6.priority
        3366
        >>> flask7 = Flask.fromdict(
        ...     {'a': [{'1': '[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]',
        ...             'p': 'MPGPVKBZDNHNFN-UHFFFAOYSA-N-VGQKNXIBNQ',
        ...             'r': 'XCWZRNYOZMDZKY-UHFFFAOYSA-N-DTHNXTGSVV'}],
        ...      'b': [{'1': '[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]',
        ...             'p': 'XCWZRNYOZMDZKY-UHFFFAOYSA-N-DTHNXTGSVV',
        ...             'r': 'XTELGNPIQSNRGM-UHFFFAOYSA-M-VAZDFSBNPQ'}],
        ...      's': 'C[OH+]C.CO.[OH-]',
        ...      '1@': ['[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]']})
        >>> print flask7.incoming[0]
        XTELGNPIQSNRGM-UHFFFAOYSA-M-VAZDFSBNPQ>>XCWZRNYOZMDZKY-UHFFFAOYSA-N-DTHNXTGSVV
        >>> print flask7.outgoing[0]
        XCWZRNYOZMDZKY-UHFFFAOYSA-N-DTHNXTGSVV>>MPGPVKBZDNHNFN-UHFFFAOYSA-N-VGQKNXIBNQ
        >>> print flask7.rules_applied
        ['[C+0:1]-[#1+0:2]>>[C-1:1].[#1+1:2]']
        >>> flask8 = Flask(smiles='CO.')
        Traceback (most recent call last):
        FlaskError: Empty SMILES string in input

    """

    #
    # Flask class variables
    #

    # Minimal attributes
    _attr_min = ['key']

    # Structure attributes
    _attr_struct = ['smiles', 'molecules', 'incoming', 'outgoing']

    # Dependent attributes (read-only)
    _attr_dep = ['inchi', 'inchikey', 'smileskey', 'sumformula']

    # Variable attributes (writable, computed default)
    _attr_var = ['charge', 'mult', 'priority', 'chemistry', 'ts', 'etag',
                 'energy', 'status', 'tags', 'attempts', 'generation',
                 'generation_journal', 'precursor', 'precursor_journal',
                 'energy_change', 'error', 'quality', 'rules_applied']

    #
    # Flask internal methods
    #

    # SMILES string and incoming/outgoing transformations
    def _set_smiles_trans(self, smiles=None, incoming=None, outgoing=None):

        # SMILES string empty
        if not smiles:
            raise FlaskError('Empty SMILES string in input')

        # Acccept period-separated SMILES strings
        if isinstance(smiles, basestring):
            smiles = smiles.split('.')

        # No empty SMILES strings allowed
        if '' in smiles:
            raise FlaskError('Empty SMILES string in input')

        # Set formulae for the constituent molecules
        try:
            self.molecules = sorted(Formula(smiles=s) for s in smiles)
        except MoleculeError:
            raise FlaskError('Invalid SMILES string in input')

        # Set SMILES string for the total flask
        self._smiles = '.'.join(mol.smiles for mol in self.molecules)

        # Sanitize and save as RDKit molecule
        self._rdmol = Chem.MolFromSmiles(self._smiles, sanitize=True)

        # Raise exception
        if self._rdmol is None:
            raise FlaskError('Reading SMILES string failed')

        # Set incoming and outgoing transformations
        if incoming is None:
            incoming = []
        self.incoming = [Transformation(
            **{t2l[k]: v for k, v in trans.items()}) for trans in incoming]
        if outgoing is None:
            outgoing = []
        self.outgoing = [Transformation(
            **{t2l[k]: v for k, v in trans.items()}) for trans in outgoing]

    # INChI key
    def _set_key(self):
        self._key = self.inchikey + '-' + self.smileskey

    # Lazy computation of variable attributes
    def _attr_compute(self, attr):

        # Compute default values
        if attr == 'inchi':
            return Chem.MolToInchi(self._rdmol)
        elif attr == 'inchikey':
            return Chem.InchiToInchiKey(Chem.MolToInchi(self._rdmol))
        elif attr == 'smileskey':
            return controlhash(self.smiles)
        elif attr == 'sumformula':
            return AllChem.CalcMolFormula(self._rdmol)
        elif attr == 'charge':
            return AllChem.GetFormalCharge(self._rdmol)
        elif attr == 'mult':
            return int(Descriptors.NumRadicalElectrons(self._rdmol)) + 1
        elif attr == 'priority':
            return (sum(
                mol.priority ** 2 for mol in self.molecules) +
                100 * self.generation ** 2 + self.attempts ** 3)
        elif attr == 'quality':
            return 0
        elif attr == 'chemistry':
            return set().union(mol.chemistry for mol in self.molecules)
        elif attr == 'ts':
            return datetime.datetime.utcnow()
        elif attr == 'etag':
            return ''
        elif attr == 'energy':
            return None
        elif attr == 'status':
            return Status.INITIAL
        elif attr == 'tags':
            return []
        elif attr == 'generation':
            return 0
        elif attr == 'generation_journal':
            return [0]
        elif attr == 'attempts':
            return 0
        elif attr == 'precursor':
            return None
        elif attr == 'precursor_journal':
            return []
        elif attr == 'energy_change':
            return None
        elif attr == 'error':
            return ''
        elif attr == 'rules_applied':
            return []
        else:
            return

    #
    # Flask magic methods
    #

    def __init__(self, smiles=None, incoming=None, outgoing=None, **kwargs):

        # Set SMILES string and incoming/outgoing transformations
        self._set_smiles_trans(smiles, incoming, outgoing)

        # Set INChI key
        self._set_key()
        if not hasattr(self, 'key'):
            raise FlaskError('Cannot compute flask key')

        # Set variable arguments
        for attr, value in kwargs.iteritems():
            if attr in self._attr_var:
                setattr(self, attr, value)

        # Set creation timestamp
        if 'ts' not in kwargs:
            self.ts = datetime.datetime.utcnow()

    def __getattr__(self, attr):

        # Field names, needed to stop infinite recursion
        if attr[0] == '_':
            raise AttributeError

        # Computable attributes
        else:

            # Corresponding field name
            field = '_' + attr

            # Minimal or structure attributes
            if attr in self._attr_min or attr in self._attr_struct:
                return getattr(self, field)

            # Dependent attributes
            elif attr in self._attr_dep:
                return self._attr_compute(attr)

            # Variable attributes
            elif attr in self._attr_var:
                try:
                    return getattr(self, field)
                except AttributeError:
                    value = self._attr_compute(attr)
                    setattr(self, field, value)
                    return value

            else:
                raise AttributeError

    def __setattr__(self, attr, value):

        # Field names are just set
        if attr[0] == '_':
            self.__dict__[attr] = value

        # Computable attributes
        else:

            # Corresponding field name
            field = '_' + attr

            # Minimal or dependent attributes cannot be set
            if attr in self._attr_min or attr in self._attr_dep:
                raise AttributeError

            # Structure and variable attributes
            elif attr in self._attr_struct or attr in self._attr_var:
                setattr(self, field, value)

            else:
                raise AttributeError

    def __delattr__(self, attr):

        # Field names are directly deleted
        if attr[0] == '_':
            del self.__dict__[attr]

        # Computable attributes
        else:

            # Corresponding field name
            field = '_' + attr

            # Minimal or dependent attributes cannot be deleted
            if attr in self._attr_min or attr in self._attr_dep:
                raise AttributeError

            # Variable attributes, returns to computed default value
            elif attr in self._attr_struct or attr in self._attr_var:
                delattr(self, field)

            else:
                raise AttributeError

    def __str__(self):
        return '%s (%s/%d)' % (self.smiles, self.key, self.priority)

    # Equality
    def __eq__(self, other):
        return self.key == other.key

    # Inequality
    def __ne__(self, other):
        return self.key != other.key

    def __lt__(self, other):
        return self.key < other.key

    def __le__(self, other):
        return self.key <= other.key

    def __gt__(self, other):
        return self.key > other.key

    def __ge__(self, other):
        return self.key >= other.key

    def __hash__(self):
        """Key is used for hashing."""
        return hash(self.key)

    #
    # Flask public methods
    #

    @classmethod
    def fromdict(cls, inp):
        """Flask constructor from dict representation.

        Args:
            inp (dict): Flask dict representation

        Examples:
            >>> flask = Flask.fromdict({"smiles": ["[C]=O", "C"]})
            >>> print flask
            [C]=O.C (RVYIIQVVKDJVBA-UHFFFAOYSA-N-POZGZFAWVS/1040)
        """
        return cls(**{t2l[k]: v for k, v in inp.items()})

    def todict(self, export_struct=True, export_dep=False,
               export_var=True, export_default=True, exclude=[]):
        """
        Convert Flask object to JSON representation.

        Args:
            export_struct (bool, optional): Export structure attributes
            export_dep (bool, optional): Export dependent attributes
            export_var (bool, optional): Export variable attributes
            export_default (bool, optional): Export default variable values
            exclude (list, optional): Keys to exclude from export

        Returns:
            dict: Flask in dict representation

        Examples:
            >>> outp = Flask(smiles=['O=C=O', 'C']).todict()
            >>> print outp # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
             {'a': [], 'b': [], 'd': 2192, '1@': [], 'k':
             [{'i': None, 's': 'O=C=O',
               '_id': 'CURLTUGMZLYLDI-UHFFFAOYSA-N-QVJQPNRXRU'},
              {'i': None, 's': 'C',
               '_id': 'VNWKTOKETHGBQD-UHFFFAOYSA-N-JOHIQMXCOB'}],
              'j': 2000, 'm': 1, '3': 0, 'q': 0, '3*': [0], 's': 'O=C=O.C',
              '2': '', '5': '', '4': ..., '0*': [], '8': 0,
              '_id': 'KDRIEERWEFJUSB-UHFFFAOYSA-N-TTYXZLGDPJ',
              '+': 0, 't': []}
        """

        # List of exclusions
        if isinstance(exclude, basestring):
            exclude = [exclude]

        # Export data dict
        data = {}

        # Set key to key
        if 'key' not in exclude:
            data[t2s['key']] = self.key

        # Add dependent attributes
        if export_dep:
            for attr in self._attr_dep:
                if attr in exclude:
                    continue
                value = copy(getattr(self, attr, None))
                if value is not None:
                    data[t2s[attr]] = value

        # Add structure attributes
        if export_struct:
            for struct in self._attr_struct:
                if struct in exclude:
                    continue
                if hasattr(self, struct):
                    value = getattr(self, struct)
                    if isinstance(value, list):
                        data[t2s[struct]] = [s.todict(
                            export_var=False, export_key=True) for s in value]
                    else:
                        data[t2s[struct]] = value

        # Add variable attributes
        if export_var:
            for attr in self._attr_var:
                if attr in exclude:
                    continue
                value = copy(getattr(self, attr, None))
                if value is not None:
                    # Export all values
                    if export_default:
                        data[t2s[attr]] = value
                    # Only export non-default values
                    else:
                        value_default = self._attr_compute(attr)
                        if value != value_default:
                            data[t2s[attr]] = value

        return data

    def copy(self, **kwargs):
        """Make deep copy of Flask object.

        Args:
            **kwargs: See todict() methods

        Returns:
            Flask: Deep copy of current object

        Examples:
            >>> flask1 = Flask(smiles=['O', 'CO', 'CCO'],
            ...                chemistry=['water', 'alcohol'],
            ...                quality=1)
            >>> print flask1
            CCO.CO.O (VLOVSFJPGNJHMU-UHFFFAOYSA-N-IMGARWUFOL/3464)
            >>> flask2 = flask1.copy()
            >>> print flask1 == flask2
            True
            >>> print flask1 is flask2
            False
            >>> print flask1.todict()
            ...     # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
            {'a': [], 'b': [], 'd': 3464, '1@': [], 'k':
            [{'i': None, 's': 'CCO',
              '_id': 'LFQSCWFLJHTTHZ-UHFFFAOYSA-N-XUDWUARODM'},
             {'i': None, 's': 'CO',
              '_id': 'OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB'},
             {'i': None, 's': 'O',
              '_id': 'XLYOFNOQVPJJNP-UHFFFAOYSA-N-GUDGVOUDOU'}],
             'z': ['water', 'alcohol'], 'm': 1, '3': 0, 'j': 2000, 'q': 0,
             '3*': [0], 's': 'CCO.CO.O', '2': '', '5': '', '4': ..., '0*': [],
             '8': 0, '_id': 'VLOVSFJPGNJHMU-UHFFFAOYSA-N-IMGARWUFOL',
             '+': 1, 't': []}
        """

        return self.fromdict(self.todict(**kwargs))

    def update(self, other):
        """Update Flask with data from other object.

        Args:
            other (Flask/dict): Flask to use

        Raises:
            FlaskError: Invalid Flask input

        Examples:
            >>> flask1 = Flask(smiles=['C=O', 'O'])
            >>> print flask1
            C=O.O (MGJURKDLIJVDEO-UHFFFAOYSA-N-UNAGLVYAXJ/1224)
            >>> flask2 = Flask(smiles=['O', 'C=O'], priority=1400,
            ...                chemistry=['oxide', 'inorganic'])
            >>> flask1.update(flask2)
            >>> print flask1
            C=O.O (MGJURKDLIJVDEO-UHFFFAOYSA-N-UNAGLVYAXJ/1400)
            >>> print flask1.chemistry
            ['oxide', 'inorganic']
            >>> print flask1.priority
            1400
            >>> flask3 = Flask(smiles=['C=O', 'N'])
            >>> flask1.update(flask3)
            Traceback (most recent call last):
            FlaskError: Update cannot change key
        """

        if isinstance(other, Flask):
            pass
        elif isinstance(other, dict):
            other = Flask.fromdict(other)
        else:
            raise FlaskError('Invalid flask input in update')

        # Check if key changes
        if self.key != other.key:
            raise FlaskError('Update cannot change key')

        # Overwrite old variable attributes with new ones
        for attr in other._attr_var:
            try:
                setattr(self, attr, (getattr(other, attr)))
            except AttributeError:
                pass


class Transformation(object):
    """Transformation between flasks.

        Attributes:
            reactant (Flask): Reactant flask
            product (Flask): Product flask
            rule (str): Reaction rule as SMARTS string

        Examples:
            >>> trans = Transformation(
            ...          reactant='COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF',
            ...          product='DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND',
            ...          rule='CO.CO>>COC.O')
            >>> print trans
            COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF>>DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND
            >>> print trans.reactant
            COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF
            >>> print hash(trans)
            8308792969227272020
            >>> trans.rule = 'CCO>>C=C.O'
            Traceback (most recent call last):
            AttributeError: can't set attribute
            >>> print trans.rule
            CO.CO>>COC.O
        """

    def __init__(self, reactant, product, rule):
        """Transformation object constructor."""
        self._reactant = reactant
        self._product = product
        self._rule = rule

    @property
    def reactant(self):
        return self._reactant

    @property
    def product(self):
        return self._product

    @property
    def rule(self):
        return self._rule

    @property
    def key(self):
        return self.reactant + '>>' + self.product

    def __str__(self):
        return self.key

    def __eq__(self, other):
        return self.key == other.key

    def __ne__(self, other):
        return self.key != other.key

    def __lt__(self, other):
        return self.key < other.key

    def __le__(self, other):
        return self.key <= other.key

    def __gt__(self, other):
        return self.key > other.key

    def __ge__(self, other):
        return self.key >= other.key

    def __hash__(self):
        return hash(self.key)

    @classmethod
    def fromdict(cls, inp):
        """Transformation constructor from dict representation.

        Args:
            inp (dict): Transformation dict representation

        Returns:
            Transformation: New Tranformation object

        Examples:
            >>> inp = Transformation(
            ... reactant='COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF',
            ... product='DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND',
            ... rule='CO.CO>>COC.O').todict()
            >>> print inp  # doctest: +NORMALIZE_WHITESPACE
            {'1': 'CO.CO>>COC.O',
             'p': 'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND',
             'r': 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF'}
        """
        return cls(**{t2l[k]: v for k, v in inp.items()})

    def todict(self, **kwargs):
        """Export Transformation to dict representation.

        Args:
            **kwargs: Additional options (absorbs unsuitable args)

        Returns:
            outp (dict): Tranformation in dict representation

        Examples:
            >>> trans = Transformation.fromdict(
            ...          {"reactant": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF",
            ...           "product": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
            ...           "rule":  "CO.CO>>COC.O"})
            >>> print trans
            COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF>>DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND
        """

        # Export data dict
        data = {}

        # Add reactant, product, rule
        for attr in ['reactant', 'product', 'rule']:
            data[t2s[attr]] = getattr(self, attr)

        return data

    def copy(self, **kwargs):
        """Make deep copy of Transformation object.

        Args:
            **kwargs: See todict() method

        Returns:
            Transformation: Deep copy of current object

        Examples:
            >>> trans = Transformation.fromdict(
            ...          {"reactant": "COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF",
            ...           "product": "DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND",
            ...           "rule":  "CO.CO>>COC.O"})
            >>> trans1 = trans.copy()
            >>> trans1 == trans
            True
            >>> trans1 is trans
            False
            >>> print trans1
            COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF>>DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND
        """
        return self.fromdict(self.todict(**kwargs))
