"""Storage classes and interfaces for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    StorageClient: Base storage client
    Storage: Base storage interface
    FileStorage: Base file storage interface
    QueryRequest: Query request information for Storage class
    UpdateRequest: Update request information for Storage class
    UpdateLock: Update lock information for Storage class interactions
    QueryCursor: Query cursor object for Storage class reads
"""

__all__ = ['StorageClient', 'Storage', 'FileStorage', 'QueryRequest',
           'UpdateRequest', 'UpdateLock', 'QueryCursor']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '05/30/2015'


class StorageClient(object):
    def __init__(self, client=None, host='localhost', port=80,
                 **kwargs):
        raise NotImplementedError

    def close(self):
        """Close connection to storage client."""
        raise NotImplementedError

    def storage(self, **kwargs):
        """Create new Storage instance."""
        raise NotImplementedError

    def query_request(self, **kwargs):
        """Create new QueryRequest instance."""
        raise NotImplementedError

    def update_request(self, **kwargs):
        """Create new UpdateRequest instance."""
        raise NotImplementedError

    def update_lock(self, **kwargs):
        """Create new UpdateLock instance."""
        raise NotImplementedError


class Storage(object):
    """Base storage interface."""

    def __init__(self, **kwargs):
        raise NotImplementedError

    def find(self, request, lock=None, **kwargs):
        """Retrieve records from storage. Should allow for pessimistic and
        optimistic offline locks.

        Args:
            request (QueryRequest): Query specification
            lock (UpdateLock, optional): Lock specifications
            kwargs: Additional options

        Returns:
            cursor (QueryCursor): Iterator over retrieved items
        """
        raise NotImplementedError

    def update(self, requests, lock=None, multi=False, **kwargs):
        """Update records in storage.

        Args:
            requests (list): List of UpdateRequest objects
            lock (UpdateLock, optional): Lock specifications
            multi (bool, optional): Apply to multiple reocrds
            kwargs: ADditional options

        Returns:
            result (QueryResult): Summary of query results
        """
        raise NotImplementedError

    def delete(self, requests, multi=False, **kwargs):
        """Delete records from storage.

        Args:
            requests (list): List of QueryRequest objects
            multi (bool, optional): Apply to multiple records
            kwargs: Additional options
        """
        raise NotImplementedError

    def clear(self):
        raise NotImplementedError


class FileStorage(object):
    """Base file storage interface."""

    def __init__(self, **kwargs):
        raise NotImplementedError

    def get(self, key):
        raise NotImplementedError

    def set(self, key, value):
        raise NotImplementedError

    def delete(self, key):
        raise NotImplementedError


class QueryRequest(object):
    """Query request information for Storage class."""

    def __init__(self, query=None, projection=None, limit=None, sort=None,
                 **kwargs):
        raise NotImplementedError


class UpdateRequest(object):
    """Update request information for Storage class."""

    def __init__(self, query=None, add=None, remove=None, union=None,
                 intersection=None, overwrite=True, **kwargs):
        """UpdateRequest object constructor.

        Args:
            query (dict, optional): Update query
            add (dict, optional): Entries to add
            remove (dict, optional): Entries to remove
            union (dict, optional): Entries to add by union
            intersection (dict, optional): Entries to add by intersection
            overwrite (bool, optional): Overwrite flag
            **kwargs: Additional options
        """
        raise NotImplementedError


class UpdateLock(object):
    """Update lock information for Storage class interactions.
    Allows for pessimistic and/or optimistic offline locking.

    Pessimistic locking reserves items by applying the update
    in the lock argument before execution. Upon success, the
    update given by the unlock argument is applied.

    Optimistic locking retrieves items for execution without
    exclusive locking. It compares the key and the timestamp
    of the successful result to the database and rejects update
    if they differ.
    """

    def __init__(self, optimistic=True, pessimistic=False,
                 lock=None, **kwargs):
        """UpdateLock object constructor.

        Args:
            optimistic (bool, optional): Optimistic locking
            pessimistic (bool, optional): Pessimistic locking
            lock (dict, optional): Locking update
            **kwargs: Additional options
        """
        raise NotImplementedError


class QueryCursor(object):
    """Query cursor object for Storage class reads."""

    def __init__(self, cursor, **kwargs):
        """QueryCursor object constructor

        Args:
            cursor (QueryCursor): Cursor to clone
            **kwargs: Additional options
        """
        raise NotImplementedError
