"""Cache implementation using Memcached backend.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    MemcachedClient: Cache client wrapper for Memcached
    MemcachedCache: Cache interface using Memcached
"""

__all__ = ['MemcachedClient', 'MemcachedCache']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.7'
__date__ = '06/09/2015'

#
# Standard library
#

import socket

#
# Third-party modules
#

try:
    from pymemcache import client as pymemcache_client
except ImportError:
    pymemcache_client = None


#
# colibri submodules
#

from colibri.cache import CacheClient, Cache
from colibri.exceptions import ConnectionError, ExecutionError


class MemcachedClient(CacheClient):

    def __init__(self, client=None, host='localhost', port=11211,
                 **kwargs):
        """Only stores location of a cache instance, does not open it.

        Args:
            client (MemcachedClient, optional): Client to clone
            host (str, optional): Cache hostname
            port (int, optional): Cache port

        Raises:
            ConnectionError: Connection failure
        """

        # Raise exception if no client is available
        if pymemcache_client is None:
            raise ExecutionError('No Memcached client found')

        # Hostname
        self._host = host

        # Port
        self._port = port

        # Clone cache hostname and port
        if client is not None:
            self._host = client._host
            self._port = client._port

        # Test cache connection
        try:
            cache = pymemcache_client.Client(server=(self._host, self._port))
            cache.stats()
        except socket.error:
            raise ConnectionError('Cannot connect to Memcached server')

    def __str__(self):
        return 'MemcachedClient %s:%s' % (self._host, self._port)

    def close(self):
        pass

    def stats(self):
        cache = pymemcache_client.Client(server=(self._host, self._port))
        return cache.stats()

    def cache(self, **kwargs):
        """Create new MemcachedCache instance."""

        return MemcachedCache(client=self, **kwargs)


class MemcachedCache(Cache):

    def __init__(self, client=None, host='localhost', port=11211,
                 key_prefix='ns', expire=300, **kwargs):
        """Cache instance in Memcached.

        Parameters:
            client: Memcached client, optional
            host (str, optional): Database hostname (default: 'localhost')
            port (int, optional): Database port (default: 11211)
            key_prefix (str, optional): Key prefix (default: 'ns')
            expire (int, optional): Expiration time (in s, default: 300s)
        """

        # Raise exception if no client is available
        if pymemcache_client is None:
            raise ExecutionError('No Memcached client found')

        # Clone MemcachedClient instance
        if client is not None:
            self._client = client

            # Hostname
            self._host = client._host

            # Port
            self._port = client._port

        # Open new MemcachedClient instance
        else:
            self._client = MemcachedClient(host=host, port=port, **kwargs)

            # Hostname
            self._host = host

            # Port
            self._port = port

        # Key prefix
        self._key_prefix = key_prefix

        # Open new Memcached connection
        self._cache = pymemcache_client.Client(
            server=(self._host, self._port), key_prefix=key_prefix,
            **kwargs)

        # Expiration time
        self._expire = expire

    def __str__(self):
        return 'MemcachedCache %s:%d/%s (TTL=%ds)' % (
            self._host, self._port, self._key_prefix, self._expire)

    def close(self):
        """Close the connection to Memcached client."""
        self._cache.close()

    def get(self, keys):
        """Retrieves one or more values from cache by keys.

        Args:
            keys (str/list): Keys to return

        Returns:
            items: Key-value dictionary
        """

        if isinstance(keys, basestring):
            return self._cache.get(keys)
        else:
            return self._cache.get_many(keys)

    def set(self, items):
        """Submits one or more key-value pairs to cache.

        Args:
            items (tuple/dict): Key-value to add
        """

        if isinstance(items, tuple):
            self._cache.set(items[0], items[1], expire=self._expire)
        else:
            self._cache.set_many(items, expire=self._expire)

    def set_blocking(self, items):
        """Submits one or more key-value pairs to cache and
        waits for a server reply.

        Args:
            items (tuple): Key-value to add
        """

        if isinstance(items, tuple):
            self._cache.set(items[0], items[1], expire=self._expire,
                            noreply=False)
        else:
            self._cache.set_many(items, expire=self._expire, noreply=False)

    def delete(self, keys):
        """Deletes one or more keys from cache.

        Args:
            keys (str/list): Keys to delete
        """

        if isinstance(keys, basestring):
            self._cache.delete(keys)
        else:
            self._cache.delete_many(keys)

    def clear(self):
        """Clears the entire cache."""
        self._cache.flush_all()
