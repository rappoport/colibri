"""Library generation tasks using RDKit.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    RDKitFlaskReactionTask: Flask generation task using RDKit

"""

__all__ = ['RDKitFlaskReactionTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '06/03/2015'

#
# Library modules
#

from itertools import combinations, chain

#
# colibri submodules
#


from colibri.data import Formula, Flask, Transformation
from colibri.exceptions import MoleculeError, FlaskError, ReactionRuleError, \
    NoInput
from colibri.utils import t2s
from colibri.enums import Status, Sort, Reactivity
from colibri.task import Task
from colibri.task.rdkitlibgen import RDKitLibraryGenerator


class RDKitFlaskReactionTask(Task):
    """FlaskReactionTask using RDKit."""

    # Check options
    def _check_opts(self):

        # Batch size should not exceed number of tasks per cycle
        if self._opts['batch_size'] > self._opts['tasks_per_cycle']:
            self._context.logger.warning(
                'Reducing batch size to number of tasks per cycle')
        self._opts = self._opts.update(
            batch_size={
                'key': 'batch_size',
                'value': self._opts['tasks_per_cycle'],
                'precedence': 'command'})

    def _init_rules(self):

        # Check for react_rules options
        if not self._opts.get('react_rule'):
            self._context.logger.info('No input rules')
            raise NoInput

        # Process reaction rules
        self._react_rules = [
            rule.value for rule in self._opts.list('react_rule')]

    # Initialize library generators
    def _init_lib_gen(self):

        # Create local reactivity pattern from input
        reactivity = self._opts.get('reactivity', Reactivity.ALL)
        self._reactivity_uni = reactivity & Reactivity.UNI
        self._reactivity_cycl = reactivity & Reactivity.CYCL
        self._reactivity_bi = reactivity & Reactivity.BI

        # Exact atom mapping
        react_exact_mapping = self._opts.get('react_exact_mapping', True)
        if not react_exact_mapping:
            self._context.logger.error(
                'Exact mapping is required in RDKitFlaskReactionTask')
            self._reactivity_uni = False
            self._reactivity_cycl = False
            self._reactivity_bi = False
            return

        # Unimolecular reactions
        if self._reactivity_uni:
            self._generators_uni = []
            for rule in self._react_rules:

                try:
                    if RDKitLibraryGenerator.molecularity(rule) != 1:
                        continue

                    lib_gen = RDKitLibraryGenerator.init_generator(
                        rule, exact=react_exact_mapping)
                except ReactionRuleError as e:
                    self._context.logger.error(
                        'Invalid unimolecular rule: %s' % e)
                    continue

                self._generators_uni.append(lib_gen)
                self._context.logger.debug(
                    'Initialized%s unimolecular generator for rule %s' % (
                        ' exact' if lib_gen.exact else '', lib_gen.rule))

            # No valid unimolecular rules available
            if not self._generators_uni:
                self._reactivity_uni = False

        # Ring opening and closing reactions
        if self._reactivity_cycl:
            self._generators_cycl = []
            for rule in self._react_rules:

                try:
                    # Ring closing rules are constructed from bimolecular rules
                    # Ring opening rules are constructed from unimolecular ones
                    molecularity = RDKitLibraryGenerator.molecularity(rule)
                    num_products = RDKitLibraryGenerator.num_products(rule)
                    if not (molecularity == 1 and num_products == 2 or
                            molecularity == 2 and num_products == 1):
                        continue

                    # Check if univalent atoms are present
                    if RDKitLibraryGenerator.univalent_atoms(rule):
                        continue

                    lib_gen = RDKitLibraryGenerator.init_generator(
                        rule, exact=react_exact_mapping, cycl=True)
                except ReactionRuleError as e:
                    self._context.logger.error(
                        'Invalid cyclization rule: %s' % e)
                    continue

                self._generators_cycl.append(lib_gen)
                self._context.logger.debug(
                    'Initialized%s cyclization generator for rule %s' % (
                        ' exact' if lib_gen.exact else '', lib_gen.rule))

            # No valid cyclization rules available
            if not self._generators_cycl:
                self._reactivity_cycl = False

        # Bimolecular reactions
        if self._reactivity_bi:
            self._generators_bi = []
            for rule in self._react_rules:

                try:
                    if RDKitLibraryGenerator.molecularity(rule) != 2:
                        continue

                    lib_gen = RDKitLibraryGenerator.init_generator(
                        rule, exact=react_exact_mapping)
                except ReactionRuleError as e:
                    self._context.logger.error(
                        'Invalid bimolecular rule: %s' % e)
                    continue

                self._generators_bi.append(lib_gen)
                self._context.logger.debug(
                    'Initialized%s bimolecular generator for rule %s' % (
                        ' exact' if lib_gen.exact else '', lib_gen.rule))

            # No valid bimolecular rules available
            if not self._generators_bi:
                self._reactivity_bi = False

    # Retrieve reactive flask from storage
    def _retrieve_reactive_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['status']: Status.REACTIVE_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['status']: Status.REACTIVE}
        input_limit = self._opts['batch_size']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Input iterable
        self._input = self._context.flask_storage.find(
            input_request, self._lock)

        # Cursor is empty
        count = self._input.count()
        if count == 0:
            self._context.logger.info('No input flasks')
            raise NoInput

        self._context.logger.info(
            'Retrieved %d reactive documents, opt/pess lock: %r/%r' % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Output and rollback flask lists
        self._output = []
        self._rollback = []

        # Children flask dict
        self._flasks = {}

    # Read reactive flask from storage
    def _read_reactive_flask(self):

        # Retrieve input from cursor
        data = next(self._input)

        # Convert to Flask object
        self._data = Flask.fromdict(data)

        # Rules applied
        self._rules = {}

        # Store rollback update
        rollback = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['status']: Status.REACTIVE},
            remove={t2s['etag']: 1})
        self._rollback.append(rollback)

        self._context.logger.info('<%s>: retrieved flask' % self._data)

    # Generate unimolecular reactions
    def _prepare_formula_uni(self):

        # Check for sets of reactants and products seen
        reactants_seen = set()
        products_seen = set()

        # Reactant sum formula
        reactants_sum_formula = self._data.sumformula

        # Set rule counters to zero
        for lib_gen in self._generators_uni:
            self._rules[lib_gen.rule] = 0

        # Iterate over molecules in flask
        for index in xrange(len(self._data.molecules)):

            # Extract information about input molecules
            mol1 = self._data.molecules[index]
            smiles1 = mol1.smiles
            gen = self._data.generation + 1

            # Check if reactant has been seen
            if mol1 in reactants_seen:
                continue
            reactants_seen.add(mol1)

            # Iterate over rules
            for lib_gen in self._generators_uni:

                # Set current rule argument
                self._current_rule = lib_gen.rule

                # Iterate over sets of reaction products
                for products in lib_gen.react(smiles1):

                    # Check if tuple of products has been seen
                    if products in products_seen:
                        continue
                    products_seen.add(products)

                    # Unchanged constituent molecules
                    molecules = [mol for i, mol in enumerate(
                                 self._data.molecules) if i != index]

                    # Flask SMILES string
                    smiles = '.'.join([mol.smiles for mol in molecules] +
                                      list(products))

                    # Product flask
                    try:

                        # Add reaction products to flask
                        for product in products:
                            molecules.append(Formula(smiles=product))

                        # Create product flask
                        smiles = [mol.smiles for mol in molecules]
                        flask = Flask(smiles=smiles,
                                      generation=gen,
                                      generation_journal=[gen],
                                      precursor=self._data.energy,
                                      precursor_journal=[self._data.energy],
                                      tags=self._data.tags)

                    # Skip product if Flask constructor raises exception
                    except (MoleculeError, FlaskError) as e:
                        self._context.logger.warning(
                            'Product %s invalid: %s' % (smiles, e))
                        continue

                    # Skip product if sum formula of the flask changes
                    if flask.sumformula != reactants_sum_formula:
                        self._context.logger.warning(
                            'Product %s invalid: sum formula changes' % smiles)
                        continue

                    # Add incoming Transformation
                    flask.incoming.append(Transformation(
                        reactant=self._data.key, product=flask.key,
                        rule=self._current_rule))

                    # Increment rule counter
                    self._rules[self._current_rule] += 1

                    # Return children flask
                    self._context.logger.info(
                        '<%s>: unimolecular product' % flask)
                    yield flask

    # Generate cyclization reactions
    def _prepare_formula_cycl(self):

        # Check for sets of reactants and products seen
        reactants_seen = set()
        products_seen = set()

        # Reactant sum formula
        reactants_sum_formula = self._data.sumformula

        # Set rule counters to zero
        for lib_gen in self._generators_cycl:
            self._rules[lib_gen.rule] = 0

        # Iterate over molecules in flask
        for index in xrange(len(self._data.molecules)):

            # Extract information about input molecules
            mol1 = self._data.molecules[index]
            smiles1 = mol1.smiles
            gen = self._data.generation + 1

            # Check if reactant has been seen
            if mol1 in reactants_seen:
                continue
            reactants_seen.add(mol1)

            # Iterate over rules
            for lib_gen in self._generators_cycl:

                # Set current rule argument
                self._current_rule = lib_gen.rule

                # Iterate over sets of reaction products
                for products in lib_gen.react(smiles1):

                    # Check if tuple of products has been seen
                    if products in products_seen:
                        continue
                    products_seen.add(products)

                    # Unchanged constituent molecules
                    molecules = [mol for i, mol in enumerate(
                                 self._data.molecules) if i != index]

                    # SMILES string for debug output
                    smiles = '.'.join([mol.smiles for mol in molecules] +
                                      list(products))

                    # Product flask
                    try:

                        # Add reaction products to flask
                        for product in products:
                            molecules.append(Formula(smiles=product))

                        # Construct product flask
                        smiles = [mol.smiles for mol in molecules]
                        flask = Flask(smiles=smiles,
                                      generation=gen,
                                      generation_journal=[gen],
                                      precursor=self._data.energy,
                                      precursor_journal=[self._data.energy],
                                      tags=self._data.tags)

                    # Skip product if Flask constructor raises exception
                    except (MoleculeError, FlaskError) as e:
                        self._context.logger.warning(
                            'Product %s invalid: %s' % (smiles, e))
                        continue

                    # Skip product if sum formula of the flask changes
                    if flask.sumformula != reactants_sum_formula:
                        self._context.logger.warning(
                            'Product %s invalid: sum formula changes' % smiles)
                        continue

                    # Add incoming Transformation
                    flask.incoming.append(Transformation(
                        reactant=self._data.key, product=flask.key,
                        rule=self._current_rule))

                    # Increment rule counter
                    self._rules[self._current_rule] += 1

                    # Return children flask
                    self._context.logger.info(
                        '<%s>: cyclization product' % flask)
                    yield flask

    # Generate bimolecular reactions
    def _prepare_formula_bi(self):

        # Check for sets of reactants and products seen
        reactants_seen = set()
        products_seen = set()

        # Reactant sum formula
        reactants_sum_formula = self._data.sumformula

        # Set rule counters to zero
        for lib_gen in self._generators_bi:
            self._rules[lib_gen.rule] = 0

        # Loop over all pairs of molecules, including order
        for index1, index2 in combinations(
                xrange(len(self._data.molecules)), 2):

            # Extract information about input molecules
            mol1 = self._data.molecules[index1]
            mol2 = self._data.molecules[index2]
            smiles1 = mol1.smiles
            smiles2 = mol2.smiles
            gen = self._data.generation + 1

            # mol1 <= mol2 ensured by Flask constructor
            reactants = tuple([mol1, mol2])

            # Check if reactants have been seen
            if reactants in reactants_seen:
                continue
            reactants_seen.add(reactants)

            # Iterate over rules
            for lib_gen in self._generators_bi:

                # Set current rule argument
                self._current_rule = lib_gen.rule

                # Iterate over sets of reaction products
                for products in chain(
                        lib_gen.react(smiles1, smiles2),
                        lib_gen.react(smiles2, smiles1)):

                    # Check if tuple of products has been seen
                    if products in products_seen:
                        continue
                    products_seen.add(products)

                    # Unchanged constituent molecules
                    molecules = [mol for i, mol in enumerate(
                        self._data.molecules) if i != index1 and i != index2]

                    # SMILES string for debug output
                    smiles = '.'.join([mol.smiles for mol in molecules] +
                                      list(products))

                    # Product flask
                    try:

                        # Add reaction products to flask
                        for product in products:
                            molecules.append(Formula(smiles=product))

                        # Construct product flask
                        flask = Flask(smiles=[mol.smiles for mol in molecules],
                                      generation=gen,
                                      generation_journal=[gen],
                                      precursor=self._data.energy,
                                      precursor_journal=[self._data.energy],
                                      tags=self._data.tags)

                    # Skip product if Flask constructor raises exception
                    except (MoleculeError, FlaskError) as e:
                        self._context.logger.warning(
                            'Product %s invalid: %s' % (smiles, e))
                        continue

                    # Skip product if sum formula of the flask changes
                    if flask.sumformula != reactants_sum_formula:
                        self._context.logger.warning(
                            'Product %s invalid: sum formula changes' % smiles)
                        continue

                    # Add incoming Transformation
                    flask.incoming.append(Transformation(
                        reactant=self._data.key, product=flask.key,
                        rule=self._current_rule))

                    # Increment rule counter
                    self._rules[self._current_rule] += 1

                    # Return children flask
                    self._context.logger.info(
                        '<%s>: bimolecular product' % flask)
                    yield flask

    # Generator routine producing data instances
    def _prepare_formula(self):

        if self._reactivity_uni:
            for data in self._prepare_formula_uni():
                yield data

        if self._reactivity_cycl:
            for data in self._prepare_formula_cycl():
                yield data

        if self._reactivity_bi:
            for data in self._prepare_formula_bi():
                yield data

    # Write flask output
    def _write_flask_doc(self):

        # Iterate over all product flasks
        for output_data in self._prepare_formula():

            # Add Transformation to the outgoing list
            trans = Transformation(
                reactant=self._data.key, product=output_data.key,
                rule=self._current_rule)
            self._data.outgoing.append(trans)

            # Add update requests for children flasks
            update = self._context.mongoclient.update_request(
                query={t2s['key']: output_data.key},
                replace={t2s['status']: output_data.status},
                add=output_data.todict(exclude=[
                    'key', 'tags', 'incoming', 'status',
                    'precursor_journal', 'generation_journal']),
                union={t2s['tags']: output_data.tags,
                       t2s['incoming']: [trans.todict()],
                       t2s['precursor_journal']:
                           output_data.precursor_journal,
                       t2s['generation_journal']:
                           output_data.generation_journal})
            self._flasks[output_data.key] = update

            self._context.logger.info('<%s>: created update' % output_data)

        # Create update request for parent flask
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['status']: Status.UNREACTIVE},
            union={
                t2s['outgoing']: [
                    ftrans.todict() for ftrans in self._data.outgoing],
                t2s['rules_applied']: sorted(self._rules.keys())},
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Task setup
    def setup(self):

        self._context.logger.info('Started task')

        # Check options
        self._check_opts()

        # Initialize rules
        self._init_rules()

        # Initialize library generators
        self._init_lib_gen()

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of input reactive documents
        self._retrieve_reactive_doc()

    # Perform the task
    def run(self):

        # Read reactive flask from storage
        self._read_reactive_flask()

        # Generate product flasks
        self._write_flask_doc()

        self._context.logger.info('Completed successfully')

    # Submit batch of output documents
    def submit(self):

        # Submit children flasks to storage
        if self._flasks:
            self._context.flask_storage.update(self._flasks.itervalues())
            self._context.logger.info(
                'Submitted %d flasks' % len(self._flasks))

        # Submit parent flask updates to storage
        self._context.flask_storage.update(self._output, self._lock)
        self._context.logger.info(
            'Submitted %d documents, opt/pess lock: %r/%r' % (
                len(self._output), self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

    # Roll back results if an exception occurs
    def rollback(self):

        # Send rollback update to storage
        self._context.flask_storage.update(self._rollback, self._lock)
        self._context.logger.warning(
            'Rolled back %d documents' % len(self._rollback))
