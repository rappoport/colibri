"""Task classes for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    Task: Base class for tasks

"""

__all__ = ['Task']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/01/2015'

#
# Classes
#


class Task(object):
    """Base Task object, needs to be subclassed for specific tasks.

    Subclasses need to provide an implementation of the run() method
    and optionally of setup(), retrieve(), rollback(), submit(),
    pause(), and cleanup() methods.
    """

    def __init__(self, context, opts):

        # Task context
        self._context = context

        # Task options
        self._opts = opts

    def __str__(self):
        """Return string representation.

        Returns:
            str: Task type as string
        """
        return type(self).__name__

    def setup(self):
        """Perform setup work for the task."""
        pass

    def retrieve(self):
        """Retrieve input data for the task."""
        pass

    def run(self):
        """
        Perform the task and report the result:

        - Task success: empty return
        - Task failed, retry: raise TaskWorkflow (or derived) exception
        - Task failed, no retry: raise TaskError (or derived) exception
        - Non-iterating tasks/Queue exhausted: raise StopIteration

        Subclasses must define a specific implementation.

        """
        raise NotImplementedError

    def submit(self):
        """Submit the task output to storage."""
        pass

    def pause(self):
        """Optionally pause after processing task."""
        pass

    def cleanup(self):
        """Clean up resources after the task."""
        pass

    def rollback(self):
        """Roll back results if an exception occurs."""
        pass
