"""Task classes for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Attributes:
    task_names (OrderedDict): Translation from options to task class names

Classes:
    Task: Base class for tasks
    ProcessingTask: Base class for sequential tasks
    BuildTask: Create Configuration from Formula
    GeometryTask: Create Geometry from Configuration
    NudgeGeometryTask: Create Geometry from Configuration with nudge
    PropertyTask: Create Property from Geometry
    StorageTask: Base class for storage manipulation tasks
    CleanupTask: Remove calculation locks
    SuspendTask: Suspend molecules from computation
    ResumeTask: Resume computation on previously suspended molecules
    FlaskMapperTask: Map data from flask onto Molecule instances
    FlaskReducerTask: Collect molecular data into flask
    FlaskCleanupTask: Remove calculation locks, restarts UNAVAILABLE flasks
    FlaskSuspendTask: Suspend flasks from computational queues
    FlaskResumeTask: Resume computation on previously suspended flasks
    OpenBabelBuildTask: Build task using OpenBabel
    RDKitBuildTask: Build task using RDKit
    RDKitReactionTask: Molecule generation task using RDKit
    RDKitFlaskReactionTask: Flask generation task using RDKit
    MOPACGeometryTask: Geometry task using MOPAC
    MOPACNudgeGeometryTask: Geometry task using MOPAC with geometry nudge
    OrcaGeometryTask: Geometry task using Orca
    OrcaPropertyTask: Property task using Orca
"""

__all__ = ['task_names',
           'Task', 'ProcessingTask', 'BuildTask', 'GeometryTask',
           'NudgeGeometryTask', 'PropertyTask', 'StorageTask', 'CleanupTask',
           'SuspendTask', 'ResumeTask', 'FlaskMapperTask', 'FlaskReducerTask',
           'FlaskCleanupTask', 'FlaskSuspendTask', 'FlaskResumeTask',
           'OpenBabelBuildTask', 'RDKitBuildTask', 'RDKitReactionTask',
           'RDKitFlaskReactionTask', 'MOPACGeometryTask',
           'MOPACNudgeGeometryTask', 'OrcaGeometryTask', 'OrcaPropertyTask']

__module__ = 'colibri.task'
__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/12/2015'

#
# Library modules
#

from collections import OrderedDict

#
# colibri submodules
#

from colibri.utils import ensure_logger

#
# Variables
#

# Translation table from option names to task class names
task_names = OrderedDict([
    ('openbabel_build', 'OpenBabelBuildTask'),
    ('rdkit_build', 'RDKitBuildTask'),
    ('mopac_geometry', 'MOPACGeometryTask'),
    ('mopac_nudge_geometry', 'MOPACNudgeGeometryTask'),
    ('orca_geometry', 'OrcaGeometryTask'),
    ('orca_property', 'OrcaPropertyTask'),
    ('rdkit_react', 'RDKitReactionTask'),
    ('flask_mapper', 'FlaskMapperTask'),
    ('flask_reducer', 'FlaskReducerTask'),
    ('rdkit_flask_react', 'RDKitFlaskReactionTask'),
    ('mol_cleanup', 'CleanupTask'),
    ('flask_suspend', 'FlaskSuspendTask'),
    ('mol_suspend', 'SuspendTask'),
    ('flask_cleanup', 'FlaskCleanupTask'),
    ('mol_resume', 'ResumeTask'),
    ('flask_resume', 'FlaskResumeTask')])

#
# Classes
#

# Base task
from colibri.task.basetask import Task

# Molecule tasks
from colibri.task.moltask import (
    ProcessingTask, BuildTask, GeometryTask, NudgeGeometryTask, PropertyTask,
    StorageTask, CleanupTask, SuspendTask, ResumeTask)

# Flask tasks
from colibri.task.flasktask import (
    FlaskMapperTask, FlaskReducerTask,
    FlaskCleanupTask, FlaskSuspendTask, FlaskResumeTask)

# OpenBabel tasks
try:
    from colibri.task.openbabeltask import OpenBabelBuildTask
except ImportError as e:
    logger = ensure_logger('colibri')
    logger.error('OpenBabel task import failed: %s' % e)
    OpenBabelBuildTask = None

# RDKit tasks
try:
    from colibri.task.rdkittask import RDKitBuildTask
    from colibri.task.rdkitreact import RDKitReactionTask
    from colibri.task.rdkitflasktask import RDKitFlaskReactionTask
except ImportError as e:
    logger = ensure_logger('colibri')
    logger.error('RDKit task import failed: %s' % e)
    RDKitBuildTask = None
    RDKitReactionTask = None
    RDKitFlaskReactionTask = None

# MOPAC tasks
from colibri.task.mopactask import MOPACGeometryTask, MOPACNudgeGeometryTask

# ORCA tasks
from colibri.task.orcatask import OrcaGeometryTask, OrcaPropertyTask
