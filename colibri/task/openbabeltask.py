"""Build tasks using OpenBabel and Pybel.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    OpenBabelBuildTask: Build task using OpenBabel
"""

__all__ = ['OpenBabelBuildTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.7'
__date__ = '06/08/2015'

#
# Third-party modules
#

import pybel
import openbabel as ob

# Shut up OpenBabel
ob.obErrorLog.SetOutputLevel(ob.obError)

#
# colibri submodules
#

from colibri.task import BuildTask
from colibri.data import Configuration
from colibri.exceptions import ExecutionError


class OpenBabelBuildTask(BuildTask):
    """BuildTask using OpenBabel."""

    # Convert formula in work doc to configuration
    def _run_configuration_builder(self):

        # Parse formula as SMILES string using OpenBabel
        pymol = pybel.readstring('smi', self._data.formula.smiles)
        pymol.make3D(steps=self._opts['configuration_steps'])
        obmol = pymol.OBMol
        obmol.Center()
        obmol.ToInertialFrame()
        obmol.SetTotalCharge(self._data.formula.charge)
        obmol.SetTotalSpinMultiplicity(self._data.formula.mult)
        xyz = pymol.write('xyz')

        if not xyz:
            raise ExecutionError('Cannot create XYZ coordinates')

        # Create Configuration object
        self._data.configuration = Configuration(
            xyz=xyz, charge=pymol.charge, mult=pymol.spin,
            index=self._data.formula.index,
            program=self._opts['configuration_program'],
            version=self._opts['configuration_version'])

        self._context.logger.info('<%s>: processed output' % self._data)
