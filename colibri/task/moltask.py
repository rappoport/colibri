"""Molecule task classes for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    ProcessingTask: Base class for sequential tasks
    BuildTask: Create Configuration from Formula
    GeometryTask: Create Geometry from Configuration
    NudgeGeometryTask: Create Geometry from Configuration with nudge
    PropertyTask: Create Property from Geometry
    StorageTask: Base class for storage manipulation tasks
    CleanupTask: Remove calculation locks
    SuspendTask: Suspend molecules from computation
    ResumeTask: Resume computation on previously suspended molecules
"""

__all__ = ['ProcessingTask', 'BuildTask', 'GeometryTask', 'NudgeGeometryTask',
           'PropertyTask', 'StorageTask', 'CleanupTask', 'SuspendTask',
           'ResumeTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/13/2015'

#
# Library modules
#

import time
import datetime

#
# colibri submodules
#

from colibri.data import Molecule
from colibri.enums import Level, Sort
from colibri.exceptions import MoleculeError, ExecutionError, \
    ValidationError, NoInput, BadChemistry, BadOptimization
from colibri.task import Task
from colibri.utils import controlhash, is_connected, t2s


class ProcessingTask(Task):
    """
    Base class for sequential tasks operating on Molecule inputs.
    BuildTask, GeometryTask, and PropertyTask are subclasses.

    """

    # Check options
    def _check_opts(self):

        # Batch size should not exceed number of tasks per cycle
        if self._opts['batch_size'] > self._opts['tasks_per_cycle']:
            self._context.logger.warning(
                'Reducing batch size to number of tasks per cycle')
        self._opts = self._opts.update(
            batch_size={
                'key': 'batch_size',
                'value': self._opts['tasks_per_cycle'],
                'precedence': 'command'})

    # Perform setup work for the task
    def setup(self):

        # Connect to storage for molecules
        self._context.logger.info('Started task')

        # Check options
        self._check_opts()

    # Submit batch of output documents
    def submit(self):

        # Submit update to storage
        self._context.mol_storage.update(self._output, self._lock)
        self._context.logger.info(
            'Submitted %d documents, opt/pess lock: %r/%r' % (
                len(self._output), self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

    # Roll back results if an exception occurs
    def rollback(self):

        # Send rollback update to storage
        self._context.mol_storage.update(self._rollback, self._lock)
        self._context.logger.warning(
            'Rolled back %d documents' % len(self._rollback))


class BuildTask(ProcessingTask):
    """
    Base BuildTask, creates Configuration from Formula.

    Needs to be subclassed for specific tasks. Specific implementations
    need to provide _run_configuration_builder() and (optionally)
    _validate_formula() and _write_formula_doc() methods.

    """

    # Retrieve batch of input formula documents
    def _retrieve_formula_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['level']: Level.FORMULA_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['level']: Level.FORMULA}
        input_limit = self._opts['batch_size']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Input iterable
        self._input = self._context.mol_storage.find(
            input_request, self._lock)

        # Cursor is empty
        count = self._input.count()
        if count == 0:
            self._context.logger.info('No input molecules')
            raise NoInput

        self._context.logger.info(
            'Retrieved %d formula documents, opt/pess lock: %r/%r' % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Output and rollback lists
        self._output = []
        self._rollback = []

    # Read input formula
    def _read_formula_output(self):

        # Retrieve input from cursor
        self._input_data = next(self._input)

        # Convert to Molecule object
        self._data = Molecule.fromdict(self._input_data)

        # Store rollback update
        rollback = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['level']: Level.FORMULA},
            remove={t2s['configuration']: 1, t2s['etag']: 1})
        self._rollback.append(rollback)

        # Increase number of attempts
        self._data.attempts += 1

        self._context.logger.info('<%s>: retrieved molecule' % self._data)

    # Validate structure formula
    def _validate_formula(self):
        pass

    # Specific implementation
    # Convert formula in work doc to configuration
    def _run_configuration_builder(self):
        raise NotImplementedError

    # Validate configuration output with regard to fragmentation/rearrangement
    def _validate_configuration_output(self):

        # Compare atomic compositions
        formula_atomkey = self._data.formula.atomkey
        configuration_atomkey = self._data.configuration.atomkey

        # Raise exception if sum formula changes
        if formula_atomkey != configuration_atomkey:
            self._context.logger.warning(
                '<%s>: stoichiometry change' % self._data)
            raise ValidationError('Stoichiometry change')

        # Check if connectivities are available
        formula_parts = self._data.formula.inchi.count('/')
        configuration_parts = self._data.configuration.inchi.count('/')

        # Compare connectivities
        if formula_parts > 1 and configuration_parts > 1:

            # Compare composition and connectivity
            formula_comp, formula_conn = (
                self._data.formula.inchi.split('/')[1:3])
            configuration_comp, configuration_conn = (
                self._data.configuration.inchi.split('/')[1:3])

            # Check adjacency if formula or connectivity changes in INChI
            if ('.' in configuration_comp or
                configuration_comp[0] in '1234567890' or
                    formula_conn != configuration_conn):
                if not is_connected(self._data.configuration.adjacency):
                    self._context.logger.warning(
                        '<%s>: fragmentation' % self._data)
                    raise ValidationError('Fragmentation')

                formula_adjkey = self._data.formula.adjkey
                configuration_adjkey = self._data.configuration.adjkey
                if formula_adjkey != configuration_adjkey:
                    self._context.logger.warning(
                        '<%s>: rearrangement' % self._data)
                    raise ValidationError('Rearrangement')

        self._context.logger.info('<%s>: validated output' % self._data)

    # Output formula error document if read fails
    def _write_formula_error_doc(self, level):

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._input_data[t2s['key']],
                   t2s['etag']: self._input_data[t2s['etag']]},
            replace={t2s['level']: level,
                     t2s['ts']: datetime.datetime.utcnow()},
            remove={t2s['configuration']: 1, t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s/%d>: created error update' % (
            self._input_data[t2s['key']], self._input_data[t2s['priority']]))

    # Output formula document
    def _write_formula_doc(self, level):

        # Set new level
        self._data.level = level

        # Set external tag
        self._data.etag = self._data.ts

        # Set timestamp
        self._data.ts = datetime.datetime.utcnow()

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key,
                   t2s['ts']: self._data.etag},
            replace={t2s['level']: self._data.level,
                     t2s['ts']: self._data.ts,
                     t2s['attempts']: self._data.attempts},
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Output configuration document
    def _write_configuration_doc(self, level, reset_attempts=False):

        # Set new level
        self._data.level = level

        # Set external tag
        self._data.etag = self._data.ts

        # Set timestamp
        self._data.ts = datetime.datetime.utcnow()

        # Reset number of attempts
        if reset_attempts:
            self._data.attempts = 0

        # Replace updates
        update_replace = {t2s['level']: self._data.level,
                          t2s['ts']: self._data.ts,
                          t2s['attempts']: self._data.attempts}

        # Add configuration data if available
        if getattr(self._data, 'configuration', None) is not None:
            update_replace[
                t2s['configuration']] = self._data.configuration.todict()

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key,
                   t2s['ts']: self._data.etag},
            replace=update_replace,
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of input formula documents
        self._retrieve_formula_doc()

    # Perform the task
    def run(self):

        try:

            # Read formula output from storage
            self._read_formula_output()

            # Validate structure formula
            self._validate_formula()

            # Run configuration builder
            self._run_configuration_builder()

            # Validate configuration output
            if self._opts.get('configuration_validation', False):
                self._validate_configuration_output()

        # Bad chemistry
        except BadChemistry as e:
            self._write_formula_doc(Level.FORMULA_INVALID)
            self._context.logger.warning('Bad chemistry')
            raise

        # Execution failed
        except (MoleculeError, ExecutionError) as e:
            if not getattr(self, '_data', None):
                self._write_formula_error_doc(Level.FORMULA_ERROR)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            elif self._data.attempts < self._opts['configuration_attempts']:
                self._write_configuration_doc(Level.FORMULA)
                self._context.logger.warning('Up for retry: %s' % e)
            else:
                self._write_configuration_doc(Level.CONFIGURATION_ERROR)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # Validation failed
        except ValidationError as e:
            if self._data.attempts < self._opts['configuration_attempts']:
                self._write_configuration_doc(Level.FORMULA)
                self._context.logger.warning('Up for retry: %s' % e)
            else:
                self._write_configuration_doc(Level.CONFIGURATION_INVALID)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # No errors
        else:
            self._write_configuration_doc(
                Level.CONFIGURATION, reset_attempts=True)
            self._context.logger.info('Completed successfully')


class GeometryTask(ProcessingTask):
    """
    Base GeometryTask, creates Geometry from Configuration.

    Needs to be subclassed for specific tasks. Specific implementations
    need to provide _prepare_geometry_input(),  _run_geometry_calculation(),
    and _process_geometry_output() methods.

    """

    # Retrieve batch of input configuration documents
    def _retrieve_configuration_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['level']: Level.CONFIGURATION_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['level']: Level.CONFIGURATION}
        input_limit = self._opts['batch_size']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Input iterable
        self._input = self._context.mol_storage.find(input_request, self._lock)

        # Cursor is empty
        count = self._input.count()
        if count == 0:
            self._context.logger.info('No input molecules')
            raise NoInput

        self._context.logger.info(
            ('Retrieved %d configuration documents, ' +
             'opt/pess lock: %r/%r') % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Output and rollback lists
        self._output = []
        self._rollback = []

    # Read input configuration
    def _read_configuration_output(self):

        # Retrieve input from cursor
        self._input_data = next(self._input)

        # Convert to Molecule object
        self._data = Molecule.fromdict(self._input_data)

        # Store rollback update
        rollback = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['level']: Level.CONFIGURATION},
            remove={t2s['geometry']: 1,
                    t2s['etag']: 1})
        self._rollback.append(rollback)

        # Increase number of attempts
        self._data.attempts += 1

        self._context.logger.info('<%s>: retrieved molecule' % self._data)

    # Specific implementation
    # Convert structure in work doc from XYZ to program input
    def _prepare_geometry_input(self):
        raise NotImplementedError

    # Specific implementation
    # Perform geometry optimization
    def _run_geometry_calculation(self, geometry_input):
        raise NotImplementedError

    # Save raw output file to file storage
    def _write_raw_output(self, geometry_output):

        # Raw output identifier
        raw_output_key = self._data.key + '.' + controlhash(
            geometry_output, pos=8)

        # Save raw output
        self._context.raw_filestorage.set(
            key=raw_output_key, value=geometry_output, molkey=self._data.key,
            filetype='geometry')

        self._context.logger.info('<%s>: stored raw output' % self._data)

    # Specific implementation
    # Process geometry optimization output
    def _process_geometry_output(self, geometry_output):
        raise NotImplementedError

    # Check if fragmentation occurred or connectivity changed
    def _validate_geometry_output(self):

        # Check if connectivities are available
        configuration_parts = self._data.configuration.inchi.count('/')
        geometry_parts = self._data.geometry.inchi.count('/')

        # Atoms
        if configuration_parts == 1:

            # Compare composition
            configuration_comp = (
                self._data.configuration.inchi.split('/')[1])
            geometry_comp = (
                self._data.geometry.inchi.split('/')[1])

            # Raise exception if formula changes
            if configuration_comp != geometry_comp:
                self._context.logger.warning(
                    '<%s>: stoichiometry change' % self._data)
                raise ValidationError('Stoichiometry change')

        # Molecules
        else:

            # No connectivity available
            if geometry_parts == 1:
                self._context.logger.warning('<%s>: atomization' % self._data)
                raise ValidationError('Atomization')

            # Check adjacency
            if not is_connected(self._data.geometry.adjacency):
                self._context.logger.warning(
                    '<%s>: fragmentation' % self._data)
                raise BadOptimization('Fragmentation')

            # Rearrangement check based on adjacency keys
            configuration_adjkey = self._data.configuration.adjkey
            geometry_adjkey = self._data.geometry.adjkey
            if configuration_adjkey != geometry_adjkey:
                self._context.logger.warning(
                    '<%s>: rearrangement' % self._data)
                raise BadOptimization('Rearrangement')

            # Raise exception if no convergence or energy missing
            if not self._data.geometry.converged:
                self._context.logger.warning(
                    '<%s>: no convergence' % self._data)
                raise ValidationError('No convergence')

        if self._data.geometry.energy == 0.0:
            self._context.logger.warning('<%s>: zero energy' % self._data)
            raise ValidationError('Zero energy')

        self._context.logger.info('<%s>: validated output' % self._data)

    # Output configuration error document if read fails
    def _write_configuration_error_doc(self, level):

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._input_data[t2s['key']],
                   t2s['etag']: self._input_data[t2s['etag']]},
            replace={t2s['level']: level,
                     t2s['ts']: datetime.datetime.utcnow()},
            remove={t2s['geometry']: 1, t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s/%d>: created error update' % (
            self._input_data[t2s['key']], self._input_data[t2s['priority']]))

    # Write geometry input and output to MongoDB
    def _write_geometry_doc(self, level, reset_attempts=False):

        # Set new level
        self._data.level = level

        # Set external tag
        self._data.etag = self._data.ts

        # Set timestamp
        self._data.ts = datetime.datetime.utcnow()

        # Reset number of attempts
        if reset_attempts:
            self._data.attempts = 0

        # Add updates
        update_replace = {t2s['level']: self._data.level,
                          t2s['quality']: self._data.quality,
                          t2s['ts']: self._data.ts,
                          t2s['attempts']: self._data.attempts}

        # Add geometry data if available
        if getattr(self._data, 'geometry', None) is not None:
            update_replace[t2s['geometry']] = self._data.geometry.todict()

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key,
                   t2s['ts']: self._data.etag},
            replace=update_replace,
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of input configuration documents
        self._retrieve_configuration_doc()

    # Perform the task
    def run(self):

        try:

            # Read configuration output from MongoDB
            self._read_configuration_output()

            # Prepare input for geometry optimization
            geometry_input = self._prepare_geometry_input()

            # Run geometry optimization in subprocess
            geometry_output = self._run_geometry_calculation(geometry_input)

            # Write raw output
            if self._opts.get('raw_write', False):
                self._write_raw_output(geometry_output)

            # Process geometry optimization output
            self._process_geometry_output(geometry_output)

            # Validate geometry optimization output
            if self._opts.get('geometry_validation', False):
                self._validate_geometry_output()

        # Execution failed
        except (MoleculeError, ExecutionError) as e:
            if not getattr(self, '_data', None):
                self._write_configuration_error_doc(Level.CONFIGURATION_ERROR)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            elif self._data.attempts < self._opts['geometry_attempts']:
                self._write_geometry_doc(Level.CONFIGURATION)
                self._context.logger.warning('Up for retry: %s' % e)
            else:
                self._write_geometry_doc(Level.GEOMETRY_ERROR)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # Optimization failed, retry with nudge
        except BadOptimization as e:
            if self._data.attempts < self._opts['geometry_attempts']:
                if self._opts.get('geometry_nudge'):
                    self._write_geometry_doc(Level.CONFIGURATION_NUDGE,
                                             reset_attempts=True)
                    self._context.logger.warning('Retry with nudge: %s' % e)
                else:
                    self._write_geometry_doc(Level.CONFIGURATION)
                    self._context.logger.warning('Up for retry: %s' % e)
            else:
                self._write_geometry_doc(Level.GEOMETRY_INVALID)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # Validation failed
        except ValidationError as e:
            if self._data.attempts < self._opts['geometry_attempts']:
                self._write_geometry_doc(Level.CONFIGURATION)
                self._context.logger.warning('Up for retry: %s' % e)
            else:
                self._write_geometry_doc(Level.GEOMETRY_INVALID)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # No errors
        else:
            self._write_geometry_doc(Level.GEOMETRY, reset_attempts=True)
            self._context.logger.info('Completed successfully')


class NudgeGeometryTask(ProcessingTask):
    """
    Base NudgeGeometryTask, creates Geometry from Configuration using nudge.

    Needs to be subclassed for specific tasks. Specific implementations
    need to provide _prepare_geometry_input(),  _run_geometry_calculation(),
    and _process_geometry_output() methods.

    """

    # Retrieve batch of input configuration documents
    def _retrieve_configuration_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['level']: Level.CONFIGURATION_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['level']: Level.CONFIGURATION_NUDGE}
        input_limit = self._opts['batch_size']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Input iterable
        self._input = self._context.mol_storage.find(input_request, self._lock)

        # Cursor is empty
        count = self._input.count()
        if count == 0:
            self._context.logger.info('No input molecules')
            raise NoInput

        self._context.logger.info(
            ('Retrieved %d configuration documents, ' +
             'opt/pess lock: %r/%r') % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Output and rollback lists
        self._output = []
        self._rollback = []

    # Read input configuration
    def _read_configuration_nudge_output(self):

        # Retrieve input from cursor
        self._input_data = next(self._input)

        # Convert to Molecule object
        self._data = Molecule.fromdict(self._input_data)

        # Store rollback update
        rollback = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['level']: Level.CONFIGURATION_NUDGE},
            remove={t2s['geometry']: 1,
                    t2s['etag']: 1})
        self._rollback.append(rollback)

        # Increase number of attempts
        self._data.attempts += 1

        self._context.logger.info('<%s>: retrieved molecule' % self._data)

    # Specific implementation
    # Convert structure in work doc from XYZ to program input
    def _prepare_nudgegeometry_input(self):
        raise NotImplementedError

    # Specific implementation
    # Perform geometry optimization with nudge
    def _run_nudgegeometry_calculation(self, geometry_input):
        raise NotImplementedError

    # Save raw output file to file storage
    def _write_raw_output(self, geometry_output):

        # Raw output identifier
        raw_output_key = self._data.key + '.' + controlhash(
            geometry_output, pos=8)

        # Save raw output
        self._context.raw_filestorage.set(
            key=raw_output_key, value=geometry_output, molkey=self._data.key,
            filetype='geometry')

        self._context.logger.info('<%s>: stored raw output' % self._data)

    # Specific implementation
    # Process geometry optimization output
    def _process_nudgegeometry_output(self, geometry_output):
        raise NotImplementedError

    # Check if fragmentation occurred or connectivity changed
    def _validate_nudgegeometry_output(self):

        # Check if connectivities are available
        configuration_parts = self._data.configuration.inchi.count('/')
        geometry_parts = self._data.geometry.inchi.count('/')

        # Atoms
        if configuration_parts == 1:

            # Compare composition
            configuration_comp = (
                self._data.configuration.inchi.split('/')[1])
            geometry_comp = (
                self._data.geometry.inchi.split('/')[1])

            # Raise exception if formula changes
            if configuration_comp != geometry_comp:
                self._context.logger.warning(
                    '<%s>: stoichiometry change' % self._data)
                raise ValidationError('Stoichiometry change')

        # Molecules
        else:

            # No connectivity available
            if geometry_parts == 1:
                self._context.logger.warning('<%s>: atomization' % self._data)
                raise ValidationError('Atomization')

            # Compare composition and connectivity
            configuration_comp, configuration_conn = (
                self._data.configuration.inchi.split('/')[1:3])
            geometry_comp, geometry_conn = (
                self._data.geometry.inchi.split('/')[1:3])

            # Check adjacency
            if not is_connected(self._data.geometry.adjacency):
                self._context.logger.warning(
                    '<%s>: fragmentation' % self._data)
                raise BadOptimization('Fragmentation')

            configuration_adjkey = self._data.configuration.adjkey
            geometry_adjkey = self._data.geometry.adjkey
            if configuration_adjkey != geometry_adjkey:
                self._context.logger.warning(
                    '<%s>: rearrangement' % self._data)
                raise BadOptimization('Rearrangement')

            # Raise exception if no convergence or energy missing
            if not self._data.geometry.converged:
                self._context.logger.warning(
                    '<%s>: no convergence' % self._data)
                raise ValidationError('No convergence')

        if self._data.geometry.energy == 0.0:
            self._context.logger.warning('<%s>: zero energy' % self._data)
            raise ValidationError('Zero energy')

        self._context.logger.info('<%s>: validated output' % self._data)

    # Output configuration error document if read fails
    def _write_configuration_error_doc(self, level):

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._input_data[t2s['key']],
                   t2s['etag']: self._input_data[t2s['etag']]},
            replace={t2s['level']: level,
                     t2s['ts']: datetime.datetime.utcnow()},
            remove={t2s['geometry']: 1, t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s/%d>: created error update' % (
            self._input_data[t2s['key']], self._input_data[t2s['priority']]))

    # Write geometry input and output to MongoDB
    def _write_geometry_doc(self, level, reset_attempts=False):

        # Set new level
        self._data.level = level

        # Set external tag
        self._data.etag = self._data.ts

        # Set timestamp
        self._data.ts = datetime.datetime.utcnow()

        # Reset number of attempts
        if reset_attempts:
            self._data.attempts = 0

        # Add updates
        update_replace = {t2s['level']: self._data.level,
                          t2s['quality']: self._data.quality,
                          t2s['ts']: self._data.ts,
                          t2s['attempts']: self._data.attempts}

        # Add geometry data if available
        if getattr(self._data, 'geometry', None) is not None:
            update_replace[t2s['geometry']] = self._data.geometry.todict()

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key,
                   t2s['ts']: self._data.etag},
            replace=update_replace,
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of input configuration documents
        self._retrieve_configuration_doc()

    # Perform the task
    def run(self):

        try:

            # Read configuration output from MongoDB
            self._read_configuration_nudge_output()

            # Prepare input for geometry optimization
            geometry_input = self._prepare_nudgegeometry_input()

            # Run geometry optimization in subprocess
            geometry_output = self._run_nudgegeometry_calculation(
                geometry_input)

            # Write raw output
            if self._opts.get('raw_write', False):
                self._write_raw_output(geometry_output)

            # Process geometry optimization output
            self._process_nudgegeometry_output(geometry_output)

            # Validate geometry optimization output
            if self._opts.get('geometry_validation', False):
                self._validate_nudgegeometry_output()

        # Execution failed
        except (MoleculeError, ExecutionError) as e:
            if not getattr(self, '_data', None):
                self._write_configuration_error_doc(Level.CONFIGURATION_ERROR)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            else:
                self._write_geometry_doc(Level.GEOMETRY_ERROR)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # Optimization failed, retry with nudge
        except BadOptimization as e:
            if self._data.attempts < self._opts['geometry_nudge_attempts']:
                self._write_geometry_doc(Level.CONFIGURATION_NUDGE)
                self._context.logger.warning('Retry with nudge: %s' % e)
            else:
                self._write_geometry_doc(Level.GEOMETRY_INVALID)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # Validation failed
        except ValidationError as e:
            self._write_geometry_doc(Level.GEOMETRY_INVALID)
            self._context.logger.warning('Completed unsuccessfully: %s' % e)
            raise

        # No errors
        else:
            self._write_geometry_doc(Level.GEOMETRY, reset_attempts=True)
            self._context.logger.info('Completed successfully')


class PropertyTask(ProcessingTask):
    """Base PropertyTask, creates Property from Geometry.

    Needs to be subclassed for specific tasks. Specific implementations
    need to provide _prepare_property_input(),  _run_property_calculation(),
    and _process_property_output() methods.

    """

    # Retrieve batch of input geometry documents
    def _retrieve_geometry_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['level']: Level.GEOMETRY_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['level']: Level.GEOMETRY}
        input_limit = self._opts['batch_size']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Input iterable
        self._input = self._context.mol_storage.find(input_request, self._lock)

        # Cursor is empty
        count = self._input.count()
        if count == 0:
            self._context.logger.info('No input molecules')
            raise NoInput

        self._context.logger.info(
            'Retrieved %d geometry documents, opt/pess lock: %r/%r' % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Output and rollback lists
        self._output = []
        self._rollback = []

    # Read input geometry
    def _read_geometry_output(self):

        # Retrieve input from cursor
        data = next(self._input)

        # Convert to Molecule object
        self._data = Molecule.fromdict(data)

        # Store rollback update
        rollback = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['level']: Level.GEOMETRY},
            remove={t2s['geometry']: 1,
                    t2s['etag']: 1})
        self._rollback.append(rollback)

        # Increase number of attempts
        self._data.attempts += 1

        self._context.logger.info('<%s>: retrieved molecule' % self._data)

    # Specific implementation
    # Create property computation input
    def _prepare_property_input(self):
        raise NotImplementedError

    # Specific implementation
    # Perform property calculation
    def _run_property_calculation(self, property_input):
        raise NotImplementedError

    # Save raw output file to GridFS
    def _write_raw_output(self, property_output):

        # Raw output identifier
        raw_output_key = self._data.key + '.' + controlhash(
            property_output, pos=8)

        # Save raw output
        self._context.raw_filestorage.set(
            key=raw_output_key, value=property_output, molkey=self._data.key,
            filetype='property')

        self._context.logger.info('<%s>: stored raw output' % self._data)

    # Specific implementation
    # Process property computation output
    def _process_property_output(self, property_output):
        raise NotImplementedError

    # Validate property calculation output
    def _validate_property_output(self):
        pass

    # Write results to MongoDB
    def _write_property_doc(self, level, reset_attempts=False):

        # Set new level
        self._data.level = level

        # Set external tag
        self._data.etag = self._data.ts

        # Set timestamp
        self._data.ts = datetime.datetime.utcnow()

        # Reset number of attempts
        if reset_attempts:
            self._data.attempts = 0

        # Add updates
        update_add = {t2s['level']: self._data.level,
                      t2s['ts']: self._data.ts,
                      t2s['attempts']: self._data.attempts}

        # Add property data if available
        if getattr(self._data, 'property', None) is not None:
            update_add[t2s['property']] = self._data.property.todict()

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key,
                   t2s['ts']: self._data.etag},
            replace=update_add,
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of input geometry documents
        self._retrieve_geometry_doc()

    # Perform the task
    def run(self):

        try:

            # Read geometry output from MongoDB
            self._read_geometry_output()

            # Prepare input for property calculation
            property_input = self._prepare_property_input()

            # Run property calculation in subprocess
            property_output = self._run_property_calculation(property_input)

            # Write raw output
            if self._opts.get('raw_write', False):
                self._write_raw_output(property_output)

            # Process geometry optimization output
            self._process_property_output(property_output)

            # Validate property calculation output
            if self._opts.get('property_validation', False):
                self._validate_property_output()

        # Execution failed
        except (MoleculeError, ExecutionError) as e:
            if self._data.attempts < self._opts['property_attempts']:
                self._write_property_doc(Level.GEOMETRY)
                self._context.logger.warning('Up for retry: %s' % e)
            else:
                self._write_property_doc(Level.PROPERTY_ERROR)
                self._context.logger.warning(
                    'Completed unsuccessfully: %s' % e)
            raise

        # No errors
        else:
            self._write_property_doc(Level.PROPERTY, reset_attempts=True)
            self._context.logger.info('Completed successfully')


class StorageTask(Task):
    """Base class for database manipulation tasks."""

    # Perform setup work for the task
    def setup(self):

        self._context.logger.info('Started task')

        # Create counter to raise StopIteration after one pass
        self._counter = iter((True, ))

    # Submit batch of updates
    def submit(self):

        # Submit update to storage
        self._context.mol_storage.update(self._output, multi=True)
        self._context.logger.info(
            'Submitted %d updates' % len(self._output))


class CleanupTask(StorageTask):
    """Cleanup task.

      - Remove calculation locks
      - Reset FORMULA_UNREACTIVE molecules

    """

    # Remove locks
    def _remove_locks(self):

        # Output queue
        self._output = []

        # Formula lock
        cleanup_query = {t2s['level']: Level.FORMULA_LOCKED}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=cleanup_query, replace={t2s['level']: Level.FORMULA},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Configuration lock
        cleanup_query = {t2s['level']: Level.CONFIGURATION_LOCKED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=cleanup_query, replace={t2s['level']: Level.CONFIGURATION},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Geometry lock
        cleanup_query = {t2s['level']: Level.GEOMETRY_LOCKED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=cleanup_query, replace={t2s['level']: Level.GEOMETRY},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Property lock
        cleanup_query = {t2s['level']: Level.PROPERTY_LOCKED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=cleanup_query, replace={t2s['level']: Level.PROPERTY},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Submit batch of updates
        self._context.mol_storage.update(self._output, multi=True)

        # Log output
        self._context.logger.info('Removed locks')

    # Reset FORMULA_UNREACTIVE molecules
    def _reset_formula_unreactive_mols(self):

        # Prepare cleanup query parameters
        cleanup_query = {t2s['level']: Level.FORMULA_UNREACTIVE}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Update request
        update = self._context.mongoclient.update_request(
            query=cleanup_query,
            replace={t2s['level']: Level.FORMULA,
                     t2s['attempts']: 0},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Log output
        self._context.logger.info('Reset FORMULA_UNREACTIVE molecules')

    # Perform setup work for the task
    def setup(self):

        self._context.logger.info('Started task')

        # Create counter to raise StopIteration after one pass
        self._counter = iter((True, ))

    # Perform the task
    def run(self):

        # Call counter to raise StopIteration after one pass
        next(self._counter)

        # Remove calculation locks
        self._remove_locks()

        # Reset FORMULA_UNREACTIVE molecules
        self._reset_formula_unreactive_mols()

        self._context.logger.info('Completed successfully')

    # Optionally pause after processing task
    def pause(self):

        # Sleep
        time.sleep(self._opts.get('cleanup_interval', 0))


class SuspendTask(StorageTask):
    """Suspends molecules from computational queues."""

    # Suspend molecules
    def _suspend_mols(self):

        # Output queue
        self._output = []

        # Formula level
        suspend_query = {t2s['level']: Level.FORMULA}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                suspend_query[t2s['tags']] = tags_select
            else:
                suspend_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=suspend_query,
            replace={t2s['level']: Level.FORMULA_SUSPENDED})
        self._output.append(update)

        # Configuration level
        suspend_query = {t2s['level']: Level.CONFIGURATION}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                suspend_query[t2s['tags']] = tags_select
            else:
                suspend_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=suspend_query,
            replace={t2s['level']: Level.CONFIGURATION_SUSPENDED})
        self._output.append(update)

        # Geometry level
        suspend_query = {t2s['level']: Level.GEOMETRY}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                suspend_query[t2s['tags']] = tags_select
            else:
                suspend_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=suspend_query,
            replace={t2s['level']: Level.GEOMETRY_SUSPENDED})
        self._output.append(update)

        # Property level
        suspend_query = {t2s['level']: Level.PROPERTY}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                suspend_query[t2s['tags']] = tags_select
            else:
                suspend_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=suspend_query,
            replace={t2s['level']: Level.PROPERTY_SUSPENDED})
        self._output.append(update)

        # Submit batch of updates
        self._context.mol_storage.update(self._output, multi=True)

        # Log output
        self._context.logger.info('Reset molecule levels')

    # Perform the task
    def run(self):

        # Call counter to raise StopIteration after one pass
        next(self._counter)

        # Suspend molecules
        self._suspend_mols()

        self._context.logger.info('Completed successfully')

    # Optionally pause after processing task
    def pause(self):

        # Sleep
        time.sleep(self._opts.get('suspend_interval', 0))


class ResumeTask(StorageTask):
    """Resume computation on previously suspended molecules."""

    # Resume computation
    def _resume_mols(self):

        # Output queue
        self._output = []

        # Formula level
        resume_query = {t2s['level']: Level.FORMULA_SUSPENDED}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                resume_query[t2s['tags']] = tags_select
            else:
                resume_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=resume_query, replace={t2s['level']: Level.FORMULA})
        self._output.append(update)

        # Configuration level
        resume_query = {t2s['level']: Level.CONFIGURATION_SUSPENDED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                resume_query[t2s['tags']] = tags_select
            else:
                resume_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=resume_query, replace={t2s['level']: Level.CONFIGURATION})
        self._output.append(update)

        # Geometry level
        resume_query = {t2s['level']: Level.GEOMETRY_SUSPENDED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                resume_query[t2s['tags']] = tags_select
            else:
                resume_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=resume_query, replace={t2s['level']: Level.GEOMETRY})
        self._output.append(update)

        # Property level
        resume_query = {t2s['level']: Level.PROPERTY_SUSPENDED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                resume_query[t2s['tags']] = tags_select
            else:
                resume_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=resume_query, replace={t2s['level']: Level.PROPERTY})
        self._output.append(update)

        # Submit batch of updates
        self._context.mol_storage.update(self._output, multi=True)

        # Log output
        self._context.logger.info('Reset molecule levels')

    # Perform the task
    def run(self):

        # Call counter to raise StopIteration after one pass
        next(self._counter)

        # Resume computation
        self._resume_mols()

        self._context.logger.info('Completed successfully')

    # Optionally pause after processing task
    def pause(self):

        # Sleep
        time.sleep(self._opts.get('resume_interval', 0))
