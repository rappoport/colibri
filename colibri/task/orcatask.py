"""Geometry and property tasks using Orca.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    OrcaGeometryTask: Geometry task using Orca
    OrcaPropertyTask: Property task using Orca
"""

__all__ = ['OrcaGeometryTask', 'OrcaPropertyTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '06/08/2015'

#
# Standard modules
#

import re
import os
import string
import signal
import shutil
import tempfile
import subprocess

#
# colibri submodules
#

from colibri.task import GeometryTask, PropertyTask
from colibri.data import Geometry, Property
from colibri.exceptions import ExecutionError, ExecutionTimeout
from colibri.utils import controlhash


class OrcaGeometryTask(GeometryTask):
    """Geometry task using Orca."""

    # Convert structure in work doc from XYZ to Orca input
    def _prepare_geometry_input(self):

        # Convert to Orca input
        xyz = self._data.configuration.xyz
        coords = xyz.split('\n', 2)[-1]

        # If input template provided, use it
        if self._opts.get('geometry_input_template', ''):
            geometry_input = string.Template(
                self._opts['geometry_input_template']).substitute(
                xyz=coords, charge=int(self._data.configuration.charge),
                mult=self._data.configuration.mult,
                tempdir=self._opts['scratch_path'])

        # In the absence of an input template, construct generic input
        else:
            num_atoms = len(xyz.split('\n')) - 2
            if num_atoms > 1:
                geometry_input = (
                    '! opt ' + self._opts['geometry_options'] + '\n' +
                    ('* xyz %d %d\n' % (int(self._data.configuration.charge),
                                        self._data.configuration.mult)) +
                    coords + '*\n')
            else:
                geometry_input = (
                    '! ' + self._opts['geometry_options'] + '\n' +
                    ('* xyz %d %d\n' % (self._data.configuration.charge,
                     self._data.configuration.mult)) +
                    coords + '*\n')

            self._context.logger.info('<%s>: prepared input' % self._data)

        return geometry_input

    # Perform Orca calculation
    def _run_geometry_calculation(self, geometry_input):

        # Find Orca executable, resolving all symlinks
        qual_path = ''
        for path in self._opts['executable_path'].split(':'):
            qual_path = os.path.realpath(
                os.path.join(path, self._opts['geometry_exe']))
            if os.path.exists(qual_path) and os.access(qual_path, os.X_OK):
                break
        else:
            raise ExecutionError('Executable not found')

        # Create temporary directory
        try:
            dir_name = tempfile.mkdtemp(
                prefix='orca_', dir=self._opts['scratch_path'])
        except OSError:
            raise ExecutionError('Cannot create scratch directory')

        # Write input file
        start_dir = os.getcwd()
        os.chdir(dir_name)
        input_file = 'orca.in'
        input_f = open(input_file, 'w')
        input_f.write(geometry_input)
        input_f.close()

        # Open output file
        output_file = 'orca.out'
        output_f = open(output_file, 'w')

        # Start Orca process
        proc = subprocess.Popen(
            [qual_path, input_file], stdout=output_f, stderr=subprocess.PIPE)

        self._context.logger.info('<%s>: started calculation' % self._data)

        # Set alarm timer
        signal.alarm(self._opts.get('geometry_timeout', 0))
        try:
            stdout, stderr = proc.communicate()
            signal.alarm(0)

            # Read Orca output from file
            output_f.close()
            output_f = open(output_file, 'r')
            geometry_output = output_f.read()

            # Raise exception if not output is found
            if not geometry_output:
                raise ExecutionError('Calculation failed')
            output_f.close()

            self._context.logger.info(
                '<%s>: finished calculation' % self._data)

        except ExecutionTimeout:

            # Kill Orca process and set error code
            proc.kill()
            self._context.logger.info(
                '<%s>: calculation timed out' % self._data)
            raise ExecutionError('Calculation timeout')

        finally:
            os.chdir(start_dir)
            shutil.rmtree(dir_name, ignore_errors=True)

        return geometry_output

    # Process Orca output and add to data
    def _process_geometry_output(self, geometry_output):

        # Raw output key
        if self._opts.get('raw_write', False):
            raw_output_key = self._data.key + '.' + controlhash(
                geometry_output, pos=8)
        else:
            raw_output_key = None

        # Set error field
        self._data.error = raw_output_key

        # Regex for floating-point numbers
        number_string = (r'[+-]?(?:\d+\.\d*|\d+|\.\d+)' +
                         r'(?:[EDed][+-]?\d+)?|\*+|\?+|NaN|Infinity')

        # Regex for chemical element symbols
        element_string = r'[A-Z][a-z]?'

        # Regex for SCF convergence
        re_scf_conv = re.compile(r'SCF CONVERGED AFTER\s*\d+\s*CYCLES')

        # Regex for convergence
        re_conv = re.compile(
            r'FINAL ENERGY EVALUATION AT THE STATIONARY POINT')

        # Regex for single-point calculation
        re_spe = re.compile(
            r'Single Point Calculation')

        # Regex for Orca total energy in H
        re_totale = re.compile(
            r'^\s*FINAL SINGLE POINT ENERGY\s+(\S+)\s*$', re.MULTILINE)

        # Regex for Cartesian coordinates
        re_coordtitle = re.compile(r'CARTESIAN COORDINATES \(ANGSTROEM\)')

        re_coordxyz = re.compile(
            r'^\s*(' + element_string + r')\s+(' + number_string +
            r')\s+(' + number_string + r')\s+(' + number_string +
            r')\s*$', re.MULTILINE)

        # Regex for optimization cycle
        re_optcycle = re.compile(
            r'^\s*\*\s*GEOMETRY OPTIMIZATION CYCLE\s*\d+\s*\*\s*$',
            re.MULTILINE)

        # Regex for empty line
        re_emptyline = re.compile('^\s*$', re.MULTILINE)

        # Check for single-point calculation
        m = re_spe.search(geometry_output)
        if m:
            start_pos = m.end()
            m = re_scf_conv.search(geometry_output)
            if m:
                converged = True
            else:
                converged = False
        else:
            # Use converged data or (if not converged) last
            # optimization step
            m = re_conv.search(geometry_output)
            if m:
                converged = True
                start_pos = m.end()
            else:
                converged = False
                pos = None
                for pos in re_optcycle.finditer(geometry_output):
                    pass
                if pos is None:
                    raise ExecutionError('No optimization cycles')
                start_pos = pos.end()

        # Match final total energy in H
        m = re_totale.search(geometry_output, start_pos)
        if m:
            energy = float(m.group(1))
        else:
            raise ExecutionError('No energy available')

        # Find start of cartesian coordinates
        m = re_coordtitle.search(geometry_output, start_pos)
        coordtitle_pos = m.end()

        # Find end of cartesian coordinates
        m = re_emptyline.search(geometry_output, coordtitle_pos)
        emptyline_pos = m.start()

        # Extract XYZ coordinates in Angstrom
        xyz = ''
        atoms = 0
        for m in re_coordxyz.finditer(
                geometry_output, coordtitle_pos, emptyline_pos):
            atoms += 1
            xyz += ('%s %12.6f %12.6f %12.6f\n' % (m.group(1),
                    float(m.group(2)), float(m.group(3)), float(m.group(4))))
        # Bug in OpenBabel screws up SMILES string
        # xyz = '%d\nOrca Energy: %20.12f\n' % (atoms, energy) + xyz
        xyz = '%d\n\n' % (atoms,) + xyz

        # Raise exception if output cannot be converted
        if len(xyz) < 3:
            raise ExecutionError('Invalid output')

        # Unset error field
        self._data.error = ''

        # Create Geometry object
        self._data.geometry = Geometry(
            xyz=xyz, charge=self._data.configuration.charge,
            mult=self._data.configuration.mult,
            index=self._data.configuration.index,
            energy=energy, converged=converged,
            program=self._opts['geometry_program'],
            version=self._opts['geometry_version'],
            method=self._opts['geometry_method'],
            options=self._opts['geometry_options'],
            raw_output=raw_output_key)

        self._context.logger.info('<%s>: processed output' % self._data)


class OrcaPropertyTask(PropertyTask):
    """Property task using Orca."""

    # Create property computation input
    def _prepare_property_input(self):

        # Convert to Orca input
        xyz = self._data.geometry.xyz
        coords = xyz.split('\n', 2)[-1]

        # If input template provided, use it
        if self._opts.get('property_input_template', ''):
            property_input = string.Template(
                self._opts['property_input_template']).substitute(
                xyz=coords, charge=int(self._data.geometry.charge),
                mult=self._data.geometry.mult,
                tempdir=self._opts['scratch_path'])

        # Otherwise provide generic input
        else:
            property_input = (
                '! ' + self._opts['property_options'] + '\n' +
                ('* xyz %d %d\n' % (int(self._data.geometry.charge),
                                    self._data.geometry.mult)) +
                coords + '*\n')

        self._context.logger.info('<%s>: prepared input' % self._data)

        return property_input

    # Perform Orca calculation
    def _run_property_calculation(self, property_input):

        # Find Orca executable
        qual_path = ''
        for path in self._opts['executable_path'].split(':'):
            qual_path = os.path.realpath(
                os.path.join(path, self._opts['property_exe']))
            if os.path.exists(qual_path) and os.access(qual_path, os.X_OK):
                break
        else:
            raise ExecutionError('Executable not found')

        # Create temporary directory
        try:
            dir_name = tempfile.mkdtemp(
                prefix='orca_', dir=self._opts['scratch_path'])
        except OSError:
            raise ExecutionError('Cannot create scratch directory')

        # Write input file
        start_dir = os.getcwd()
        os.chdir(dir_name)
        input_file = 'orca.in'
        input_f = open(input_file, 'w')
        input_f.write(property_input)
        input_f.close()

        # Orca output file
        output_file = 'orca.out'
        output_f = open(output_file, 'w')

        # Start Orca process
        proc = subprocess.Popen(
            [qual_path, input_file], stdout=output_f, stderr=subprocess.PIPE)

        self._context.logger.info('<%s>: started calculation' % self._data)

        # Set alarm timer
        signal.alarm(self._opts.get('property_timeout', 0))
        try:
            stdout, stderr = proc.communicate()
            signal.alarm(0)

            # Read Orca output from file
            output_f.close()
            output_f = open(output_file, 'r')
            property_output = output_f.read()

            # Raise exception if not output is found
            if not property_output:
                raise ExecutionError('Calculation failed')
            output_f.close()

            self._context.logger.info(
                '<%s>: finished calculation' % self._data)

        except ExecutionTimeout:

            # Kill Orca process and set error code
            proc.kill()
            self._context.logger.info(
                '<%s>: calculation timed out' % self._data)
            raise ExecutionError('Calculation timeout')

        finally:
            os.chdir(start_dir)
            shutil.rmtree(dir_name, ignore_errors=True)

        return property_output

    # Process Orca output and add to data
    def _process_property_output(self, property_output):

        # Raw output identifier
        if self._opts.get('raw_write', False):
            raw_output_key = self._data.key + '.' + controlhash(
                property_output, pos=8)
        else:
            raw_output_key = None

        # Set error field
        self._data.error = raw_output_key

        # Regex for floating-point numbers
        number_string = (r'[+-]?(?:\d+\.\d*|\d+|\.\d+)' +
                         r'(?:[EDed][+-]?\d+)?|\*+|\?+|NaN|Infinity')

        # Regex for chemical element symbols
        element_string = r'[A-Z][a-z]?'

        # Regex for Orca total energy in H
        re_totale = re.compile(
            r'^\s*Total Energy\s+:\s+(' + number_string + ') Eh', re.MULTILINE)

        # Regex for Cartesian coordinates
        re_coordtitle = re.compile(r'CARTESIAN COORDINATES \(ANGSTROEM\)')

        re_coordxyz = re.compile(
            r'^\s*(' + element_string + r')\s+(' + number_string +
            r')\s+(' + number_string + r')\s+(' + number_string +
            r')\s*$', re.MULTILINE)

        # Regex for excitation spectrum
        re_exctitle = re.compile(
            r'^\s+ABSORPTION SPECTRUM VIA TRANSITION ELECTRIC DIPOLE MOMENTS',
            re.MULTILINE)

        re_excprop = re.compile(
            r'^\s+\d+\s+(' + number_string + r')\s+.*' +
            r'\s+(' + number_string +
            r')\s+(' + number_string + r')\s+(' + number_string +
            r')\s*$', re.MULTILINE)

        # Regex for empty line
        re_emptyline = re.compile('^\s*$', re.MULTILINE)

        # Regex for correct execution
        re_noerror = re.compile(
            r'^\s+\*\*\* ORCA-CIS/TD-DFT FINISHED WITHOUT ERROR \*\*\*',
            re.MULTILINE)

        # Confirm that no errors occured
        m = re_noerror.search(property_output)
        if not m:
            raise ExecutionError('Excitation calculation failed')

        # Match final total energy in H
        m = re_totale.search(property_output)
        if m:
            energy = float(m.group(1))
        else:
            energy = 0.0

        # Find start of cartesian coordinates
        m = re_coordtitle.search(property_output)
        coordtitle_pos = m.end()

        # Find end of cartesian coordinates
        m = re_emptyline.search(property_output, coordtitle_pos)
        emptyline_pos = m.start()

        # Extract XYZ coordinates in Angstrom
        xyz = ''
        atoms = 0
        for m in re_coordxyz.finditer(
                property_output, coordtitle_pos, emptyline_pos):
            atoms += 1
            xyz += ('%s %12.6f %12.6f %12.6f\n' % (m.group(1),
                    float(m.group(2)), float(m.group(3)), float(m.group(4))))
        # Bug in OpenBabel screws up SMILES string
        # xyz = '%d\nOrca Energy: %20.12f\n' % (atoms, energy) + xyz
        xyz = '%d\n\n' % (atoms,) + xyz

        # Raise exception if output cannot be converted
        if len(xyz) < 3:
            raise ExecutionError('Invalid output')

        # Find start of excitation energies
        m = re_exctitle.search(property_output)
        exctitle_pos = m.end()

        # Find end of excitation energies
        m = re_emptyline.search(property_output, exctitle_pos)
        emptyline_pos = m.start()

        # Find properties
        energies = []
        dipoles_x = []
        dipoles_y = []
        dipoles_z = []
        state = 0
        for m in re_excprop.finditer(
                property_output, exctitle_pos, emptyline_pos):
            state += 1
            energies.append(float(m.group(1)))
            dipoles_x.append(float(m.group(2)))
            dipoles_y.append(float(m.group(3)))
            dipoles_z.append(float(m.group(4)))

        # Confirm that no errors occured
        if state == 0:
            raise ExecutionError('Invalid output')

        # Add properties to dict
        properties = {
            'excitation_energies': energies,
            'transition_dipoles_x': dipoles_x,
            'transition_dipoles_y': dipoles_y,
            'transition_dipoles_z': dipoles_z
        }

        # Property units
        units = {
            'excitation_energies': 'cm^-1',
            'transition_dipoles_x': 'au',
            'transition_dipoles_y': 'au',
            'transition_dipoles_z': 'au'
        }

        # Unset error field
        self._data.error = ''

        # Create Property object
        self._data.property = Property(
            xyz=xyz, charge=self._data.geometry.charge,
            mult=self._data.geometry.mult,
            index=self._data.geometry.index,
            energy=energy, properties=properties, units=units,
            program=self._opts['property_program'],
            version=self._opts['property_version'],
            method=self._opts['property_method'],
            options=self._opts['property_options'],
            raw_output=raw_output_key)

        self._context.logger.info('<%s>: processed output' % self._data)
