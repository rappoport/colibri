"""Build task using RDKit.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    RDKitBuildTask: Build task using RDKit
"""

__all__ = ['RDKitBuildTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '06/08/2015'

#
# Third-party modules
#

import pybel
from rdkit import Chem
from rdkit.Chem import AllChem
import openbabel as ob

# Shut up OpenBabel
ob.obErrorLog.SetOutputLevel(ob.obError)

#
# colibri submodules
#

from colibri.task import BuildTask
from colibri.data import Configuration
from colibri.exceptions import ExecutionError, BadChemistry


class RDKitBuildTask(BuildTask):
    """BuildTask using RDKit."""

    # Validate structure formula
    def _validate_formula(self):

        # Parse formula as SMILES string using RDKit
        rdmol = Chem.MolFromSmiles(self._data.formula.smiles)

        # Do a roundtrip conversion for testing
        # Workaround since the radical positions are not recognized
        rdproducttmp = Chem.MolFromSmiles(Chem.MolToSmiles(
            Chem.RemoveHs(rdmol)))

        # Discard all molecules having more than 2 charged atoms
        charges = [at.GetFormalCharge() for at in rdproducttmp.GetAtoms()]
        if (sum(abs(c) for c in charges) > 2 and
        # if ((sum(charges) > 2 or sum(abs(c) for c in charges) > 4) and
                rdproducttmp.GetNumAtoms() > 1):
            raise BadChemistry('Charge problem')

        # Discard all molecular radicals
        if (sum(at.GetNumRadicalElectrons() for at
                in rdproducttmp.GetAtoms()) > 0 and
                rdproducttmp.GetNumAtoms() > 1):
            raise BadChemistry('Radical problem')

        # Discard all systems with too many rings
        if (len(Chem.GetSymmSSSR(rdproducttmp)) * 2 >
                rdproducttmp.GetNumAtoms()):
            raise BadChemistry('Ring problem')

    # Convert formula in work doc to configuration
    def _run_configuration_builder(self):

        # Parse formula as SMILES string using RDKit
        rdmol = Chem.MolFromSmiles(
            self._data.formula.smiles, sanitize=True)
        rdmol = Chem.AddHs(rdmol)
        ret = AllChem.EmbedMolecule(rdmol)
        if ret == -1:
            raise ExecutionError('Cannot create XYZ coordinates')
        AllChem.UFFOptimizeMolecule(
            rdmol, maxIters=self._opts['configuration_steps'])
        mdlmol = Chem.MolToMolBlock(rdmol)

        # Create XYZ configuration output using Pybel
        pymol = pybel.readstring('mol', mdlmol)
        obmol = pymol.OBMol
        obmol.Center()
        obmol.ToInertialFrame()
        obmol.SetTotalCharge(self._data.formula.charge)
        obmol.SetTotalSpinMultiplicity(self._data.formula.mult)
        xyz = pymol.write('xyz')

        # Create Configuration object
        self._data.configuration = Configuration(
            xyz=xyz, charge=pymol.charge, mult=pymol.spin,
            index=self._data.formula.index,
            program=self._opts['configuration_program'],
            version=self._opts['configuration_version'])

        self._context.logger.info('<%s>: processed output' % self._data)
