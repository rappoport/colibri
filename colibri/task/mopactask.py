"""Geometry tasks using MOPAC.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    MOPACGeometryTask: Geometry task using MOPAC
    MOPACNudgeGeometryTask: Geometry task using MOPAC with geometry nudge
"""

__all__ = ['MOPACGeometryTask', 'MOPACNudgeGeometryTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.7'
__date__ = '06/08/2015'

#
# Standard modules
#

import re
import os
import signal
import shutil
import tempfile
import subprocess

#
# Third-party modules
#

import openbabel as ob

# Shut up OpenBabel
ob.obErrorLog.SetOutputLevel(ob.obError)


#
# colibri submodules
#

from colibri.task import GeometryTask, NudgeGeometryTask
from colibri.data import Geometry
from colibri.utils import controlhash
from colibri.exceptions import ExecutionError, ExecutionTimeout

#
# Classes
#


class MOPACGeometryTask(GeometryTask):
    """Geometry task using MOPAC."""

    # Spin states
    _spin_states = ['', 'singlet', 'doublet', 'triplet', 'quartet',
                    'quintet', 'sextet', 'septet', 'octet', 'nonet']

    # Spin multiplicities
    _spin_mult = {'SINGLET': 1, 'DOUBLET': 2, 'TRIPLET': 3, 'QUARTET': 4,
                  'QUINTET': 5, 'SEXTET': 6, 'SEPTET': 7, 'OCTET': 8,
                  'NONET': 9}

    # Convert structure in work doc from XYZ to MOPAC input
    def _prepare_geometry_input(self):

        # Optimization in Cartesian coordinates
        optimization = 'xyz'

        # Geometry options
        options = self._opts['geometry_options']

        # Spin state as string
        spin = self._spin_states[self._data.configuration.mult]

        # Charge
        charge = 'charge=%d' % self._data.configuration.charge

        # Open-shell
        if self._data.configuration.mult != 1:
            uhf = 'uhf'
        else:
            uhf = ''

        # Full options string
        full_options = str(' '.join([s for s in [
            optimization, options, spin, charge, uhf] if s]))

        # Create molecule object
        mol = ob.OBMol()
        mol.SetTotalCharge(self._data.configuration.charge)
        mol.SetTotalSpinMultiplicity(self._data.configuration.mult)

        # Convert XYZ to MOPAC input
        conv = ob.OBConversion()
        conv.SetInAndOutFormats('xyz', 'mop')
        conv.AddOption('k', ob.OBConversion.OUTOPTIONS, full_options)
        conv.ReadString(mol, str(self._data.configuration.xyz))
        geometry_input = conv.WriteString(mol)

        # Raise exception if conversion fails
        if not geometry_input:
            raise ExecutionError('Conversion failed')

        self._context.logger.info('<%s>: prepared input' % self._data)

        return geometry_input

    # Perform MOPAC calculation
    def _run_geometry_calculation(self, geometry_input):

        # Find MOPAC executable
        qual_path = ''
        for path in self._opts['executable_path'].split(':'):
            qual_path = os.path.join(path, self._opts['geometry_exe'])
            if os.path.exists(qual_path) and os.access(qual_path, os.X_OK):
                break
        else:
            raise ExecutionError('Executable not found')

        # Create temporary directory
        try:
            dir_name = tempfile.mkdtemp(
                prefix='mopac_', dir=self._opts['scratch_path'])
        except OSError:
            raise ExecutionError('Cannot create scratch directory')

        # Write input file
        start_dir = os.getcwd()
        os.chdir(dir_name)
        input_file = 'mopac.mop'
        input_f = open(input_file, 'w')
        input_f.write(geometry_input)
        input_f.close()

        # Start MOPAC process
        proc = subprocess.Popen(
            [qual_path, input_file], stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        self._context.logger.info('<%s>: started calculation' % self._data)

        # Set alarm timer
        signal.alarm(self._opts.get('geometry_timeout', 0))
        try:
            stdout, stderr = proc.communicate()
            signal.alarm(0)

            # Read MOPAC output file
            output_file = 'mopac.out'
            output_f = open(output_file, 'r')
            geometry_output = output_f.read()

            # Raise exception if no output is found
            if not geometry_output:
                raise ExecutionError('Calculation failed')
            output_f.close()

            self._context.logger.info(
                '<%s>: finished calculation' % self._data)

        except ExecutionTimeout:

            # Kill MOPAC process and set error code
            proc.kill()
            self._context.logger.info(
                '<%s>: calculation timed out' % self._data)
            raise ExecutionError('Calculation timeout')

        finally:
            os.chdir(start_dir)
            shutil.rmtree(dir_name, ignore_errors=True)

        return geometry_output

    # Process MOPAC output
    def _process_geometry_output(self, geometry_output):

        # Raw output key
        if self._opts.get('raw_write', False):
            raw_output_key = self._data.key + '.' + controlhash(
                geometry_output, pos=8)
        else:
            raw_output_key = None

        # Set error field
        self._data.error = raw_output_key

        # Regex for floating-point numbers
        number_string = (r'[+-]?(?:\d+\.\d*|\d+|\.\d+)' +
                         r'(?:[EDed][+-]?\d+)?|\*+|\?+|NaN|Infinity')

        # Regex for chemical element symbols
        element_string = r'[A-Z][a-z]?'

        # Regex for MOPAC total energy in eV
        re_totale = re.compile(r'^\s*TOTAL ENERGY\s*=\s*(\S+)\s*EV\s*$',
                               re.MULTILINE)

        # Match total energy
        m = re_totale.search(geometry_output)
        if m:
            energy = float(m.group(1))
        else:
            energy = 0.0

        # Regex for molecular charge
        re_charge = re.compile(
            r'^\s*\*\s*CHARGE ON SYSTEM\s*=\s*(-?\d+)\s*$', re.MULTILINE)

        # Molecular charge
        m = re_charge.search(geometry_output)
        if m:
            charge = int(m.group(1))
        else:
            charge = 0

        # Regex for spin multiplicity
        re_spin = re.compile(
            r'^\s*(SINGLET|DOUBLET|TRIPLET|QUARTET' +
            r'|QUINTET|SEXTET|SEPTET|OCTET|NONET) STATE CALCULATION\s*$',
            re.MULTILINE)

        # Spin multiplicity
        m = re_spin.search(geometry_output)
        if m:
            mult = self._spin_mult[m.group(1)]
        else:
            mult = 1

        # Regex for no-op message
        re_noop = re.compile(
            r'^\s*GRADIENTS WERE INITIALLY ACCEPTABLY SMALL\s*$', re.MULTILINE)

        # Regex for convergence
        re_conv = re.compile(
            r'^\s*GEOMETRY OPTIMISED USING EIGENVECTOR FOLLOWING \(EF\)\.\s*$',
            re.MULTILINE)

        # Converged flag
        converged = False

        # No optimization necessary
        m = re_noop.search(geometry_output)
        if m:
            converged = True
            quality = 0
            start_pos = m.end()

        else:

            # Check convergence
            m = re_conv.search(geometry_output)

            # Optimization converged
            if m:
                converged = True
                quality = 0
                start_pos = m.end()

        # Get converged coordinates
        if converged:

            # Regex for Cartesian coordinates
            re_coordtitle = re.compile(
                r'^\s*CARTESIAN COORDINATES\s*$\n', re.MULTILINE)
            re_coordxyz = re.compile(
                r'^\s*\d+\s*(' + element_string + r')\s+(' + number_string +
                r')\s+(' + number_string + r')\s+(' + number_string +
                r')\s*$', re.MULTILINE)

            # Regex for empty line
            re_emptyline = re.compile('^\s*$', re.MULTILINE)

            # Find start of Cartesian coordinates
            m = re_coordtitle.search(geometry_output, start_pos)
            if m:
                has_coord = True
                coordtitle_pos = m.end()

                # Find end of Cartesian coordinates
                m = re_emptyline.search(geometry_output, coordtitle_pos)
                emptyline_pos = m.start()
            else:
                has_coord = False

        # Optimization failed, use last coordinates
        else:
            quality = 3
            start_pos = 0

            # Regex for Cartesian coordinates
            re_coordtitle = re.compile(
                r'^\s*CURRENT VALUE OF GEOMETRY\s*$\n.*\n\n\n', re.MULTILINE)
            re_coordxyz = re.compile(
                r'^\s*(' + element_string + r')\s+(' + number_string +
                r')\s+\+1\s+(' + number_string +
                r')\s+\+1\s+(' + number_string +
                r')\s+\+1\s*$', re.MULTILINE)

            # Regex for empty line
            re_emptyline = re.compile('^\s*$', re.MULTILINE)

            # Find start of Cartesian coordinates
            m = re_coordtitle.search(geometry_output, start_pos)
            if m:
                has_coord = True
                coordtitle_pos = m.end()

                # Find end of Cartesian coordinates
                m = re_emptyline.search(geometry_output, coordtitle_pos)
                emptyline_pos = m.start()
            else:
                has_coord = False

        # Extract XYZ coordinates in Angstrom
        if has_coord:
            xyz = ''
            atoms = 0
            for m in re_coordxyz.finditer(
                    geometry_output, coordtitle_pos, emptyline_pos):
                atoms += 1
                xyz += ('%s %12.6f %12.6f %12.6f\n' % (
                    m.group(1), float(m.group(2)), float(m.group(3)),
                    float(m.group(4))))

            # Bug in OpenBabel screws up SMILES string if energy is present
            xyz = '%d\n\n' % (atoms,) + xyz

        # XYZ coordinates not available
        else:
            raise ExecutionError('XYZ coordinates not available')

        # Unset error field
        self._data.error = ''

        # Set quality field
        self._data.quality = quality

        # Create Geometry object
        self._data.geometry = Geometry(xyz=xyz, charge=charge, mult=mult,
                                       index=self._data.configuration.index,
                                       energy=energy, converged=converged,
                                       program=self._opts['geometry_program'],
                                       version=self._opts['geometry_version'],
                                       method=self._opts['geometry_method'],
                                       options=self._opts['geometry_options'],
                                       raw_output=raw_output_key)

        self._context.logger.info('<%s>: processed output' % self._data)


class MOPACNudgeGeometryTask(NudgeGeometryTask):
    """Geometry task using MOPAC with geometry nudge."""

    # Spin states
    _spin_states = ['', 'singlet', 'doublet', 'triplet', 'quartet',
                    'quintet', 'sextet', 'septet', 'octet', 'nonet']

    # Spin multiplicities
    _spin_mult = {'SINGLET': 1, 'DOUBLET': 2, 'TRIPLET': 3, 'QUARTET': 4,
                  'QUINTET': 5, 'SEXTET': 6, 'SEPTET': 7, 'OCTET': 8,
                  'NONET': 9}

    # Convert structure in work doc from XYZ to MOPAC input
    def _prepare_nudgegeometry_input(self):

        # Optimization in Cartesian coordinates
        optimization = 'xyz'

        # Geometry options
        options = self._opts['geometry_nudge_options']

        # Spin state as string
        spin = self._spin_states[self._data.configuration.mult]

        # Charge
        charge = 'charge=%d' % self._data.configuration.charge

        # Open-shell
        if self._data.configuration.mult != 1:
            uhf = 'uhf'
        else:
            uhf = ''

        # Geometry nudge
        if not self._opts.get('geometry_nudge_factor'):
            strength = self._opts['geometry_nudge_strength']
        else:
            strength = (self._opts['geometry_nudge_strength'] *
                        self._opts['geometry_nudge_factor'] **
                        max(self._data.attempts - 1, 0))
        nudge = 'geo_ref="ref.mop"%f' % float(strength)
        self._context.logger.debug(
            '<%s>: nudge strength %.2f' % (self._data, strength))

        # Full options string
        full_options = str(' '.join([s for s in [
            optimization, options, spin, charge, uhf, nudge] if s]))

        # Create molecule object
        mol = ob.OBMol()
        mol.SetTotalCharge(self._data.configuration.charge)
        mol.SetTotalSpinMultiplicity(self._data.configuration.mult)

        # Convert XYZ to MOPAC input
        conv = ob.OBConversion()
        conv.SetInAndOutFormats('xyz', 'mop')
        conv.AddOption('k', ob.OBConversion.OUTOPTIONS, full_options)
        conv.ReadString(mol, str(self._data.configuration.xyz))
        geometry_input = conv.WriteString(mol)

        # Raise exception if conversion fails
        if not geometry_input:
            raise ExecutionError('Conversion failed')

        self._context.logger.info('<%s>: prepared input' % self._data)

        return geometry_input

    # Perform MOPAC calculation
    def _run_nudgegeometry_calculation(self, geometry_input):

        # Find MOPAC executable
        qual_path = ''
        for path in self._opts['executable_path'].split(':'):
            qual_path = os.path.join(path, self._opts['geometry_nudge_exe'])
            if os.path.exists(qual_path) and os.access(qual_path, os.X_OK):
                break
        else:
            raise ExecutionError('Executable not found')

        # Create temporary directory
        try:
            dir_name = tempfile.mkdtemp(
                prefix='mopac_', dir=self._opts['scratch_path'])
        except OSError:
            raise ExecutionError('Cannot create scratch directory')

        # Write input file
        start_dir = os.getcwd()
        os.chdir(dir_name)
        input_file = 'mopac.mop'
        input_f = open(input_file, 'w')
        input_f.write(geometry_input)
        input_f.close()

        # Write reference file
        ref_file = 'ref.mop'
        ref_f = open(ref_file, 'w')
        ref_f.write(geometry_input)
        ref_f.close()

        # Start MOPAC process
        proc = subprocess.Popen(
            [qual_path, input_file], stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        self._context.logger.info('<%s>: started calculation' % self._data)

        # Set alarm timer
        signal.alarm(self._opts.get('geometry_nudge_timeout', 0))
        try:
            stdout, stderr = proc.communicate()
            signal.alarm(0)

            # Read MOPAC output file
            output_file = 'mopac.out'
            output_f = open(output_file, 'r')
            geometry_output = output_f.read()

            # Raise exception if no output is found
            if not geometry_output:
                raise ExecutionError('Calculation failed')
            output_f.close()

            self._context.logger.info(
                '<%s>: finished calculation' % self._data)

        except ExecutionTimeout:

            # Kill MOPAC process and set error code
            proc.kill()
            self._context.logger.info(
                '<%s>: calculation timed out' % self._data)
            raise ExecutionError('Calculation timeout')

        finally:
            os.chdir(start_dir)
            shutil.rmtree(dir_name, ignore_errors=True)

        return geometry_output

    # Process MOPAC output
    def _process_nudgegeometry_output(self, geometry_output):

        # Raw output key
        if self._opts.get('raw_write', False):
            raw_output_key = self._data.key + '.' + controlhash(
                geometry_output, pos=8)
        else:
            raw_output_key = None

        # Set error field
        self._data.error = raw_output_key

        # Regex for floating-point numbers
        number_string = (r'[+-]?(?:\d+\.\d*|\d+|\.\d+)' +
                         r'(?:[EDed][+-]?\d+)?|\*+|\?+|NaN|Infinity')

        # Regex for chemical element symbols
        element_string = r'[A-Z][a-z]?'

        # Regex for MOPAC total energy in eV
        re_totale = re.compile(r'^\s*TOTAL ENERGY\s*=\s*(\S+)\s*EV\s*$',
                               re.MULTILINE)

        # Match total energy
        m = re_totale.search(geometry_output)
        if m:
            energy = float(m.group(1))
        else:
            energy = 0.0

        # Regex for molecular charge
        re_charge = re.compile(
            r'^\s*\*\s*CHARGE ON SYSTEM\s*=\s*(-?\d+)\s*$', re.MULTILINE)

        # Molecular charge
        m = re_charge.search(geometry_output)
        if m:
            charge = int(m.group(1))
        else:
            charge = 0

        # Regex for spin multiplicity
        re_spin = re.compile(
            r'^\s*(SINGLET|DOUBLET|TRIPLET|QUARTET' +
            r'|QUINTET|SEXTET|SEPTET|OCTET|NONET) STATE CALCULATION\s*$',
            re.MULTILINE)

        # Spin multiplicity
        m = re_spin.search(geometry_output)
        if m:
            mult = self._spin_mult[m.group(1)]
        else:
            mult = 1

        # Regex for no-op message
        re_noop = re.compile(
            r'^\s*GRADIENTS WERE INITIALLY ACCEPTABLY SMALL\s*$', re.MULTILINE)

        # Regex for convergence
        re_conv = re.compile(
            r'^\s*HERBERTS TEST WAS SATISFIED IN BFGS\s*$', re.MULTILINE)

        # Converged flag
        converged = False

        # No optimization necessary
        m = re_noop.search(geometry_output)
        if m:
            converged = True
            quality = 0
            start_pos = m.end()

        else:

            # Check convergence
            m = re_conv.search(geometry_output)

            # Optimization converged
            if m:
                converged = True
                quality = 2
                start_pos = m.end()

        # Get converged coordinates
        if converged:

            # Regex for Cartesian coordinates
            re_coordtitle = re.compile(
                r'^\s*CARTESIAN COORDINATES\s*$\n', re.MULTILINE)
            re_coordxyz = re.compile(
                r'^\s*\d+\s*(' + element_string + r')\s+(' + number_string +
                r')\s+(' + number_string + r')\s+(' + number_string +
                r')\s*$', re.MULTILINE)

            # Regex for empty line
            re_emptyline = re.compile('^\s*$', re.MULTILINE)

            # Find start of Cartesian coordinates
            m = re_coordtitle.search(geometry_output, start_pos)
            if m:
                has_coord = True
                coordtitle_pos = m.end()

                # Find end of Cartesian coordinates
                m = re_emptyline.search(geometry_output, coordtitle_pos)
                emptyline_pos = m.start()
            else:
                has_coord = False

        # Optimization failed, use last coordinates
        else:
            quality = 3
            start_pos = 0

            # Regexes for start of Cartesian coordinates
            re_coordstart1 = re.compile(
                r'^\s*CURRENT VALUE OF GEOMETRY\s*$\n.*\n\n\n', re.MULTILINE)
            re_coordstart2 = re.compile(
                r'^\s*FINAL  POINT  AND  DERIVATIVES\s*$\n', re.MULTILINE)
            re_coordtitle2 = re.compile(
                r'^\s*CARTESIAN COORDINATES\s*$\n', re.MULTILINE)

            # Regexes for Cartesian coordinates
            re_coordxyz = re.compile(
                r'^\s*\d*\s*(' + element_string + r')\s+(' + number_string +
                r')\s+(?:\+1)?\s+(' + number_string +
                r')\s+(?:\+1)?\s+(' + number_string +
                r')\s*(?:\+1)?\s*$', re.MULTILINE)

            # Regex for empty line
            re_emptyline = re.compile('^\s*$', re.MULTILINE)

            # Find start of Cartesian coordinates
            m = re_coordstart1.search(geometry_output, start_pos)
            if m:
                has_coord = True
                coordtitle_pos = m.end()

                # Find end of Cartesian coordinates
                m = re_emptyline.search(geometry_output, coordtitle_pos)
                emptyline_pos = m.start()
            else:
                # Find start of Cartesian coordinates
                m = re_coordstart2.search(geometry_output, start_pos)
                if m:
                    has_coord = True
                    coordstart_pos = m.end()

                    m = re_coordtitle2.search(geometry_output, coordstart_pos)
                    if m:
                        has_coord = True
                        coordtitle_pos = m.end()

                        # Find end of Cartesian coordinates
                        m = re_emptyline.search(
                            geometry_output, coordtitle_pos)
                        emptyline_pos = m.start()

                    else:
                        has_coord = False
                else:
                    has_coord = False

        # Extract XYZ coordinates in Angstrom
        if has_coord:
            xyz = ''
            atoms = 0
            for m in re_coordxyz.finditer(
                    geometry_output, coordtitle_pos, emptyline_pos):
                atoms += 1
                xyz += ('%s %12.6f %12.6f %12.6f\n' % (
                    m.group(1), float(m.group(2)), float(m.group(3)),
                    float(m.group(4))))

            # Bug in OpenBabel screws up SMILES string if energy is present
            xyz = '%d\n\n' % (atoms,) + xyz

        # XYZ coordinates not available
        else:
            raise ExecutionError('XYZ coordinates not available')

        # Unset error field
        self._data.error = ''

        # Set quality field
        self._data.quality = quality

        # Create Geometry object
        self._data.geometry = Geometry(
            xyz=xyz, charge=charge, mult=mult,
            index=self._data.configuration.index,
            energy=energy, converged=converged,
            program=self._opts['geometry_nudge_program'],
            version=self._opts['geometry_nudge_version'],
            method=self._opts['geometry_nudge_method'],
            options=self._opts['geometry_nudge_options'],
            raw_output=raw_output_key)

        self._context.logger.info('<%s>: processed output' % self._data)
