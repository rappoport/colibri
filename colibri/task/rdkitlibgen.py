"""Library generators for molecule generation based on reactivity rules
using RDKit.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    RDKitLibraryGenerator: Base class for all library generators
    RDKitUniLibraryGenerator: Library generator for unimolecular reactions
    RDKitCyclLibraryGenerator: Library generator for cyclization reactions
    RDKitBiLibraryGenerator: Library generator for bimolecular reactions
"""

__all__ = ['RDKitLibraryGenerator', 'RDKitUniLibraryGenerator',
           'RDKitCyclLibraryGenerator', 'RDKitBiLibraryGenerator']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '06/04/2015'

#
# Standard library
#

import re

#
# Third-party modules
#

from rdkit import Chem
from rdkit.Chem import AllChem

#
# colibri modules
#

from colibri.exceptions import ReactionRuleError

#
# Local variables
#

_re_rule = re.compile(
    r'^\[(.*?)\]([\.\-=#]?)\[(.*?)\]>>\[(.*?)\]([\.\-=#]?)\[(.*?)\]$')
_re_lhs = re.compile(r'^(.*):(\d+)$')
_re_rhs = re.compile(r'^([A-Z][a-z]?|#\d+|\*)([+-]\d+):(\d+)$')
_re_lhs_at = re.compile(r'^([A-Z][a-z]?|\#\d+)([+-]\d+)$')


class RDKitLibraryGenerator(object):
    """Base class for all RDKit-based library generators."""

    @staticmethod
    def parse(rule):

        parsed_rule = {'reactant': {}, 'product': {}}

        try:
            r_at1, parsed_rule['reactant']['bond'], r_at2, \
                p_at1, parsed_rule['product']['bond'], p_at2 = \
                _re_rule.match(rule).groups()
        except (ValueError, AttributeError):
            raise ReactionRuleError('Invalid rule structure')

        try:
            r_atstr1, parsed_rule['reactant']['ind1'] = \
                _re_lhs.match(r_at1).groups()
            r_atstr2, parsed_rule['reactant']['ind2'] = \
                _re_lhs.match(r_at2).groups()
            parsed_rule['reactant']['atom1'] = [
                dict(zip(['element', 'charge'], _re_lhs_at.match(
                    r_atdef1).groups())) for r_atdef1 in r_atstr1.split(',')]
            parsed_rule['reactant']['atom2'] = [
                dict(zip(['element', 'charge'], _re_lhs_at.match(
                    r_atdef2).groups())) for r_atdef2 in r_atstr2.split(',')]
        except (ValueError, AttributeError):
            raise ReactionRuleError('Invalid reactant definition')

        try:
            parsed_rule['product']['atom1'] = [{}]
            parsed_rule['product']['atom2'] = [{}]
            parsed_rule['product']['atom1'][0]['element'], \
                parsed_rule['product']['atom1'][0]['charge'], \
                parsed_rule['product']['ind1'] = _re_rhs.match(p_at1).groups()
            parsed_rule['product']['atom2'][0]['element'], \
                parsed_rule['product']['atom2'][0]['charge'], \
                parsed_rule['product']['ind2'] = _re_rhs.match(p_at2).groups()
        except (ValueError, AttributeError):
            raise ReactionRuleError('Invalid product definition')

        return parsed_rule

    @staticmethod
    def molecularity(rule):

        # Need '>>' in rule
        if '>>' not in rule:
            raise ReactionRuleError('Need >> in reaction rule')

        # Reaction molecularity
        return rule.split('>>')[0].count('.') + 1

    @staticmethod
    def num_products(rule):

        # Need '>>' in rule
        if '>>' not in rule:
            raise ReactionRuleError('Need >> in reaction rule')

        # Determine number of products
        return rule.split('>>')[1].count('.') + 1

    @staticmethod
    def univalent_atoms(rule):
        univalent_elements = ['#1', 'Li', 'Na', 'K', 'Rb', 'Cs', 'Fr',
                              'F', 'Cl', 'Br', 'I', 'At']
        parsed_rule = RDKitLibraryGenerator.parse(rule)
        return (any([r_at1['element'] in univalent_elements
                    for r_at1 in parsed_rule['reactant']['atom1']]) or
                any([r_at2['element'] in univalent_elements
                    for r_at2 in parsed_rule['reactant']['atom2']]))

    @classmethod
    def init_generator(cls, rule, exact=True, cycl=False):

        # Convert to string
        rule = str(rule)

        # Need '>>' in rule
        if '>>' not in rule:
            raise ReactionRuleError('Need >> in reaction rule')

        # Determine reaction molecularity, number of products
        molecularity = cls.molecularity(rule)
        num_products = cls.num_products(rule)

        # Unimolecular and ring opening reactions
        if molecularity == 1:
            if cycl and num_products == 2:
                cycl_rule = (rule.split('>>')[0] + '>>(' +
                             rule.split('>>')[1] + ')')
                return RDKitCyclLibraryGenerator(cycl_rule, exact=exact)
            else:
                return RDKitUniLibraryGenerator(rule, exact=exact)

        # Bimolecular and ring closing reactions
        elif molecularity == 2:
            if cycl and num_products == 1:
                cycl_rule = ('(' + rule.split('>>')[0] + ')>>' +
                             rule.split('>>')[1])
                return RDKitCyclLibraryGenerator(cycl_rule, exact=exact)
            else:
                return RDKitBiLibraryGenerator(rule, exact=exact)

    def __init__(self, rule, exact=True):

        # SMIRKS rule describing the reaction
        self._rule = rule

        # Exact atom mapping
        self._exact = exact

        # Construct RDKit reaction generator
        try:
            self._lib_gen = AllChem.ReactionFromSmarts(rule)
        except (ValueError, RuntimeError) as e:
            raise ReactionRuleError(e)

    @property
    def rule(self):
        return self._rule

    @property
    def exact(self):
        return self._exact


class RDKitUniLibraryGenerator(RDKitLibraryGenerator):
    """Library generator for unimolecular reactions."""

    def react(self, reactant1):

        # Exact atom mapping
        if self._exact:

            rdreactant1 = Chem.AddHs(Chem.MolFromSmiles(reactant1))

            # Convert to Kekule form
            Chem.Kekulize(rdreactant1, clearAromaticFlags=True)

            try:

                # Iterate over product sets
                for rdproducts in self._lib_gen.RunReactants([rdreactant1]):

                    try:

                        # Iterate over products
                        for rdproduct in rdproducts:

                            # Sanitization step
                            Chem.SanitizeMol(rdproduct)

                            # Remove all implicit hydrogens
                            for atom in rdproduct.GetAtoms():
                                atom.SetNoImplicit(True)

                        # Remove hydrogens, convert to SMILES, and return
                        yield tuple(
                            sorted([Chem.MolToSmiles(Chem.RemoveHs(rdproduct))
                                    for rdproduct in rdproducts]))

                    except (ValueError, AttributeError):
                        continue

            except ValueError:
                pass

        # Implicit hydrogens allowed
        else:

            rdreactant1 = Chem.MolFromSmiles(reactant1)

            # Convert to Kekule form
            Chem.Kekulize(rdreactant1, clearAromaticFlags=True)

            try:

                # Iterate over product sets
                for rdproducts in self._lib_gen.RunReactants([rdreactant1]):

                    # Iterate over products
                    for rdproduct in rdproducts:

                        # Sanitization step
                        Chem.SanitizeMol(rdproduct)

                    # Convert to SMILES and return
                    yield tuple(sorted([Chem.MolToSmiles(rdproduct)
                                for rdproduct in rdproducts]))

            except ValueError:
                pass


class RDKitCyclLibraryGenerator(RDKitUniLibraryGenerator):
    """Library generator for cyclization reactions."""

    pass


class RDKitBiLibraryGenerator(RDKitLibraryGenerator):
    """Library generator for bimolecular reactions."""

    def react(self, reactant1, reactant2):

        # Exact atom mapping
        if self._exact:

            rdreactant1 = Chem.AddHs(Chem.MolFromSmiles(reactant1))
            rdreactant2 = Chem.AddHs(Chem.MolFromSmiles(reactant2))

            # Convert to Kekule form
            Chem.Kekulize(rdreactant1, clearAromaticFlags=True)
            Chem.Kekulize(rdreactant2, clearAromaticFlags=True)

            try:

                # Iterate over product sets
                for rdproducts in self._lib_gen.RunReactants(
                        [rdreactant1, rdreactant2]):

                    try:

                        # Iterate over products
                        for rdproduct in rdproducts:

                            # Sanitization step
                            Chem.SanitizeMol(rdproduct)

                            # Remove all implicit hydrogens
                            for atom in rdproduct.GetAtoms():
                                atom.SetNoImplicit(True)

                        # Remove hydrogens, convert to SMILES, and return
                        yield tuple(sorted([
                            Chem.MolToSmiles(Chem.RemoveHs(rdproduct))
                                    for rdproduct in rdproducts]))

                    except (ValueError, AttributeError):
                        continue

            except ValueError:
                pass

        # Implicit hydrogens allowed
        else:

            rdreactant1 = Chem.MolFromSmiles(reactant1)
            rdreactant2 = Chem.MolFromSmiles(reactant2)

            # Convert to Kekule form
            Chem.Kekulize(rdreactant1, clearAromaticFlags=True)
            Chem.Kekulize(rdreactant2, clearAromaticFlags=True)

            try:

                # Iterate over product sets
                for rdproducts in self._lib_gen.RunReactants(
                        [rdreactant1, rdreactant2]):

                    # Iterate over products
                    for rdproduct in rdproducts:

                        # Sanitization step
                        Chem.SanitizeMol(rdproduct)

                    # Convert to SMILES and return
                    yield tuple(sorted([Chem.MolToSmiles(rdproduct)
                                for rdproduct in rdproducts]))

            except ValueError:
                pass
