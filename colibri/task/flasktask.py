"""Task clases for colibri involving flask manipulations.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    FlaskMapperTask: Map data from flask onto Molecule instances
    FlaskReducerTask: Collect molecular data into flask
    FlaskCleanupTask: Remove calculation locks, restarts UNAVAILABLE flasks
    FlaskSuspendTask: Suspend flasks from computation
    FlaskResumeTask: Resume computation on previously suspended flasks
"""

__all__ = ['FlaskMapperTask', 'FlaskReducerTask', 'FlaskCleanupTask',
           'FlaskSuspendTask', 'FlaskResumeTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/13/2015'

#
# Library modules
#

import time
import datetime

#
# colibri submodules
#

from colibri.task import Task
from colibri.data import Flask, Molecule, CacheMolecule
from colibri.exceptions import NoInput, IncompleteInput, ExistingInput, \
    HiEnergy, HiBarrier, MaxGeneration, BadChemistry, ExecutionError, \
    ValidationError
from colibri.utils import t2s
from colibri.enums import Level, Status, Sort


#
# Classes
#

class FlaskMapperTask(Task):
    """FlaskMapperTask, maps molecular data from flask."""

    # Check options
    def _check_opts(self):
        # Batch size should not exceed number of tasks per cycle
        if self._opts['batch_size'] > self._opts['tasks_per_cycle']:
            self._context.logger.warning(
                'Reducing batch size to number of tasks per cycle')
        self._opts = self._opts.update(
            batch_size={
                'key': 'batch_size',
                'value': self._opts['tasks_per_cycle'],
                'precedence': 'command'})

    # Read initial documents from storage
    def _retrieve_initial_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['status']: Status.INITIAL_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['status']: Status.INITIAL}
        input_limit = self._opts['batch_size']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Input iterable
        self._input = self._context.flask_storage.find(
            input_request, self._lock)

        # Cursor is empty
        count = self._input.count()
        if count == 0:
            self._context.logger.info('No input flasks')
            raise NoInput

        self._context.logger.info(
            'Retrieved %d initial documents, opt/pess lock: %r/%r' % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Output and rollback flask lists
        self._output = []
        self._rollback = []

        # Output molecule dict
        self._molecules = {}

        # Cached molecule filter
        self._cache_molecules = set()

    # Read initial flask from storage
    def _read_initial_flask(self):

        # Retrieve input from cursor
        data = next(self._input)

        # Convert to Flask object
        self._data = Flask.fromdict(data)

        # Store rollback update
        rollback = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['status']: Status.INITIAL},
            remove={t2s['etag']: 1})
        self._rollback.append(rollback)

        self._context.logger.info('<%s>: retrieved flask' % self._data)

    # Merge flask journal
    def _merge_flask_journal(self):

        # Merge precursor energies
        if self._data.precursor_journal:
            max_precursor = max(self._data.precursor_journal)

            # Higher-energy precursor found
            if max_precursor > self._data.precursor:
                self._data.precursor = max_precursor

        # Energy change
        if self._data.energy is not None and self._data.precursor is not None:
            energy_change = self._data.energy - self._data.precursor

            # Set energy change field
            self._data.energy_change = energy_change

            # Raise exception if energy change exceeds threshold
            if (self._opts.get('max_energy_change') is not None and
                    energy_change > self._opts['max_energy_change']):
                raise HiBarrier

        # Merge generation numbers
        if self._data.generation_journal:
            min_generation = min(self._data.generation_journal)

            # Lower generation number found
            if min_generation < self._data.generation:
                self._data.generation = min_generation

        # Raise exception if generation exceeds threshold
        if (self._opts.get('max_generation') is not None and
                min_generation >= self._opts['max_generation']):
            raise MaxGeneration

    # Merge flask status
    def _merge_flask_status(self):

        # Error message found
        if self._data.error:

            # Bad chemistry
            if self._data.error == 'BadChemistry':
                raise BadChemistry

            # Geometry or configuration execution error
            elif self._data.error == 'ExecutionError':
                raise ExecutionError

            # Geometry or configuration validation error
            elif self._data.error == 'ValidationError':
                raise ValidationError

            # High energy
            elif self._data.error == 'HiEnergy':
                raise HiEnergy

        # Energy field is available
        if self._data.energy is not None and self._data.rules_applied:
            raise ExistingInput

    # Output molecule documents
    def _write_mol_input(self):

        # Dictionary of missing molecules
        new_molecules = {}

        # Iterate over molecules in flask
        for mol in self._data.molecules:

            # Construct Molecule object
            output_data = Molecule(smiles=mol.smiles)

            # Append to list of keys
            if (output_data.key in new_molecules or
                    output_data.key in self._molecules or
                    output_data.key in self._cache_molecules):
                continue

            # Update generation
            output_data.generation = self._data.generation

            # Update tags
            output_data.tags = self._data.tags

            # Append update request to molecule output dict
            update = self._context.mongoclient.update_request(
                query={t2s['key']: output_data.key},
                add=output_data.todict(exclude=['key', 'tags']),
                union={t2s['tags']: output_data.tags})
            new_molecules[output_data.key] = update

        # Check if molecules are present in mapping cache
        if new_molecules and self._context.mol_map_cache is not None:
            for key, val in self._context.mol_map_cache.get(
                    new_molecules.keys()).iteritems():
                self._cache_molecules.add(key)

        # Store number for statistics
        num_cache_molecules = len(self._cache_molecules)

        # Molecules still missing
        new_molecules = {key: update for key, update in
                         new_molecules.iteritems() if key not in
                         self._cache_molecules}

        # Submit only missing molecules
        self._molecules.update(new_molecules)

        # Store number for statistics
        num_storage_molecules = len(self._molecules)

        # Add keys of new molecules to mapping cache
        if self._context.mol_map_cache is not None:
            self._context.mol_map_cache.set(
                {key: '' for key in new_molecules.keys()})

        self._context.logger.debug(
            'Created %d molecules, in cache/stored: %d/%d' % (
                len(self._data.molecules), num_cache_molecules,
                num_storage_molecules))

    # Output flask document
    def _write_flask_doc(self, status):

        # Set new status
        self._data.status = status

        # Set external tag
        self._data.etag = self._data.ts

        # Set timestamp
        self._data.ts = datetime.datetime.utcnow()

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key,
                   t2s['ts']: self._data.etag},
            replace={t2s['status']: self._data.status,
                     t2s['precursor']: self._data.precursor,
                     t2s['generation']: self._data.generation,
                     t2s['energy_change']: self._data.energy_change,
                     t2s['ts']: self._data.ts},
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Task setup
    def setup(self):

        self._context.logger.info('Started task')

        # Check options
        self._check_opts()

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of input initial documents
        self._retrieve_initial_doc()

    # Perform the task
    def run(self):

        try:

            # Read flask input from storage
            self._read_initial_flask()

            # Merge flask journal
            self._merge_flask_journal()

            # Merge flask status
            self._merge_flask_status()

            # Write molecule input to storage
            self._write_mol_input()

        # Bad Chemistry
        except BadChemistry as e:
            self._write_flask_doc(Status.REACTIVE_BADCHEM)
            self._context.logger.warning(
                'Completed unsuccessfully: Bad chemistry')
            raise

        # Execution failed
        except ExecutionError as e:
            self._write_flask_doc(Status.ERROR)
            self._context.logger.warning('Completed unsuccessfully: %s' % e)
            raise

        # Validation failed
        except ValidationError as e:
            self._write_flask_doc(Status.INVALID)
            self._context.logger.warning('Completed unsuccessfully: %s' % e)
            raise

        # High energy
        except HiEnergy as e:
            self._write_flask_doc(Status.REACTIVE_HIENERGY)
            self._context.logger.warning(
                'Completed unsuccessfully: High energy')
            raise

        # Stopped because of large energy change
        except HiBarrier as e:
            self._write_flask_doc(Status.REACTIVE_HIBARRIER)
            self._context.logger.warning(
                'Completed unsuccessfully: High barrier')
            raise

        # Stopped because of maximum generation number
        except MaxGeneration as e:
            self._write_flask_doc(Status.REACTIVE_MAXGEN)
            self._context.logger.warning(
                'Completed unsuccessfully: Max. generation')
            raise

        # Flask energy has already been successfully computed
        except ExistingInput as e:
            self._write_flask_doc(Status.UNREACTIVE)
            self._context.logger.info('Completed successfully')

        # Flask not yet processed
        else:
            self._write_flask_doc(Status.RUNNING)
            self._context.logger.info('Completed successfully')

    # Submit batch of output documents
    def submit(self):

        # Submit molecule documents to storage
        if self._molecules:
            self._context.mol_storage.update(self._molecules.itervalues())
            self._context.logger.info(
                'Submitted %d molecules' % len(self._molecules))

        # Submit flask updates to storage
        self._context.flask_storage.update(self._output, self._lock)
        self._context.logger.info(
            'Submitted %d flasks, opt/pess lock: %r/%r' % (
                len(self._output), self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

    # Roll back results if an exception occurs
    def rollback(self):

        # Send rollback update to storage
        self._context.flask_storage.update(self._rollback, self._lock)
        self._context.logger.warning(
            'Rolled back %d flasks' % len(self._rollback))


class FlaskReducerTask(Task):
    """FlaskReducerTask, collects molecular data into flask."""

    # Check options
    def _check_opts(self):

        # Batch size should not exceed number of tasks per cycle
        if self._opts['batch_size'] > self._opts['tasks_per_cycle']:
            self._context.logger.warning(
                'Reducing batch size to number of tasks per cycle')
        self._opts = self._opts.update(
            batch_size={
                'key': 'batch_size',
                'value': self._opts['tasks_per_cycle'],
                'precedence': 'command'})

    # Retrieve running flasks from storage
    def _retrieve_running_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['status']: Status.RUNNING_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['status']: Status.RUNNING}
        input_limit = self._opts['batch_size']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Input iterable
        self._input = self._context.flask_storage.find(
            input_request, self._lock)

        # Cursor is empty
        count = self._input.count()
        if count == 0:
            self._context.logger.info('No input flasks')
            raise NoInput

        self._context.logger.info(
            'Retrieved %d running documents, opt/pess lock: %r/%r' % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Output and rollback flask lists
        self._output = []
        self._rollback = []

        # Input molecule dict
        self._molecules = {}

    # Read running flask from storage
    def _read_running_flask(self):

        # Retrieve input from cursor
        data = next(self._input)

        # Convert to Flask object
        self._data = Flask.fromdict(data)

        # Store rollback update
        rollback = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key},
            replace={t2s['status']: Status.RUNNING},
            remove={t2s['etag']: 1})
        self._rollback.append(rollback)

        # Increase number of attempts
        self._data.attempts += 1

        # Check number of generations
        # Raise exception if generation exceeds threshold
        if (self._opts.get('max_generation') is not None and
                self._data.generation >= self._opts['max_generation']):
            raise MaxGeneration

        # Check if energy change field is already available
        # Raise exception if energy change exceeds threshold
        if (self._opts.get('max_energy_change') is not None and
                self._data.energy_change is not None):
            if self._data.energy_change > self._opts['max_energy_change']:
                raise HiBarrier

        self._context.logger.info('<%s>: retrieved flask' % self._data)

    # Retrieve missing molecules from storage
    def _retrieve_mol_output(self):

        # Dictionary of missing molecules
        new_molecules = {}

        # Number of molecules in memory, cache and storage
        num_memory_molecules = 0
        num_cache_molecules = 0
        num_storage_molecules = 0

        # Construct request for missing molecules
        for mol in self._data.molecules:

            # Construct Molecule object
            input_data = Molecule(smiles=mol.smiles)
            key = input_data.key

            # Append to list of keys
            if key not in self._molecules:
                new_molecules[key] = CacheMolecule()
            else:
                num_memory_molecules += 1

        # Check reducing cache for missing molecules first
        self._cache_molecules = {}
        if new_molecules and self._context.mol_red_cache is not None:
            for key, val in self._context.mol_red_cache.get(
                    new_molecules.keys()).iteritems():
                self._cache_molecules[key] = CacheMolecule(val)

            # Merge with existing molecule data
            self._molecules.update(self._cache_molecules)

            # Store number for statistics
            num_cache_molecules = len(self._cache_molecules)

        # Molecules still missing
        new_molecules = {key: val for key, val in new_molecules.iteritems()
                         if key not in self._cache_molecules}

        # Query storage for missing molecules
        if new_molecules:
            input_query = {t2s['key']: {'$in': new_molecules.keys()}}
            input_request = self._context.mongoclient.query_request(
                query=input_query)

            # Retrieve available new molecules and append to
            # molecules dict and reducing cache
            storage_molecules = {}
            for input_data in self._context.mol_storage.find(input_request):
                key = input_data[t2s['key']]
                cmol = CacheMolecule.fromdict(input_data)
                new_molecules[key] = cmol

                # Only cache if result has energy; all levels are
                # converted to GEOMETRY internally
                if cmol.level in Level.HAS_ENERGY:
                    storage_molecules[key] = cmol

            # Merge with existing molecule data
            self._molecules.update(new_molecules)

            # Add to reducing cache
            if self._context.mol_red_cache is not None:
                self._context.mol_red_cache.set(storage_molecules)

            # Store number for statistics
            num_storage_molecules = len(storage_molecules)

        self._context.logger.debug(
            'Requested %d molecules, got from mem/cache/store: %d/%d/%d' % (
                len(self._data.molecules), num_memory_molecules,
                num_cache_molecules, num_storage_molecules))

    # Update molecule entries in flask
    def _read_mol_output(self):

        # Molecule levels
        output_levels = [
            self._molecules[mol.key].level for mol in self._data.molecules]

        # Fail fast if BadChemistry, ExecutionError, or
        # ValidationError is encountered
        if (Level.FORMULA_ERROR in output_levels or
                Level.FORMULA_INVALID in output_levels):
            self._data.error = 'BadChemistry'
            raise BadChemistry
        elif Level.GEOMETRY_ERROR in output_levels:
            self._data.error = 'ExecutionError'
            raise ExecutionError('Geometry error')
        elif Level.CONFIGURATION_ERROR in output_levels:
            self._data.error = 'ExecutionError'
            raise ExecutionError('Configuration error')
        elif Level.GEOMETRY_INVALID in output_levels:
            self._data.error = 'ValidationError'
            raise ValidationError('Invalid geometry')
        elif Level.CONFIGURATION_INVALID in output_levels:
            self._data.error = 'ValidationError'
            raise ValidationError('Invalid configuration')

        # Compute flask energy
        flask_energy = 0.

        # Retrieve molecules from dict
        for mol in self._data.molecules:

            # Retrieve molecules from dict
            output_data = self._molecules.get(mol.key)

            # Molecule not retrieved
            if output_data.empty():
                raise IncompleteInput

            # Check results
            if output_data.level in Level.INCOMPLETE:
                raise IncompleteInput

            # Molecule energy missing
            if output_data.energy is None:
                self._data.error = 'ExecutionError'
                raise ExecutionError('Geometry error')

            # Add molecule energy to total flask energy
            flask_energy += output_data.energy

        # Set flask energy
        self._data.energy = flask_energy

        self._context.logger.info('<%s>: updated flask' % self._data)

        # Energy relative to reference flask
        if self._opts.get('max_rel_energy') is not None:

            # Find reference energy
            if self._opts.get('ref_energy') is not None:

                # Reference energy
                if (self._data.energy - self._opts['ref_energy'] >
                        self._opts['max_rel_energy']):
                    self._data.error = 'HiEnergy'
                    raise HiEnergy
            else:
                self._context.logger.warning('No reference energy available')

        # Energy change
        if self._data.precursor:
            energy_change = self._data.energy - self._data.precursor

            # Set energy change field
            self._data.energy_change = energy_change

            # Raise exception if energy change exceeds threshold
            if (self._opts.get('max_energy_change') is not None and
                    energy_change > self._opts['max_energy_change']):
                raise HiBarrier

    # Write flask output
    def _write_flask_doc(self, status, reset_attempts=False):

        # Set new status
        self._data.status = status

        # Set external tag
        self._data.etag = self._data.ts

        # Timestamp
        self._data.ts = datetime.datetime.utcnow()

        # Reset number of attempts
        if reset_attempts:
            self._data.attempts = 0

        # Append update request to output queue
        update = self._context.mongoclient.update_request(
            query={t2s['key']: self._data.key,
                   t2s['ts']: self._data.etag},
            replace={t2s['status']: self._data.status,
                     t2s['energy']: self._data.energy,
                     t2s['energy_change']: self._data.energy_change,
                     t2s['attempts']: self._data.attempts,
                     t2s['error']: self._data.error,
                     t2s['ts']: self._data.ts},
            remove={t2s['etag']: 1})
        self._output.append(update)

        self._context.logger.info('<%s>: created update' % self._data)

    # Task setup
    def setup(self):

        self._context.logger.info('Started task')

        # Check options
        self._check_opts()

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of input running documents
        self._retrieve_running_doc()

    # Perform the task
    def run(self):

        try:

            # Read running flask from storage
            self._read_running_flask()

            # Retrieve missing molecules from storage
            self._retrieve_mol_output()

            # Update molecule entries in flask
            self._read_mol_output()

        # No molecule data available
        except IncompleteInput as e:
            if self._data.attempts < self._opts['reduce_attempts']:
                self._write_flask_doc(Status.RUNNING)
                self._context.logger.debug('Up for retry: %s' % e)
            else:
                self._write_flask_doc(Status.UNAVAILABLE)
            self._context.logger.debug('Completed unsuccessfully: %s' % e)
            raise

        # Bad Chemistry
        except BadChemistry as e:
            self._write_flask_doc(Status.REACTIVE_BADCHEM)
            self._context.logger.debug(
                'Completed unsuccessfully: Bad chemistry')
            raise

        # Execution failed
        except ExecutionError as e:
            self._write_flask_doc(Status.ERROR)
            self._context.logger.debug('Completed unsuccessfully: %s' % e)
            raise

        # Validation failed
        except ValidationError as e:
            self._write_flask_doc(Status.INVALID)
            self._context.logger.debug('Completed unsuccessfully: %s' % e)
            raise

        # High energy
        except HiEnergy as e:
            self._write_flask_doc(Status.REACTIVE_HIENERGY)
            self._context.logger.warning(
                'Completed unsuccessfully: High energy')
            raise

        # Stopped because of high barrier
        except HiBarrier as e:
            self._write_flask_doc(Status.REACTIVE_HIBARRIER)
            self._context.logger.debug(
                'Completed unsuccessfully: High barrier')
            raise

        # Stopped because of maximum generation number
        except MaxGeneration as e:
            self._write_flask_doc(Status.REACTIVE_MAXGEN)
            self._context.logger.debug(
                'Completed unsuccessfully: Max. generation')
            raise

        # No errors
        else:
            self._write_flask_doc(Status.REACTIVE, reset_attempts=True)
            self._context.logger.debug('Completed sucessfully')

    # Submit batch of output documents
    def submit(self):

        # Submit update to storage
        self._context.flask_storage.update(self._output, self._lock)
        self._context.logger.info(
            'Submitted %d documents, opt/pess lock: %r/%r' % (
                len(self._output), self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

    # Roll back results if an exception occurs
    def rollback(self):

        # Send rollback update to storage
        self._context.flask_storage.update(self._rollback, self._lock)
        self._context.logger.warning(
            'Rolled back %d documents' % len(self._rollback))


class FlaskStorageTask(Task):
    """Base class for flask database manipulation tasks."""

    # Perform setup work for the task
    def setup(self):

        self._context.logger.info('Started task')

        # Create counter to raise StopIteration after one pass
        self._counter = iter((True, ))

    # Submit batch of updates
    def submit(self):

        # Submit update to storage
        if self._output:
            self._context.flask_storage.update(self._output, multi=True)
            self._context.logger.info(
                'Submitted %d updates' % len(self._output))


class FlaskCleanupTask(FlaskStorageTask):
    """Flask cleanup task.

      - Remove calculation locks
      - Restart UNAVAILABLE flasks
      - Restart REACTIVE_MAXGEN flasks if max. generation is changed
      - Restart REACTIVE_HIBARRIER flasks if energy threshold is changed

    """

    # Remove locks
    def _remove_locks(self):

        # Initial lock
        cleanup_query = {t2s['status']: Status.INITIAL_LOCKED}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=cleanup_query,
            replace={t2s['status']: Status.INITIAL},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Running lock
        cleanup_query = {t2s['status']: Status.RUNNING_LOCKED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=cleanup_query,
            replace={t2s['status']: Status.RUNNING},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Reactive lock
        cleanup_query = {t2s['status']: Status.REACTIVE_LOCKED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=cleanup_query,
            replace={t2s['status']: Status.REACTIVE},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Log output
        self._context.logger.info('Removed locks')

    # Restart UNAVAILABLE flasks
    def _restart_unavailable_flasks(self):

        # Prepare cleanup query parameters
        cleanup_query = {t2s['status']: Status.UNAVAILABLE}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Update request
        update = self._context.mongoclient.update_request(
            query=cleanup_query,
            replace={t2s['status']: Status.RUNNING,
                     t2s['attempts']: 0},
            remove={t2s['etag']: 1})
        self._output.append(update)

        # Log output
        self._context.logger.info('Restarted UNAVAILABLE flasks')

    # Restart REACTIVE_MAXGEN flasks if maximum generation number
    # is increased
    def _restart_maxgen_flasks(self):

        # Prepare input query parameters
        cleanup_query = {t2s['status']: Status.REACTIVE_MAXGEN}

        # Maximum generation defined
        if self._opts.get('max_generation') is not None:
            cleanup_query[t2s['generation']] = {
                '$lt': self._opts['max_generation']}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Update request
        update = self._context.mongoclient.update_request(
            query=cleanup_query,
            replace={t2s['status']: Status.REACTIVE})
        self._output.append(update)

        # Log output
        self._context.logger.info('Restarted REACTIVE_MAXGEN flasks')

    # Restart REACTIVE_HIBARRIER flasks if maximum energy change
    # threshold is increased
    def _restart_hienergy_flasks(self):

        # Prepare input query parameters
        cleanup_query = {t2s['status']: Status.REACTIVE_HIBARRIER}

        # Maximum energy change threshold is defined
        if self._opts.get('max_energy_change'):
            cleanup_query[t2s['energy_change']] = {
                '$lte': self._opts['max_energy_change']}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                cleanup_query[t2s['tags']] = tags_select
            else:
                cleanup_query[t2s['tags']] = {'$all': tags_select}

        # Update request
        update = self._context.mongoclient.update_request(
            query=cleanup_query,
            replace={t2s['status']: Status.REACTIVE})
        self._output.append(update)

        # Log output
        self._context.logger.info('Restarted REACTIVE_HIBARRIER flasks')

    # Perform the task
    def run(self):

        # Call counter to raise StopIteration after one pass
        next(self._counter)

        # Output queue
        self._output = []

        # Remove calculation locks
        self._remove_locks()

        # Restart UNAVAILABLE flasks
        self._restart_unavailable_flasks()

        # Restart REACTIVE_MAXGEN flasks
        self._restart_maxgen_flasks()

        # Restart REACTIVE_HIBARRIER flasks
        self._restart_hienergy_flasks()

        self._context.logger.info('Completed successfully')

    # Optionally pause after processing task
    def pause(self):

        # Sleep
        time.sleep(self._opts.get('cleanup_interval', 0))


class FlaskSuspendTask(FlaskStorageTask):
    """Suspends flasks from computational queues."""

    # Suspend flasks
    def _suspend_flasks(self):

        # Initial status
        suspend_query = {t2s['status']: Status.INITIAL}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                suspend_query[t2s['tags']] = tags_select
            else:
                suspend_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=suspend_query,
            replace={t2s['status']: Status.INITIAL_SUSPENDED})
        self._output.append(update)

        # Running status
        suspend_query = {t2s['status']: Status.RUNNING}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                suspend_query[t2s['tags']] = tags_select
            else:
                suspend_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=suspend_query,
            replace={t2s['status']: Status.RUNNING_SUSPENDED})
        self._output.append(update)

        # Reactive status
        suspend_query = {t2s['status']: Status.REACTIVE}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                suspend_query[t2s['tags']] = tags_select
            else:
                suspend_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=suspend_query,
            replace={t2s['status']: Status.REACTIVE_SUSPENDED})
        self._output.append(update)

        self._context.logger.info('Suspended flask status')

    # Perform the task
    def run(self):

        # Call counter to raise StopIteration after one pass
        next(self._counter)

        # Output queue
        self._output = []

        # Suspend molecules
        self._suspend_flasks()

        self._context.logger.info('Completed successfully')

    # Optionally pause after processing task
    def pause(self):

        # Sleep
        time.sleep(self._opts.get('suspend_interval', 0))


class FlaskResumeTask(FlaskStorageTask):
    """Resume computation on previously suspended flasks."""

    # Resume computation
    def _resume_flasks(self):

        # Initial status
        resume_query = {t2s['status']: Status.INITIAL_SUSPENDED}

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                resume_query[t2s['tags']] = tags_select
            else:
                resume_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=resume_query,
            replace={t2s['status']: Status.INITIAL})
        self._output.append(update)

        # Running status
        resume_query = {t2s['status']: Status.RUNNING_SUSPENDED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                resume_query[t2s['tags']] = tags_select
            else:
                resume_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=resume_query,
            replace={t2s['status']: Status.RUNNING})
        self._output.append(update)

        # Reactive status
        resume_query = {t2s['status']: Status.REACTIVE_SUSPENDED}

        # Tag selection
        if tags_select:
            if isinstance(tags_select, basestring):
                resume_query[t2s['tags']] = tags_select
            else:
                resume_query[t2s['tags']] = {'$all': tags_select}

        # Create update request object
        update = self._context.mongoclient.update_request(
            query=resume_query,
            replace={t2s['status']: Status.REACTIVE})
        self._output.append(update)

        self._context.logger.info('Resumed flask status')

    # Perform the task
    def run(self):

        # Call counter to raise StopIteration after one pass
        next(self._counter)

        # Output queue
        self._output = []

        # Resume computation
        self._resume_flasks()

        self._context.logger.info('Completed successfully')

    # Optionally pause after processing task
    def pause(self):

        # Sleep
        time.sleep(self._opts.get('resume_interval', 0))
