"""Task classes involving molecule generation based on reactivity rules
using RDKit.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    RDKitReactionTask: Molecule generation task using RDKit
"""

__all__ = ['RDKitReactionTask']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '06/02/2015'


#
# Library modules
#

import itertools as it


#
# colibri submodules
#

from colibri.task.basetask import Task
from colibri.data import Formula, Molecule
from colibri.enums import Level, Sort, Reactivity
from colibri.exceptions import NoInput, MoleculeError, ReactionRuleError
from colibri.utils import t2s
from colibri.task.rdkitlibgen import RDKitLibraryGenerator


class RDKitReactionTask(Task):
    """RDKitReactionTask, Molecule generation task using RDKit."""

    def _init_rules(self):

        # Check for react_rules options
        if not self._opts.get('react_rule'):
            self._context.logger.info('No input rules')
            raise NoInput

        # Process reaction rules
        self._react_rules = [
            rule.value for rule in self._opts.list('react_rule')]

    # Initialize library generators
    def _init_lib_gen(self):

        # Create local reactivity pattern from input
        reactivity = self._opts.get('reactivity', Reactivity.ALL)
        self._reactivity_uni = reactivity & Reactivity.UNI
        self._reactivity_cycl = reactivity & Reactivity.CYCL
        self._reactivity_bi = reactivity & Reactivity.BI

        # Exact atom mapping
        react_exact_mapping = self._opts.get('react_exact_mapping', False)

        # Unimolecular reactions
        if self._reactivity_uni:
            self._generators_uni = []
            for rule in self._react_rules:

                try:
                    if RDKitLibraryGenerator.molecularity(rule) != 1:
                        continue

                    lib_gen = RDKitLibraryGenerator.init_generator(
                        rule, exact=react_exact_mapping)
                except ReactionRuleError as e:
                    self._context.logger.error(
                        'Invalid unimolecular rule: %s' % e)
                    continue

                self._generators_uni.append(lib_gen)
                self._context.logger.debug(
                    'Initialized%s unimolecular generator for rule %s' % (
                        ' exact' if lib_gen.exact else '', lib_gen.rule))

            # No valid unimolecular rules available
            if not self._generators_uni:
                self._reactivity_uni = False

        # Ring opening and closing reactions
        if self._reactivity_cycl:
            self._generators_cycl = []
            for rule in self._react_rules:

                try:
                    # Ring closing rules are constructed from bimolecular rules
                    # Ring opening rules are constructed from unimolecular ones
                    molecularity = RDKitLibraryGenerator.molecularity(rule)
                    num_products = RDKitLibraryGenerator.num_products(rule)
                    if not (molecularity == 1 and num_products == 2 or
                            molecularity == 2 and num_products == 1):
                        continue

                    lib_gen = RDKitLibraryGenerator.init_generator(
                        rule, exact=react_exact_mapping, cycl=True)
                except ReactionRuleError as e:
                    self._context.logger.error(
                        'Invalid cyclization rule: %s' % e)
                    continue

                self._generators_cycl.append(lib_gen)
                self._context.logger.debug(
                    'Initialized%s cyclization generator for rule %s' % (
                        ' exact' if lib_gen.exact else '', lib_gen.rule))

            # No valid cyclization rules available
            if not self._generators_cycl:
                self._reactivity_cycl = False

        # Bimolecular reactions
        if self._reactivity_bi:
            self._generators_bi = []
            for rule in self._react_rules:

                try:
                    if RDKitLibraryGenerator.molecularity(rule) != 2:
                        continue

                    lib_gen = RDKitLibraryGenerator.init_generator(
                        rule, exact=react_exact_mapping)
                except ReactionRuleError as e:
                    self._context.logger.error(
                        'Invalid bimolecular rule: %s' % e)
                    continue

                self._generators_bi.append(lib_gen)
                self._context.logger.debug(
                    'Initialized%s bimolecular generator for rule %s' % (
                        ' exact' if lib_gen.exact else '', lib_gen.rule))

            # No valid bimolecular rules available
            if not self._generators_bi:
                self._reactivity_bi = False

    # Retrieve formula documents from storage
    def _retrieve_formula_doc(self):

        # Prepare locks
        optimistic = False
        pessimistic = False
        lock = None

        # Optimistic lock
        if self._opts['optimistic_lock']:
            optimistic = True

        # Pessimistic lock
        if self._opts['pessimistic_lock']:
            pessimistic = True
            lock = {
                t2s['level']: Level.FORMULA_LOCKED,
                t2s['etag']: self._context.name}

        # Create lock object
        self._lock = self._context.mongoclient.update_lock(
            optimistic=optimistic, pessimistic=pessimistic,
            lock=lock)

        # Prepare input query parameters
        input_query = {t2s['level']: Level.FORMULA}
        input_limit = self._opts['limit']
        input_sort = [[t2s['priority'], Sort.ASCENDING]]

        # Tag selection
        tags_select = self._opts.get('tags', [])
        if tags_select:
            if isinstance(tags_select, basestring):
                input_query[t2s['tags']] = tags_select
            else:
                input_query[t2s['tags']] = {'$all': tags_select}

        # Create query request object
        input_request = self._context.mongoclient.query_request(
            query=input_query, limit=input_limit, sort=input_sort)

        # Retrieve entire input iterable at once
        self._input = list(self._context.mol_storage.find(
            input_request, self._lock))

        # No input available
        count = len(self._input)
        if count == 0:
            self._context.logger.info('No input molecules')
            raise NoInput

        self._context.logger.info(
            'Retrieved %d formula documents, opt/pess lock: %r/%r' % (
                count, self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

        # Store output and rollback updates
        self._output = []
        self._rollback = []
        for data in self._input:
            mol = Molecule.fromdict(data)

            # Output update
            output = self._context.mongoclient.update_request(
                query={t2s['key']: mol.key},
                replace={t2s['level']: Level.FORMULA_UNREACTIVE},
                remove={t2s['etag']: 1})
            self._output.append(output)

            # Rollback update
            rollback = self._context.mongoclient.update_request(
                query={t2s['key']: mol.key},
                replace={t2s['level']: Level.FORMULA},
                remove={t2s['etag']: 1})
            self._rollback.append(rollback)

        # Children molecules dict
        self._molecules = {}

    # Generate unimolecular reactions
    def _prepare_formula_uni(self, source):

        # Iterate over input source
        for mol1 in source:

            # Extract information about input molecules
            smiles1 = mol1.formula.smiles
            gen = mol1.generation + 1

            # Iterate over rules
            for lib_gen in self._generators_uni:

                # Generate reaction products
                for products in lib_gen.react(smiles1):

                    # Create total SMILES string for products
                    smiles = '.'.join(sorted(products))

                    try:

                        # Construct Molecule object
                        for product in products:
                            formula = Formula(
                                smiles=product,
                                program=self._opts['react_program'],
                                version=self._opts['react_version'])
                            data = Molecule(formula=formula,
                                            generation=gen,
                                            tags=mol1.tags)

                            self._context.logger.info(
                                '<%s>: unimolecular product' % data)
                            yield data

                    # Skip product if Molecule constructor raises exception
                    except MoleculeError as e:
                        self._context.logger.warning(
                            'Product %s invalid: %s' % (smiles, e))
                        continue

    # Generate cyclization reactions
    def _prepare_formula_cycl(self, source):

        # Iterate over molecules
        for mol1 in source:

            # Extract information about input molecules
            smiles1 = mol1.formula.smiles
            gen = mol1.generation + 1

            # Iterate over rules
            for lib_gen in self._generators_cycl:

                # Generate reaction products
                for products in lib_gen.react(smiles1):

                    # Create total SMILES string for products
                    smiles = '.'.join(sorted(products))

                    try:

                        # Construct Molecule object
                        for product in products:
                            formula = Formula(
                                smiles=product,
                                program=self._opts['react_program'],
                                version=self._opts['react_version'])
                            data = Molecule(formula=formula,
                                            generation=gen,
                                            tags=mol1.tags)

                        self._context.logger.info(
                            '<%s>: cyclization product' % data)
                        yield data

                    # Skip product if Molecule constructor raises exception
                    except MoleculeError as e:
                        self._context.logger.warning(
                            'Product %s invalid: %s' % (smiles, e))
                        continue

    # Generate bimolecular reactions
    def _prepare_formula_bi(self, source):

        # Loop over all pairs of molecules, including order
        for mol1, mol2 in source:

            # Extract information about input molecules
            smiles1 = mol1.formula.smiles
            smiles2 = mol2.formula.smiles
            gen1 = mol1.generation + 1
            gen2 = mol2.generation + 1
            gen = max(gen1, gen2)
            tags = list(set(mol1.tags) | set(mol2.tags))

            # Iterate over rules
            for lib_gen in self._generators_bi:

                # Generate reaction products
                for products in lib_gen.react(smiles1, smiles2):

                    # Create total SMILES string for products
                    smiles = '.'.join(sorted(products))

                    try:

                        # Construct Molecule object
                        for product in products:
                            formula = Formula(
                                smiles=product,
                                program=self._opts['react_program'],
                                version=self._opts['react_version'])
                            data = Molecule(formula=formula,
                                            generation=gen,
                                            tags=tags)

                            self._context.logger.info(
                                '<%s>: bimolecular product' % data)
                            yield data

                    # Skip product if Molecule constructor raises exception
                    except MoleculeError as e:
                        self._context.logger.warning(
                            'Product %s invalid: %s' % (smiles, e))
                        continue

    # Generator routine producing data instances
    def _prepare_formula(self):

        # Input molecules set
        current_mols = {Molecule.fromdict(data) for data in self._input}

        # Generations of input molecules
        current_gens = [mol.generation for mol in current_mols]

        # Minimum generation
        min_generation = min(current_gens)

        # Maximum generation
        max_generation = self._opts.get('max_generation')
        if max_generation is None:
            max_generation = 1
        max_generation += max(current_gens)

        # Start generations
        for gen in xrange(min_generation, max_generation):

            # New molecules set
            new_mols = set()

            # Unimolecular rules
            if self._reactivity_uni:
                for data in self._prepare_formula_uni(
                        it.ifilter(
                            lambda m: m.generation == gen, current_mols)):
                    if data not in new_mols and data not in current_mols:
                        new_mols.add(data)
                        yield data

            # Cyclization rules
            if self._reactivity_cycl:
                for data in self._prepare_formula_cycl(
                        it.ifilter(
                            lambda m: m.generation == gen, current_mols)):
                    if data not in new_mols and data not in current_mols:
                        new_mols.add(data)
                        yield data

            # Bimolecular rules
            if self._reactivity_bi:
                for data in self._prepare_formula_bi(
                    it.ifilter(
                        lambda m: m[0].generation == gen and
                        m[1].generation <= gen,
                        it.product(current_mols, repeat=2))):
                    if data not in new_mols and data not in current_mols:
                        new_mols.add(data)
                        yield data

            # Update or replace molecular pool with new molecules
            if self._reactivity_bi and self._opts.get(
                    'react_update', True):
                current_mols.update(new_mols)
            else:
                current_mols = new_mols.copy()

    # Generate data to insert and supply to storage
    def _write_formula_doc(self):

        # Add update requests for children molecules
        for output_data in self._prepare_formula():
            update = self._context.mongoclient.update_request(
                query={t2s['key']: output_data.key},
                replace={t2s['level']: output_data.level},
                add=output_data.todict(exclude=['key', 'tags', 'level']),
                union={t2s['tags']: output_data.tags})
            self._molecules[output_data.key] = update

            self._context.logger.info('<%s>: created update' % output_data)

    # Task setup
    def setup(self):

        self._context.logger.info('Started task')

        # Create counter to raise StopIteration after one pass
        self._counter = iter((True, ))

        # Initialize rules
        self._init_rules()

        # Initialize library generators
        self._init_lib_gen()

    # Retrieve input data for the task
    def retrieve(self):

        # Retrieve batch of formula documents
        self._retrieve_formula_doc()

    # Perform the task
    def run(self):

        # Call counter to raise StopIteration after one pass
        next(self._counter)

        # Generate product molecules
        self._write_formula_doc()

        self._context.logger.info('Completed successfully')

    # Submit batch of output documents
    def submit(self):

        # Submit children molecules to storage
        if self._molecules:
            self._context.mol_storage.update(self._molecules.itervalues())
            self._context.logger.info(
                'Submitted %d molecules' % len(self._molecules))

        # Submit parent molecule updates to storage
        self._context.mol_storage.update(self._output, self._lock)
        self._context.logger.info(
            'Submitted %d documents, opt/pess lock: %r/%r' % (
                len(self._output), self._opts['optimistic_lock'],
                self._opts['pessimistic_lock']))

    # Roll back results if an exception occurs
    def rollback(self):

        # Send rollback update to storage
        self._context.mol_storage.update(self._rollback, self._lock)
        self._context.logger.warning(
            'Rolled back %d documents' % len(self._rollback))
