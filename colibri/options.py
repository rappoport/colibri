"""Options classes for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    OptionItem: Base representation of a single program argument or option
    Options: Immutable collection of program arguments and options
"""

__all__ = ['OptionItem', 'Options']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '08/01/2015'


#
# Standard library
#

import itertools as it

#
# colibri submodules
#

from colibri.enums import Precedence, Target
from colibri.utils import randomid


class OptionItem(object):
    """Object representing a single option key-value pair. Needs to specify
    at least the option group or option key. All other fields are optional.
    """

    __slots__ = ('group', 'key', 'value', 'default', 'description',
                 'precedence', 'target')

    def __init__(self, **kwargs):
        """OptionItem object constructor

        Args:
            group: Option group
            key: Option key (may be same as group)
            value: Option value
            default: Option default
            description: Human-readable option description
            precedence: Option precedence
                ('default', 'site', 'env', 'user', 'project', 'command')
            target: Option target
                ('Dispatcher', 'Calculator', and subclasses)

        Raises:
            ValueError: Invalid input value

        Examples:
            >>> opt = OptionItem(key='db_hostname', value='localhost')
            >>> print opt # doctest: +NORMALIZE_WHITESPACE
            OptionItem(group='db_hostname', key='db_hostname',
                       value='localhost')
            >>> print opt.priority
            35004300
            >>> opt1 = OptionItem(key='db_hostname', default='localhost',
            ...                   precedence='user', target='colibri')
            >>> print opt1 # doctest: +NORMALIZE_WHITESPACE
            OptionItem(group='db_hostname', key='db_hostname',
                       default='localhost', precedence='user',
                       target='colibri')
            >>> print opt1.value
            None
            >>> print opt1.default
            localhost
            >>> print opt1.priority
            33004300
            >>> print opt1.precedence
            user
            >>> print opt1.target
            colibri
            >>> print opt1.todict() # doctest: +NORMALIZE_WHITESPACE
             {'default': 'localhost', 'group': 'db_hostname',
              'precedence': 'user', 'key': 'db_hostname', 'target': 'colibri'}
            >>> opt2 = OptionItem(value=12)
            Traceback (most recent call last):
            ValueError: OptionItem needs group or key specification
            >>> opt2 = OptionItem(**{'key': 'db_hostname',
            ...                   'default': 'http://www.example.org',
            ...                   'target': 'colibri.task.rdkitbuildtask',
            ...                   'precedence': 'command'})
            >>> print opt2 # doctest: +NORMALIZE_WHITESPACE
            OptionItem(group='db_hostname', key='db_hostname',
                       default='http://www.example.org', precedence='command',
                       target='colibri.task.rdkitbuildtask')
            >>> print opt1 > opt2
            True
            >>> print opt1 == opt2
            False
            >>> opt3 = OptionItem.fromdict(**{'key': 'db_hostname',
            ...                        'default': 'http://www.example.org',
            ...                        'precedence': 'user'})
            >>> print opt2 < opt3
            True
            >>> print opt1 == opt3
            True
        """

        # Set only the fields in __slots__, ignore everything else
        # Ignore all None values
        for field in self.__slots__:
            val = kwargs.get(field)
            if val is not None:
                setattr(self, field, val)

        # Option group, use key if group is not specified on input
        if self.group is None:
            self.group = self.key
            if self.group is None:
                raise ValueError('OptionItem needs group or key specification')

    def __repr__(self):
        """Construct string representation of the object attributes."""

        fields = []
        for field in self.__slots__:
            val = getattr(self, field, None)
            if val is not None:
                fields.append('%s=%r' % (field, val))

        return type(self).__name__ + '(' + ', '.join(fields) + ')'

    def __getattr__(self, attr):
        """Return None for missing attributes."""
        return None

    def __eq__(self, other):
        """Equality test based on priority, have to have same group."""

        if self.group != other.group:
            return NotImplemented
        return self.priority == other.priority

    def __ne__(self, other):
        """Inequality test based on priority, have to have same group."""

        if self.group != other.group:
            return NotImplemented
        return self.priority != other.priority

    def __le__(self, other):
        """Less-or-equal test based on priority, have to have same group."""

        if self.group != other.group:
            raise NotImplemented
        return self.priority <= other.priority

    def __lt__(self, other):
        """Less-than test based on priority, have to have same group."""

        if self.group != other.group:
            raise NotImplemented
        return self.priority < other.priority

    def __ge__(self, other):
        """Greater-or-equal test based on priority, have to have same group."""

        if self.group != other.group:
            raise NotImplemented
        return self.priority >= other.priority

    def __gt__(self, other):
        """Greater-than test based on priority, have to have same group."""

        if self.group != other.group:
            raise NotImplemented
        return self.priority > other.priority

    def todict(self):
        """Export object as Python dictionary."""

        fields = {}
        for field in self.__slots__:
            val = getattr(self, field, None)
            if val is not None:
                fields[field] = val
        return fields

    @classmethod
    def fromdict(cls, **kwargs):
        """New OptionItem object constructor."""

        return cls(**kwargs)

    @property
    def priority(self):
        """OptionItem priority. Lower values indicate higher priority."""

        # Default precedence: default (lowest precedence)
        precedence = self.precedence
        if precedence is None:
            precedence = 'DEFAULT'
        else:
            precedence = precedence.upper()

        # Default target: colibri (module-wide)
        target = self.target
        if target is None:
            target = 'COLIBRI'
        else:
            target = target.upper()

        return (getattr(Precedence, precedence) * 10000 +
                getattr(Target, target.split('.')[-1]))


class Options(object):
    """
    Immutable collection of program arguments and options, encoded
    as OptionItem objects.

    Options object can be initialized in the following ways:
    - List of existing Options objects, OptionItem objects,
      dicts, or strings;
    - Dictionary with OptionItem objects, dicts, or strings
    - dicts are converted to OptionItem objects;
    - strings are converted to OptionItem objects with value = True;

    For all list arguments, a random valid identifier is generated.
    Non-data keyword arguments:
    - scope: Identifies the current option target
    - precedence: Default precedence value for OptionItem objects
    - target: Default target value for OptionItem objects

    After initialization, Options objects are not modifiable. Upon
    mutation methods (set, update, delete, filter), a new Options
    instance is created and populated with the modified data.

    Examples:
        >>> data = {
        ...   "db_hostname": {
        ...     "key": "db_hostname",
        ...     "default": "localhost",
        ...     "description": "MongoDB database server name.",
        ...     "precedence": "default",
        ...     "target": "colibri"
        ...   },
        ...   "db_port": {
        ...     "key": "db_port",
        ...     "default": 27017,
        ...     "description": "MongoDB database server port.",
        ...     "precedence": "default",
        ...     "target": "colibri"
        ...   },
        ...   "configuration_program_rdkit": {
        ...     "key": "configuration_program",
        ...     "default": "RDKit",
        ...     "description": "Program name in build task.",
        ...     "precedence": "default",
        ...     "target": "colibri.task.rdkitbuildtask"
        ...   },
        ...   "configuration_version_rdkit": {
        ...     "key": "configuration_version",
        ...     "value": "2015.3.1",
        ...     "description": "Program version in build task.",
        ...     "precedence": "default",
        ...     "target": "colibri.task.rdkitbuildtask"
        ...   },
        ...   "configuration_program_openbabel": {
        ...     "key": "configuration_program",
        ...     "default": "OpenBabel",
        ...     "description": "Program name in build task.",
        ...     "precedence": "default",
        ...     "target": "colibri.task.openbabeltask"
        ...   },
        ...   "configuration_version_openbabel": {
        ...     "key": "configuration_version",
        ...     "value": "2.3.1",
        ...     "description": "Program version in build task.",
        ...     "precedence": "default",
        ...     "target": "colibri.task.openbabeltask"
        ...   }
        ... }

        >>> opts = Options(**data)
        >>> print opts['db_hostname']
        localhost
        >>> print opts.scope
        colibri

        # Add option with higher precedence but narrower target
        >>> opts1 = opts.update(db_hostname={
        ...     'key': 'db_hostname', 'default': 'http://www.example.org',
        ...     'target': 'colibri.task.rdkitbuildtask',
        ...     'precedence': 'command'})
        >>> print opts1['db_hostname']
        localhost
        >>> opts1 is opts
        False

        # Add option with same precedence and target
        >>> opts2 = opts.update(db_hostname={
        ...    'key': 'db_hostname', 'value': 'http://www.example.org'})
        >>> print opts2['db_hostname']
        http://www.example.org

        # Add option with higher precedence but same target
        >>> opts3 = opts.update(db_hostname={
        ...    'key': 'db_hostname', 'default': 'http://www.example.org',
        ...    'precedence': 'command'})
        >>> print opts3['db_hostname']
        http://www.example.org
        >>> print [{'default': opt.default, 'priority': opt.priority} for opt
        ... in opts3.list('db_hostname')] # doctest: +NORMALIZE_WHITESPACE
        [{'default': 'http://www.example.org', 'priority': 30004300},
         {'default': 'localhost', 'priority': 35004300}]

        # Narrower scope target
        >>> opts4 = Options(scope='colibri.task.rdkitbuildtask', **data)
        >>> print opts4['db_hostname']
        localhost

        # Add option with higher precedence and same target
        >>> opts5 = opts4.update(db_hostname={
        ... 'key': 'db_hostname', 'default': 'http://www.example.org',
        ... 'target': 'colibri.task.rdkitbuildtask', 'precedence': 'command'})
        >>> print opts5['db_hostname']
        http://www.example.org

        # Add option with same precedence and mismatched target
        >>> opts6 = opts4.update(db_hostname={
        ...     'key': 'db_hostname', 'default': 'http://www.example.org',
        ...     'target': 'colibri.task.openbabeltask',
        ...     'precedence': 'default'})
        >>> print opts6['db_hostname']
        localhost

        # Set non-data keyword argument (scope)
        >>> opts7 = opts4.set(scope='colibri.task.rdkitbuildtask')
        >>> print opts7['db_hostname']
        localhost
        >>> print opts7.scope
        colibri.task.rdkitbuildtask
        >>> opts7 is opts4
        False

        # Set option
        >>> opts8 = opts4.set(db_hostname={
        ...     'key': 'db_hostname', 'default': 'http://www.example.org',
        ...     'target': 'colibri.task.rdkitbuildtask',
        ...     'precedence': 'default'})
        >>> print opts8['db_hostname']
        http://www.example.org

        # Delete option
        >>> opts9 = opts.delete('db_hostname')
        >>> print opts9['db_hostname']
        Traceback (most recent call last):
        KeyError
        >>> print opts9.get('db_hostname')
        None

        # Delete multiple options
        >>> print (opts9.get('db_hostname'), opts9.get('db_port'))
        (None, 27017)
        >>> opts10 = opts9.delete('db_hostname', 'db_port')
        >>> print (opts10.get('db_hostname'), opts10.get('db_port'))
        (None, None)

        # Filter by target
        >>> opts11 = opts4.filter(target='colibri.task.rdkitbuildtask')
        >>> print (opts11['configuration_program'],
        ...        opts11['configuration_version'])
        ('RDKit', '2015.3.1')

        # List of all options
        >>> print opts11.list() # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
        [OptionItem(... key='configuration_version', ...),
         OptionItem(... key='configuration_program', ...)]

        # Iterator over all options
        >>> print list(opts11.iterlist())
        ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
        [OptionItem(... key='configuration_version', ...),
         OptionItem(... key='configuration_program', ...)]

        # Highest-precedence item
        >>> print opts.item('db_hostname')
        ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
        OptionItem(group='db_hostname', key='db_hostname',
        default='localhost', description='...',
        precedence='default', target='colibri')

        # Filter by target
        >>> opts12 = opts4.filter(target='colibri.task.openbabeltask')
        >>> print (opts12.get('configuration_program'),
        ...        opts12.get('configuration_version'))
        (None, None)

        # Set scope
        >>> opts13 = opts12.set(scope='colibri.task.openbabeltask')
        >>> print (opts13.get('configuration_program'),
        ...        opts13.get('configuration_version'))
        ('OpenBabel', '2.3.1')

        # Roundtrip object conversion via repr/eval
        >>> opts14 = eval(repr(opts))
        >>> print opts14['db_hostname']
        localhost
        >>> print opts14.scope
        colibri

        # Roundtrip object conversion via fromdict/todict
        >>> opts15 = Options.fromdict(**opts.todict())
        >>> print opts15['db_hostname']
        localhost
        >>> print opts15.scope
        colibri
        >>> print opts15.precedence
        default

        # Set target and precedence
        >>> opts16 = opts15.set(precedence='user', target='colibri.task')
        >>> print opts16.scope
        colibri
        >>> print opts16.precedence
        user
        >>> print opts16.target
        colibri.task
    """

    def __init__(self, *args, **kwargs):

        # Non-data keyword arguments and default values
        self._nondata = {'scope': 'colibri',
                         'precedence': 'default',
                         'target': 'colibri'}

        # Read non-data keyword arguments
        for key, val in self._nondata.iteritems():
            setattr(self, '_' + key, kwargs.get(key, val))

        # Dictionary of all options
        self._all = {}

        # Process list arguments
        for arg in args:

            # Random valid identifier is generated
            key = '_' + randomid(pos=6)

            # Options object
            if isinstance(arg, Options):
                kwargs.update(**arg.todict())

            # OptionItem object
            elif isinstance(arg, OptionItem):
                # Set precedence and target if undefined
                if getattr(arg, 'precedence', None) is None:
                    arg.precedence = self.precedence
                if getattr(arg, 'target', None) is None:
                    arg.target = self.target
                self._all[key] = arg

            # dict representation of OptionItem
            elif isinstance(arg, dict):
                # Set precedence and target if undefined
                if arg.get('precedence') is None:
                    arg['precedence'] = self.precedence
                if arg.get('target') is None:
                    arg['target'] = self.target
                self._all[key] = OptionItem(**arg)

            # Single string is interpreted as OptionItem with value True
            elif isinstance(arg, str):
                self._all[key] = OptionItem(
                    key=arg, value=True, precedence=self.precedence,
                    target=self.target)
            else:
                raise ValueError

        # Process keyword arguments
        for key, val in kwargs.iteritems():

            # Ignore non-data keyword arguments
            if key in self._nondata:
                continue

            # Add extra characters if key is already taken
            orig_key = key
            if key in self._all:
                key += ('_' + randomid(pos=6))

            # OptionItem object
            if isinstance(val, OptionItem):
                # Set precedence and target if undefined
                if getattr(val, 'precedence', None) is None:
                    val.precedence = self.precedence
                if getattr(val, 'target', None) is None:
                    val.target = self.target
                self._all[key] = val

            # dict representation of OptionItem
            elif isinstance(val, dict):
                # Set precedence and target if undefined
                if val.get('precedence') is None:
                    val['precedence'] = self.precedence
                if val.get('target') is None:
                    val['target'] = self.target
                self._all[key] = OptionItem(**val)

            # Interpreted as key-value pair
            else:
                self._all[key] = OptionItem(
                    key=key, value=val, group=orig_key,
                    precedence=self.precedence, target=self.target)

        # Uppercase the scope for comparisons
        scope = self._scope.upper()

        # Select options active in current target, sorted by group
        self._active = {}
        for key, opt in self._all.iteritems():
            if scope.find(opt.target.upper()) != 0:
                continue
            self._active.setdefault(opt.group, []).append(opt)

        # Sort options for each option group
        for key, vals in self._active.iteritems():
            vals.sort()

    def __repr__(self):
        """Construct string representation of the object attributes."""

        # Add all options
        opts = []
        for key, opt in self._all.iteritems():
            opts.append('%s=%r' % (key, opt))

        # Add non-data keyword arguments
        for key in self._nondata.iterkeys():
            opts.append('%s=%r' % (key, getattr(self, '_' + key)))

        return type(self).__name__ + '(' + ', '.join(opts) + ')'

    def __getitem__(self, key):
        """
        Return option value of highest precedence and matching target.
        Options are assumed to be sorted by ascending precedence.
        If option value is not available, the default of highest
        precedence (or None) is returned. Raises KeyError if the option
        is not found.

        Args:
            key (str): Key name

        Raises:
            KeyError: Key not found
        """

        # Key absent in the list of active options
        if key not in self._active:
            raise KeyError

        # Search options for value
        vals = filter(lambda v: v is not None, [
            getattr(opt, 'value', None) for opt in self._active[key]])
        if vals:
            return vals[0]
        else:
            # Search options for default
            vals = filter(lambda v: v is not None, [
                getattr(opt, 'default', None) for opt in self._active[key]])
            if vals:
                return vals[0]
            else:
                return None

    def __len__(self):
        """Return number of groups."""
        return len(self._active)

    def __iter__(self):
        """Iterate over the options."""
        return iter(self._active)

    def get(self, key, default=None):
        """Return option value or option default of highest precedence.
        Return None if option is not found.

        Args:
            key (str): Key name
            default (str, optional): Default value
        """

        try:
            return self[key]
        except KeyError:
            return default

    def list(self, key=None):
        """
        Return a list of all OptionItem objects for a given group or
        for all groups if input is None.
        Options are sorted by ascending precedence. Raises KeyError
        if the specified group is not found.

        Args:
            key (str, optional): Key name. All keys returned if None.
        """

        if key is None:
            return [opt for values in self._active.values() for opt in values]
        else:
            return self._active[key]

    def iterlist(self, key=None):
        """
        Return an iterator over all OptionItem objects for a given
        group or for all groups if input is None.
        Options are sorted by ascending precedence. Raises KeyError
        if the specified group is not found.

        Args:
            key (str, optional): Key name. All keys returned if None.
        """

        if key is None:
            return it.chain.from_iterable(self._active.itervalues())
        else:
            return iter(self._active[key])

    def item(self, key):
        """
        Returns the highest-precedence OptionItem object for a given group.
        Raises KeyError if the specified group is not found.

        Args:
            key (str): Key name
        """

        return next(self.iterlist(key))

    def set(self, **kwargs):
        """
        Return a new Options object with options modified.
        Old options or non-data keyword arguments are overwritten.
        The original Options object is not changed. Same keyword
        input parameters as in the Options constructor are allowed.

        """

        # Copy existing options
        opts = self.todict()

        # Set additional options
        for key, val in kwargs.iteritems():

            # Ignore non-data keyword arguments
            if key in self._nondata:
                continue

            # OptionItem object
            if isinstance(val, OptionItem):
                opts[key] = val

            # dict representation of OptionItem
            elif isinstance(val, dict):
                opts[key] = OptionItem(**val)

            # Interpreted as key-value pair
            else:
                opts[key] = OptionItem(key=key, value=val)

        # Add non-data keyword arguments from input.
        for key in self._nondata.iterkeys():
            if key in kwargs:
                opts[key] = kwargs[key]

        return self.fromdict(**opts)

    def update(self, *args, **kwargs):
        """
        Return a new Options object containing additional options.
        Old options are not overwritten. The original Options object
        is not changed. Same list and keyword arguments as in the
        Options constructor are allowed.

        """

        # Copy existing options
        opts = self.todict()

        # Add additional options
        for key, val in kwargs.iteritems():

            # Ignore non-data keyword arguments
            if key in self._nondata:
                continue

            # Add extra characters if key is already taken
            orig_key = key
            if key in self._all:
                key += ('_' + randomid(pos=6))

            # OptionItem or its dict representation
            if isinstance(val, OptionItem) or isinstance(val, dict):
                opts[key] = val

            # Interpreted as key-value pair
            else:
                opts[orig_key] = OptionItem(
                    key=key, value=val, group=orig_key,
                    precedence=self.precedence, target=self.target)

        # Add non-data keyword arguments from object.
        # Non-data keyword arguments from input are ignored.
        # Use set() to modify non-data keyword arguments.
        for key in self._nondata.iterkeys():
            opts[key] = getattr(self, '_' + key)

        return self.fromdict(*args, **opts)

    def delete(self, *args):
        """
        Return a new Options object with the specified keys removed.
        Does not remove non-data keyword arguments.
        The original Options object is not changed.

        """

        # Output options
        opts = {}

        # Delete options, if the keys in their option bodies match
        for key, opt in self.todict().iteritems():

            # Ignore non-data keyword arguments
            if key in self._nondata:
                continue

            # Add non-matching options
            if opt.get('group') not in args:
                opts[key] = opt

        return self.fromdict(**opts)

    def filter(self, **kwargs):
        """
        Return a new Options object filtered by option fields.
        Only equality tests are currently supported.
        The original Options object is not changed.

        """

        # Output options
        opts = {}

        # Filter by field-value pairs. All tests must return True.
        # Only equality tests are currently supported.
        for key, opt in self.todict().iteritems():

            # Ignore non-data keyword arguments
            if key in self._nondata:
                continue

            # Add matching options
            for field, val in kwargs.iteritems():
                if opt.get(field) != val:
                    break
            else:
                opts[key] = opt

        # Add non-data keyword arguments from object.
        for key in self._nondata.iterkeys():
            opts[key] = getattr(self, '_' + key)

        return self.fromdict(**opts)

    def todict(self, nondata=True):
        """Export object as Python dictionary.

        Args:
            nondata (bool, optional): Export non-data attributes
        """

        # Return options
        opts = {}
        for key, opt in self._all.iteritems():

            # Ignore non-data keyword arguments
            if key in self._nondata:
                continue

            # Add to dictionary
            opts[key] = opt.todict()

        # Add non-data keyword arguments from object
        if nondata:
            for key in self._nondata.iterkeys():
                opts[key] = getattr(self, '_' + key)

        return opts

    @classmethod
    def fromdict(cls, *args, **kwargs):
        """New Options object constructor."""
        return cls(*args, **kwargs)

    @property
    def scope(self):
        """Options scope: current target for OptionItem objects."""
        return self._scope

    @property
    def precedence(self):
        """Default precedence for OptionItem objects."""
        return self._precedence

    @property
    def target(self):
        """Default target for OptionItem objects."""
        return self._target
