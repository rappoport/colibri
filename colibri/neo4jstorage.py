"""Graph storage implementation using the Neo4J backend.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    Neo4JClient: Neo4J client wrapper class
    Neo4JGraphStorage: Graph storage interface via Neo4J Cypher resource
"""

__all__ = ['Neo4JClient', 'Neo4JGraphStorage']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '01/12/2016'

#
# Standard library
#


#
# Third-party modules
#

# This code breaks with py2neo 4.x
# Make sure to install py2neo>=3,<4

try:
    import py2neo
    from py2neo.database import GraphError, Unauthorized
    from py2neo.packages.httpstream.http import SocketError
except ImportError:
    py2neo = None


#
# colibri submodules
#

from colibri.exceptions import ConnectionError, ExecutionError


class Neo4JClient(object):
    """Neo4J client wrapper class."""

    def __init__(self, client=None, host='localhost', port=7474,
                 proto='http', user='neo4j', password='neo4j',
                 **kwargs):
        """
        Neo4JClient object constructor.

        Args:
            client (Neo4JClient, optional): Client to clone
            host (str, optional): Neo4J host
            port (int, optional): Neo4J port
            proto (str, optional): Neo4J connection protocol
            user (str, optional): Neo4J user name
            password (str, optional): Neo4J user password
            **kwargs: Additional keyword arguments

        Raises:
            Unauthorized: Access to Neo4J unauthorized
            ConnectionError: Other problems connecting to Neo4J server
        """

        # Raise exception if Neo4J client library is not available
        if py2neo is None:
            raise ExecutionError('No Neo4J client found')

        # Hostname
        self._host = host

        # Port
        self._port = port

        # Proto
        self._proto = proto

        # User name
        self._user = user

        # User password
        self._password = password

        # Default keyword arguments
        # bolt = False: No BOLT for now
        allkwargs = {'bolt': False}

        # Add user-defined arguments
        allkwargs.update(kwargs)

        # Clone open connection to Neo4J
        if client is not None:
            self._client = client._client

        else:
            try:
                py2neo.authenticate(
                    '%s:%d' % (self._host, self._port), self._user,
                    self._password)

                self._graph = py2neo.Graph('%s://%s:%d/db/data' % (
                    self._proto, self._host, self._port), **allkwargs)
            except Unauthorized:
                raise ConnectionError('Access to Neo4J server unauthorized')
            except GraphError:
                raise ConnectionError('Cannot reach Neo4J server')

    def __str__(self):
        return 'Neo4JClient %s:%d (%s)' % (self._host, self._port, self._proto)

    def server_info(self):
        """Return server information."""
        try:
            info = self._graph.dbms.config
        except (GraphError, SocketError):
            raise ConnectionError('Cannot get Neo4J info')
        return info

    def close(self):
        """Close the connection to Neo4J client."""
        pass

    @property
    def graph(self):
        """Expose underlying graph resource."""
        return self._graph

    def graph_storage(self, **kwargs):
        """
        Create new Neo4JGraphStorage instance.

        Args:
            **kwargs: See Neo4JGraphStorage constructor

        Returns:
            graph_storage: Neo4JGraphStorage instance
        """
        return Neo4JGraphStorage(client=self, **kwargs)


class Neo4JGraphStorage(object):
    """Graph storage interface via Neo4J Cypher resource."""

    def __init__(self, client=None, host='localhost', port=7474, proto='http',
                 **kwargs):
        """Neo4JGraphStorage object constructor.

        Args:
            client (Neo4JClient, optional): Neo4J client
            host (str, optional): Neo4J storage host
            port (int, optional): Neo4J storage port
            proto (str, optional): Neo4J connection protocol
            **kwargs: Description
        """

        # Clone open connection to Neo4J
        if client is not None:
            self._client = client

            # Hostname
            self._host = client._host

            # Port
            self._port = client._port

            # Protocol
            self._proto = client._proto

        # Open new Neo4J client
        else:
            self._client = Neo4JClient(host=host, port=port, proto=proto,
                                       **kwargs)

            # Hostname
            self._host = host

            # Port
            self._port = port

            # Protocol
            self._proto = proto

        # Save default graph resource
        self._graph = self._client.graph

    def __str__(self):
        return 'Neo4JGraphStorage %s:%d (%s)' % (self._host, self._port,
                                                 self._proto)

    def __getattr__(self, key):
        """Delegate attribute calls to default graph resource."""
        return getattr(self._graph, key)
