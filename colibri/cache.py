"""Caching classes and interfaces for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    CacheClient: Base cache client class
    Cache: Base cache interface
"""

__all__ = ['CacheClient', 'Cache']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '06/09/2015'


class CacheClient(object):
    def __init__(self, client=None, host='localhost', port=80,
                 **kwargs):
        raise NotImplementedError

    def close(self):
        """Close connection to cache client."""
        raise NotImplementedError

    def cache(self, **kwargs):
        """Create new Cache instance."""
        raise NotImplementedError


class Cache(object):
    """Base cache interface."""

    def __init__(self, **kwargs):
        raise NotImplementedError

    def __str__(self):
        return '<Cache>'

    def get(self, keys):
        raise NotImplementedError

    def set(self, items):
        raise NotImplementedError

    def delete(self, keys):
        raise NotImplementedError

    def clear(self):
        raise NotImplementedError
