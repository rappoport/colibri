"""Storage implementations using the MongoDB backend.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    MongoClient: MongoDB client wrapper class
    MongoStorage: Storage interface for the MongoDB backend
    MongoFileStorage: File storage interface using Mongo GridFS
    MongoQueryRequest: Query request for the MongoDB backend
    MongoUpdateRequest: Update request for the MongoDB backend
    MongoUpdateLock: Update lock for the MongoDB backend
    MongoQueryCursor: Query cursor for the MongoDB backend
"""


__all__ = ['MongoClient', 'MongoStorage', 'MongoFileStorage',
           'MongoQueryRequest', 'MongoUpdateRequest', 'MongoUpdateLock',
           'MongoQueryCursor']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/15/2015'


#
# Standard library
#

import time
import datetime

#
# Third-party modules
#

import pymongo
from pymongo.bulk import BulkOperationBuilder
from pymongo.errors import AutoReconnect, ConnectionFailure, BulkWriteError
import gridfs
from gridfs.errors import FileExists

#
# pymongo version check
#

# Check pymongo for version (for bulk interface)
if pymongo.version_tuple < (2, 7, 0):
    raise RuntimeError('Pymongo version >= 2.7 required')

#
# colibri submodules
#

from colibri.exceptions import InternalError, ConnectionError
from colibri.storage import StorageClient, Storage, FileStorage, \
    QueryRequest, UpdateRequest, UpdateLock, QueryCursor


class MongoClient(StorageClient):
    """MongoDB storage client."""

    def __init__(self, client=None, host='localhost', port=27017,
                 **kwargs):
        """MongoClient object constructor.

        Args:
            client (MongoClient, optional): Client to clone
            host (str, optional): MongoDB host
            port (int, optional): MongoDB port
            **kwargs: Additional options

        Raises:
            ConnectionError: Problems connecting to MongoDB server
        """

        # Hostname
        self._host = host

        # Port
        self._port = port

        # Default keyword arguments
        # connect = False: Do not connect on start
        # w = 1: Replication factor for writing
        # j = 1: Enable journaling
        #
        # Argument connect is set to False to prevent the
        # MongoDB client from connecting to the server
        # on start. This leads to race conditions in the
        # driver under multiprocessing module.
        # https://jira.mongodb.org/browse/PYTHON-961
        allkwargs = {'connect': False,
                     'w': 1,
                     'j': True}

        # Add user-defined arguments
        allkwargs.update(kwargs)

        # Clone open connection to MongoDB
        if client is not None:
            self._client = client._client

        # Open new MongoDB connection
        else:
            try:
                self._client = pymongo.MongoClient(
                    host=host, port=port, **allkwargs)
            except ConnectionFailure:
                raise ConnectionError('Cannot reach MongoDB server')

    def __str__(self):
        return 'MongoClient %s:%d' % (self._host, self._port)

    def __getitem__(self, key):
        """Dictionary-style access to databases and collections."""
        return self._client.__getitem__(key)

    def __getattr__(self, key):
        """Attribute-style access to databases and collections."""
        return self._client.__getattr__(key)

    def close(self):
        """Close the connection to MongoDB client."""
        self._client.close()

    def server_info(self):
        """Return server information."""
        try:
            info = self._client.server_info()
        except ConnectionFailure:
            raise ConnectionError('Cannot get MongoDB info')
        return info

    def database_names(self):
        """Return existing database names."""
        return self._client.database_names()

    def storage(self, **kwargs):
        """Create new MongoStorage instance.

        Args:
            **kwargs: See MongoStorage constructor

        Returns:
            storage: MongoStorage instance
        """

        return MongoStorage(client=self, **kwargs)

    def file_storage(self, **kwargs):
        """Create new MongoFileStorage instance.

        Args:
            **kwargs: See MongoFileStorage constructor

        Returns:
            file_storage: MongoFileStorage instance
        """

        return MongoFileStorage(client=self, **kwargs)

    def query_request(self, **kwargs):
        """Create new MongoQueryRequest instance.

        Args:
            **kwargs: See MongoQueryRequest constructor

        Returns:
            query_request: MongoQueryRequest instance
        """

        return MongoQueryRequest(**kwargs)

    def update_request(self, **kwargs):
        """Create new MongoUpdateRequest instance.

        Args:
            **kwargs: See MongoUpdateRequest constructor

        Returns:
            update_request: MongoUpdateRequest instance
        """

        return MongoUpdateRequest(**kwargs)

    def update_lock(self, **kwargs):
        """Create new MongoUpdateLock instance.

        Args:
            **kwargs: See MongoUpdateLock constructor

        Returns:
            update_lock: MongoUpdateLock instance
        """

        return MongoUpdateLock(**kwargs)


class MongoStorage(Storage):
    """Storage interface using MongoDB collection."""

    def __init__(self, client=None, host='localhost', port=27017, db='test',
                 collection='ns', write_attempts=5, write_interval=10,
                 **kwargs):
        """MongoStorage object constructor.

        Args:
            client (MongoClient, optional): Storage client to clone
            host (str, optional): Storage host
            port (int, optional): Storage port
            db (str, optional): Database name (default: 'test')
            collection (str, optional): Collection name (default: 'ns')
            write_attempts (int, optional): # of write attempts (default: 5)
            write_interval (int, optional): Write interval (default: 10)
            **kwargs: Additional options
        """

        # Clone open connection to MongoDB
        if client is not None:
            self._client = client

            # Hostname
            self._host = client._host

            # Port
            self._port = client._port

        # Open new MongoDB client
        else:
            self._client = MongoClient(host=host, port=port, **kwargs)

            # Hostname
            self._host = host

            # Port
            self._port = port

        # Database name
        self._db = db

        # Collection name
        self._collection = collection

        # MongoDB collection
        self._storage = self._client[db][collection]

        # Number of write attempts
        self._write_attempts = write_attempts

        # Write interval
        self._write_interval = write_interval

    def __str__(self):
        return 'MongoStorage %s:%d/%s/%s' % (
            self._host, self._port, self._db, self._collection)

    def find(self, request, lock=None, **kwargs):
        """Retrieve records from MongoDB storage. Allows for pessimistic and
           optimistic offline locks.

        Parameters:
            request (MongoQueryRequest): Query specifications
            lock (MongoUpdateLock, optional): Lock specifications
            kwargs: Additional options

        Returns:
            cursor (MongoQueryCursor): Query result cursor

        Raises:
            ConnectionError: Problem with storage connection
        """

        # Query specification
        query = request.query

        # Query projection
        projection = request.projection

        # Query limit
        limit = request.limit

        # Query sort order
        sort = request.sort

        # Pessimistic lock
        if lock is not None and lock.pessimistic:

            # Locking query
            locked = lock.lock

            # Locking update
            update = {'$set': locked}

            # Use bulk write operation for locking
            op = BulkOperationBuilder(self._storage, ordered=False)
            bw = op.find(query)

            # Number of operations limited
            if limit:
                for _ in xrange(limit):
                    bw.update_one(update)
            # Apply to all matching documents
            else:
                bw.update(update)

            try:
                op.execute()
            # Locking failed
            except (BulkWriteError, ConnectionFailure) as e:
                raise ConnectionError('Cannot lock records: %s' % e)

            # Read locked records
            try:
                cursor = MongoQueryCursor(self._storage.find(
                    locked, projection, sort=sort, **kwargs))
            except ConnectionFailure as e:
                raise ConnectionError('Cannot read locked records: %s' % e)

        # No (or optimistic) record locking
        else:
            try:
                cursor = MongoQueryCursor(
                    self._storage.find(query, projection, limit=limit,
                                       sort=sort, **kwargs))
            except ConnectionFailure:
                raise ConnectionError

        return cursor

    def update(self, requests, lock=None, multi=False, **kwargs):
        """Update records in storage.

        Args:
            requests (list): List of UpdateRequest objects
            lock (UpdateLock, optional): Update lock (pessimistic/optimistic)
            multi (bool, optional): Apply to multiple records
            kwargs: Additional options

        Raises:
            ConnectionError: Problem with storage connection
        """

        if not requests:
            return

        # Pessimistic and/or optimistic locking
        if lock is not None:
            for write_attempt in xrange(self._write_attempts):

                # Construct operation object
                op = BulkOperationBuilder(self._storage, ordered=False)
                for request in requests:
                    query = request.query
                    update = request.update
                    overwrite = request.overwrite

                    # Overwrite mode, update specs are ignored
                    if overwrite is not None:
                        op.find(query).replace_one(overwrite)

                    # Update mode
                    else:
                        op.find(query).update_one(update)

                # Try to execute operation object
                try:
                    op.execute()
                # Sleep and try to reconnect
                except AutoReconnect:
                    time.sleep(self._write_interval)
                # Cannot write, ignore for now
                # Might need to roll back here
                except (BulkWriteError, ConnectionFailure) as e:
                    raise ConnectionError(
                        'Cannot write to storage: %s' % e)
                else:
                    break
            else:
                raise ConnectionError(
                    'Cannot write to storage: Number of retries exceeded')

        # Multi update
        elif multi:
            for request in requests:
                query = request.query
                update = request.update
                overwrite = request.overwrite

                # Only update is possible here, overwrite specs are ignored
                for write_attempt in xrange(self._write_attempts):
                    try:
                        self._storage.update(query, update, multi=True)
                    except AutoReconnect:
                        time.sleep(self._write_interval)
                    else:
                        break
                else:
                    raise ConnectionError(
                        'Cannot write to storage: Number of retries exceeded')

        # No locking (upsert)
        else:
            for write_attempt in xrange(self._write_attempts):

                # Construct operation object
                op = BulkOperationBuilder(self._storage, ordered=False)
                for request in requests:
                    query = request.query
                    update = request.update
                    overwrite = request.overwrite

                    # Overwrite mode, update specs are ignored
                    if overwrite is not None:
                        op.find(query).upsert().replace_one(overwrite)

                    # Update mode
                    else:
                        op.find(query).upsert().update_one(update)

                # Try to execute operation object
                try:
                    op.execute()
                # Sleep and try to reconnect
                except AutoReconnect:
                    time.sleep(self._write_interval)
                # Cannot write, ignore for now
                # Might need to roll back here
                except (BulkWriteError, ConnectionFailure) as e:
                    raise ConnectionError(
                        'Cannot write to storage: %s' % e)
                else:
                    break
            else:
                raise ConnectionError(
                    'Cannot write to storage: Number of retries exceeded')

    def delete(self, requests, multi=False, **kwargs):
        """Delete records from storage

        Args:
            requests (list): List of QueryRequest objects
            multi (bool, optional): Apply to multiple records
            kwargs: Additional options
        """

        if not requests:
            return

        # Multi update
        if multi:
            for request in requests:
                for write_attempt in xrange(self._write_attempts):
                    try:
                        self._storage.remove(request.query)
                    except AutoReconnect:
                        time.sleep(self._write_interval)
                    else:
                        break
                else:
                    raise ConnectionError(
                        'Cannot write to storage: Number of retries exceeded')

        # Delete individual records
        else:
            for write_attempt in xrange(self._write_attempts):

                # Construct operation object
                op = BulkOperationBuilder(self._storage, ordered=False)
                for request in requests:
                    op.find(request.query).remove_one()

                # Try to execute operation object
                try:
                    op.execute()
                # Sleep and try to reconnect
                except AutoReconnect:
                    time.sleep(self._write_interval)
                # Cannot write
                except (BulkWriteError, ConnectionFailure) as e:
                    raise ConnectionError(
                        'Cannot write to storage: %s' % e)
                else:
                    break
            else:
                raise ConnectionError(
                    'Cannot write to storage: Number of retries exceeded')

    def clear(self):
        """Clear the storage."""
        self._storage.remove({})


class MongoFileStorage(FileStorage):
    """File storage interface using MongoDB GridFS."""

    def __init__(self, client=None, host='localhost', port=27017, db='test',
                 collection='ns', write_attempts=5, write_interval=10,
                 **kwargs):
        """MongoFileStorage object constructor.

        Args:
            client (MongoClient, optional): MongoDB storage client
            host (str, optional): MongoDB storage host
            port (int, optional): MongoDB storage port
            db (str, optional): Database name (default: 'test')
            collection (str, optional): Collection name (default: 'ns')
            write_attempts (int, optional): # of write attempts (default: 5)
            write_interval (int, optional): Write interval in s (default: 10)
            **kwargs: Additional options
        """

        # Clone open connection to MongoDB
        if client is not None:
            self._client = client

            # Hostname
            self._host = client._host

            # Port
            self._port = client._port

        # Open new MongoDB client
        else:
            self._client = MongoClient(host=host, port=port, **kwargs)

            # Hostname
            self._host = host

            # Port
            self._port = port

        # Database name
        self._db = db

        # Collection name
        self._collection = collection

        # MongoDB collection
        self._filestorage = gridfs.GridFS(
            self._client[db], collection=collection)

        # Number of write attempts
        self._write_attempts = write_attempts

        # Write interval
        self._write_interval = write_interval

    def __str__(self):
        return 'MongoFileStorage %s:%d/%s/%s' % (
            self._host, self._port, self._db, self._collection)

    def get(self, key):
        return self._filestorage.get(key)

    def set(self, key, value, **kwargs):
        for write_attempt in xrange(self._write_attempts):
            try:
                self._filestorage.put(value, _id=key, **kwargs)
                break
            except AutoReconnect:
                time.sleep(self._write_interval)
            except FileExists:
                break
        else:
            raise ConnectionError(
                'Cannot write to file storage: Number of retries exceeded')

    def delete(self, key):
        self._filestorage.delete(key)


class MongoQueryRequest(QueryRequest):
    """Query request for MongoDB collection.

    Attributes:
        limit (int): Maximum number of returned results
        projection (dict): Query projection
        query (dict): Query specification
        sort (dict): Sort specification
        ts (datetime.datetime): Timestamp
    """

    def __init__(self, query=None, projection=None, limit=0, sort=None,
                 **kwargs):
        """MongoQueryRequest object constructor.

        Args:
            query (dict, optional): Query specification
            projection (dict, optional): Query projection
            limit (int, optional): Maximum number of returned results
            sort (dict, optional): Sort specification
            **kwargs: Additional options
        """

        # Query specification
        if query is None:
            query = {}
        self.query = query

        # Query projector
        self.projection = projection

        # Query limit
        self.limit = limit

        # Query sort order
        self.sort = sort

        # Time stamp
        self.ts = datetime.datetime.utcnow()


class MongoUpdateRequest(UpdateRequest):
    """
    Update request for MongoDB collection.

    Attributes:
        add (dict): Item add (set on insert) specification
        intersection (dict): Item update by intersection
        overwrite (dict): Unconditionally replace entire document with new one
        query (dict): Query specification
        remove (dict): Item remove specification
        replace (dict): Item replace (unconditional set) specfication
        ts (datetime.datetime): Timestamp
        union (dict): Item update by union
        update (dict): Document update specification, computed from add,
                       replace, remove, union, intersection specs
    """

    def __init__(self, query=None, add=None, replace=None, remove=None,
                 union=None, intersection=None, overwrite=None,
                 **kwargs):
        """MongoUpdateRequest object constructor.

        Args:
            query (dict, optional): Query specification
            add (dict, optional): Item add (set on insert) specification
            replace (dict, optional): Item replace (unconditional set) spec
            remove (dict, optional): Item remove specification
            union (dict, optional): Item update by union
            intersection (dict, optional): Item update by intersection
            overwrite (dict, optional): Unconditionally replace entire document
                                        with new one
            **kwargs: Additional options
        """

        # Query specification
        if query is None:
            query = {}
        self.query = query

        # Overwrite document, update specs are ignored
        if overwrite is not None:
            self.overwrite = overwrite
            self.update = None

        # Update document
        else:
            update = {}

            # Add instructions
            self.add = add
            if add is not None:
                update['$setOnInsert'] = add

            # Replace instructions
            self.replace = replace
            if replace is not None:
                update['$set'] = replace

            # Remove instructions
            self.remove = remove
            if remove is not None:
                update['$unset'] = remove

            # Union instructions
            self.union = union
            if union is not None:
                union_update = {}
                for k, v in union.iteritems():
                    if v:
                        union_update[k] = {'$each': v}
                if union_update:
                    update['$addToSet'] = union_update

            # Intersection instructions
            self.intersection = intersection
            if intersection is not None:
                intersection_update = {}
                for k, v in intersection.iteritems():
                    if v:
                        intersection_update[k] = {'$nin': v}
                if intersection_update:
                    update['$pull'] = intersection_update

            # Set update document
            self.update = update

            # No overwrite specification
            self.overwrite = None

        # Time stamp
        self.ts = datetime.datetime.utcnow()


class MongoUpdateLock(UpdateLock):
    """Update lock instance for MongoDB collection.

    Attributes:
        lock (dict): Locking specification
        optimistic (bool): Optimistic locking
        pessimistic (bool): Pessimistic locking
    """

    def __init__(self, optimistic=True, pessimistic=False,
                 lock=None, **kwargs):
        """MongoUpdateLock object constructor.

        Args:
            optimistic (bool, optional): Optimistic locking
            pessimistic (bool, optional): Pessimistic locking
            lock (dict, optional): Locking update
            **kwargs: Additonal options

        Raises:
            InternalError: Internal code inconsistency
        """

        # Optimistic offline lock
        self.optimistic = optimistic

        # Pessimistic offline lock
        self.pessimistic = pessimistic

        # Pessimistic lock requires locking update
        if pessimistic and lock is None:
            raise InternalError('Locking update missing')

        # Locking update
        self.lock = lock


class MongoQueryCursor(QueryCursor):
    """Query cursor for MongoDB collection. Implements Iterator interface."""

    def __init__(self, cursor, **kwargs):
        self._cursor = cursor

    def __iter__(self):
        return self

    def next(self):
        try:
            return next(self._cursor)
        except ConnectionFailure as e:
            raise ConnectionError('Cannot read cursor: %s' % e)

    def count(self):
        try:
            return self._cursor.count(with_limit_and_skip=True)
        except ConnectionFailure as e:
            raise ConnectionError('Cannot read cursor: %s' % e)

    def batch_size(self, batch_size):
        try:
            self._cursor.batch_size(batch_size)
        except ConnectionFailure as e:
            raise ConnectionError('Cannot read cursor: %s' % e)

    def close(self):
        try:
            self._cursor.close()
        except ConnectionFailure as e:
            raise ConnectionError('Cannot close cursor: %s' % e)
