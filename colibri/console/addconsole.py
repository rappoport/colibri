"""API for colib add console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    AddConsoleCommand: colib add console subcommand
"""

__all__ = ['AddConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/11/2015'

#
# colibri submodules
#

from colibri.api import AddCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.utils import ensure_logger

#
# Classes
#


class AddConsoleCommand(ConsoleCommand):
    """colib add console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib add subcommand.

        Args:
            subparsers (ArgumentParser): colib subparsers
        """

        # Add subparser
        cls._subparser = subparsers.add_parser(
            'add', help='add data to database')

        # Add exec argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'add execution arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-e', '--molecules', nargs='+', metavar='MOL',
            help='input molecules as SMILES/JSON string')
        cls._exec_parser_args.append('molecules')

        cls._exec_parser.add_argument(
            '-E', '--flasks', nargs='+', metavar='FLASK',
            help='input flasks as SMILES/JSON string')
        cls._exec_parser_args.append('flasks')

        cls._exec_parser.add_argument(
            '-i', '--molecule-file', dest='molecule_file', metavar='FILE',
            help='molecule input from file, one SMILES/JSON string per line')
        cls._exec_parser_args.append('molecule_file')

        cls._exec_parser.add_argument(
            '-I', '--flask-file', dest='flask_file', metavar='FILE',
            help='flask input from file, one SMILES/JSON string per line')
        cls._exec_parser_args.append('flask_file')

        # Add configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'add configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

        # Individual configuration arguments
        cls._conf_parser.add_argument(
            '-f', '--update-on-insert', dest='update_on_insert',
            action='store_true', default=None,
            help='update database entries on insert')
        cls._conf_parser_args.append('update_on_insert')

        cls._conf_parser.add_argument(
            '-F', '--overwrite-on-insert', dest='overwrite_on_insert',
            action='store_true', default=None,
            help='overwrite database entries on insert')
        cls._conf_parser_args.append('overwrite_on_insert')

        cls._conf_parser.add_argument(
            '-@', '--tags', nargs='+', help='molecule/flask tags')
        cls._conf_parser_args.append('tags')

        cls._conf_parser.add_argument(
            '-M', '--batch-size', dest='batch_size', type=int, metavar='N',
            help='batch size')
        cls._conf_parser_args.append('batch_size')

    def __init__(self, args, opts):
        """AddConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Molecule string input
        molecules = args.get('molecules', [])

        # Molecule file input
        if args.get('molecule_file'):
            molecules += [
                m.strip() for m in open(args['molecule_file']).readlines()]

        # Flask string input
        flasks = args.get('flasks', [])

        # Flask file input
        if args.get('flask_file'):
            flasks += [
                f.strip() for f in open(args['flask_file']).readlines()]

        # Process configuration command-line arguments for add subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save AddComand object
            self._cmd = AddCommand(molecules=molecules, flasks=flasks,
                                   opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('addconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
