"""colibri console program and commands

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Functions:
    console_program: Program call for colibri console scripts

Classes:
    ConsoleProgram: colibri console program classs
    ConsoleCommand: Base class for colibri console commands
"""

__all__ = ['console_program', 'ConsoleProgram', 'ConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/15/2015'

#
# Library modules
#

import json
import os.path
import argparse
import logging


#
# colibri submodules
#

from colibri.options import Options


#
# Public functions
#

def console_program(subcommands, argv):
    """Program call for colibri console scripts.

    Args:
        subcommands (dict): Mapping of subcommand names to ConsoleCommand
                            subclasses

    Returns:
        result (Result): Result of ConsoleCommand run
    """

    # Construct ConsoleProgram
    prg = ConsoleProgram()

    # Add parser
    parser = prg.add_parser()

    # Add subparsers
    subparsers = prg.add_subparsers()

    # Add individual subparsers
    for cmd in subcommands:
        subcommands[cmd].add_subparser(subparsers)

    # Parse command-line arguments
    args = vars(parser.parse_args(argv[1:]))

    # Keep only non-None values
    args = {key: val for key, val in args.items() if val is not None}

    # Read options from configuration files and environment variables
    opts = prg.read_config_files(args)

    # Add configuration command-line options
    opts = opts.update(prg.get_conf_arguments(args))

    # Run the subcommand
    result = subcommands[args['cmd']](args, opts).execute()

    return result

#
# Classes
#


class ConsoleProgram(object):
    """colibri console frontend."""

    @classmethod
    def add_parser(cls):
        """Add parser for colib console program.

        Returns:
            ArgumentParser: parser object
        """

        # Add parser
        cls._parser = argparse.ArgumentParser(
            prog='colib',
            description='colibri is your lightweight and gregarious ' +
                        'chemistry explorer')

        # Execution argument group
        cls._exec_parser = cls._parser.add_argument_group(
            'execution arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Execution program arguments

        cls._exec_parser.add_argument(
            '--version', action='version', version='%(prog)s v' + __version__)

        cls._exec_parser.add_argument(
            '--no-config', dest='no_config', action='store_true',
            help='ignore all configuration files')
        cls._exec_parser_args.append('no_config')

        cls._exec_parser.add_argument(
            '--no-site-config', dest='no_site_config', action='store_true',
            help='skip site configuration file')
        cls._exec_parser_args.append('no_site_config')

        cls._exec_parser.add_argument(
            '--site-config-path', dest='site_config_path', metavar='PATH',
            default='/usr/local/etc',
            help='location of site configuration. Default: /usr/local/etc')
        cls._exec_parser_args.append('site_config_path')

        cls._exec_parser.add_argument(
            '--no-user-config', dest='no_user_config', action='store_true',
            help='skip user configuration file')
        cls._exec_parser_args.append('no_user_config')

        cls._exec_parser.add_argument(
            '--no-env-config', dest='no_env_config', action='store_true',
            help='skip configuration environment variables')
        cls._exec_parser_args.append('no_env_config')

        cls._exec_parser.add_argument(
            '--no-proj-config', dest='no_proj_config', action='store_true',
            help='skip project configuration file')
        cls._exec_parser_args.append('no_proj_config')

        cls._exec_parser.add_argument(
            '--config-file', dest='config_file', metavar='FILE',
            default=None,
            help='colibri configuration file name. Default: colibri.json')
        cls._exec_parser_args.append('config_file')

        # Configuration argument group
        cls._conf_parser = cls._parser.add_argument_group(
            'configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

        # Configuration program arguments
        cls._conf_parser.add_argument(
            '-d', '--db-hostname', dest='db_hostname', metavar='HOST',
            help='MongoDB database hostname')
        cls._conf_parser_args.append('db_hostname')

        cls._conf_parser.add_argument(
            '--db-port', dest='db_port', metavar='PORT', type=int,
            help='MongoDB database port')
        cls._conf_parser_args.append('db_port')

        cls._conf_parser.add_argument(
            '--write-attempts', dest='write_attempts', metavar='N',
            help='number of database write attempts before warning is issued')
        cls._conf_parser_args.append('write_attempts')

        cls._conf_parser.add_argument(
            '--write-interval', dest='write_interval', metavar='T', type=int,
            help='interval in s between database write attempts')
        cls._conf_parser_args.append('write_interval')

        cls._conf_parser.add_argument(
            '-t', '--mol-database', dest='mol_database', metavar='DB',
            help='MongoDB molecule database name')
        cls._conf_parser_args.append('mol_database')

        cls._conf_parser.add_argument(
            '-k', '--mol-collection', dest='mol_collection', metavar='COLL',
            help='MongoDB molecule collection name')
        cls._conf_parser_args.append('mol_collection')

        cls._conf_parser.add_argument(
            '-T', '--flask-database', dest='flask_database', metavar='DB',
            help='MongoDB flask database name')
        cls._conf_parser_args.append('flask_database')

        cls._conf_parser.add_argument(
            '-K', '--flask-collection', dest='flask_collection',
            metavar='COLL', help='MongoDB flask collection name')
        cls._conf_parser_args.append('flask_collection')

        cls._conf_parser.add_argument(
            '--with-raw-write', dest='raw_write', action='store_true',
            default=None, help='write raw output files')
        cls._conf_parser.add_argument(
            '--no-raw-write', dest='raw_write', action='store_false',
            default=None, help='do not write raw output files')
        cls._conf_parser_args.append('raw_write')

        cls._conf_parser.add_argument(
            '--raw-database', dest='raw_database', metavar='DB',
            help='MongoDB raw file database name')
        cls._conf_parser_args.append('raw_database')

        cls._conf_parser.add_argument(
            '--raw-collection', dest='raw_collection', metavar='COLL',
            help='MongoDB raw collection prefix')
        cls._conf_parser_args.append('raw_collection')

        cls._conf_parser.add_argument(
            '-H', '--with-cache', dest='with_cache', action='store_true',
            default=None, help='use Memcached cache')
        cls._conf_parser.add_argument(
            '--no-cache', dest='with_cache', action='store_false',
            default=None, help='do not use Memcached cache')
        cls._conf_parser_args.append('with_cache')

        cls._conf_parser.add_argument(
            '--cache-hostname', dest='cache_hostname', metavar='URL',
            help='Memcached cache server location')
        cls._conf_parser_args.append('cache_hostname')

        cls._conf_parser.add_argument(
            '--cache-port', dest='cache_port', metavar='PORT', type=int,
            help='Memcached cache port')
        cls._conf_parser_args.append('cache_hostname')

        cls._conf_parser.add_argument(
            '--cache-expire', dest='cache_expire', metavar='T', type=int,
            help='Memcached key expiration time in s')
        cls._conf_parser_args.append('cache_expire')

        cls._conf_parser.add_argument(
            '--scratch-path', dest='scratch_path', metavar='PATH',
            help='scratch directory for task execution')
        cls._conf_parser_args.append('scratch_path')

        cls._conf_parser.add_argument(
            '--executable-path', dest='executable_path', metavar='PATH',
            help='search path for external executables')
        cls._conf_parser_args.append('executable_path')

        cls._conf_parser.add_argument(
            '-n', '--tasks-per-calc', dest='tasks_per_calc', metavar='N',
            type=int, help='number of tasks per calculator')
        cls._conf_parser_args.append('tasks_per_calc')

        cls._conf_parser.add_argument(
            '-N', '--tasks-per-cycle', dest='tasks_per_cycle', metavar='N',
            type=int, help='number of tasks per calculator cycle')
        cls._conf_parser_args.append('tasks_per_cycle')

        cls._conf_parser.add_argument(
            '--count-idle-tasks', dest='count_idle_tasks',
            action='store_true', default=None,
            help='count idle tasks as completed')
        cls._conf_parser_args.append('count_idle_tasks')

        cls._conf_parser.add_argument(
            '--save-completed-tasks', dest='save_completed_tasks',
            action='store_true', default=None,
            help='save completed tasks in calculator for debugging')
        cls._conf_parser_args.append('save_completed_tasks')

        cls._conf_parser.add_argument(
            '--polling-interval', dest='polling_interval', metavar='T',
            type=int, help='interval for calculator polling in s')
        cls._conf_parser_args.append('polling_interval')

        cls._conf_parser.add_argument(
            '--idle-interval', dest='idle_interval', metavar='T', type=int,
            help='wait interval for database queries by idle tasks in s')
        cls._conf_parser_args.append('idle_interval')

        cls._conf_parser.add_argument(
            '-L', '--logging-level', dest='logging_level', metavar='LEVEL',
            choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
            help='logging level')
        cls._conf_parser.add_argument(
            '-v', '--verbose', dest='logging_level', action='store_const',
            const='DEBUG', help='set logging level to DEBUG')
        cls._conf_parser.add_argument(
            '-q', '--quiet', dest='logging_level', action='store_const',
            const='ERROR', help='set logging level to ERROR')
        cls._conf_parser_args.append('logging_level')

        cls._conf_parser.add_argument(
            '--action-delay', dest='action_delay', metavar='N', type=int,
            help='delay in s before shutdown or calculator restart')
        cls._conf_parser_args.append('action_delay')

        cls._conf_parser.add_argument(
            '-p', '--with-persistent-pool', dest='persistent_pool',
            action='store_true', default=None,
            help='maintain a persistent calculator pool')
        cls._conf_parser.add_argument(
            '-P', '--no-persistent-pool', dest='persistent_pool',
            action='store_false', default=None,
            help='do not maintain a persistent calculator pool')
        cls._conf_parser_args.append('persistent_pool')

        cls._conf_parser.add_argument(
            '-!', '--with-shutdown', dest='with_shutdown', action='store_true',
            default=None, help='shut down calculators after being idle')
        cls._conf_parser.add_argument(
            '--no-shutdown', dest='with_shutdown', action='store_false',
            default=None, help='do not shut down calculators')
        cls._conf_parser_args.append('with_shutdown')

        cls._conf_parser.add_argument(
            '--idle-tasks-per-calc', dest='idle_tasks_per_calc',
            metavar='N', type=int,
            help='number of idle tasks before calculator declared idle')
        cls._conf_parser_args.append('idle_tasks_per_calc')

        cls._conf_parser.add_argument(
            '--shutdown-quorum', dest='shutdown_quorum', metavar='N',
            type=int, help='number of idle calculators before shutdown')
        cls._conf_parser_args.append('shutdown_quorum')

        return cls._parser

    @classmethod
    def add_subparsers(cls):
        """Add subparsers for colib subcommands

        Returns:
            ArgumentParser: colib subparsers
        """

        # Parsers for colibri subcommands
        cls._subparsers = cls._parser.add_subparsers(
            dest='cmd', title='colibri subcommands')
        return cls._subparsers

    @classmethod
    def read_config_files(cls, args):
        """Read options from configuration files and environment variables.

        Args:
            args (dict): Program arguments

        Returns:
            Options: Program options
        """

        # Empty Options object
        opts = Options()

        # Determine location of colibri default configuration
        conf_dir = os.path.join(
            os.path.dirname(
                os.path.dirname(os.path.dirname(__file__))), 'config')
        default_conf_file = os.path.join(conf_dir, 'colibri.json')

        # Read site configuration defaults if available
        if os.path.exists(default_conf_file):
            with open(default_conf_file) as f:
                default_opts = Options.fromdict(
                    precedence='default', **json.load(f))
                opts = opts.update(default_opts)

        # Determine location of colibri site configuration
        if (not args['no_config'] and not args['no_site_config'] and
                os.path.isdir(args['site_config_path'])):
            site_conf_file = os.path.join(
                args['site_config_path'], 'colibri.json')

            # Read site configuration defaults if available
            if os.path.exists(site_conf_file):
                with open(site_conf_file) as f:
                    site_opts = Options.fromdict(
                        precedence='site', **json.load(f))
                opts = opts.update(site_opts)

        # Read user-wide options from home directory
        if (not args['no_config'] and not args['no_user_config']):
            user_conf_file = os.path.join(
                os.environ['HOME'], '.colibri', 'colibri.json')
            if os.path.exists(user_conf_file):
                with open(user_conf_file) as f:
                    user_opts = Options.fromdict(
                        precedence='user', **json.load(f))
                    opts = opts.update(user_opts)

        # Read colibri environment variables
        if (not args['no_config'] and not args['no_env_config']):
            env_kv = {}
            for key, val in os.environ.iteritems():

                # Add COLIBRI_* variables to dictionary
                if key.startswith('COLIBRI_'):
                    env_kv[key[8:].lower()] = val

            if env_kv:
                env_opts = Options(precedence='env', **env_kv)
                opts = opts.update(env_opts)

        # Read local project configuration
        if (not args['no_config'] and not args['no_proj_config']):
            if args.get('config_file'):
                proj_config_file = args['config_file']
            else:
                proj_config_file = os.path.join(os.getcwd(), 'colibri.json')

            if os.path.exists(proj_config_file):
                with open(proj_config_file) as f:
                    proj_opts = Options.fromdict(
                        precedence='project', **json.load(f))
                    opts = opts.update(proj_opts)

        return opts

    @classmethod
    def get_conf_arguments(cls, args):
        """Process program configuration arguments from command line.

        Args:
            args (dict): Configuration arguments

        Returns:
            Options: Program options
        """

        # Process general command-line arguments
        conf_kv = {}
        for key in cls._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Add to the Options object
        if conf_kv:
            opts = Options(precedence='command', **conf_kv)
        else:
            opts = Options()

        return opts

    @classmethod
    def print_help(cls):
        """Print help message to the console."""
        cls._parser.print_help()


class ConsoleCommand(object):
    """Base ConsoleCommand object, needs to be subclassed for specific commands.

    Subclasses need to implement a constructor, an execute method,
    and (optionally) add_subparser() and print_help() class methods.
    """

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib cmd subcommand.

        Args:
            ArgumentParser: colib subparsers
        """
        # Add subparser
        cls._subparser = subparsers.add_parser('cmd', help='default command')

    def print_help(self, logging_level='INFO'):
        """Print help message to the console."""

        # Conditionally print formatted help message
        if getattr(logging, logging_level) < logging.ERROR:
            type(self)._subparser.print_help()

    def __init__(self, args, opts):
        raise NotImplementedError

    def __str__(self):
        """Return string representation

        Returns:
            str: ConsoleCommand type as string
        """
        return type(self).__name__

    def execute(self):
        """Execute the console command set by the constructor.

        Returns:
            Result: Execution result
        """
        if getattr(self, '_cmd', None):
            return self._cmd.execute()

    @property
    def cmd(self):
        """Return the underlying command."""
        return self._cmd
