"""API for colib suspend console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    SuspendConsoleCommand: colib suspend console subcommand
"""

__all__ = ['SuspendConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/18/2015'

#
# colibri submodules
#

from colibri.api import SuspendCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.utils import ensure_logger

#
# Classes
#


class SuspendConsoleCommand(ConsoleCommand):
    """colib suspend console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib suspend console subcommand.

        Args:
            subparsers (ArgumentParser): colib subparsers
        """

        # Suspend subparser
        cls._subparser = subparsers.add_parser(
            'suspend', help='suspend computation')

        # Suspend execution argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'suspend executions arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-e', '--molecules', action='store_true',
            help='suspend molecules')
        cls._exec_parser_args.append('molecules')

        cls._exec_parser.add_argument(
            '-E', '--flasks', action='store_true',
            help='suspend flasks')
        cls._exec_parser_args.append('flasks')

        # Suspend configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'suspend configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

        # Individual configuration arguments
        cls._conf_parser.add_argument(
            '-V', '--suspend-interval', dest='suspend_interval', type=int,
            metavar='T',
            help='interval between execution of suspend tasks in s')
        cls._conf_parser_args.append('suspend_interval')

        cls._conf_parser.add_argument(
            '-@', '--tags', nargs='+', metavar='TAG',
            help='select only molecules/flasks having all specified tags')
        cls._conf_parser_args.append('tags')

    def __init__(self, args, opts):
        """SuspendConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Process configuration command-line arguments for suspend subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save SuspendCommand object
            self._cmd = SuspendCommand(
                molecules=args.get('molecules'), flasks=args.get('flasks'),
                opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('suspendconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
