"""API for colib config console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    ConfigConsoleCommand: colib config console subcommand
"""

__all__ = ['ConfigConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/02/2015'

#
# Standard library
#

import os
import os.path

#
# colibri submodules
#

from colibri.api import ConfigCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options, OptionItem
from colibri.utils import ensure_logger

#
# Classes
#


class ConfigConsoleCommand(ConsoleCommand):
    """colib config console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):

        # Fetch subparser
        cls._subparser = subparsers.add_parser(
            'config',  help='configure colibri')

        # Fetch exec argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'config execution arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Individual execution arguments

        # Write to site configuration file
        cls._exec_parser.add_argument(
            '--site', action='store_true', default=None,
            help='write to site configuration file')
        cls._exec_parser_args.append('site')

        # Write to user configuration file
        cls._exec_parser.add_argument(
            '--user', action='store_true', default=None,
            help='write to user configuration file')
        cls._exec_parser_args.append('user')

        # Configuration file name and location
        cls._exec_parser.add_argument(
            '--config-file', dest='config_file', metavar='FILE',
            default=None,
            help='colibri configuration file name. Default: colibri.json')
        cls._exec_parser_args.append('config_file')

        # Option group
        cls._exec_parser.add_argument(
            '--group', default=None, help='option group')
        cls._exec_parser_args.append('group')

        # Option key
        cls._exec_parser.add_argument(
            '--key', default=None, help='option key')
        cls._exec_parser_args.append('key')

        # Option value
        cls._exec_parser.add_argument(
            '--value', default=None, help='option value')
        cls._exec_parser_args.append('value')

        # Option default
        cls._exec_parser.add_argument(
            '--default', default=None, help='option default')
        cls._exec_parser_args.append('default')

        # Option description
        cls._exec_parser.add_argument(
            '--description', default=None, metavar='DESC',
            help='option description')
        cls._exec_parser_args.append('description')

        # Option precedence
        cls._exec_parser.add_argument(
            '--precedence', default=None, metavar='LEVEL',
            help='option precedence')
        cls._exec_parser_args.append('precedence')

        # Option target
        cls._exec_parser.add_argument(
            '--target', default=None, help='option target')
        cls._exec_parser_args.append('target')

        # Option type
        cls._exec_parser.add_argument(
            '--type', default='str', help='option type')
        cls._exec_parser_args.append('type')

        # Append option
        cls._exec_parser.add_argument(
            '--append', action='store_true', help='append option')
        cls._exec_parser_args.append('append')

        # Config configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'config configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

    def __init__(self, args, opts):
        """ConfigConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Output configuration file given explicitly
        if args.get('config_file'):
            config_file = args['config_file']

        # Site-wide configuration file
        elif args.get('site'):
            config_file = os.path.join(
                args['site_config_path'], 'colibri.json')

        # User-wide configuration file
        elif args.get('user'):
            config_file = os.path.join(
                os.environ['HOME'], '.colibri', 'colibri.json')

        # Project-wide configuration file by default
        else:
            config_file = os.path.join(os.getcwd(), 'colibri.json')

        # Set option type
        if args.get('value') is not None:
            try:
                t = eval(args['type'])
            except NameError:
                raise CommandError('Option type %s invalid' % args['type'])
            if isinstance(t, type):
                args['value'] = t(args['value'])

        # New option
        try:
            opt = OptionItem(
                **{k: v for k, v in args.items() if k in OptionItem.__slots__
                    and v is not None})
        except ValueError as e:
            raise CommandError(e)

        # Process configuration command-line arguments for clean subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save ConfigCommand object
            self._cmd = ConfigCommand(
                config_file=config_file, opt=opt, append=args.get('append'),
                opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('configconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
