"""API for colib clean console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    CleanConsoleCommand: colib clean console subcommand
"""

__all__ = ['CleanConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/21/2015'

#
# colibri submodules
#

from colibri.api import CleanCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.utils import ensure_logger


#
# Classes
#

class CleanConsoleCommand(ConsoleCommand):
    """colib clean console command."""

    @classmethod
    def add_subparser(cls, subparsers):

        # Clean subparser
        cls._subparser = subparsers.add_parser(
            'clean', help='clean database')

        # Clean exec argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'clean execution arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-f', '--force', dest='force', action='store_true',
            help='do not request confirmation')
        cls._exec_parser_args.append('force_clean')

        # Clean conf argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'clean configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

    def __init__(self, args, opts):
        """CleanConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Always clean cache
        cache = True

        # Force clean, no confirmation
        if args['force']:
            molecules = True
            flasks = True

        # Request confirmation
        else:
            molecules = False
            flasks = False

            # Confirmation for cleaning molecule collection
            inp = ''
            while inp not in ['y', 'n']:
                inp = raw_input('Clean molecule collection? [y/N] ')
                inp = inp.lower() if inp else 'n'
                if inp == 'y':
                    molecules = True

            # Confirmation for cleaning flask collection
            inp = ''
            while inp not in ['y', 'n']:
                inp = raw_input('Clean flask collection? [y/N] ')
                inp = inp.lower() if inp else 'n'
                if inp == 'y':
                    flasks = True

        # Process configuration command-line arguments for clean subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save CleanCommand object
            self._cmd = CleanCommand(
                molecules=molecules, flasks=flasks,
                cache=cache, opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('cleanconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
