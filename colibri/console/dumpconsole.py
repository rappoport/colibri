"""API for colib dump console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    DumpConsoleCommand: colib dump console subcommand
"""

__all__ = ['DumpConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/23/2015'

#
# colibri submodules
#

from colibri.api import DumpCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.utils import ensure_logger

#
# Classes
#


class DumpConsoleCommand(ConsoleCommand):
    """colib dump console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib dump console subcommand.

        Args:
            subparsers (ArgumentParser): colib subparsers
        """

        # Dump subparser
        cls._subparser = subparsers.add_parser(
            'dump', help='dump database as graph file')

        # Dump execution argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'dump execution arguments')

        # Dump execution argument names
        cls._exec_parser_args = []

        cls._exec_parser.add_argument(
            '--export-dep', dest='export_dep', action='store_true',
            help='export dependent flask attributes')
        cls._exec_parser.add_argument(
            '--no-export-dep', dest='export_dep', action='store_false',
            help='do not export dependent flask attributes')
        cls._exec_parser_args.append('export_dep')

        cls._exec_parser.add_argument(
            '--export-var', dest='export_var', action='store_true',
            help='export variable flask attributes')
        cls._exec_parser.add_argument(
            '--no-export-var', dest='export_var', action='store_false',
            help='do not export variable flask attributes')
        cls._exec_parser_args.append('export_var')

        cls._exec_parser.add_argument(
            '--export-default', dest='export_default',
            action='store_true',
            help='export default values of flask attributes')
        cls._exec_parser.add_argument(
            '--no-export-default', dest='export_default',
            action='store_false',
            help='do not export default values of flask attributes')
        cls._exec_parser_args.append('export_default')

        cls._exec_parser.add_argument(
            '--exclude', dest='exclude', nargs='+', metavar='ATTR',
            help='excluded attribute list')
        cls._exec_parser_args.append('exclude')

        cls._exec_parser.add_argument(
            '-o', '--output-file', dest='output_file', metavar='FILE',
            default='test.graphml', help='output file')
        cls._exec_parser_args.append('output_file')

        # Dump configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'dump configuration arguments')

        # Dump configuration argument names
        cls._conf_parser_args = []

        cls._conf_parser.add_argument(
            '-g', '--min-generation', dest='min_generation', type=int,
            metavar='GEN', help='minimum generation for flasks (incl.)')
        cls._conf_parser_args.append('min_generation')

        cls._conf_parser.add_argument(
            '-G', '--max-generation', dest='max_generation', type=int,
            metavar='GEN', help='maximum generation for flasks (incl.)')
        cls._conf_parser_args.append('max_generation')

        cls._conf_parser.add_argument(
            '-9', '--max-energy-change', dest='max_energy_change', type=float,
            metavar='EN', help='maximum energy change in eV')
        cls._conf_parser_args.append('max_energy_change')

        cls._conf_parser.add_argument(
            '-0', '--max-rel-energy', dest='max_rel_energy', type=float,
            metavar='EN', help='maximum energy above reference in eV')
        cls._conf_parser_args.append('max_rel_energy')

        cls._conf_parser.add_argument(
            '-1', '--ref-energy', dest='ref_energy', type=float, metavar='EN',
            help='reference energy in eV')
        cls._conf_parser_args.append('ref_energy')

        cls._conf_parser.add_argument(
            '-R', '--ref-flask', dest='ref_flask', metavar='FLASK',
            help='reference flask')
        cls._conf_parser_args.append('ref_flask')

        cls._conf_parser.add_argument(
            '-@', '--tags', dest='tags', nargs='+', metavar='TAG',
            help='select only flasks having all specified tags')
        cls._conf_parser_args.append('tags')

    def __init__(self, args, opts):
        """DumpConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Process configuration command-line arguments for stat subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save DumpCommand object
            self._cmd = DumpCommand(
                export_dep=args.get('export_dep', False),
                export_var=args.get('export_var', True),
                export_default=args.get('export_default', False),
                exclude=args.get('exclude', []),
                output_file=args.get('output_file'), opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('dumpconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
