"""API for colib fetch console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    FetchConsoleCommand: colib fetch console subcommand
"""

__all__ = ['FetchConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/18/2015'

#
# Standard library
#

import sys

#
# colibri submodules
#

from colibri.api import FetchCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.utils import ensure_logger

#
# Classes
#


class FetchConsoleCommand(ConsoleCommand):
    """colib fetch console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib fetch subcommand.

        Args:
            subparsers (ArgumentParser): colib subparsers
        """

        # Fetch subparser
        cls._subparser = subparsers.add_parser(
            'fetch',  help='fetch data from database')

        # Fetch exec argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'fetch execution arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-e', '--molecules', nargs='*', metavar='MOL',
            help='output molecules as JSON strings, optionally by SMILES/key')
        cls._exec_parser_args.append('molecules')

        cls._exec_parser.add_argument(
            '-E', '--flasks', nargs='*', metavar='FLASK',
            help='output flasks as JSON strings, optionally by SMILES/key')
        cls._exec_parser_args.append('flasks')

        cls._exec_parser.add_argument(
            '-o', '--molecule-file', dest='molecule_file', metavar='FILE',
            help='molecule output to file, one JSON string per line')
        cls._exec_parser_args.append('molecule_file')

        cls._exec_parser.add_argument(
            '-O', '--flask-file', dest='flask_file', metavar='FILE',
            help='flask output to file, one JSON string per line')
        cls._exec_parser_args.append('flask_file')

        cls._exec_parser.add_argument(
            '--append', action='store_true', help='append output to file')
        cls._exec_parser_args.append('append')

        # Fetch conf argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'fetch configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

        # Individual configuration arguments
        cls._conf_parser.add_argument(
            '-l', '--level', nargs='+', metavar='LEVEL',
            help='select only molecule with given level(s)')
        cls._conf_parser_args.append('level')

        cls._conf_parser.add_argument(
            '-j', '--status', nargs='+', metavar='STATUS',
            help='select only flasks with given status(es)')
        cls._conf_parser_args.append('status')

        cls._conf_parser.add_argument(
            '-g', '--min-generation', dest='min_generation', type=int,
            metavar='GEN',
            help='minimum generation to select for molecules/flasks (incl.)')
        cls._conf_parser_args.append('min_generation')

        cls._conf_parser.add_argument(
            '-G', '--max-generation', dest='max_generation', type=int,
            metavar='GEN',
            help='maximum generation to select for molecules/flasks (incl)')
        cls._conf_parser_args.append('max_generation')

        cls._conf_parser.add_argument(
            '-@', '--tags', nargs='+', metavar='TAG',
            help='select only molecules/flasks having all specified tags')
        cls._conf_parser_args.append('tags')

        cls._conf_parser.add_argument(
            '-M', '--batch-size', dest='batch_size', type=int, metavar='N',
            help='batch size')
        cls._conf_parser_args.append('batch_size')

        cls._conf_parser.add_argument(
             '--limit', type=int, metavar='N',
             help='number of retrieved molecules/flasks')
        cls._conf_parser_args.append('limit')

    def __init__(self, args, opts):
        """FetchConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Reader coroutine
        def make_reader(output_file=None, output_flags='w'):

            # Open output file or use stdout by default
            if output_file is not None:
                f = open(output_file, output_flags)
            else:
                f = sys.stdout

            # Write to output handle
            try:
                while True:
                    f.write((yield) + '\n')
            except GeneratorExit:
                f.close()

        # Constuct and initialize molecule reader coroutine
        if (args.get('molecules') is not None or
                args.get('molecule_file') is not None):
            molecules = make_reader(
                args.get('molecule_file'), 'a' if args.get('append') else 'w')
            molecules.send(None)
        else:
            molecules = None

        # Constuct and initialize flask reader coroutine
        if (args.get('flasks') is not None or
                args.get('flask_file') is not None):
            flasks = make_reader(
                args.get('flask_file'), 'a' if args.get('append') else 'w')
            flasks.send(None)
        else:
            flasks = None

        # Process configuration command-line arguments for fetch subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save FetchCommand object
            self._cmd = FetchCommand(molecules=molecules, flasks=flasks,
                                     molecule_ids=args.get('molecules'),
                                     flask_ids=args.get('flasks'),
                                     opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('fetchconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
