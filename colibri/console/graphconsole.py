"""API for colib graph console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    GraphConsoleCommand: colib graph console subcommand
"""

__all__ = ['GraphConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/23/2015'

#
# colibri submodules
#

from colibri.api import GraphCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.utils import ensure_logger

#
# Classes
#


class GraphConsoleCommand(ConsoleCommand):
    """colib graph console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib graph console subcommand.

        Args:
            subparsers (ArgumentParser): colib subparsers
        """

        # Graph subparser
        cls._subparser = subparsers.add_parser(
            'graph', help='export database to graph storage')

        # Graph execution argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'graph execution arguments')

        # Graph execution argument names
        cls._exec_parser_args = []
        cls._exec_parser.add_argument(
            '-B', '--labels', nargs='+', help='flask labels to add')
        cls._exec_parser_args.append('labels')

        cls._exec_parser.add_argument(
            '-Z', '--keys', help='Regex for flask keys to retrieve')
        cls._exec_parser_args.append('keys')

        # Graph configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'graph configuration arguments')

        # Graph configuration argument names
        cls._conf_parser_args = []

        cls._conf_parser.add_argument(
            '-g', '--min-generation', dest='min_generation', type=int,
            metavar='GEN', help='minimum generation for flasks (incl.)')
        cls._conf_parser_args.append('min_generation')

        cls._conf_parser.add_argument(
            '-G', '--max-generation', dest='max_generation', type=int,
            metavar='GEN', help='maximum generation for flasks (incl.)')
        cls._conf_parser_args.append('max_generation')

        cls._conf_parser.add_argument(
            '-9', '--max-energy-change', dest='max_energy_change', type=float,
            metavar='EN', help='maximum energy change in eV')
        cls._conf_parser_args.append('max_energy_change')

        cls._conf_parser.add_argument(
            '-0', '--max-rel-energy', dest='max_rel_energy', type=float,
            metavar='EN', help='maximum energy above reference in eV')
        cls._conf_parser_args.append('max_rel_energy')

        cls._conf_parser.add_argument(
            '-1', '--ref-energy', dest='ref_energy', type=float, metavar='EN',
            help='reference energy in eV')
        cls._conf_parser_args.append('ref_energy')

        cls._conf_parser.add_argument(
            '-R', '--ref-flask', dest='ref_flask', metavar='FLASK',
            help='reference flask')
        cls._conf_parser_args.append('ref_flask')

        cls._conf_parser.add_argument(
            '-@', '--tags', dest='tags', nargs='+', metavar='TAG',
            help='select only flasks having all specified tags')
        cls._conf_parser_args.append('tags')

    def __init__(self, args, opts):
        """GraphConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Process configuration command-line arguments for stat subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save GraphCommand object
            self._cmd = GraphCommand(
                labels=args.get('labels'), keys=args.get('keys'),
                opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('graphconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
