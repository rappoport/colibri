"""API for colib delete console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    DeleteConsoleCommand: colib delete console subcommand
"""

__all__ = ['DeleteConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/02/2015'

#
# colibri submodules
#

from colibri.api import DeleteCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.options import Options
from colibri.utils import ensure_logger

#
# Classes
#


class DeleteConsoleCommand(ConsoleCommand):
    """colib delete console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib delete console subcommand.

        Args:
            subparsers (ArgumentParser): colib subparsers
        """

        # Fetch subparser
        cls._subparser = subparsers.add_parser(
            'delete',  help='delete data from database')

        # Fetch exec argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'delete execution arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-e', '--molecules', nargs='*', metavar='MOL',
            help='delete molecules, optionally by SMILES/key')
        cls._exec_parser_args.append('molecules')

        cls._exec_parser.add_argument(
            '-E', '--flasks', nargs='*', metavar='FLASK',
            help='delete flasks, optionally by SMILES/key')
        cls._exec_parser_args.append('flasks')

        # Fetch configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'fetch configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

        # Individual configuration arguments
        cls._conf_parser.add_argument(
            '-l', '--level', nargs='+', metavar='LEVEL',
            help='Select only molecule with given level(s)')
        cls._conf_parser_args.append('level')

        cls._conf_parser.add_argument(
            '-j', '--status', nargs='+', metavar='STATUS',
            help='Select only flasks with given status(es)')
        cls._conf_parser_args.append('status')

        cls._conf_parser.add_argument(
            '-g', '--min-generation', dest='min_generation', type=int,
            metavar='GEN',
            help='Minimum generation to select for molecules/flasks (incl.)')
        cls._conf_parser_args.append('min_generation')

        cls._conf_parser.add_argument(
            '-G', '--max-generation', dest='max_generation', type=int,
            metavar='GEN',
            help='Maximum generation to select for molecules/flasks (incl)')
        cls._conf_parser_args.append('max_generation')

        cls._conf_parser.add_argument(
            '-@', '--tags', nargs='+', metavar='TAG',
            help='Select only molecules/flasks having all specified tags')
        cls._conf_parser_args.append('tags')

    def __init__(self, args, opts):
        """DeleteConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        # Process configuration command-line arguments for fetch subparser
        conf_kv = {}
        for key in self._conf_parser_args:
            val = args.get(key)
            if val is not None:
                conf_kv[key] = val

        # Convert to Options object
        conf_opts = Options(precedence='command', **conf_kv)

        # Add to program options
        total_opts = opts.update(conf_opts)

        try:

            # Construct and save DeleteCommand object
            self._cmd = DeleteCommand(
                molecules=args.get('molecules') is not None,
                flasks=args.get('flasks') is not None,
                molecule_ids=args.get('molecules'),
                flask_ids=args.get('flasks'), opts=total_opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('deleteconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
