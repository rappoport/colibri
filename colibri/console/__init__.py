"""colibri console commands.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Attributes:
    cmd_names (dict): Mapping of command names to command class names

Functions:
    console_program: Program call for colibri console scripts

Classes:
    ConsoleProgram: colib console frontend
    ConsoleCommand: Base class for colib console commands
    InitConsoleCommand: colib init console subcommand
    AddConsoleCommand: colib add console subcommand
    FetchConsoleCommand: colib fetch console subcommand
    DeleteConsoleCommand: colib delete console subcommand
    RunConsoleCommand: colib run console subcommand
    StatConsoleCommand: colib stat console subcommand
    SuspendConsoleCommand: colib suspend console subcommand
    ResumeConsoleCommand: colib resume console subcommand
    LoadConsolecommand: colib load console subcommand
    DumpConsoleCommand: colib dump console subcommand
    GraphConsoleCommand: colib graph console subcommand
    CleanConsoleCommand: colib clean console subcommand
    ConfigConsoleCommand: colib config console subcommand
"""

__all__ = ['cmd_names', 'console_program', 'ConsoleProgram', 'ConsoleCommand',
           'InitConsoleCommand', 'AddConsoleCommand', 'FetchConsoleCommand',
           'DeleteConsoleCommand', 'RunConsoleCommand', 'StatConsoleCommand',
           'SuspendConsoleCommand', 'ResumeConsoleCommand',
           'LoadConsoleCommand', 'DumpConsoleCommand', 'GraphConsoleCommand',
           'CleanConsoleCommand', 'ConfigConsoleCommand']

__module__ = 'colibri.console'
__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/15/2015'

#
# Library modules
#

from collections import OrderedDict


#
# Variables
#

# Translation table from command names to command class names
cmd_names = OrderedDict(
    [('init', 'InitConsoleCommand'),
     ('add', 'AddConsoleCommand'),
     ('fetch', 'FetchConsoleCommand'),
     ('delete', 'DeleteConsoleCommand'),
     ('run', 'RunConsoleCommand'),
     ('stat', 'StatConsoleCommand'),
     ('suspend', 'SuspendConsoleCommand'),
     ('resume', 'ResumeConsoleCommand'),
     ('load', 'LoadConsoleCommand'),
     ('dump', 'DumpConsoleCommand'),
     ('graph', 'GraphConsoleCommand'),
     ('clean', 'CleanConsoleCommand'),
     ('config', 'ConfigConsoleCommand')
     ])

#
#
# Classes
#

# Base console command
from colibri.console.baseconsole import (
    console_program, ConsoleProgram, ConsoleCommand)

# colib init console subcommand
from colibri.console.initconsole import InitConsoleCommand

# colib add console subcommand
from colibri.console.addconsole import AddConsoleCommand

# colib fetch console subcommand
from colibri.console.fetchconsole import FetchConsoleCommand

# colib delete console subcommand
from colibri.console.deleteconsole import DeleteConsoleCommand

# colib run console subcommand
from colibri.console.runconsole import RunConsoleCommand

# colib stat console subcommand
from colibri.console.statconsole import StatConsoleCommand

# colib suspend console subcommand
from colibri.console.suspendconsole import SuspendConsoleCommand

# colib resume console subcommand
from colibri.console.resumeconsole import ResumeConsoleCommand

# colib load console subcommand
from colibri.console.loadconsole import LoadConsoleCommand

# colib dump console subcommand
from colibri.console.dumpconsole import DumpConsoleCommand

# colib graph console subcommand
from colibri.console.graphconsole import GraphConsoleCommand

# colib clean console subcommand
from colibri.console.cleanconsole import CleanConsoleCommand

# colib config console subcommand
from colibri.console.configconsole import ConfigConsoleCommand
