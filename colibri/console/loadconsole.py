"""API for colib load console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    LoadConsoleCommand: colib load console subcommand
"""

__all__ = ['LoadConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/02/2015'

#
# colibri submodules
#

from colibri.api import LoadCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.utils import ensure_logger

#
# Classes
#


class LoadConsoleCommand(ConsoleCommand):
    """colib load console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):

        # Fetch subparser
        cls._subparser = subparsers.add_parser(
            'load',  help='load graph file into database')

        # Fetch exec argument group
        cls._exec_parser = cls._subparser.add_argument_group(
            'load execution arguments')

        # List of execution arguments
        cls._exec_parser_args = []

        # Individual execution arguments
        cls._exec_parser.add_argument(
            '-i', '--input-file', dest='input_file', help='network input file')
        cls._exec_parser_args.append('input_file')

        # Load configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'load configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

    def __init__(self, args, opts):
        """LoadConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        try:

            # Construct and save LoadCommand object
            self._cmd = LoadCommand(
                input_file=args.get('input_file'), opts=opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('loadconsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
