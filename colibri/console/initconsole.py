"""API for colib init console subcommand.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    InitConsoleCommand: colib init console subcommand
"""


__all__ = ['InitConsoleCommand']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '09/16/2015'

#
# colibri submodules
#

from colibri.api import InitCommand
from colibri.console.baseconsole import ConsoleCommand
from colibri.exceptions import CommandError
from colibri.utils import ensure_logger

#
# Classes
#


class InitConsoleCommand(ConsoleCommand):
    """colib init console subcommand."""

    @classmethod
    def add_subparser(cls, subparsers):
        """Add subparser for colib init console subcommand.

        Args:
            subparsers (argparse.ArgumentParser): colib subparsers
        """

        # Add subparser
        cls._subparser = subparsers.add_parser(
            'init', help='initialize database')

        # Init configuration argument group
        cls._conf_parser = cls._subparser.add_argument_group(
            'init configuration arguments')

        # List of configuration arguments
        cls._conf_parser_args = []

        cls._conf_parser.add_argument(
            '-@', '--tags', action='store_true',
            help='add index on tags field')
        cls._conf_parser_args.append('tags')

    def __init__(self, args, opts):
        """InitConsoleCommand object constructor.

        Args:
            args (dict): Command arguments
            opts (Options): Command options

        Raises:
            SystemExit: Stop command execution
        """

        try:

            # Construct and save InitCommand object
            self._cmd = InitCommand(tags=args.get('tags', False), opts=opts)

        except CommandError as e:

            # Make logger if necessary
            logging_level = opts.get('logging_level', 'INFO')
            logger = ensure_logger('initonsole', logging_level)

            # Console help output
            self.print_help(logging_level)

            # Log and exit
            logger.error('Command error: %s' % e)
            raise SystemExit(e)
