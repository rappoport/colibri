"""Exceptions and signal handlers for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Exception Classes:
    (BaseException)
      |
      + -- NoInput
      |
      + -- TaskWorkflow
      |    |
      |    + -- IncompleteInput
      |    + -- ExistingInput
      |    + -- HiEnergy
      |    + -- HiBarrier
      |    + -- MaxGeneration
      |    + -- BadChemistry
      |    + -- BadOptimization
      |
      +- (Exception)
        |
        + -- InternalError
        |
        + -- CommandError
        |
        + -- ScriptError
        |
        + -- DataError
        |    |
        |    + -- MoleculeError
        |    |    + -- StructureError
        |    |    + -- Structure3DError
        |    |
        |    + -- FlaskError
        |    + -- TransformationError
        |    + -- ReactionRuleError
        |
        + -- TaskError
             |
             + -- ConnectionError
             + -- ExecutionError
             + -- ExecutionTimeout
             + -- ValidationError


Signal Handlers:
    timeout_handler
    interrupt_handler
"""

__all__ = ['NoInput', 'TaskWorkflow', 'IncompleteInput', 'ExistingInput',
           'HiEnergy', 'HiBarrier', 'MaxGeneration', 'BadChemistry',
           'BadOptimization', 'InternalError', 'CommandError', 'ScriptError',
           'DataError', 'MoleculeError', 'StructureError', 'Structure3DError',
           'FlaskError', 'TransformationError', 'ReactionRuleError',
           'TaskError', 'ConnectionError', 'ExecutionError',
           'ExecutionTimeout', 'ValidationError',
           'timeout_handler', 'interrupt_handler']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '10/08/2015'

#
# Exceptions and signal handlers
#


class NoInput(BaseException):

    def __init__(self):
        desc = 'No Input'
        super(NoInput, self).__init__(desc)


class TaskWorkflow(BaseException):

    def __init__(self, desc=None):
        self.desc = desc

    def __str__(self):
        return str(self.desc)


class IncompleteInput(TaskWorkflow):

    def __init__(self):
        desc = 'Incomplete Input'
        super(IncompleteInput, self).__init__(desc)


class ExistingInput(TaskWorkflow):

    def __init__(self):
        desc = 'Existing Input'
        super(ExistingInput, self).__init__(desc)


class HiEnergy(TaskWorkflow):

    def __init__(self):
        desc = 'High-Energy Species'
        super(HiEnergy, self).__init__(desc)


class HiBarrier(TaskWorkflow):

    def __init__(self):
        desc = 'High-Barrier Species'
        super(HiBarrier, self).__init__(desc)


class MaxGeneration(TaskWorkflow):

    def __init__(self):
        desc = 'Generation Limit Reached'
        super(MaxGeneration, self).__init__(desc)


class BadChemistry(TaskWorkflow):
    pass


class BadOptimization(TaskWorkflow):
    pass


class InternalError(Exception):

    def __init__(self, desc=None):
        self.desc = desc

    def __str__(self):
        return str(self.desc)


class CommandError(Exception):

    def __init__(self, desc=None):
        self.desc = desc

    def __str__(self):
        return str(self.desc)


class ScriptError(Exception):

    def __init__(self, desc=None):
        self.desc = desc

    def __str__(self):
        return str(self.desc)


class DataError(Exception):

    def __init__(self, desc=None):
        self.desc = desc

    def __str__(self):
        return str(self.desc)


class MoleculeError(DataError):
    pass


class StructureError(MoleculeError):
    pass


class Structure3DError(MoleculeError):
    pass


class FlaskError(DataError):
    pass


class TransformationError(DataError):
    pass


class ReactionRuleError(DataError):
    pass


class TaskError(Exception):

    def __init__(self, desc=None):
        self.desc = desc

    def __str__(self):
        return str(self.desc)


class ConnectionError(TaskError):
    pass


class ExecutionError(TaskError):
    pass


class ExecutionTimeout(TaskError):

    def __init__(self):
        desc = 'Execution Timeout'
        super(ExecutionTimeout, self).__init__(desc)


class ValidationError(TaskError):
    pass


# Timeout handler
def timeout_handler(signum, frame):
    raise TimeoutError


# Interrupt handler
def interrupt_handler(signum, frame):
    raise KeyboardInterrupt
