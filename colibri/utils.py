"""Utility variables and methods for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Attributes:
    t2l (dict): Translation table for field names (short -> long)
    t2s (dict): Translation table for field names (long -> short)

Functions:
    base26: Encodes positive number in base 26 and returns last pos digits
    controlhash: Hash function for strings. Used for canonical SMILES strings
                 as well as raw output files
    randomid: Random valid identifier of given length pos
    default: Custom-class encoder (from bson.json_util)
    object_hook: Custom-class decoder (from bson.json_util)
    ensure_logger: Create logger if necessary and (re)set logging level
    list_accumulator: Generate simple list accumulator for coroutines
    adjcompress: Create compressed string representation of adjacency list adj
    is_connected: Determine using BFS if adjacency list is connected
    composition: Format atom composition dict as comma-separated string
"""

__all__ = ['t2l', 't2s', 'base26', 'controlhash', 'pathwayhash',
           'randomid', 'default', 'object_hook', 'ensure_logger',
           'list_accumulator', 'is_connected', 'composition']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '07/29/2015'

#
# Library modules
#

import hashlib
import logging
import random
import string
from collections import deque

#
# Third-party modules
#

from bson.json_util import default, object_hook


#
# Internal variables
#

# Alphabet
_alphabet = string.uppercase
_base = 26

#
# Public variables
#

# Translation tables for field names
t2l = {
    'a': 'outgoing', 'b': 'incoming', 'c': 'configuration', 'd': 'priority',
    'e': 'energy', 'f': 'formula', 'g': 'geometry', 'h': 'method',
    'i': 'index', 'j': 'status', 'k': 'molecules', 'l': 'level',
    'm': 'mult', 'n': 'properties', 'o': 'options', 'p': 'product',
    'q': 'charge', 'r': 'reactant', 's': 'smiles', 't': 'tags',
    'u': 'units', 'v': 'version', 'w': 'program', 'x': 'xyz',
    'y': 'property', 'z': 'chemistry', '0': 'precursor', '1': 'rule',
    '2': 'error', '3': 'generation', '4': 'ts', '5': 'etag',
    '6': 'raw_output', '7': 'converged', '8': 'attempts',
    '9': 'energy_change', '0*': 'precursor_journal',
    '3*': 'generation_journal', '+': 'quality', '-': 'adjacency',
    '1@': 'rules_applied',
    '_id': 'key', 'inchi': 'inchi', 'inchikey': 'inchikey',
    'smileskey': 'smileskey', 'adjkey': 'adjkey', 'sumformula': 'sumformula'}

t2s = {v: k for k, v in t2l.items()}

# Include identity translations for convenience
t2l.update({v: v for v in t2l.values()})
t2s.update({v: v for v in t2s.values()})

#
# Public functions
#


def base26(num, pos=10):
    """
    Encodes num > 0 in base 26 and returns last pos digits (default: 10).

    Args:
        num (int): Number to encode
        pos (int, optional): Hash length

    Raises:
        ValueError: Negative number

    Example:
        >>> base26(1)
        'B'
        >>> base26(13)
        'N'
        >>> base26(675)
        'ZZ'
        >>> base26(0)
        'A'
        >>> base26(-1)
        Traceback (most recent call last):
        ValueError: Input must be non-negative

    """

    # Number has to be non-negative
    if num < 0:
        raise ValueError('Input must be non-negative')

    if num == 0:
        return 'A'

    # Perform conversion
    digits = []

    while num:
        digits.append(_alphabet[num % _base])
        num /= _base

    return ''.join(digits[pos - 1::-1])


def controlhash(s, pos=10):
    """
    Computes base-26 encoded SHA256 hash of SMILES string.

    Args:
        s (str): Input SMILES string
        pos (int, optional): Hash length

    Returns:
        str: Hash value

    Example:
        >>> controlhash('CO.CO')
        'BGEJERMAWF'

    """

    return base26(int(hashlib.sha256(s).hexdigest(), 16), pos)


def invertletter(l):
    """
    Compute the inverted letter in alphabet with respect to basis.

    >>> invertletter('A')
    'Z'
    >>> invertletter('Z')
    'A'
    >>> invertletter('W')
    'D'

    """
    return _alphabet[-(_alphabet.index(l) + 1)]


def pathwayhash(klist=[]):
    """
    Compute 14-10-1-10 base-26 key for a pathway from a list of keys.

    Args:
        klist (list): List of keys in 14-10-1-10 format

    Returns:
        str: 14-10-1-10 pathway key hash

    >>> klist = ['HMYNUKPKYAKNHH-UHFFFAOYSA-N-CYTGKLTRXZ',
    ...          'HMYNUKPKYAKNHH-UHFFFAOYSA-N-VMUOHRWKPI',
    ...          'HMYNUKPKYAKNHH-UHFFFAOYSA-N-USZFGHNXNT',
    ...          'GCTLCWGZMYAKDA-UHFFFAOYSA-O-PSOPOPGKTV',
    ...          'GUOWDRPSXZHSPH-UHFFFAOYSA-P-OXROIZFTIG',
    ...          'IPAXLHRYFUKSDA-UHFFFAOYSA-O-SLXEXZDWSS',
    ...          'IMROMDMJAWUWLK-UHFFFAOYSA-N-XPZJQXJJLV',
    ...          'IMROMDMJAWUWLK-UHFFFAOYSA-N-SFMKXYMLNT',
    ...          'FFBBVMLFFNDZFC-UHFFFAOYSA-N-NPTUEDIUGS',
    ...          'HTYLIELGIMVDFK-UHFFFAOYSA-O-TONFZTBMWV']
    >>> pathwayhash(klist)
    'QTSLUVJOVWCATY-CBAXVYLVET-W-NMSJEHDLIE'
    >>> klist = ['HTYLIELGIMVDFK-UHFFFAOYSA-O-TONFZTBMWV',
    ...          'FFBBVMLFFNDZFC-UHFFFAOYSA-N-NPTUEDIUGS',
    ...          'IMROMDMJAWUWLK-UHFFFAOYSA-N-SFMKXYMLNT',
    ...          'IMROMDMJAWUWLK-UHFFFAOYSA-N-XPZJQXJJLV',
    ...          'IPAXLHRYFUKSDA-UHFFFAOYSA-O-SLXEXZDWSS',
    ...          'GUOWDRPSXZHSPH-UHFFFAOYSA-P-OXROIZFTIG',
    ...          'GCTLCWGZMYAKDA-UHFFFAOYSA-O-PSOPOPGKTV',
    ...          'HMYNUKPKYAKNHH-UHFFFAOYSA-N-USZFGHNXNT',
    ...          'HMYNUKPKYAKNHH-UHFFFAOYSA-N-VMUOHRWKPI',
    ...          'HMYNUKPKYAKNHH-UHFFFAOYSA-N-CYTGKLTRXZ']
    >>> pathwayhash(klist)
    'QTSLUVJOVWCATY-CBAXVYLVET-D-NMSJEHDLIE'
    >>> klist = ['HMYNUKPKYAKNHH-UHFFFAOYSA-N-CYTGKLTRXZ',
    ...          'HMYNUKPKYAKNHH-UHFFFAOYSA-N-VMUOHRWKPI',
    ...          'HMYNUKPKYAKNHH-UHFFFAOYSA-N-CYTGKLTRXZ']
    >>> pathwayhash(klist)
    'CCFZUUENOPEUXW-XZIRWGZUBF-N-LYOSAYJBHP'
    >>> klist = ['HMYNUKPKYAKNHH-UHFFFAOYSA-N-CYTGKLTRXZ']
    >>> pathwayhash(klist)
    'HMYNUKPKYAKNHH-UHFFFAOYSA-N-CYTGKLTRXZ'
    >>> pathwayhash()
    Traceback (most recent call last):
    ValueError: Empty input key list

    """

    if not klist:
        raise ValueError('Empty input key list')

    # Only one key
    if len(klist) == 1:
        return klist[0]

    # More than one key. Ensure last key is larger or equal than first key
    invert = False
    if hash(str(klist[0])) > hash(str(klist[-1])):
        invert = True
        klist = reversed(klist)

    # Split keys into components
    c1, c2, c3, c4 = zip(*[key.split('-') for key in klist])

    # Hash components in 14-10-1-10 pattern
    h1 = controlhash('>>'.join(c1), pos=14)
    h2 = controlhash('>>'.join(c2), pos=10)
    h3 = controlhash('>>'.join(c3), pos=1)
    h4 = controlhash('>>'.join(c4), pos=10)

    # Invert h3 if necessary, join components
    return '-'.join([h1, h2, invertletter(h3) if invert else h3, h4])


def randomid(pos=10):
    """Returns random valid identifier of given length pos (default: 10)."""
    return (''.join([random.choice(
            string.ascii_letters + string.digits) for _ in range(pos)]))


def ensure_logger(name=None, logging_level='INFO',
                  fmt='[%(name)s] %(levelname)s %(asctime)s %(message)s'):
    """Create new logger if necessary and (re)set logging level.

    Args:
        name (str, optional): Logger name
        logging_level (str, optional): Logging level
        fmt (str, optional): Format string

    Returns:
        logger (logging.Logger): New logger
    """

    # Get logger
    logger = logging.getLogger(name)
    logger.addHandler(logging.NullHandler())

    # Root logger
    root_logger = logging.getLogger()

    # Handler is not enabled for requested level
    if (root_logger.handlers and
            root_logger.getEffectiveLevel() != logging_level):

        # Remove current handler
        handler = root_logger.handlers[0]
        root_logger.removeHandler(handler)

    # No handlers attached to root logger
    if not root_logger.handlers:

        # Set Formatter and StreamHandler for the root logger
        form = logging.Formatter(fmt=fmt)
        stream = logging.StreamHandler()
        stream.setFormatter(form)
        root_logger.addHandler(stream)

        # Set level
        root_logger.setLevel(getattr(logging, logging_level))

    return logger


def list_accumulator():

    # List accumulator
    accumulator = []

    # Coroutine-making function
    def make_reader():
        # Accumulate output as list
        try:
            while True:
                accumulator.append((yield))
        except GeneratorExit:
            pass

    # Create and initialize reader
    reader = make_reader()
    reader.send(None)

    return (accumulator, reader)


def adjcompress(adj):
    """Create compressed string representation of adjacency list adj.

    Args:
        adj (list of lists): Adjacency list

    Returns:
        str: Compressed string representation

    Examples:
        >>> print adjcompress(
        ... [[1], [0, 2, 3, 4], [1, 5, 6], [1], [1], [2], [2]])
        ;0;1;1;1;2;2
        >>> print adjcompress([[1], [0, 3, 4], [5, 6], [1], [1], [2], [2]])
        ;0;;1;1;2;2
        >>> print adjcompress([[]])
        <BLANKLINE>
        >>> print adjcompress([])
        Traceback (most recent call last):
        ValueError: Empty adjacency list
    """
    if len(adj) < 1:
        raise ValueError('Empty adjacency list')
    return ';'.join(','.join(
        [str(m) for m in nlist if m < n]) for n, nlist in enumerate(adj))


def is_connected(adj):
    """Determine using BFS if adjacency list is connected.

    Args:
        adj (list of lists): Adjacency list

    Returns:
        bool: True if adjacency list is connected

    Examples:
        >>> is_connected([[1], [0, 2, 3, 4], [1, 5, 6], [1], [1], [2], [2]])
        True
        >>> is_connected([[1], [0, 3, 4], [5, 6], [1], [1], [2], [2]])
        False
        >>> is_connected([])
        True
        >>> is_connected([0])
        True
    """

    # Number of nodes
    length = len(adj)

    # Consider single node or empty list as connected
    if length < 2:
        return True

    # Accumulate component nodes on comp using queue for BFS
    queue = deque([0])
    comp = set()
    while queue:
        n = queue.popleft()
        comp.add(n)
        for m in adj[n]:
            if m not in comp:
                queue.append(m)

    # If all nodes in comp, the adjacency list is connected
    return len(comp) == length


def composition(atoms):
    """Format atom composition dict as comma-separated string.

    Args:
        atoms: Atom composition dict

    Returns:
        str: Sum formula representation

    Examples:
        >>> print composition({'H': 4, 'C': 1, 'O': 2})
        C 1,H 4,O 2
        >>> print composition({'Cl': 1})
        Cl 1
        >>> print composition({})
        <BLANKLINE>
    """

    # Text accumulator
    s = []

    # Put carbon first, then hydrogen
    if 'C' in atoms:
        s.append('C %d' % atoms['C'])
    if 'H' in atoms:
        s.append('H %d' % atoms['H'])

    # All other elements in alphabetic order
    for at in sorted(atoms.keys()):
        if at in ['C', 'H']:
            continue
        s.append('%s %d' % (at, atoms[at]))

    return ','.join(s)
