"""Enumeration types for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    Result: Enumeration type for Task, Calculator, Command return values
    Reactivity: Enumeration type for reactivity types
    Level: Enumeration type for Molecule levels
    Status: Enumeration type for Flask status
    Precedence: Enumeration type for Option precedence
    Target: Enumeration type for Option target
    Sort: Ascending/Descending (from pymongo)
"""

__all__ = ['Result', 'Reactivity', 'Level', 'Status', 'Precedence', 'Target',
           'Sort']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '07/29/2015'

#
# Third-party modules
#

import pymongo

#
# Classes
#


class Result(object):
    """Enumeration type for Task, Calculator, Command return values."""

    OK = 0
    TASK_UNSUCCESSFUL = 1
    TASK_ERROR = 2
    CALC_ERROR = 4
    COMMAND_ERROR = 8
    SCRIPT_ERROR = 16


class Reactivity(object):
    """Enumeration type for reactivity types."""

    UNI = 1
    CYCL = 2
    BI = 4
    ALL = 7


class Level(object):
    """Enumeration type for Molecule levels."""

    FORMULA = 1000
    FORMULA_LOCKED = 1001
    FORMULA_SUSPENDED = 1002
    FORMULA_UNREACTIVE = 1003

    FORMULA_ERROR = 1090
    FORMULA_INVALID = 1091

    CONFIGURATION = 1100
    CONFIGURATION_LOCKED = 1101
    CONFIGURATION_SUSPENDED = 1102

    CONFIGURATION_NUDGE = 1110

    CONFIGURATION_ERROR = 1190
    CONFIGURATION_INVALID = 1191

    GEOMETRY = 1200
    GEOMETRY_LOCKED = 1201
    GEOMETRY_SUSPENDED = 1202

    GEOMETRY_ERROR = 1290
    GEOMETRY_INVALID = 1291

    PROPERTY = 1300
    PROPERTY_LOCKED = 1301
    PROPERTY_SUSPENDED = 1302

    PROPERTY_ERROR = 1390
    PROPERTY_INVALID = 1391

    HAS_ENERGY = [GEOMETRY, GEOMETRY_LOCKED, GEOMETRY_SUSPENDED, PROPERTY,
                  PROPERTY_LOCKED, PROPERTY_SUSPENDED, PROPERTY_ERROR,
                  PROPERTY_INVALID]

    INCOMPLETE = [FORMULA, FORMULA_LOCKED, FORMULA_SUSPENDED,
                  FORMULA_UNREACTIVE, CONFIGURATION, CONFIGURATION_LOCKED,
                  CONFIGURATION_SUSPENDED, CONFIGURATION_NUDGE]


class Status(object):
    """Enumeration type for Flask status."""

    INITIAL = 2000
    INITIAL_LOCKED = 2001
    INITIAL_SUSPENDED = 2002

    INITIAL_ERROR = 2090

    RUNNING = 2100
    RUNNING_LOCKED = 2101
    RUNNING_SUSPENDED = 2102

    REACTIVE = 2200
    REACTIVE_LOCKED = 2201
    REACTIVE_SUSPENDED = 2202

    REACTIVE_HIENERGY = 2210
    REACTIVE_HIBARRIER = 2211
    REACTIVE_MAXGEN = 2212
    REACTIVE_BADCHEM = 2213

    UNREACTIVE = 2300
    UNAVAILABLE = 2310

    ERROR = 2390
    INVALID = 2391

    HAS_ENERGY = [REACTIVE, REACTIVE_LOCKED, REACTIVE_SUSPENDED,
                  REACTIVE_HIENERGY, REACTIVE_HIBARRIER, REACTIVE_MAXGEN,
                  UNREACTIVE]


class Precedence(object):
    """Enumeration type for option precedence."""

    # Command-line argument
    COMMAND = 3000

    # Project-wide options
    PROJECT = 3100

    # Environmental variables
    ENV = 3200

    # User-wide options
    USER = 3300

    # Site-wide options
    SITE = 3400

    # Source-wide defaults
    DEFAULT = 3500


class Target(object):
    """Enumeration type for option target."""

    # Task-specific options
    CLEANUPTASK = 4000
    SUSPENDTASK = 4001
    RESUMETASK = 4002

    OPENBABELBUILDTASK = 4010
    RDKITBUILDTASK = 4011

    RDKITREACTIONTASK = 4020

    MOPACGEOMETRYTASK = 4030
    ORCAGEOMETRYTASK = 4031

    ORCAPROPERTYTASK = 4040

    FLASKCLEANUPTASK = 4100
    FLASKSUSPENDTASK = 4101
    FLASKRESUMETASK = 4102
    FLASKMAPPERTASK = 4103
    FLASKREDUCERTASK = 4104

    RDKITFLASKREACTIONTASK = 4110

    # Common options for all tasks
    TASK = 4200

    # Module-wide options
    COLIBRI = 4300


class Sort(object):
    """Enumeration type for sort order."""

    ASCENDING = pymongo.ASCENDING
    DESCENDING = pymongo.DESCENDING
