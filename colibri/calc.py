"""Context, Calculator, and Dispatcher classes for colibri.

Copyright 2015 Dmitrij Rappoport

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Classes:
    Context: Container for communication between tasks and outside processes
    Calculator: Constructs and runs tasks on a single processor
    Dispatcher: Runs one or more Calculator instances
"""

__all__ = ['Context', 'Calculator', 'Dispatcher']

__author__ = 'Dmitrij Rappoport'
__email__ = 'dmrappoport@gmail.com'
__version__ = '0.9'
__date__ = '04/15/2015'


#
# Standard library
#

import time
import signal
import logging
import multiprocessing

#
# colibri submodules
#

import colibri.task
from colibri.mongostorage import MongoClient
from colibri.memcachedcache import MemcachedClient
from colibri.enums import Result
from colibri.exceptions import (
    NoInput, TaskWorkflow, DataError, TaskError, ConnectionError,
    ExecutionError, timeout_handler, interrupt_handler)
from colibri.utils import ensure_logger


class Context(object):
    """Container for communication between tasks and outside processes."""

    def __init__(self, opts):
        """Context object constructor.

        Args:
            opts (Options): Context options

        Raises:
            ExecutionError: Error in Context initialization
        """

        # Context task option
        task_opt = opts.item('task')

        # Context name
        self.name = task_opt.key

        # Context task class
        self.task_class = task_opt.value

        # Context options
        self.opts = opts.set(scope='colibri.task.' + self.task_class)

        # Initialize logger
        self.logger = logging.getLogger(self.name)
        self.logger.debug('Initiated local logger')

        # Connect to MongoDB
        self.mol_storage = None
        self.flask_storage = None
        self.raw_filestorage = None

        # Open MongoDB connection
        try:
            self.mongoclient = MongoClient(
                host=self.opts['db_hostname'], port=self.opts['db_port'])
            self.logger.debug('Connected to MongoDB storage')
        except ConnectionError as e:
            self.logger.error('MongoDB storage not available: %s' % e)
            raise ExecutionError('MongoDB storage is required')

        # Open molecule storage
        self.mol_storage = self.mongoclient.storage(
            db=self.opts['mol_database'],
            collection=self.opts['mol_collection'],
            write_attempts=self.opts['write_attempts'],
            write_interval=self.opts['write_interval'])
        self.logger.debug('Connected to %s' % self.mol_storage)

        # Open flask storage
        self.flask_storage = self.mongoclient.storage(
            db=self.opts['flask_database'],
            collection=self.opts['flask_collection'],
            write_attempts=self.opts['write_attempts'],
            write_interval=self.opts['write_interval'])
        self.logger.debug(
            'Connected to %s' % self.flask_storage)

        # Open GridFS connection for raw file storage
        self.raw_filestorage = self.mongoclient.file_storage(
            db=self.opts['raw_database'],
            collection=self.opts['raw_collection'],
            write_attempts=self.opts['write_attempts'],
            write_interval=self.opts['write_interval'])
        self.logger.debug('Connected to %s' % self.raw_filestorage)

        # Initialize Memcached connection
        self.memcachedclient = None
        self.mol_map_cache = None
        self.mol_red_cache = None

        # Memcached connection
        if self.opts.get('with_cache', False):
            try:
                self.memcachedclient = MemcachedClient(
                    host=self.opts['cache_hostname'],
                    port=self.opts['cache_port'])
                self.logger.debug('Connected to Memcached cache')

                # Mapping cache
                self.mol_map_cache = self.memcachedclient.cache(
                    key_prefix='MAP:', expire=self.opts.get('cache_expire', 0))
                self.logger.debug(
                    'Connected to %s' % self.mol_map_cache)

                # Reducing cache
                self.mol_red_cache = self.memcachedclient.cache(
                    key_prefix='RED:', expire=self.opts.get('cache_expire', 0))
                self.logger.debug(
                    'Connected to %s' % self.mol_red_cache)
            except ConnectionError as e:
                self.logger.error('Memcached cache not available: %s' % e)

    def __del__(self):

        # Close MongoDB connection
        self.mongoclient.close()
        self.logger.debug('Closed connection to MongoDB storage')

        # Close Memcached connection (no-op currently)
        if self.memcachedclient:
            self.memcachedclient.close()
            self.logger.debug('Closed connection to Memcached cache')


class Calculator(multiprocessing.Process):
    """Calculator class, constructs and runs tasks on a single processor."""

    def __init__(self, opts, disp=None):
        """Calculator object constructor.

        Args:
            opts (Options): Calculator options
            disp (Dispatcher, optional): Dispatcher object
        """
        # Initialize process instance
        super(Calculator, self).__init__()

        # Signal handlers for children processes
        signal.signal(signal.SIGALRM, timeout_handler)
        signal.signal(signal.SIGTERM, interrupt_handler)
        signal.signal(signal.SIGTSTP, interrupt_handler)

        # Calculator task option
        task_opt = opts.item('task')

        # Calculator name
        self._name = task_opt.key

        # Calculator task class
        self._task_class = vars(colibri.task)[task_opt.value]

        # Calculator options
        self._opts = opts.set(scope='colibri.task')

        # Create logger if necessary
        self._logger = ensure_logger(self._name, self._opts['logging_level'])

        # Demonize the calculator
        self.daemon = True

        # Initialize Context instance
        self._context = Context(self._opts)

        # No dispatcher available
        if disp is None:
            # (Non-shared) dictionary for statistics
            self._stats = {'tasks_successful': 0,
                           'tasks_unsuccessful': 0,
                           'calculators_idle': 0}

            # Set event and lock instances
            self._termination_flag = multiprocessing.Event()
            self._tasks_successful_lock = multiprocessing.Lock()
            self._tasks_unsuccessful_lock = multiprocessing.Lock()
            self._calculators_idle_lock = multiprocessing.Lock()

        # Dispatcher instance provided
        else:
            self._stats = disp._stats
            self._termination_flag = disp._termination_flag
            self._tasks_successful_lock = disp._tasks_successful_lock
            self._tasks_unsuccessful_lock = disp._tasks_unsuccessful_lock
            self._calculators_idle_lock = disp._calculators_idle_lock

        # Create log output
        self._logger.info('Starting up calculator %s' % self._name)

    def __del__(self):

        # Destroy context
        self._context = None

        # Create log output
        self._logger.info('Shutting down calculator %s' % self._name)

    def run(self):

        # Maximum number of tasks
        tasks_per_calc = self._opts['tasks_per_calc']

        # Maximum number of tasks per cycle
        tasks_per_cycle = self._opts['tasks_per_cycle']

        # Total number of tasks processed
        tasks_completed = 0

        # Number of idle tasks
        tasks_idle = 0

        # List of completed tasks for debugging purposes
        self._completed_tasks = []

        # Run only for given number of tasks if tasks_per_calc > 0
        # Loop until interrupted otherwise
        while True:

            # Check total number of tasks completed
            if tasks_per_calc > 0 and tasks_completed >= tasks_per_calc:
                break

            # Termination flag set by Dispatcher instance
            if self._termination_flag.is_set():
                break

            try:

                # Tasks processed in current cycle
                cycle_tasks_completed = 0

                # Instantiate a task class and run it
                task = self._task_class(self._context, self._context.opts)

                try:

                    # Perform setup work for the task
                    task.setup()

                    # Retrieve input data for the task
                    task.retrieve()

                # No input available
                except NoInput:

                    # Increase number of idle tasks
                    tasks_idle += 1

                    # Report the calculator as idle
                    if tasks_idle >= self._opts['idle_tasks_per_calc']:
                        self._stats['calculators_idle'] += 1

                    # Count idle tasks if option is set
                    if self._opts['count_idle_tasks']:
                        tasks_completed += 1

                    # Increment counter of cycle tasks
                    cycle_tasks_completed += 1

                    # Check number of tasks processed in current cycle
                    if (tasks_per_cycle > 0 and
                            cycle_tasks_completed >= tasks_per_cycle):
                        break

                    # Sleep before proceeding
                    time.sleep(self._opts.get('idle_interval', 0))
                    raise

                # Iterate until task.run() calls StopIteration
                while True:

                    # Reset number of idle tasks
                    if tasks_idle >= self._opts['idle_tasks_per_calc']:
                        with self._calculators_idle_lock:
                            self._stats['calculators_idle'] -= 1
                    tasks_idle = 0

                    # Check number of tasks processed in current cycle
                    if (tasks_per_cycle > 0 and
                            cycle_tasks_completed >= tasks_per_cycle):
                        break

                    # Task execution
                    try:
                        task.run()

                    # Data/task error or workflow exception, continue loop
                    except (DataError, TaskError, TaskWorkflow):

                        with self._tasks_unsuccessful_lock:
                            self._stats['tasks_unsuccessful'] += 1

                        # Increment counters
                        tasks_completed += 1
                        cycle_tasks_completed += 1

                    # No more tasks, finish loop
                    except StopIteration:
                        break

                    # Task succeeded, continue loop
                    else:

                        with self._tasks_successful_lock:
                            self._stats['tasks_successful'] += 1

                        # Increment counters
                        tasks_completed += 1
                        cycle_tasks_completed += 1

            # No input available
            except NoInput:
                pass

            # External interrupt or system error.
            # Exit loop, roll back calculation data if necessary
            except (KeyboardInterrupt, SystemExit, IOError, EOFError):
                if getattr(task, '_rollback', None):
                    task.rollback()
                break

            else:

                # Submit the task data to storage
                try:
                    task.submit()

                    # Optionally pause after processing task
                    task.pause()

                except TaskError:
                    if getattr(task, '_rollback', None):
                        task.rollback()

                except (KeyboardInterrupt, SystemExit, IOError, EOFError):
                    if getattr(task, '_rollback', None):
                        task.rollback()
                    break

            # Clean up resources after the task
            finally:
                task.cleanup()

                # Append to list of completed tasks for debugging
                if self._opts.get('save_completed_tasks'):
                    self._completed_tasks.append(task)

        else:
            # Reset number of idle tasks
            if tasks_idle >= self._opts['idle_tasks_per_calc']:
                with self._calculators_idle_lock:
                    self._stats['calculators_idle'] -= 1
            tasks_idle = 0

        return Result.OK


class Dispatcher(object):
    """Dispatcher, runs one or more Calculator instances."""

    def __init__(self, opts):
        """Dispatcher object constructor.

        Args:
            opts (Options): Dispatcher options
        """
        # Set signal handlers for parent process
        signal.signal(signal.SIGTERM, interrupt_handler)

        # Create new Options object with modified scope target
        self._opts = opts.set(scope='colibri')

    def run(self):

        # Create logger if necessary
        self._logger = ensure_logger('colibri', self._opts['logging_level'])

        # Execution parameters
        self._mgr = multiprocessing.Manager()

        # Status variable events and locks
        self._termination_flag = multiprocessing.Event()
        self._tasks_successful_lock = multiprocessing.Lock()
        self._tasks_unsuccessful_lock = multiprocessing.Lock()
        self._calculators_idle_lock = multiprocessing.Lock()

        # Execution stats
        self._stats = self._mgr.dict()
        self._stats['tasks_successful'] = 0
        self._stats['tasks_unsuccessful'] = 0
        self._stats['calculators_idle'] = 0

        # Calculator threads
        self._calc_pool = []
        for opt in self._opts.iterlist(key='task'):
            task_opts = self._opts.delete('task').update(
                {'key': opt.key, 'group': 'task', 'value': opt.value,
                 'target': 'colibri', 'precedence': 'command'})
            c = Calculator(task_opts, self)
            self._calc_pool.append(c)
            c.start()

        # Number of calculator threads
        calcs_total = len(self._calc_pool)

        # Set shutdown quorum
        if self._opts.get('with_shutdown', False):
            if self._opts['shutdown_quorum'] < 0:
                shutdown_quorum = int(0.75 * calcs_total + 0.5)
            else:
                shutdown_quorum = self._opts['shutdown_quorum']

        # Action cycle count for stopping / replenishing
        action_cycle = 0

        # Periodically watch calculator and task statistics
        while True:
            try:
                # Determine number of alive calculator processes
                calcs_alive = sum(1 for c in self._calc_pool
                                  if c.is_alive())

                # Produce logging output
                self._logger.info('Calculators running: %d' %
                                  calcs_alive)
                with self._tasks_successful_lock:
                    self._logger.info('Tasks successful: %d' %
                                      self._stats['tasks_successful'])
                with self._tasks_unsuccessful_lock:
                    self._logger.info('Tasks unsuccessful: %d',
                                      self._stats['tasks_unsuccessful'])
                if self._opts.get('with_shutdown', False):
                    with self._calculators_idle_lock:
                        self._logger.info(
                            'Calculators idle: %d, shutdown quorum: %d',
                            self._stats['calculators_idle'],
                            shutdown_quorum)

                time.sleep(self._opts.get('polling_interval', 0))

                # Initiate shutdown if shutdown quorum is reached
                if (self._opts.get('with_shutdown', False) and
                        self._stats['calculators_idle'] >=
                        shutdown_quorum):
                    self._logger.info('Shutdown quorum reached')
                    raise StopIteration

                # Increase action cycle count
                if self._opts.get('persistent_pool'):
                    if calcs_alive < calcs_total:
                        action_cycle += 1
                else:
                    if calcs_alive == 0:
                        action_cycle += 1

                # Number of cycles before stopping / replenishing
                if (not self._opts.get('action_delay') or
                        action_cycle == self._opts['action_delay']):

                    # Replenish calculator pool
                    if self._opts.get('persistent_pool'):
                        for i, c in enumerate(self._calc_pool):
                            if not c.is_alive():
                                # Calculator task options
                                opt = c._opts.item('task')
                                task_opts = self._opts.delete('task').update(
                                    {'key': opt.key, 'group': 'task',
                                     'value': opt.value, 'target': 'colibri',
                                     'precedence': 'command'})
                                crest = Calculator(task_opts, self)
                                self._calc_pool[i] = crest
                                crest.start()
                                c.join()

                        # Reset action cycle count
                        action_cycle = 0

                    # Stop if no calculators are running
                    else:
                        if calcs_alive == 0:
                            raise StopIteration

            # Wait for calculators to finish
            except (StopIteration, KeyboardInterrupt, SystemExit):
                self._termination_flag.set()
                try:
                    for c in self._calc_pool:
                        c.join()
                except (IOError, EOFError):
                    pass
                break

        return Result.OK
