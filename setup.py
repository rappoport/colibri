#!/usr/bin/env python

import os.path
from setuptools import setup, find_packages

setup(name='colibri',
      version='0.9',
      description='colibri is your lightweight and gregarious ' +
                  'chemistry explorer',
      long_description=open(
          os.path.join('docs', 'source', 'index.txt')).read(),
      author='Dmitrij Rappoport',
      author_email='dmrappoport@gmail.com',
      url='https://bitbucket.org/rappoport/colibri',
      download_url='https://bitbucket.org/rappoport/colibri',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: Apache Software License',
          'Operating System :: POSIX',
          'Programming Language :: Python :: 2.7',
          'Topic :: Scientific/Engineering :: Chemistry'
      ],
      license='Apache v2',
      platforms=['any'],
      packages=find_packages(exclude=['tests*']),
      install_requires=['openbabel', 'pymongo', 'py2neo'],
      extras_require={
          'caching': 'pymemcache',
          'graphml': 'igraph'
      },
      tests_require=['port_for'],
      provides=['colibri'],
      test_suite='tests.get_suite',
      include_package_data=True,
      zip_safe=False,
      scripts=['bin/colib', 'scripts/colib_proton.py',
               'scripts/colib_network.py', 'scripts/colib_doctor.py',
               'scripts/colib_key.py'],
      )
