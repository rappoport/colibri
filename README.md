[Colibri](https://bitbucket.org/rappoport/colibri)
==================================================

## About colibri ##

colibri is your Lightweight and Gregarious Chemistry Explorer. Use it to create new molecules, to construct chemical networks, and to compute their properties in a distributed fashion. colibri is written in Python.

## Prerequisites ##

* [RDKit](http://www.rdkit.org): open-source cheminformatics toolkit.
* [OpenBabel](http://openbabel.org): chemistry format converter.
* [MOPAC](http://openmopac.net): semiempirical quantum chemistry program maintained by J. Stewart.
* [ORCA](https://orcaforum.cec.mpg.de): modern electronic structure program  by F. Neese and co-workers.
* [MongoDB](http://www.mongodb.org): high-performance document database and Python driver [pymongo](https://github.com/mongodb/mongo-python-driver).
* [Memcached](http://memcached.org) and pure-Python client [pymemcache](https://github.com/pinterest/pymemcache).
* [Neo4J](http://www.neo4j.com): modern graph database (v3) and Python driver (v3) [py2neo](http://py2neo.org/2.0/).

## Working with colibri ##

     usage: colib [-h] [--version] [--no-config] [--no-site-config]
                 [--site-config-path PATH] [--no-user-config] [--no-env-config]
                 [--no-proj-config] [--config-file FILE] [-d HOST]
                 [--db-port PORT] [--write-attempts N] [--write-interval T]
                 [-t DB] [-k COLL] [-T DB] [-K COLL] [--with-raw-write]
                 [--no-raw-write] [--raw-database DB] [--raw-collection COLL] [-H]
                 [--no-cache] [--cache-hostname URL] [--cache-port PORT]
                 [--cache-expire T] [--scratch-path PATH] [--executable-path PATH]
                 [-n N] [-N N] [--count-idle-tasks] [--save-completed-tasks]
                 [--polling-interval T] [--idle-interval T] [-L LEVEL] [-v] [-q]
                 [--action-delay N] [-p] [-P] [-!] [--no-shutdown]
                 [--idle-tasks-per-calc N] [--shutdown-quorum N]
                 {init,add,fetch,delete,run,stat,suspend,resume,load,dump,graph,clean,config,doctor,key,network,proton}
                 ...

    colibri is your lightweight and gregarious chemistry explorer

    optional arguments:
      -h, --help            show this help message and exit

    execution arguments:
      --version             show program's version number and exit
      --no-config           ignore all configuration files
      --no-site-config      skip site configuration file
      --site-config-path PATH
                            location of site configuration. Default:
                            /usr/local/etc
      --no-user-config      skip user configuration file
      --no-env-config       skip configuration environment variables
      --no-proj-config      skip project configuration file
      --config-file FILE    colibri configuration file name. Default: colibri.json

    configuration arguments:
      -d HOST, --db-hostname HOST
                            MongoDB database hostname
      --db-port PORT        MongoDB database port
      --write-attempts N    number of database write attempts before warning is
                            issued
      --write-interval T    interval in s between database write attempts
      -t DB, --mol-database DB
                            MongoDB molecule database name
      -k COLL, --mol-collection COLL
                            MongoDB molecule collection name
      -T DB, --flask-database DB
                            MongoDB flask database name
      -K COLL, --flask-collection COLL
                            MongoDB flask collection name
      --with-raw-write      write raw output files
      --no-raw-write        do not write raw output files
      --raw-database DB     MongoDB raw file database name
      --raw-collection COLL
                            MongoDB raw collection prefix
      -H, --with-cache      use Memcached cache
      --no-cache            do not use Memcached cache
      --cache-hostname URL  Memcached cache server location
      --cache-port PORT     Memcached cache port
      --cache-expire T      Memcached key expiration time in s
      --scratch-path PATH   scratch directory for task execution
      --executable-path PATH
                            search path for external executables
      -n N, --tasks-per-calc N
                            number of tasks per calculator
      -N N, --tasks-per-cycle N
                            number of tasks per calculator cycle
      --count-idle-tasks    count idle tasks as completed
      --save-completed-tasks
                            save completed tasks in calculator for debugging
      --polling-interval T  interval for calculator polling in s
      --idle-interval T     wait interval for database queries by idle tasks in s
      -L LEVEL, --logging-level LEVEL
                            logging level
      -v, --verbose         set logging level to DEBUG
      -q, --quiet           set logging level to ERROR
      --action-delay N      delay in s before shutdown or calculator restart
      -p, --with-persistent-pool
                            maintain a persistent calculator pool
      -P, --no-persistent-pool
                            do not maintain a persistent calculator pool
      -!, --with-shutdown   shut down calculators after being idle
      --no-shutdown         do not shut down calculators
      --idle-tasks-per-calc N
                            number of idle tasks before calculator declared idle
      --shutdown-quorum N   number of idle calculators before shutdown

    colibri subcommands:
      {init,add,fetch,delete,run,stat,suspend,resume,load,dump,graph,clean,config,doctor,key,network,proton}
        init                initialize database
        add                 add data to database
        fetch               fetch data from database
        delete              delete data from database
        run                 run computation on database
        stat                retrieve database statistics
        suspend             suspend computation
        resume              resume computation
        load                load graph file into database
        dump                dump database as graph file
        graph               export database to graph storage
        clean               clean database
        config              configure colibri
        doctor              check database for potential issues
        key                 return flask or molecule keys
        network             run task suite for creating networks
        proton              compute the energy of a proton

## Authors ##

colibri is maintained by **Dmitrij Rappoport**, email [Dmitrij](mailto:dmrappoport@gmail.com)
